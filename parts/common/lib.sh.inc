#!/usr/bin/env bash
# common functions

#############################################################################
# variables
#############################################################################
OPENSTACK_BIN="${OPENSTACK_BIN:-openstack}"

#############################################################################
# functions
#############################################################################
function report_tools() {
    local commands=("${OPENSTACK_BIN} --version" "bash --version" "awk -W version"
        "ssh -V" "which ssh-keygen" "ncat --version" "grep --version" )
    local err_cnt=0
    for i_cmd in "${commands[@]}" "$@"; do
        echo "${i_cmd}:"
        ${i_cmd} |& head -1 | awk '{print "  " $0}'
        [ "${PIPESTATUS[0]}" != "0" ] && let "err_cnt++"
    done
    [ "${err_cnt}" -gt 0 ] && return 2
    return 0
}

function delete_object_if_exists() {
    local object="$1"
    local name="$2"

    if ${OPENSTACK_BIN} ${object} show "${name}" &>/dev/null; then
        # delete
        ${OPENSTACK_BIN} ${object} delete "${name}" &>/dev/null
        # wait for deletion
        for ((i=0;i<10;i++)); do
          ${OPENSTACK_BIN} ${object} show "${name}" &>/dev/null || \
            break
          sleep 3
        done
    fi
}

function vm_wait_for_status() {
    local name="$1"
    local status="$2"
    while true; do
        i_status="$(${OPENSTACK_BIN} server show "${SERVER_NAME}" -f value -c status)"
        echo -n "${i_status} "
        if [ "${i_status}" == "${status}" ]; then
          echo ""
          break
        fi
        sleep 2
    done
}

function test_vm_access() {
    local ip="$1"
    local port="${2:-"22"}"
    for ((i=0;i<60;i++)); do
        if ncat -z "${ip}" "${port}"; then
            echo "VM is accessible at ${ip}:${port}"
            break
        else
            echo -n .
        fi
        sleep 10
    done
}

function test_vm_access_ncat() {
    test_vm_access "$@"
}

function delete_common_objects() {
    if [ -s "${FIP_FILE}" ]; then
        echo -n 'floating-ip '
        delete_object_if_exists "floating ip" "$(head -1 "${FIP_FILE}")"
        rm -f "${FIP_FILE}"
    fi
    echo -n 'server '
    delete_object_if_exists server "${SERVER_NAME}"
    echo -n 'volume '
    delete_object_if_exists volume "${EXTRA_VOLUME_NAME}"
    echo -n 'keypair '
    delete_object_if_exists keypair "${KEYPAIR_NAME}"
}

function delete_objects_group_project() {
    delete_common_objects
    if ${OPENSTACK_BIN} router show "${ROUTER_NAME}" &>/dev/null; then
      echo -n 'disconnect-router-from-subnet '
      ${OPENSTACK_BIN} router remove subnet "${ROUTER_NAME}" "${SUBNET_NAME}"
    fi
    echo -n 'router '
    delete_object_if_exists router "${ROUTER_NAME}"
    echo -n 'subnet '
    delete_object_if_exists subnet "${SUBNET_NAME}"
    echo -n 'network '
    delete_object_if_exists network "${NETWORK_NAME}"
    echo 'security-group'
    delete_object_if_exists "security group" "${SECGROUP_NAME}"
}

function delete_objects_personal_project() {
    delete_common_objects
    echo 'security-group'
    delete_object_if_exists "security group" "${SECGROUP_NAME}"
}

function list_objects() {
    local regexp="${ENTITIES_PREFIX}-demo|${SUBNET_NAME}|${NETWORK_NAME}|${ROUTER_NAME}"
    if [ -s "${FIP_FILE}" ]; then
      regexp="${regexp}|$(head -1 "${FIP_FILE}")"
    fi
    for i_object in keypair network subnet router floating_ip security_group volume server ; do
        i_objects="$(${OPENSTACK_BIN} ${i_object/_/ } list)"
        if echo "${i_objects}" | grep -Eq "${regexp}"; then
            echo "${i_object}s:"
            echo "${i_objects}" | grep -E "^\| (ID|Name)|^\+---| ${regexp}" | awk '{print "  " $0}'
        fi
    done
}

function duration_human() {
    local secs="$1"
    if [[ "${secs}" -lt 60 ]]; then
        echo "${secs}s"
    elif [[ "${secs}" -lt $((60*60)) ]]; then
        echo "$((${secs} / 60))m$((${secs} % 60))s"
    else
        echo "$((${secs} / (60*60)))h$(( ( ${secs} % (60*60) ) /60 ))m$((${secs} % 60))s"
    fi
}

function log() {
    echo ""
    echo -e "$@"
    export STAGE_NAME="$@"
}

function log_section() {
    local terminal_size="${COLUMNS}"
    [ -z "${terminal_size}" -o "${terminal_size}" == "0" ] && terminal_size="$(tput cols)"

    local input_string="$(echo -e "$@"| tail -1)"
    local section_width=$(( ${terminal_size} - ${#input_string} - 2 ))
    local section_character="="
    local section_string=$(printf -- "${section_character}%.0s" $(seq 1 ${section_width}))
    echo ""
    echo -e "$@ ${section_string}"
    export STAGE_NAME="$@"
}

function wait_keypress_timeout() {
    local x=
    local duration_seconds=${KEYPRESS_DURATION_SECONDS:-120}

    echo -n "... (press Enter or wait $(duration_human ${duration_seconds}))"
    read -t ${duration_seconds} x || \
      echo "  [keyboard input timed out]"
}

function log_keypress() {
    log "$@"
    wait_keypress_timeout
}

function log_section_keypress() {
    log_section "$@"
    wait_keypress_timeout
}

function is_personal_project() {
    if [ -n "${OS_APPLICATION_CREDENTIAL_ID}" ]; then
        local project_id="$(${OPENSTACK_BIN} application credential show ${OS_APPLICATION_CREDENTIAL_ID} -f value -c project_id)"
        local user_id="$(${OPENSTACK_BIN} application credential show ${OS_APPLICATION_CREDENTIAL_ID} -f value -c user_id)"
        local project_name="$(${OPENSTACK_BIN} project show "${project_id}" -fvalue -c name)"
        local user_name="$(${OPENSTACK_BIN} user show "${user_id}" -fvalue -c name)"
        echo "${project_name}"
        [[ "${project_name}" == "${user_name}" && "${user_name}" =~ [a-fA-F0-9]+@[a-z.]+ ]]
    elif [ -n "${OS_USERNAME}" -a -n "${OS_PROJECT_NAME}" ]; then
        echo "${OS_PROJECT_NAME}"
        [[ "${OS_PROJECT_NAME}" == "${OS_USERNAME}" && "${OS_USERNAME}" =~ [a-fA-F0-9]+@[a-z.]+ ]]
    else
        return 2
    fi
}

function myexit() {
    local ecode="${1:-0}"
    if [ "${ecode}" == 0 ]; then
        echo "Successfuly exiting from stage \"${STAGE_NAME}\""
    else
        echo -e "\nAbnormaly exiting from stage \"${STAGE_NAME}\""
    fi
    exit ${ecode}
}
