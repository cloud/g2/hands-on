# Cloud hands-on part 6, G2 Kubernetes-OpenStack cloud deployment - Kubernetes & FluxCD & ceph & OpenStack

## Objectives
* [ ] Refresh [hands-on #5](https://gitlab.ics.muni.cz/cloud/g2/hands-on/-/blob/master/parts/5-g2-infra-vms-k8s/README.md) and fullfill the [before hands-on steps](https://gitlab.ics.muni.cz/cloud/g2/hands-on/-/blob/master/parts/6-g2-infra-k8s-fluxcd-ceph-ostack/README.md#before-hands-on)
* [ ] Bootstrap G2 infra using FluxCD
  * [ ] Summary questions answered
* [ ] Deploy internal ceph to provide k8s internal distributed storage
  * [ ] Summary questions answered
* [ ] Deploy part of the OpenStack on Kubernetes
  * [ ] Summary questions answered
* [ ] Flux CD pitfalls, getting used to GitOps with FluxCD
* [ ] Q&A

## Before hands-on

### Read through hands-on #5

[Read briefly again through hands-on #5](../5-g2-infra-vms-k8s/README.md).

### Redeploy IaaS infractructure for G2 cloud to contain ceph backing storage

Re-use [hands-on #5 infrastructure](../5-g2-infra-vms-k8s/README.md) with running kubernetes.
Update Terraform description to get 2x 100-500GB disks to each controlplane VM. There are no other requirements (both ceph, ephem will work).

Goal: controlplane hosts (VMs) contain `/dev/sda` mounted as `/` and also `/dev/sd{b,c}` just present not partitioned / not formatted / not mounted, your infrastructure then will look like this diagram (note green disks).

![](./pictures/k8s-on-demand-architecture-3.png)

#### Example of adding additional disks
```sh
# List infrastructure existing infrastructure:
[freznicek@lenovo-t14 ~ 0]$ openstack server list | grep cloud-hands-on-5
| 1722823a-137b-4f3c-9156-1f9a15c5cb10 | cloud-hands-on-5-control-1         | ACTIVE | cloud-hands-on-5_network=10.0.0.82                      | ubuntu-jammy-x86_64 | c3.8core-16ram |
| 22fec933-5522-4262-baa2-0543917cb63e | cloud-hands-on-5-control-3         | ACTIVE | cloud-hands-on-5_network=10.0.0.124                     | ubuntu-jammy-x86_64 | c3.8core-16ram |
| 6687afc5-b89d-4bb6-a51f-fff976829366 | cloud-hands-on-5-control-2         | ACTIVE | cloud-hands-on-5_network=10.0.0.220                     | ubuntu-jammy-x86_64 | c3.8core-16ram |
| f8d397c7-5a60-45c4-9fd3-059bcebce1ee | cloud-hands-on-5-workers-1         | ACTIVE | cloud-hands-on-5_network=10.0.0.142                     | ubuntu-jammy-x86_64 | g2.medium      |
| 9ebc9e88-3d95-4456-ba26-528ecab67dd4 | cloud-hands-on-5-workers-2         | ACTIVE | cloud-hands-on-5_network=10.0.0.253                     | ubuntu-jammy-x86_64 | g2.medium      |
| 5b3c32f6-0d7d-4e5e-87c0-4ee406c026c0 | cloud-hands-on-5-bastion-server    | ACTIVE | cloud-hands-on-5_network=10.0.0.31, 195.113.243.177     | ubuntu-jammy-x86_64 | g2.tiny        |

# Validate kubernetes functionality and existing workload in default namespace
[freznicek@lenovo-t14 ~ 0]$ kubectl get deploy,service,pod
NAME                    READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/awkws   3/3     3            3           4d21h

NAME                 TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)        AGE
service/awkws        NodePort    10.233.46.164   <none>        80:30276/TCP   4d21h
service/kubernetes   ClusterIP   10.233.0.1      <none>        443/TCP        5d18h

NAME                         READY   STATUS    RESTARTS   AGE
pod/awkws-75f4b67cb4-46m6g   1/1     Running   0          4d21h
pod/awkws-75f4b67cb4-fk4l8   1/1     Running   0          4d21h
pod/awkws-75f4b67cb4-hgnwp   1/1     Running   0          4d21h

# Doublecheck whether you have additional disks
[freznicek@lenovo-t14 ~ 0]$ for i_cp_ip in 10.0.0.{82,220,124} ; do ssh -J ubuntu@195.113.243.177 ubuntu@$i_cp_ip 'uname -a;lsblk | grep ^sd'; done
Linux cloud-hands-on-5-control-1 5.15.0-73-generic #80-Ubuntu SMP Mon May 15 15:18:26 UTC 2023 x86_64 x86_64 x86_64 GNU/Linux
sda       8:0    0    80G  0 disk
sdb       8:16   0   500G  0 disk
sdc       8:32   0   500G  0 disk
Linux cloud-hands-on-5-control-2 5.15.0-73-generic #80-Ubuntu SMP Mon May 15 15:18:26 UTC 2023 x86_64 x86_64 x86_64 GNU/Linux
sda       8:0    0    80G  0 disk
sdb       8:16   0   500G  0 disk
sdc       8:32   0   500G  0 disk
Linux cloud-hands-on-5-control-3 5.15.0-73-generic #80-Ubuntu SMP Mon May 15 15:18:26 UTC 2023 x86_64 x86_64 x86_64 GNU/Linux
sda       8:0    0    80G  0 disk
sdb       8:16   0   500G  0 disk
sdc       8:32   0   500G  0 disk

# If you are missing extra sdb and sdc disks there feel free to follow hands-on #2
# https://gitlab.ics.muni.cz/cloud/g2/hands-on/-/blob/master/parts/2/README.md?ref_type=heads#b1-add-to-all-nodes-not-bastion-2-external-disks

# Doublecheck controlplane VMs are having reasonable load (let say < 2.0)
[freznicek@lenovo-t14 ~ 255]$ for i_cp_ip in 10.0.0.{82,220,124} ; do ssh -J ubuntu@195.113.243.177 ubuntu@$i_cp_ip 'uname -a;uptime'; done
Linux cloud-hands-on-5-control-1 5.15.0-73-generic #80-Ubuntu SMP Mon May 15 15:18:26 UTC 2023 x86_64 x86_64 x86_64 GNU/Linux
 12:03:17 up 5 days, 23:07,  0 users,  load average: 0.69, 1.04, 1.23
Linux cloud-hands-on-5-control-2 5.15.0-73-generic #80-Ubuntu SMP Mon May 15 15:18:26 UTC 2023 x86_64 x86_64 x86_64 GNU/Linux
 12:03:18 up 5 days, 23:07,  0 users,  load average: 1.89, 1.75, 1.82
Linux cloud-hands-on-5-control-3 5.15.0-73-generic #80-Ubuntu SMP Mon May 15 15:18:26 UTC 2023 x86_64 x86_64 x86_64 GNU/Linux
 12:03:18 up 5 days, 23:07,  0 users,  load average: 2.29, 1.96, 1.69

```

### Install all needed tools on local computer

Doublecheck tools, [follow instructions to install them](https://gitlab.ics.muni.cz/cloud/knowledgebase/-/blob/master/team/onboarding/workstation-or-vm-requirements.md#installation-example-on-ubuntu-jammy-22041-lts).

#### kubectl version 1.23.z ... 1.25.z

```sh
[freznicek@lenovo-t14 ~ 0]$ kubectl version
Client Version: version.Info{Major:"1", Minor:"25", GitVersion:"v1.25.10", GitCommit:"e770bdbb87cccdc2daa790ecd69f40cf4df3cc9d", GitTreeState:"clean", BuildDate:"2023-05-17T14:12:20Z", GoVersion:"go1.19.9", Compiler:"gc", Platform:"linux/amd64"}
Kustomize Version: v4.5.7
...
```

#### flux version **exactly** 0.41.2 !

```sh
[freznicek@lenovo-t14 ~ 0]$ flux --version
flux version 0.41.2
```

#### helm client version 3.13.0 or newer

```sh
[freznicek@lenovo-t14 ~ 1]$ helm version
version.BuildInfo{Version:"v3.13.0", GitCommit:"825e86f6a7a38cef1112bfa606e4127a706749b1", GitTreeState:"clean", GoVersion:"go1.20.8"}
```

#### sops 3.7.3 or newer

```sh
[freznicek@lenovo-t14 ~ 0]$ sops --version
sops 3.7.3
```

#### age 1.1.1 or newer

```sh
[freznicek@lenovo-t14 ~ 0]$ age --version
1.1.1
```

#### others

```sh
[freznicek@lenovo-t14 02-debugger 0]$ yq --version
yq (https://github.com/mikefarah/yq/) version 4.24.2
[freznicek@lenovo-t14 02-debugger 0]$ jq --version
jq-1.6
```

### Doublecheck kubernetes health

```sh
# Identify not-functional apps / pods
$ kubectl --context ho5 get po --all-namespaces | grep -Ev "1/1|2/2|3/3|4/4|5/5|6/6|7/7|Completed"
NAMESPACE     NAME                                                 READY   STATUS             RESTARTS           AGE

# Listen for 2 minutes k8s events
$ kubectl --context ho5 get event --all-namespaces --watch
```


### Get familiar with GitOps concept

* https://docs.google.com/presentation/d/1OacNz74qsvuyPe6CuLbX-PLX4tlHiYobERD_LzzhKtc/edit#slide=id.g290edbdf74b_0_161
* https://about.gitlab.com/topics/gitops/
* https://www.redhat.com/en/topics/devops/what-is-gitops

### Get familiar with Kubernetes operator concept

* https://kubernetes.io/docs/concepts/extend-kubernetes/operator/

## Steps

We are using [prod-ostrava OpenStack cloud exclusively](https://ostrava.openstack.cloud.e-infra.cz/) in this hands-on, reusing hands-on #5 infra.

While working the below steps it is recommended to close all unnecessary applications.


### Bootstrap G2 infra using FluxCD

To start implementing GitOps infrastructure with FluxCD you need following:
 * Functional kubernetes cluster
 * GitOps repository with initial infrastructure incl. token for reading/writing to the repository, use repository https://gitlab.ics.muni.cz/cloud/g2/hands-on-ceph-openstack-lma (different from the production sites)
 * Bootstrap FluxCD into Kubernetes incl. encryption keys

#### Verify your kubernetes cluster health

```sh
[freznicek@lenovo-t14 ~ 0]$ kubectl --context ho5 get po --all-namespaces | grep -Ev "1/1|2/2|3/3|4/4|5/5|6/6|7/7|Completed"
NAMESPACE     NAME                                                 READY   STATUS             RESTARTS           AGE
```

You may eventually see `kube-vip*`, `openstack-cloud-controller-manager-*` and `csi-cinder-*` pods being reported as failing.
* Kube-vip are static pods and need to be removed from /etc/kubernetes/manifests (`mv /etc/kubernetes/manifests/kube-vip.yml /root` on each controlplane).
* Deployments failing can be disabled via scaling deployent downto 0 replicas (for example `kubectl --context ho5 -n kube-system scale deploy csi-cinder-controllerplugin --replicas=0`).
* Daemonsets failing may be disabled via adding non-existing nodeSelector (`kubectl --context ho5 -n kube-system patch daemonset openstack-cloud-controller-manager -p '{"spec":{"template":{"spec":{"nodeSelector":{"x":"y"}}}}}'`)


#### Verify FluxCD vs Kubernetes version compatibility

```sh
[freznicek@lenovo-t14 ~ 0]$ flux --context ho5 check --pre
► checking prerequisites
✗ flux 0.41.2 <2.3.0 (new version is available, please upgrade)
✔ Kubernetes 1.24.17 >=1.20.6-0
✔ prerequisites checks passed
```

#### Generate credentials

##### A. Generate your own Gitlab project token

Go to [g2/hands-on-ceph-openstack-lma repository Settings/Access Tokens page](https://gitlab.ics.muni.cz/cloud/g2/hands-on-ceph-openstack-lma/-/settings/access_tokens) page and create your own project token with following guidelines:
 * the name should contain `<your-name>` and `hands-on-6` strings
 * token has to have scopes `api, read_api, read_repository, write_repository`
 * token has to have role `Maintainer`

Example token name is `flux-cd-freznicek-hands-on-6`

Keep your token at safe place not telling.

##### B. Generate your own encryption / decryption keys for FluxCD (sops, age)

```sh
[freznicek@lenovo-t14 ~ 0]$ age-keygen -o ~/.config/sops/age/freznicek-hands-on-6-key.txt
Public key: age19fhf7zsahwkksgst3zjgucxv3dr0nqpf7974q4qv2v6w94asu5xsjvtv8l
[freznicek@lenovo-t14 ~ 130]$ wc  ~/.config/sops/age/freznicek-hands-on-6-key.txt
  3   8 189 /home/freznicek/.config/sops/age/freznicek-hands-on-6-key.txt
```

#### Insert decryption FluxCD credentials inside the cluster

```sh
[freznicek@lenovo-t14 ~ 0]$ kubectl --context ho5 create namespace flux-system
namespace/flux-system created
[freznicek@lenovo-t14 ~ 0]$ cat ~/.config/sops/age/freznicek-hands-on-6-key.txt | kubectl --context ho5 create secret generic sops-age-key-freznicek-hands-on-6 --namespace=flux-system --from-file=age.agekey=/dev/stdin
secret/sops-age-key-freznicek-hands-on-6 created
```

#### Prepare initial version of the infrastructure in GitOps repository

This step consists of two phases:
* Initialization of GitOps FluxCD repository structure, [done for hands-on already as commit](https://gitlab.ics.muni.cz/cloud/g2/hands-on-ceph-openstack-lma/-/commit/f8dd16a0ee4fdad5e1501d4527d6eff3a84055fb). Directory structure follows [FluxCD monorepo recommendations](https://fluxcd.io/flux/guides/repository-structure/).
* b] Initialization of your infrastructure [example](https://gitlab.ics.muni.cz/cloud/g2/hands-on-ceph-openstack-lma/-/merge_requests/1)
  * reusing prepared infrastructure already defined in `infrastructure/` dir
  * adding decryption key fo your toplevel kustomizations
  * creation of cluster directories in `apps/` and `clusters/`

#### Bootstrap infrastructure

Initial infrastructure bootstrap can be achieved by following command(s):

```sh
$ flux --context=ho5 bootstrap gitlab --hostname gitlab.ics.muni.cz \
       --owner=cloud/g2 --repository=hands-on-ceph-openstack-lma --branch downstream \
       --path clusters/freznicek-hands-on-6
Please enter your GitLab personal access token (PAT): **************
► connecting to https://gitlab.ics.muni.cz
► cloning branch "downstream" from Git repository "https://gitlab.ics.muni.cz/cloud/g2/hands-on-ceph-openstack-lma.git"
✔ cloned repository
► generating component manifests
✔ generated component manifests
✔ committed sync manifests to "downstream" ("794295a968be23a07e2a12020f54e6d4003c858a")
► pushing component manifests to "https://gitlab.ics.muni.cz/cloud/g2/hands-on-ceph-openstack-lma.git"
► installing components in "flux-system" namespace
✔ installed components
✔ reconciled components
► determining if source secret "flux-system/flux-system" exists
► generating source secret
✔ public key: ecdsa-sha2-nistp384 AAAAE2VjZHNhLXNoYTItbmlzdHAzODQAAAAIbmlzdHAzODQAAABhBE/mrLi+olg2fuyWHZ0znwdaX7RRgMS9v46Y5B1s4rERGe9YqxoZgplivu66CUdJ38f5RZtEjbyhxGx4evaBoPKP10rZd/N6lJkYTa2QDZEi8r822NkqStAPMXs5uW8WUA==
✔ configured deploy key "flux-system-downstream-flux-system-./clusters/freznicek-hands-on-6" for "https://gitlab.ics.muni.cz/cloud/g2/hands-on-ceph-openstack-lma"
► applying source secret "flux-system/flux-system"
✔ reconciled source secret
► generating sync manifests
✔ generated sync manifests
✔ committed sync manifests to "downstream" ("f90ecb14967ea8fc9ddddfb7eda18e02d1ce5d36")
► pushing sync manifests to "https://gitlab.ics.muni.cz/cloud/g2/hands-on-ceph-openstack-lma.git"
► applying sync manifests
✔ reconciled sync configuration
◎ waiting for Kustomization "flux-system/flux-system" to be reconciled
✗ Get "https://195.113.243.177:6443/apis/kustomize.toolkit.fluxcd.io/v1beta2/namespaces/flux-system/kustomizations/flux-system": context deadline exceeded
► confirming components are healthy
✔ helm-controller: deployment ready
✔ kustomize-controller: deployment ready
✔ notification-controller: deployment ready
✔ source-controller: deployment ready
✔ all components are healthy
✗ bootstrap failed with 1 health check failure(s)
[freznicek@lenovo-t14 hands-on-ceph-openstack-lma.git 1]$
```

You may see some health-checks failing.

#### Fixing FluxCD issues (if needed)

Note: If you encounter following behavior perform [Kubernetes service timeouts fix described below](#fixing-k8s-infrastructure).

```sh
[freznicek@lenovo-t14 ~ 130]$ flux get ks
NAME            REVISION        SUSPENDED       READY   MESSAGE
flux-system                     False           False   failed to download archive, error: GET http://source-controller.flux-system.svc.cluster.local./gitrepository/flux-system/flux-system/80d8f136.tar.gz giving up after 10 attempt(s): Get "http://source-controller.flux-system.svc.cluster.local./gitrepository/flux-system/flux-system/80d8f136.tar.gz": dial tcp 10.233.49.97:80: i/o timeout
```

#### Infra state validations and troubleshooting


Final validation that bootstrap suceeded can be performed via commands:

```sh
# Check FluxCD components
[freznicek@lenovo-t14 hands-on-ceph-openstack-lma.git 1]$ flux --context=ho5 check
► checking prerequisites
✗ flux 0.41.2 <2.3.0 (new version is available, please upgrade)
✔ Kubernetes 1.24.17 >=1.20.6-0
► checking controllers
✔ helm-controller: deployment ready
► ghcr.io/fluxcd/helm-controller:v0.31.2
✔ kustomize-controller: deployment ready
► ghcr.io/fluxcd/kustomize-controller:v0.35.1
✔ notification-controller: deployment ready
► ghcr.io/fluxcd/notification-controller:v0.33.0
✔ source-controller: deployment ready
► ghcr.io/fluxcd/source-controller:v0.36.1
► checking crds
✔ alerts.notification.toolkit.fluxcd.io/v1beta2
✔ buckets.source.toolkit.fluxcd.io/v1beta2
✔ gitrepositories.source.toolkit.fluxcd.io/v1beta2
✔ helmcharts.source.toolkit.fluxcd.io/v1beta2
✔ helmreleases.helm.toolkit.fluxcd.io/v2beta1
✔ helmrepositories.source.toolkit.fluxcd.io/v1beta2
✔ kustomizations.kustomize.toolkit.fluxcd.io/v1beta2
✔ ocirepositories.source.toolkit.fluxcd.io/v1beta2
✔ providers.notification.toolkit.fluxcd.io/v1beta2
✔ receivers.notification.toolkit.fluxcd.io/v1beta2
✔ all checks passed

# Check deployed FluxCD resources
[freznicek@lenovo-t14 02-debugger 0]$ flux --context=ho5 get all --all-namespaces
NAMESPACE       NAME                                    REVISION                        SUSPENDED       READY   MESSAGE
default         gitrepository/custom-helm-charts        downstream@sha1:0f215790        False           True    stored artifact for revision 'downstream@sha1:0f215790'
default         gitrepository/oauth2-proxy              downstream@sha1:a66a1f9d        False           True    stored artifact for revision 'downstream@sha1:a66a1f9d'
default         gitrepository/openstack-helm            downstream@sha1:471e8dc8        False           True    stored artifact for revision 'downstream@sha1:471e8dc8'
default         gitrepository/openstack-helm-infra      downstream@sha1:8844cc47        False           True    stored artifact for revision 'downstream@sha1:8844cc47'
default         gitrepository/rook-ceph                 downstream@sha1:f7c9b8df        False           True    stored artifact for revision 'downstream@sha1:f7c9b8df'
flux-system     gitrepository/flux-system               downstream@sha1:e4362d6f        False           True    stored artifact for revision 'downstream@sha1:e4362d6f'

NAMESPACE       NAME                                    REVISION        SUSPENDED       READY   MESSAGE
default         helmrepository/appuio                   sha256:8fef888a False           True    stored artifact: revision 'sha256:8fef888a'
default         helmrepository/bitnami                                  False           True    Helm repository is ready
default         helmrepository/itera                    sha256:6616f395 False           True    stored artifact: revision 'sha256:6616f395'
default         helmrepository/loki-stack               sha256:74dc4c38 False           True    stored artifact: revision 'sha256:74dc4c38'
default         helmrepository/prometheus-community     sha256:b9ace8ba False           True    stored artifact: revision 'sha256:b9ace8ba'
default         helmrepository/rook-ceph                sha256:817afac6 False           True    stored artifact: revision 'sha256:817afac6'
default         helmrepository/weave-gitops                             False           True    Helm repository is ready

NAMESPACE       NAME                                            REVISION        SUSPENDED       READY   MESSAGE
default         helmchart/default-helm-toolkit                  0.2.50          False           True    packaged 'helm-toolkit' chart with version '0.2.50'
default         helmchart/kube-system-ingress-kube-system       0.2.13          False           True    packaged 'ingress' chart with version '0.2.13'

NAMESPACE       NAME                            REVISION        SUSPENDED       READY   MESSAGE
default         helmrelease/helm-toolkit        0.2.50          False           True    Release reconciliation succeeded
kube-system     helmrelease/ingress-kube-system                 False           False   install retries exhausted

NAMESPACE       NAME                            REVISION                        SUSPENDED       READY   MESSAGE
flux-system     kustomization/flux-system       downstream@sha1:e4362d6f        False           True    Applied revision: downstream@sha1:e4362d6f
flux-system     kustomization/infrastructure                                    False           False   Health check failed after 5m2.636206149s: timeout waiting for: [HelmRelease/kube-system/ingress-kube-system status: 'InProgress']

# Check toplevel kustomizations
[freznicek@lenovo-t14 02-debugger 0]$ flux --context=ho5 get kustomization
NAME            REVISION                        SUSPENDED       READY   MESSAGE
flux-system     downstream@sha1:e4362d6f        False           True    Applied revision: downstream@sha1:e4362d6f
infrastructure                                  False           False   Health check failed after 5m2.636206149s: timeout waiting for: [HelmRelease/kube-system/ingress-kube-system status: 'InProgress']
```

This situation proves:
* FluxCD was able to read and parse GiOps infra configuration `Kustomization/flux-system` being Ready
* FluxCD created [`Kustomization/infrastructure` based on our request](https://gitlab.ics.muni.cz/cloud/g2/hands-on-ceph-openstack-lma/-/merge_requests/1/diffs#diff-content-7058e20a10342cf70f46041a385d49bfad65f2db) but this Kustomization is not Ready


##### Troubleshoot Kustomization not being ready
Inspection of `Kustomization/infrastructure` is needed now.

At first we want to see what is inside the `Kustomization/infrastructure`.

```sh
[freznicek@lenovo-t14 02-debugger 0]$ flux --context=ho5 tree kustomization infrastructure
Kustomization/flux-system/infrastructure
├── Deployment/default/debugger
├── HelmRelease/default/helm-toolkit
├── HelmRelease/kube-system/ingress-kube-system
│   ├── ServiceAccount/kube-system/ingress-error-pages
│   ├── ServiceAccount/kube-system/kube-system-ingress-kube-system-ingress
│   ├── ConfigMap/kube-system/ingress-bin
│   ├── ConfigMap/kube-system/ingress-conf
│   ├── ConfigMap/kube-system/ingress-services-tcp
│   ├── ConfigMap/kube-system/ingress-services-udp
│   ├── ClusterRole/kube-system-ingress-kube-system-ingress
│   ├── ClusterRoleBinding/kube-system-ingress-kube-system-ingress
│   ├── Role/kube-system/kube-system-ingress-kube-system-ingress
│   ├── RoleBinding/kube-system/kube-system-ingress-kube-system-ingress
│   ├── Service/kube-system/ingress-error-pages
│   ├── Service/kube-system/ingress-exporter
│   ├── Service/kube-system/ingress
│   ├── DaemonSet/kube-system/ingress
│   └── Deployment/kube-system/ingress-error-pages
├── GitRepository/default/custom-helm-charts
├── GitRepository/default/oauth2-proxy
├── GitRepository/default/openstack-helm
├── GitRepository/default/openstack-helm-infra
├── GitRepository/default/rook-ceph
├── HelmRepository/default/appuio
├── HelmRepository/default/bitnami
├── HelmRepository/default/itera
├── HelmRepository/default/loki-stack
├── HelmRepository/default/prometheus-community
├── HelmRepository/default/rook-ceph
└── HelmRepository/default/weave-gitops
```

Make sure you understand why `Kustomization/infrastructure` contains above resources (see [your own infra initialization MR](https://gitlab.ics.muni.cz/cloud/g2/hands-on-ceph-openstack-lma/-/merge_requests/1/diffs#diff-content-7058e20a10342cf70f46041a385d49bfad65f2db) and [GitOps repo structure initialization](https://gitlab.ics.muni.cz/cloud/g2/hands-on-ceph-openstack-lma/-/commit/f8dd16a0ee4fdad5e1501d4527d6eff3a84055fb)).

![](./pictures/flux-cd-infra-manifests-and-gitops-repository.png)

List all resources are in not-expected state:

```sh
[freznicek@lenovo-t14 02-debugger 0]$ flux --context=ho5 get all --all-namespaces
NAMESPACE       NAME                                    REVISION                        SUSPENDED       READY   MESSAGE
default         gitrepository/custom-helm-charts        downstream@sha1:0f215790        False           True    stored artifact for revision 'downstream@sha1:0f215790'
default         gitrepository/oauth2-proxy              downstream@sha1:a66a1f9d        False           True    stored artifact for revision 'downstream@sha1:a66a1f9d'
default         gitrepository/openstack-helm            downstream@sha1:471e8dc8        False           True    stored artifact for revision 'downstream@sha1:471e8dc8'
default         gitrepository/openstack-helm-infra      downstream@sha1:8844cc47        False           True    stored artifact for revision 'downstream@sha1:8844cc47'
default         gitrepository/rook-ceph                 downstream@sha1:f7c9b8df        False           True    stored artifact for revision 'downstream@sha1:f7c9b8df'
flux-system     gitrepository/flux-system               downstream@sha1:e4362d6f        False           True    stored artifact for revision 'downstream@sha1:e4362d6f'

NAMESPACE       NAME                                    REVISION        SUSPENDED       READY   MESSAGE
default         helmrepository/appuio                   sha256:8fef888a False           True    stored artifact: revision 'sha256:8fef888a'
default         helmrepository/bitnami                                  False           True    Helm repository is ready
default         helmrepository/itera                    sha256:6616f395 False           True    stored artifact: revision 'sha256:6616f395'
default         helmrepository/loki-stack               sha256:74dc4c38 False           True    stored artifact: revision 'sha256:74dc4c38'
default         helmrepository/prometheus-community     sha256:b9ace8ba False           True    stored artifact: revision 'sha256:b9ace8ba'
default         helmrepository/rook-ceph                sha256:817afac6 False           True    stored artifact: revision 'sha256:817afac6'
default         helmrepository/weave-gitops                             False           True    Helm repository is ready

NAMESPACE       NAME                                            REVISION        SUSPENDED       READY   MESSAGE
default         helmchart/default-helm-toolkit                  0.2.50          False           True    packaged 'helm-toolkit' chart with version '0.2.50'
default         helmchart/kube-system-ingress-kube-system       0.2.13          False           True    packaged 'ingress' chart with version '0.2.13'

NAMESPACE       NAME                            REVISION        SUSPENDED       READY   MESSAGE
default         helmrelease/helm-toolkit        0.2.50          False           True    Release reconciliation succeeded
kube-system     helmrelease/ingress-kube-system                 False           False   install retries exhausted

NAMESPACE       NAME                            REVISION                        SUSPENDED       READY   MESSAGE
flux-system     kustomization/flux-system       downstream@sha1:e4362d6f        False           True    Applied revision: downstream@sha1:e4362d6f
flux-system     kustomization/infrastructure                                    False           False   Health check failed after 5m2.379021199s: timeout waiting for: [HelmRelease/kube-system/ingress-kube-system status: 'InProgress']

[freznicek@lenovo-t14 02-debugger 0]$
```
From this listing you identify that following two are not ready:
* `kustomization/infrastructure` an its child
  * `helmrelease/ingress-kube-system` in `kube-system` namespace


Let's look more in detail to `helmrelease/ingress-kube-system`:

```sh
# Ask FluxCD for details (not enough)
[freznicek@lenovo-t14 02-debugger 0]$ flux --context=ho5 -n kube-system get helmrelease
NAME                    REVISION        SUSPENDED       READY   MESSAGE
ingress-kube-system                     False           False   install retries exhausted

# Ask kubectl for details (first get then describe)
[freznicek@lenovo-t14 02-debugger 0]$ kubectl --context=ho5 -n kube-system get helmrelease
NAME                  AGE   READY   STATUS
ingress-kube-system   10h   False   install retries exhausted
[freznicek@lenovo-t14 02-debugger 0]$ kubectl --context=ho5 -n kube-system describe helmrelease
Name:         ingress-kube-system
Namespace:    kube-system
Labels:       kustomize.toolkit.fluxcd.io/name=infrastructure
              kustomize.toolkit.fluxcd.io/namespace=flux-system
Annotations:  <none>
API Version:  helm.toolkit.fluxcd.io/v2beta1
Kind:         HelmRelease
Metadata:
  Creation Timestamp:  2024-06-06T19:31:46Z
  Finalizers:
    finalizers.fluxcd.io
  Generation:  1
  Managed Fields:
    ...
Spec:
  Chart:
    Spec:
      Chart:               ingress
      Interval:            1m
      Reconcile Strategy:  ChartVersion
      Source Ref:
        Kind:         GitRepository
        Name:         openstack-helm-infra
        Namespace:    default
      Version:        *
  Interval:           5m
  Storage Namespace:  kube-system
  Target Namespace:   kube-system
  Values:
    Deployment:
      Mode:  cluster
      Type:  DaemonSet
    Images:
      Tags:
        dep_check:            registry.gitlab.ics.muni.cz:443/cloud/g2/container-registry/docker.itera.io__stackanetes__kubernetes-entrypoint:v1.0.0
        ...
    Labels:
      error_server:
        node_selector_key:    openstack-control-plane
        node_selector_value:  enabled
      Server:
        node_selector_key:    openstack-control-plane
        node_selector_value:  enabled
    Network:
      host_namespace:  true
    Pod:
      Replicas:
        error_page:  2
    release_uuid:    8ca1081d-f2b1-4494-aa0b-b14496e132f8
Status:
  Conditions:
    Last Transition Time:  2024-06-06T19:41:06Z
    Message:               install retries exhausted
    Reason:                InstallFailed
    Status:                False
    Type:                  Ready
    Last Transition Time:  2024-06-06T19:40:21Z
    Message:               Helm install failed: timed out waiting for the condition

Last Helm logs:

    Reason:                        InstallFailed
    Status:                        False
    Type:                          Released
  Failures:                        49
  Helm Chart:                      default/kube-system-ingress-kube-system
  Install Failures:                1
  Last Attempted Revision:         0.2.13
  Last Attempted Values Checksum:  7905846cb539a223b82dde16eb3f0ed1110e3843
  Last Release Revision:           1
  Observed Generation:             1
Events:
  Type     Reason  Age                 From             Message
  ----     ------  ----                ----             -------
  Warning  error   14m (x48 over 10h)  helm-controller  reconciliation failed: install retries exhausted

# Find and describe the pods (why are they in Pending?)
[freznicek@lenovo-t14 02-debugger 0]$ kubectl --context=ho5 -n kube-system get po | grep ingress
ingress-error-pages-7c76b686fd-s4bf4                 0/1     Pending   0                10h
ingress-error-pages-7c76b686fd-x6dx6                 0/1     Pending   0                10h
[freznicek@lenovo-t14 02-debugger 0]$ kubectl --context=ho5 -n kube-system describe po ingress-error-pages-7c76b686fd-s4bf4
Name:             ingress-error-pages-7c76b686fd-s4bf4
Namespace:        kube-system
Priority:         0
Service Account:  ingress-error-pages
Node:             <none>
Labels:           application=ingress
                  component=error-pages
                  pod-template-hash=7c76b686fd
                  release_group=kube-system-ingress-kube-system
Annotations:      configmap-bin-hash: 7a6b18fef96aa3d4d853faf80af396bc0fcf2c1326726ac33af1f20fb0328f65
                  configmap-etc-hash: 084c05cac95676dc864092dea1138d3237b569806e970292b26900f66ff19a62
                  openstackhelm.openstack.org/release_uuid: 8ca1081d-f2b1-4494-aa0b-b14496e132f8
Status:           Pending
IP:
IPs:              <none>
Controlled By:    ReplicaSet/ingress-error-pages-7c76b686fd
Init Containers:
  init:
    Image:      registry.gitlab.ics.muni.cz:443/cloud/g2/container-registry/docker.itera.io__stackanetes__kubernetes-entrypoint:v1.0.0
    Port:       <none>
    Host Port:  <none>
    Command:
      kubernetes-entrypoint
    Environment:
      POD_NAME:                    ingress-error-pages-7c76b686fd-s4bf4 (v1:metadata.name)
      NAMESPACE:                   kube-system (v1:metadata.namespace)
      INTERFACE_NAME:              eth0
      PATH:                        /usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/
      DEPENDENCY_SERVICE:
      DEPENDENCY_DAEMONSET:
      DEPENDENCY_CONTAINER:
      DEPENDENCY_POD_JSON:
      DEPENDENCY_CUSTOM_RESOURCE:
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from kube-api-access-htz89 (ro)
Containers:
  ingress-error-pages:
    Image:        registry.gitlab.ics.muni.cz:443/cloud/g2/container-registry/docker.itera.io__google_containers__defaultbackend:1.4
    Port:         8080/TCP
    Host Port:    0/TCP
    Liveness:     http-get http://:8080/healthz delay=30s timeout=5s period=10s #success=1 #failure=3
    Environment:  <none>
    Mounts:
      /tmp from pod-tmp (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from kube-api-access-htz89 (ro)
Conditions:
  Type           Status
  PodScheduled   False
Volumes:
  pod-tmp:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:
    SizeLimit:  <unset>
  kube-api-access-htz89:
    Type:                    Projected (a volume that contains injected data from multiple sources)
    TokenExpirationSeconds:  3607
    ConfigMapName:           kube-root-ca.crt
    ConfigMapOptional:       <nil>
    DownwardAPI:             true
QoS Class:                   BestEffort
Node-Selectors:              openstack-control-plane=enabled
Tolerations:                 node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                             node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
Events:
  Type     Reason            Age                  From               Message
  ----     ------            ----                 ----               -------
  Warning  FailedScheduling  19m (x25 over 173m)  default-scheduler  0/5 nodes are available: 2 node(s) didn't match Pod's node affinity/selector, 3 node(s) had untolerated taint {node.cloudprovider.kubernetes.io/uninitialized: true}. preemption: 0/5 nodes are available: 5 Preemption is not helpful for scheduling.

# Doublecheck the k8s node labels
[freznicek@lenovo-t14 02-debugger 1]$ kubectl --context=ho5 get no -l openstack-control-plane=enabled
No resources found
[freznicek@lenovo-t14 02-debugger 0]$ kubectl --context=ho5 get no --show-labels | grep openstack-control-plane
[freznicek@lenovo-t14 02-debugger 1]$

# Doublecheck the taints on k8s nodes
[freznicek@lenovo-t14 02-debugger 0]$ kubectl --context=ho5 get node -o custom-columns=NAME:.metadata.name,TAINTS:.spec.taints
NAME                         TAINTS
cloud-hands-on-5-control-1   [map[effect:NoSchedule key:node.cloudprovider.kubernetes.io/uninitialized value:true]]
cloud-hands-on-5-control-2   [map[effect:NoSchedule key:node.cloudprovider.kubernetes.io/uninitialized value:true]]
cloud-hands-on-5-control-3   [map[effect:NoSchedule key:node.cloudprovider.kubernetes.io/uninitialized value:true]]
cloud-hands-on-5-workers-1   <none>
cloud-hands-on-5-workers-2   <none>
```
Summary:
 * `helmrelease/ingress-kube-system` deploys to namespace `kube-system`
 * `helmrelease/ingress-kube-system` deploys to nodes labelled  `openstack-control-plane=enabled`
 * Unfortunatelly there are taints on the k8s nodes
   * Read more information about [taints and tolerations](https://kubernetes.io/docs/concepts/scheduling-eviction/taint-and-toleration/)

Fixes:
* A] remove taints
* B] properly label nodes (not just for definition of controlplane / compute but also for definition where OpenStack components will execute, additionally to initialize rook-ceph crashmap)

```sh
# A] Removing taints
[freznicek@lenovo-t14 02-debugger 0]$ for i in 1 2 3; do kubectl --context=ho5 taint nodes cloud-hands-on-5-control-${i} node.cloudprovider.kubernetes.io/uninitialized=true:NoSchedule-; done
node/cloud-hands-on-5-control-1 untainted
node/cloud-hands-on-5-control-2 untainted
node/cloud-hands-on-5-control-3 untainted
[freznicek@lenovo-t14 02-debugger 0]$ kubectl get node -o custom-columns=NAME:.metadata.name,TAINTS:.spec
NAME                         TAINTS
cloud-hands-on-5-control-1   map[podCIDR:10.233.64.0/24 podCIDRs:[10.233.64.0/24]]
cloud-hands-on-5-control-2   map[podCIDR:10.233.65.0/24 podCIDRs:[10.233.65.0/24]]
cloud-hands-on-5-control-3   map[podCIDR:10.233.66.0/24 podCIDRs:[10.233.66.0/24]]
cloud-hands-on-5-workers-1   map[podCIDR:10.233.67.0/24 podCIDRs:[10.233.67.0/24]]
cloud-hands-on-5-workers-2   map[podCIDR:10.233.68.0/24 podCIDRs:[10.233.68.0/24]]

# B] K8S node labelling
[freznicek@lenovo-t14 02-debugger 0]$ function k8s_label_controlplane() {
  local labels="ceph-rgw=enabled horizon=enabled openstack-control-plane-vnc=enabled openstack-control-plane=enabled openstack-mon-node=enabled openstack-mon=enabled openvswitch=enabled rook-ceph-operator=true rook-ceph-role=storage-node topology.kubernetes.io/zone=ostack-vm linuxbridge=enabled topology.rook.io/rack=rackA"
  local kubectl="${KUBECTL:-"kubectl"}"

  for i_node in "$@"; do
    for i_label in ${labels}; do
      ${KUBECTL} label no ${i_node} "${i_label}"
    done
  done
}

function k8s_label_workers() {
  local labels="openstack-compute-node=enabled openvswitch=enabled rook-ceph-operator=true topology.kubernetes.io/zone=ostack-vm"
  local kubectl="${KUBECTL:-"kubectl"}"

  for i_node in "$@"; do
    for i_label in ${labels}; do
      ${KUBECTL} label no ${i_node} "${i_label}"
    done
  done
}
[freznicek@lenovo-t14 02-debugger 0]$ KUBECTL="kubectl --context=ho5" k8s_label_controlplane cloud-hands-on-5-control-{1,2,3}
node/cloud-hands-on-5-control-1 labeled
...
node/cloud-hands-on-5-control-3 labeled
[freznicek@lenovo-t14 02-debugger 0]$ KUBECTL="kubectl --context=ho5" k8s_label_workers cloud-hands-on-5-workers-{1,2}
node/cloud-hands-on-5-workers-1 labeled
...
node/cloud-hands-on-5-workers-2 labeled
[freznicek@lenovo-t14 02-debugger 0]$

# Doublecheck we now have nodes for `helmrelease/ingress-kube-system` pods
[freznicek@lenovo-t14 02-debugger 0]$ kubectl --context=ho5 get no -l openstack-control-plane=enabled
NAME                         STATUS   ROLES           AGE     VERSION
cloud-hands-on-5-control-1   Ready    control-plane   6d16h   v1.24.17
cloud-hands-on-5-control-2   Ready    control-plane   6d16h   v1.24.17
cloud-hands-on-5-control-3   Ready    control-plane   6d16h   v1.24.17

# Doublecheck helmrelease/ingress-kube-system` pods are running
[freznicek@lenovo-t14 02-debugger 0]$ kubectl --context=ho5 -n kube-system get po | grep ingress
ingress-4x9vt                                        1/1     Running   0                 3m35s
ingress-error-pages-7c76b686fd-s4bf4                 1/1     Running   0                 11h
ingress-error-pages-7c76b686fd-x6dx6                 1/1     Running   0                 11h
ingress-sckld                                        1/1     Running   0                 3m35s
ingress-zwhx2                                        1/1     Running   0                 3m35s
```

Are we done? Not yet ;(
```sh
[freznicek@lenovo-t14 02-debugger 0]$ flux --context=ho5 get ks
NAME            REVISION                        SUSPENDED       READY   MESSAGE
flux-system     downstream@sha1:e4362d6f        False           True    Applied revision: downstream@sha1:e4362d6f
infrastructure                                  False           Unknown Reconciliation in progress
[freznicek@lenovo-t14 02-debugger 0]$ flux --context=ho5 -n kube-system get hr
NAME                    REVISION        SUSPENDED       READY   MESSAGE
ingress-kube-system                     False           False   install retries exhausted
```

Instruct Flux CD to reconcile `helmrelease/ingress-kube-system`
```sh
# Method A - reconcile `helmrelease/ingress-kube-system`
[freznicek@lenovo-t14 02-debugger 0]$ flux --context=ho5 -n kube-system reconcile hr ingress-kube-system
► annotating HelmRelease ingress-kube-system in kube-system namespace
✔ HelmRelease annotated
◎ waiting for HelmRelease reconciliation
✗ HelmRelease reconciliation failed: install retries exhausted

# Method B - redeploy `helmrelease/ingress-kube-system` via suspend and resume
[freznicek@lenovo-t14 02-debugger 1]$ flux --context=ho5 -n kube-system suspend hr ingress-kube-system
► suspending helmrelease ingress-kube-system in kube-system namespace
✔ helmrelease suspended
[freznicek@lenovo-t14 02-debugger 0]$ flux --context=ho5 -n kube-system resume hr ingress-kube-system
► resuming helmrelease ingress-kube-system in kube-system namespace
✔ helmrelease resumed
◎ waiting for HelmRelease reconciliation
✔ HelmRelease reconciliation completed
✔ applied revision 0.2.13
[freznicek@lenovo-t14 02-debugger 0]$ flux --context=ho5 -n kube-system get hr
NAME                    REVISION        SUSPENDED       READY   MESSAGE
ingress-kube-system     0.2.13          False           True    Release reconciliation succeeded

# Doublecheck toplevel Kustomization
[freznicek@lenovo-t14 02-debugger 0]$ flux --context=ho5 get ks
NAME            REVISION                        SUSPENDED       READY   MESSAGE                                     
flux-system     downstream@sha1:e4362d6f        False           True    Applied revision: downstream@sha1:e4362d6f  
infrastructure                                  False           False   Health check failed after 5m1.000729227s: timeout waiting for: [HelmRelease/kube-system/ingress-kube-system status: 'InProgress']

# At this point it is just matter of time when `Kustomization/infrastructure` gets Ready, let's help
[freznicek@lenovo-t14 02-debugger 0]$ flux --context=ho5 reconcile ks infrastructure
► annotating Kustomization infrastructure in flux-system namespace
✔ Kustomization annotated
◎ waiting for Kustomization reconciliation
✔ applied revision downstream@sha1:e4362d6fe708ff8c9b10df27b1eca1a8b3cfba9c

# We are done here!
[freznicek@lenovo-t14 02-debugger 0]$ flux --context=ho5 get ks
NAME            REVISION                        SUSPENDED       READY   MESSAGE
flux-system     downstream@sha1:e4362d6f        False           True    Applied revision: downstream@sha1:e4362d6f
infrastructure  downstream@sha1:e4362d6f        False           True    Applied revision: downstream@sha1:e4362d6f
```

#### Wrap-up questions

1. How to disable applications (get rid of corresponding pods)
   * running as kubeadm static pods from kubernetes cluster?
   * running as deployments or daemonsets?
1. How to validate Flux CD is successfully deployed?
1. Which are the three main subdirectories in FluxCD GitOps repository? Describe them.
1. Is Flux CD server-side or client-side apply GitOps tool?
1. What methods do you know to reconcile / redeploy FluxCD deployed application? Write FluxCD client commands.

### Deploy internal ceph to provide k8s internal distributed storage


[Rook ceph](https://rook.io/) is [k8s operator](https://kubernetes.io/docs/concepts/extend-kubernetes/operator/) for deploying and managing ceph distrbuted storage.

Deployment of rook ceph can be done following way:
* prepare MR to hands-on-ceph-openstack-lma ([example](https://gitlab.ics.muni.cz/cloud/g2/hands-on-ceph-openstack-lma/-/merge_requests/2))

After merging watck kubernetes cluster with, be patient
* `watch --interval=10 "flux --context=ho5 get ks;kubectl --context=ho5 get ns rook-ceph;kubectl --context=ho5 -n rook-ceph get all;kubectl --context=ho5 -n rook-ceph get cephclusters"`
* `kubectl --context=ho5 -n rook-ceph logs -f -l app=rook-ceph-operator` # once rook-ceph-operator appears

Process started sucessfully when:
* deployment/rook-ceph-operator appeared (watch above dups logs)
* various objects are deployed inside `rook-ceph` namespace

In the middle of the process it may look like this:
```console
Every 10,0s: flux --context=ho5 get ks;kubectl --context=ho5 get ns rook-ceph;kubectl ...  lenovo-t14: Fri Jun  7 11:46:11 2024

NAME            REVISION                        SUSPENDED       READY   MESSAGE
apps-ceph                                       False           Unknown Reconciliation in progress
flux-system     downstream@sha1:bdb748e5        False           True    Applied revision: downstream@sha1:bdb748e5
infrastructure  downstream@sha1:bdb748e5        False           Unknown Reconciliation in progress
NAME        STATUS   AGE
rook-ceph   Active   10m
NAME                                             READY   STATUS              RESTARTS   AGE
pod/csi-rbdplugin-2lhz5                          2/2     Running             0          96s
pod/csi-rbdplugin-mhm44                          2/2     Running             0          101s
pod/csi-rbdplugin-provisioner-7cb8bf4fb9-lw2zn   0/5     ContainerCreating   0          102s
pod/csi-rbdplugin-provisioner-7cb8bf4fb9-vbwq9   0/5     ContainerCreating   0          102s
pod/csi-rbdplugin-qdmm7                          2/2     Running             0          102s
pod/csi-rbdplugin-qjx7d                          2/2     Running             0          101s
pod/csi-rbdplugin-r6qdj                          0/2     ContainerCreating   0          97s
pod/rook-ceph-detect-version-2cnkg               0/1     Completed           0          2m10s
pod/rook-ceph-operator-669dbfdfdc-gm2f8          1/1     Running             0          6m44s
pod/rook-ceph-tools-74576878f6-hgpsc             0/1     ContainerCreating   0          2m3s
pod/rook-discover-2nr2s                          1/1     Running             0          2m8s
pod/rook-discover-4cctd                          1/1     Running             0          2m10s
pod/rook-discover-d9dlz                          1/1     Running             0          5m9s
pod/rook-discover-gjkdg                          1/1     Running             0          5m14s
pod/rook-discover-q94vw                          1/1     Running             0          5m9s

NAME                           DESIRED   CURRENT   READY   UP-TO-DATE   AVAILABLE   NODE SELECTOR   AGE
daemonset.apps/csi-rbdplugin   5         5         1       5            1           <none>          106s
daemonset.apps/rook-discover   5         5         4       5            4           <none>          5m22s

NAME                                        READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/csi-rbdplugin-provisioner   0/2     2            0           110s
deployment.apps/rook-ceph-mon-a-canary      0/1     0            0           31s
deployment.apps/rook-ceph-mon-b-canary      0/1     0            0           29s
deployment.apps/rook-ceph-mon-c-canary      0/1     0            0           29s
deployment.apps/rook-ceph-operator          1/1     1            1           6m56s
deployment.apps/rook-ceph-tools             0/1     1            0           2m58s

NAME                                     COMPLETIONS   DURATION   AGE
job.batch/rook-ceph-csi-detect-version   1/1           51s        2m36s
job.batch/rook-ceph-detect-version       0/1           2m28s      2m37s
NAME        DATADIRHOSTPATH   MONCOUNT   AGE     PHASE         MESSAGE                 HEALTH   EXTERNAL
rook-ceph   /var/lib/rook     3          2m56s   Progressing   Configuring Ceph Mons
```

Finally (waiting for 10-15 minutes) you should see following result:

```sh
# toplevel kustomizations are ready
[freznicek@lenovo-t14 hands-on-ceph-openstack-lma.git 0]$ flux --context=ho5 get ks
NAME            REVISION                        SUSPENDED       READY   MESSAGE
apps-ceph       downstream@sha1:bdb748e5        False           True    Applied revision: downstream@sha1:bdb748e5
flux-system     downstream@sha1:bdb748e5        False           True    Applied revision: downstream@sha1:bdb748e5
infrastructure  downstream@sha1:bdb748e5        False           True    Applied revision: downstream@sha1:bdb748e5

# Ceph cluster CRD showing ceph cluster HEALTH_OK
[freznicek@lenovo-t14 hands-on-ceph-openstack-lma.git 0]$ kubectl --context=ho5 -n rook-ceph get cephclusters
NAME        DATADIRHOSTPATH   MONCOUNT   AGE   PHASE         MESSAGE                   HEALTH      EXTERNAL
rook-ceph   /var/lib/rook     3          18m   Progressing   Configuring Ceph Mgr(s)   HEALTH_OK

# Ceph cluster details shows ClusterCreated with 6 OSDs, 3 MONs and 2 MGRs
[freznicek@lenovo-t14 hands-on-ceph-openstack-lma.git 0]$ kubectl --context=ho5 -n rook-ceph describe cephclusters rook-ceph
Name:         rook-ceph
Namespace:    rook-ceph
Labels:       app.kubernetes.io/managed-by=Helm
              helm.toolkit.fluxcd.io/name=rook-ceph-cluster
              helm.toolkit.fluxcd.io/namespace=rook-ceph
Annotations:  meta.helm.sh/release-name: rook-ceph-rook-ceph-cluster
              meta.helm.sh/release-namespace: rook-ceph
API Version:  ceph.rook.io/v1
Kind:         CephCluster
Metadata:
  Creation Timestamp:  2024-06-07T09:43:39Z
  Finalizers:
    cephcluster.ceph.rook.io
  Generation:  2
  Managed Fields:
    ...
  Resource Version:  1494591
  UID:               fbe6e3a6-059c-42c3-9109-9907cc02e055
Spec:
  Ceph Version:
    Image:  quay.io/ceph/ceph:v17.2.7
  Cleanup Policy:
    Sanitize Disks:
      Data Source:  zero
      Iteration:    1
      Method:       quick
  Crash Collector:
  Dashboard:
    Enabled:           true
    Ssl:               true
  Data Dir Host Path:  /var/lib/rook
  Disruption Management:
    Machine Disruption Budget Namespace:  openshift-machine-api
    Manage Pod Budgets:                   true
    Osd Maintenance Timeout:              30
  External:
  Health Check:
    Daemon Health:
      Mon:
        Interval:  45s
      Osd:
        Interval:  1m0s
      Status:
        Interval:  1m0s
    Liveness Probe:
      Mgr:
      Mon:
      Osd:
  Log Collector:
    Enabled:       true
    Max Log Size:  500M
    Periodicity:   daily
  Mgr:
    Count:  2
    Modules:
      Enabled:  true
      Name:     pg_autoscaler
  Mon:
    Count:  3
  Monitoring:
  Network:
    Ip Family:  IPv4
    Provider:   host
  Placement:
    All:
      Node Affinity:
        Required During Scheduling Ignored During Execution:
          Node Selector Terms:
            Match Expressions:
              Key:       rook-ceph-role
              Operator:  In
              Values:
                storage-node
  Priority Class Names:
    Mgr:  system-cluster-critical
    Mon:  system-node-critical
    Osd:  system-node-critical
  Resources:
    ...
  Security:
    Kms:
  Storage:
    Nodes:
      Device Filter:  ^sd[b-c]
      Name:           cloud-hands-on-5-control-1
      Resources:
      Device Filter:  ^sd[b-c]
      Name:           cloud-hands-on-5-control-2
      Resources:
      Device Filter:  ^sd[b-c]
      Name:           cloud-hands-on-5-control-3
      Resources:
    Use All Devices:                        false
  Wait Timeout For Healthy OSD In Minutes:  10
Status:
  Ceph:
    Capacity:
      Bytes Available:  3219394621440
      Bytes Total:      3221225472000
      Bytes Used:       1830850560
      Last Updated:     2024-06-07T10:01:55Z
    Fsid:               a325ef5e-9396-4c80-af74-fc58fc78bfe7
    Health:             HEALTH_OK
    Last Changed:       2024-06-07T10:00:48Z
    Last Checked:       2024-06-07T10:01:55Z
    Previous Health:    HEALTH_WARN
    Versions:
      Mgr:
        ceph version 17.2.7 (b12291d110049b2f35e32e0de30d70e9a4c060d2) quincy (stable):  2
      Mon:
        ceph version 17.2.7 (b12291d110049b2f35e32e0de30d70e9a4c060d2) quincy (stable):  3
      Osd:
        ceph version 17.2.7 (b12291d110049b2f35e32e0de30d70e9a4c060d2) quincy (stable):  6
      Overall:
        ceph version 17.2.7 (b12291d110049b2f35e32e0de30d70e9a4c060d2) quincy (stable):  11
  Conditions:
    Last Heartbeat Time:   2024-06-07T10:01:56Z
    Last Transition Time:  2024-06-07T09:56:01Z
    Message:               Cluster created successfully
    Reason:                ClusterCreated
    Status:                True
    Type:                  Ready
    Last Heartbeat Time:   2024-06-07T10:02:04Z
    Last Transition Time:  2024-06-07T10:02:04Z
    Message:               Configuring Ceph Mgr(s)
    Reason:                ClusterProgressing
    Status:                True
    Type:                  Progressing
  Message:                 Configuring Ceph Mgr(s)
  Observed Generation:     2
  Phase:                   Progressing
  State:                   Creating
  Storage:
    Device Classes:
      Name:  hdd
  Version:
    Image:    quay.io/ceph/ceph:v17.2.7
    Version:  17.2.7-0
Events:
  Type    Reason              Age    From                          Message
  ----    ------              ----   ----                          -------
  Normal  ReconcileSucceeded  6m18s  rook-ceph-cluster-controller  successfully configured CephCluster "rook-ceph/rook-ceph"
```

We finally got ceph provided storage classes:
```sh
[freznicek@lenovo-t14 hands-on-ceph-openstack-lma.git 0]$ kubectl --context=ho5 get storageclass
NAME                   PROVISIONER                     RECLAIMPOLICY   VOLUMEBINDINGMODE   ALLOWVOLUMEEXPANSION   AGE
ceph-block (default)   rook-ceph.rbd.csi.ceph.com      Delete          Immediate           true                   167m
ceph-bucket            rook-ceph.ceph.rook.io/bucket   Delete          Immediate           false                  167m
csi-sc-cinderplugin    cinder.csi.openstack.org        Delete          Immediate           false                  6d21h
```

#### Wrap-up questions

1. Describe with your own words what exactly happened in this step with as much details as possible.

### Deploy part of the OpenStack on Kubernetes backed by distributed ceph

To deploy first OpenStack components create your own dup of [this MR](https://gitlab.ics.muni.cz/cloud/g2/hands-on-ceph-openstack-lma/-/merge_requests/3).

#### configmap and secret data sharing

We use 00-common-configmap.yaml and 00-common-encryptedsecret.yaml for sharing data among HelmReleases in the namespace.

Creation of `secret/common` is done following way:
```sh
# Prepare secret skeleton
[freznicek@lenovo-t14 03-openstack 0]$ cat 00-common-plainsecret.yaml
apiVersion: v1
stringData:
    mariadb.admin.password: ""
    mariadb.exporter.password: ""
    mariadb.sst.password: ""
    rabbitmq.admin.password: ""
kind: Secret
metadata:
    name: common
    namespace: openstack
type: Opaque

# Fill in the passwords
...

# Encrypt
[freznicek@lenovo-t14 03-openstack 0]$ head -2 ../../../scripts/encrypt_k8s_secret.sh
#!/usr/bin/env bash
# encrypt_k8s_secret.sh <sops-configuration-file> <input-yaml-secret-manifest-file>
[freznicek@lenovo-t14 03-openstack 0]$ ../../../scripts/encrypt_k8s_secret.sh \
                                         ../../../clusters/freznicek-hands-on-6/.sops.yaml \
                                         00-common-plainsecret.yaml > 00-common-encryptedsecret.yaml

```
Warning: make sure you DO NOT commit plain passwords into GitOps repository.

Once you approve and merge you may see the progress using `watch --interval=10 "flux --context=ho5 get ks;kubectl --context=ho5 get ns openstack;kubectl --context=ho5 -n openstack get deploy,job,sts,cm,secret,po"`

```sh
# In the middle of progress
Every 10,0s: flux --context=ho5 get ks;kubectl --context=ho5 get ns openstack;kubectl --context=ho5 -n openstack get deploy,job,sts,cm,secret,po                                                        lenovo-t14: Fri Jun  7 15:48:49 2024

NAME            REVISION                        SUSPENDED       READY   MESSAGE
apps-ceph       downstream@sha1:40c430b3        False           True    Applied revision: downstream@sha1:40c430b3
apps-openstack                                  False           Unknown Reconciliation in progress
flux-system     downstream@sha1:40c430b3        False           True    Applied revision: downstream@sha1:40c430b3
infrastructure  downstream@sha1:40c430b3        False           True    Applied revision: downstream@sha1:40c430b3
NAME        STATUS   AGE
openstack   Active   5m45s
NAME                                  READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/ingress               0/2     0            0           2m28s
deployment.apps/ingress-error-pages   0/2     0            0           2m28s

NAME                                     DATA   AGE
configmap/common                         1      5m42s
configmap/ingress-bin                    1      3m
configmap/ingress-conf                   4      3m
configmap/ingress-services-tcp           0      3m
configmap/ingress-services-udp           0      3m
configmap/kube-root-ca.crt               1      5m48s
configmap/openstack-ingress-controller   0      39s

NAME                                                        TYPE                 DATA   AGE
secret/common                                               Opaque               4      5m42s
secret/sh.helm.release.v1.openstack-ingress-controller.v1   helm.sh/release.v1   1      3m

NAME                                       READY   STATUS    RESTARTS   AGE
pod/ingress-76ff8d65d7-bqz85               0/1     Pending   0          4s
pod/ingress-76ff8d65d7-dgkr6               1/1     Running   0          2m31s
pod/ingress-error-pages-7bb75df8dd-99clq   1/1     Running   0          2m28s
pod/ingress-error-pages-7bb75df8dd-ctg95   1/1     Running   0          2m25s

# Visualize apps-openstack toplevel kustomization
[freznicek@lenovo-t14 ~ 0]$ flux --context=ho5 tree ks apps-openstack
Kustomization/flux-system/apps-openstack
├── Namespace/openstack
├── ConfigMap/openstack/common
├── Secret/openstack/common
├── HelmRelease/openstack/ceph-cluster-config
└── HelmRelease/openstack/ingress-controller
```

Finally you should get:
```sh
[freznicek@lenovo-t14 ~ 0]$ flux --context=ho5 get ks
NAME            REVISION                        SUSPENDED       READY   MESSAGE
apps-ceph       downstream@sha1:40c430b3        False           True    Applied revision: downstream@sha1:40c430b3
apps-openstack  downstream@sha1:40c430b3        False           True    Applied revision: downstream@sha1:40c430b3
flux-system     downstream@sha1:40c430b3        False           True    Applied revision: downstream@sha1:40c430b3
infrastructure  downstream@sha1:40c430b3        False           True    Applied revision: downstream@sha1:40c430b3
[freznicek@lenovo-t14 ~ 0]$ flux --context=ho5 -n openstack get hr
NAME                    REVISION        SUSPENDED       READY   MESSAGE
ceph-cluster-config     1.1.1           False           True    Release reconciliation succeeded
ingress-controller      0.2.13          False           True    Release reconciliation succeeded
```


#### Wrap-up questions

1. What Flux CD key is used in source encryption process, where is this key persisted?

#### Next Questions

Try to enable [all other commented apps](https://gitlab.ics.muni.cz/cloud/g2/hands-on-ceph-openstack-lma/-/merge_requests/3/diffs#22c913a11e838fb1605c7b4cb4c62a4e17d2ea0c_0_9) mariadb Galera cluster and memcached and get them deployed.


### Flux CD pitfalls, getting used to GitOps with FluxCD

Read through https://gitlab.ics.muni.cz/cloud/knowledgebase/-/blob/master/howtos/g2/how-to-deploy-g2-ostack-cloud-infrastructure.md#10-troubleshoot-flux-cd-deployment-and-fix-obstacles chapter.

## Fixing k8s infrastructure

### Kubernetes service timeouts

Follow this guide when you do not see FluxCD toplevel Kustomization ready, for instance:
```sh
[freznicek@lenovo-t14 ~ 130]$ flux get ks
NAME            REVISION        SUSPENDED       READY   MESSAGE                                                flux-system                     False           False   failed to download archive, error: GET http://source-controller.flux-system.svc.cluster.local./gitrepository/flux-system/flux-system/80d8f136.tar.gz giving up after 10 attempt(s): Get "http://source-controller.flux-system.svc.cluster.local./gitrepository/flux-system/flux-system/80d8f136.tar.gz": dial tcp 10.233.49.97:80: i/o timeout
```

#### A] Tune ETCD parameters

```sh
# etcd cluster timing change
[freznicek@lenovo-t14 hands-on-ceph-openstack-lma.git 0]$ ips=$(kubectl --context=ho5 get no -owide | grep -Eo "10.0.0.[0-9]+")
[freznicek@lenovo-t14 hands-on-ceph-openstack-lma.git 0]$ for i_ip in $ips ; do ssh -J ubuntu@195.113.243.177 ubuntu@$i_ip 'uname -a;if [ -e /etc/etcd.env ]; then sudo grep -E "ETCD_ELECTION_TIMEOUT|ETCD_HEARTBEAT_INTERVAL" /etc/etcd.env; sudo cp /etc/etcd.env /etc/etcd.env.back; sudo cat /etc/etcd.env.back | sed -r "s/ETCD_ELECTION_TIMEOUT=.+/ETCD_ELECTION_TIMEOUT=30000/g" | sed -r "s/ETCD_HEARTBEAT_INTERVAL=.+/ETCD_HEARTBEAT_INTERVAL=3000/g" > /tmp/etcd.env; sudo cp -f /tmp/etcd.env /etc/etcd.env; sudo grep -E "ETCD_ELECTION_TIMEOUT|ETCD_HEARTBEAT_INTERVAL" /etc/etcd.env;
 sudo service etcd restart; sleep 10; fi'; done
Linux cloud-hands-on-5-control-1 5.15.0-73-generic #80-Ubuntu SMP Mon May 15 15:18:26 UTC 2023 x86_64 x86_64 x86_64 GNU/Linux
ETCD_ELECTION_TIMEOUT=5000
ETCD_HEARTBEAT_INTERVAL=250
ETCD_ELECTION_TIMEOUT=30000
ETCD_HEARTBEAT_INTERVAL=3000
Linux cloud-hands-on-5-control-2 5.15.0-73-generic #80-Ubuntu SMP Mon May 15 15:18:26 UTC 2023 x86_64 x86_64 x86_64 GNU/Linux
ETCD_ELECTION_TIMEOUT=5000
ETCD_HEARTBEAT_INTERVAL=250
ETCD_ELECTION_TIMEOUT=30000
ETCD_HEARTBEAT_INTERVAL=3000
Linux cloud-hands-on-5-control-3 5.15.0-73-generic #80-Ubuntu SMP Mon May 15 15:18:26 UTC 2023 x86_64 x86_64 x86_64 GNU/Linux
ETCD_ELECTION_TIMEOUT=5000
ETCD_HEARTBEAT_INTERVAL=250
ETCD_ELECTION_TIMEOUT=30000
ETCD_HEARTBEAT_INTERVAL=3000
Linux cloud-hands-on-5-workers-1 5.15.0-73-generic #80-Ubuntu SMP Mon May 15 15:18:26 UTC 2023 x86_64 x86_64 x86_64 GNU/Linux
Linux cloud-hands-on-5-workers-2 5.15.0-73-generic #80-Ubuntu SMP Mon May 15 15:18:26 UTC 2023 x86_64 x86_64 x86_64 GNU/Linux
[freznicek@lenovo-t14 hands-on-ceph-openstack-lma.git 0]$

# test etcd cluster health
[freznicek@lenovo-t14 hands-on-ceph-openstack-lma.git 0]$ for i_ip in $ips ; do ssh -J ubuntu@195.113.243.177 ubuntu@$i_ip 'uname -a;if [ -e /etc/etcd.env ]; then sudo etcdctl --cacert=/etc/ssl/etcd/ssl/ca.pem --cert=/etc/ssl/etcd/ssl/node-$(hostname).pem --key=/etc/ssl/etcd/ssl/node-$(hostname)-key.pem  --endpoints=https://10.0.0.82:2379,https://10.0.0.220:2379,https://10.0.0.124:2379 endpoint health; fi'; done
Linux cloud-hands-on-5-control-1 5.15.0-73-generic #80-Ubuntu SMP Mon May 15 15:18:26 UTC 2023 x86_64 x86_64 x86_64 GNU/Linux
https://10.0.0.220:2379 is healthy: successfully committed proposal: took = 469.868433ms
https://10.0.0.82:2379 is healthy: successfully committed proposal: took = 766.851155ms
https://10.0.0.124:2379 is healthy: successfully committed proposal: took = 1.727092167s
Linux cloud-hands-on-5-control-2 5.15.0-73-generic #80-Ubuntu SMP Mon May 15 15:18:26 UTC 2023 x86_64 x86_64 x86_64 GNU/Linux
https://10.0.0.82:2379 is healthy: successfully committed proposal: took = 66.72205ms
https://10.0.0.220:2379 is healthy: successfully committed proposal: took = 200.530163ms
https://10.0.0.124:2379 is healthy: successfully committed proposal: took = 2.164782113s
Linux cloud-hands-on-5-control-3 5.15.0-73-generic #80-Ubuntu SMP Mon May 15 15:18:26 UTC 2023 x86_64 x86_64 x86_64 GNU/Linux
https://10.0.0.220:2379 is healthy: successfully committed proposal: took = 7.958851ms
https://10.0.0.82:2379 is healthy: successfully committed proposal: took = 10.750985ms
https://10.0.0.124:2379 is healthy: successfully committed proposal: took = 709.499259ms
Linux cloud-hands-on-5-workers-1 5.15.0-73-generic #80-Ubuntu SMP Mon May 15 15:18:26 UTC 2023 x86_64 x86_64 x86_64 GNU/Linux
Linux cloud-hands-on-5-workers-2 5.15.0-73-generic #80-Ubuntu SMP Mon May 15 15:18:26 UTC 2023 x86_64 x86_64 x86_64 GNU/Linux
[freznicek@lenovo-t14 hands-on-ceph-openstack-lma.git 0]$

# tune etcd process ionice
[freznicek@lenovo-t14 hands-on-ceph-openstack-lma.git 0]$ for i_ip in $ips ; do ssh -J ubuntu@195.113.243.177 ubuntu@$i_ip 'uname -a;if [ -e /etc/etcd.env ]; then sudo ionice -c2 -n0 -p `sudo pgrep etcd`; fi'; done
Linux cloud-hands-on-5-control-1 5.15.0-73-generic #80-Ubuntu SMP Mon May 15 15:18:26 UTC 2023 x86_64 x86_64 x86_64 GNU/Linux
Linux cloud-hands-on-5-control-2 5.15.0-73-generic #80-Ubuntu SMP Mon May 15 15:18:26 UTC 2023 x86_64 x86_64 x86_64 GNU/Linux
Linux cloud-hands-on-5-control-3 5.15.0-73-generic #80-Ubuntu SMP Mon May 15 15:18:26 UTC 2023 x86_64 x86_64 x86_64 GNU/Linux
Linux cloud-hands-on-5-workers-1 5.15.0-73-generic #80-Ubuntu SMP Mon May 15 15:18:26 UTC 2023 x86_64 x86_64 x86_64 GNU/Linux
Linux cloud-hands-on-5-workers-2 5.15.0-73-generic #80-Ubuntu SMP Mon May 15 15:18:26 UTC 2023 x86_64 x86_64 x86_64 GNU/Linux

# check load
[freznicek@lenovo-t14 hands-on-ceph-openstack-lma.git 130]$ for i_ip in $ips ; do ssh -J ubuntu@195.113.243.177 ubuntu@$i_ip 'uname -a;uptime;'; done
Linux cloud-hands-on-5-control-1 5.15.0-73-generic #80-Ubuntu SMP Mon May 15 15:18:26 UTC 2023 x86_64 x86_64 x86_64 GNU/Linux
 19:06:28 up 6 days,  6:10,  0 users,  load average: 1.87, 1.67, 1.47
Linux cloud-hands-on-5-control-2 5.15.0-73-generic #80-Ubuntu SMP Mon May 15 15:18:26 UTC 2023 x86_64 x86_64 x86_64 GNU/Linux
 19:06:29 up 6 days,  6:10,  0 users,  load average: 2.57, 1.68, 1.50
Linux cloud-hands-on-5-control-3 5.15.0-73-generic #80-Ubuntu SMP Mon May 15 15:18:26 UTC 2023 x86_64 x86_64 x86_64 GNU/Linux
 19:06:30 up 6 days,  6:10,  0 users,  load average: 1.32, 1.59, 1.48
Linux cloud-hands-on-5-workers-1 5.15.0-73-generic #80-Ubuntu SMP Mon May 15 15:18:26 UTC 2023 x86_64 x86_64 x86_64 GNU/Linux
 19:06:31 up 6 days,  6:10,  0 users,  load average: 0.19, 0.21, 0.22
Linux cloud-hands-on-5-workers-2 5.15.0-73-generic #80-Ubuntu SMP Mon May 15 15:18:26 UTC 2023 x86_64 x86_64 x86_64 GNU/Linux
 19:06:32 up 6 days,  6:10,  0 users,  load average: 0.36, 0.42, 0.32
```

#### B] Disable FluxCD networkpolicies

It was proved that calico in default configuration does not work well with FluxCD networkpolicies in place.

```sh
# Remove FluxCD networkpolicies installed during bootstrap
[freznicek@lenovo-t14 02-debugger 0]$ kubectl --context ho5 -n flux-system get networkpolicy
NAME             POD-SELECTOR                  AGE
allow-egress     <none>                        151m
allow-scraping   <none>                        151m
allow-webhooks   app=notification-controller   151m
[freznicek@lenovo-t14 02-debugger 0]$ kubectl --context ho5 -n flux-system delete networkpolicy --all
networkpolicy.networking.k8s.io "allow-egress" deleted
networkpolicy.networking.k8s.io "allow-scraping" deleted
networkpolicy.networking.k8s.io "allow-webhooks" deleted
[freznicek@lenovo-t14 02-debugger 0]$

```

Update FluxCD deployment to drop FluxCD networkpolicies in your FluxC deployment [example commit](https://gitlab.ics.muni.cz/cloud/g2/hands-on-ceph-openstack-lma/-/commit/e4362d6fe708ff8c9b10df27b1eca1a8b3cfba9c) in your own subdirectory `clusters/<cluster-name>`.

#### C] Force enable date-time synchronization

```sh
# Doublecheck current time on the VMs, restart systemd-timesyncd
[freznicek@lenovo-t14 hands-on-ceph-openstack-lma.git 0]$ ips=$(kubectl --context=ho5 get no -owide | grep -Eo "10.0.0.[0-9]+")
[freznicek@lenovo-t14 hands-on-ceph-openstack-lma.git 0]$ for i_ip in $ips ; do ssh -J ubuntu@195.113.243.177 ubuntu@$i_ip 'uname -a;sudo service systemd-timesyncd restart;sudo systemctl | grep systemd-timesyncd;timedatectl'; done
Linux cloud-hands-on-5-control-1 5.15.0-73-generic #80-Ubuntu SMP Mon May 15 15:18:26 UTC 2023 x86_64 x86_64 x86_64 GNU/Linux
  systemd-timesyncd.service                                                                                                                     loaded active running   Network Time Synchronization
               Local time: Fri 2024-06-07 05:27:45 UTC
           Universal time: Fri 2024-06-07 05:27:45 UTC
                 RTC time: Fri 2024-06-07 05:27:45
                Time zone: Etc/UTC (UTC, +0000)
System clock synchronized: yes
              NTP service: active
          RTC in local TZ: no
Linux cloud-hands-on-5-control-2 5.15.0-73-generic #80-Ubuntu SMP Mon May 15 15:18:26 UTC 2023 x86_64 x86_64 x86_64 GNU/Linux
  systemd-timesyncd.service                                                                                                                     loaded active running   Network Time Synchronization
               Local time: Fri 2024-06-07 05:27:46 UTC
           Universal time: Fri 2024-06-07 05:27:46 UTC
                 RTC time: Fri 2024-06-07 05:27:47
                Time zone: Etc/UTC (UTC, +0000)
System clock synchronized: yes
              NTP service: active
          RTC in local TZ: no
Linux cloud-hands-on-5-control-3 5.15.0-73-generic #80-Ubuntu SMP Mon May 15 15:18:26 UTC 2023 x86_64 x86_64 x86_64 GNU/Linux
  systemd-timesyncd.service                                                                                                                     loaded active running   Network Time Synchronization
               Local time: Fri 2024-06-07 05:27:48 UTC
           Universal time: Fri 2024-06-07 05:27:48 UTC
                 RTC time: Fri 2024-06-07 05:27:48
                Time zone: Etc/UTC (UTC, +0000)
System clock synchronized: yes
              NTP service: active
          RTC in local TZ: no
Linux cloud-hands-on-5-workers-1 5.15.0-73-generic #80-Ubuntu SMP Mon May 15 15:18:26 UTC 2023 x86_64 x86_64 x86_64 GNU/Linux
  systemd-timesyncd.service                                                                                                                     loaded active running   Network Time Synchronization
               Local time: Fri 2024-06-07 05:27:50 UTC
           Universal time: Fri 2024-06-07 05:27:50 UTC
                 RTC time: Fri 2024-06-07 05:27:50
                Time zone: Etc/UTC (UTC, +0000)
System clock synchronized: yes
              NTP service: active
          RTC in local TZ: no
Linux cloud-hands-on-5-workers-2 5.15.0-73-generic #80-Ubuntu SMP Mon May 15 15:18:26 UTC 2023 x86_64 x86_64 x86_64 GNU/Linux
  systemd-timesyncd.service                                                                                                                     loaded active running   Network Time Synchronization
               Local time: Fri 2024-06-07 05:27:52 UTC
           Universal time: Fri 2024-06-07 05:27:52 UTC
                 RTC time: Fri 2024-06-07 05:27:51
                Time zone: Etc/UTC (UTC, +0000)
System clock synchronized: yes
              NTP service: active
          RTC in local TZ: no
[freznicek@lenovo-t14 hands-on-ceph-openstack-lma.git 0]$
```

#### D] Tune kube-ipvs0 and nodelocaldns MTUs

Assure that `MTU(ens3) == MTU(kube-ipvs0) == MTU(nodelocaldns)`

```sh
[freznicek@lenovo-t14 ~ 0]$ ips=$(kubectl --context=ho5 get no -owide | grep -Eo "10.0.0.[0-9]+")

# Get NIC MTUs
[freznicek@lenovo-t14 ~ 0]$ for i_ip in $ips ; do ssh -J ubuntu@195.113.243.177 ubuntu@$i_ip 'uname -a;ip a|grep -E "^[0-9+]"'; done
...

# Set MTU(kube-ipvs0) == MTU(nodelocaldns) == MTU(ens3)
[freznicek@lenovo-t14 ~ 0]$ for i_ip in $ips ; do ssh -J ubuntu@195.113.243.177 ubuntu@$i_ip 'uname -a;sudo ip link set dev kube-ipvs0 mtu 8950;sudo ip link set dev nodelocaldns mtu 8950;ip a|grep -E "^[0-9+]"'; done
Linux cloud-hands-on-5-control-1 5.15.0-73-generic #80-Ubuntu SMP Mon May 15 15:18:26 UTC 2023 x86_64 x86_64 x86_64 GNU/Linux
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
2: ens3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 8950 qdisc fq_codel state UP group default qlen 1000
3: kube-ipvs0: <BROADCAST,NOARP> mtu 8950 qdisc noop state DOWN group default
4: vxlan.calico: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 8900 qdisc noqueue state UNKNOWN group default
7: nodelocaldns: <BROADCAST,NOARP> mtu 8950 qdisc noop state DOWN group default
Linux cloud-hands-on-5-control-2 5.15.0-73-generic #80-Ubuntu SMP Mon May 15 15:18:26 UTC 2023 x86_64 x86_64 x86_64 GNU/Linux
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
2: ens3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 8950 qdisc fq_codel state UP group default qlen 1000
3: kube-ipvs0: <BROADCAST,NOARP> mtu 8950 qdisc noop state DOWN group default
4: vxlan.calico: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 8900 qdisc noqueue state UNKNOWN group default
7: nodelocaldns: <BROADCAST,NOARP> mtu 8950 qdisc noop state DOWN group default
Linux cloud-hands-on-5-control-3 5.15.0-73-generic #80-Ubuntu SMP Mon May 15 15:18:26 UTC 2023 x86_64 x86_64 x86_64 GNU/Linux
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
2: ens3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 8950 qdisc fq_codel state UP group default qlen 1000
3: kube-ipvs0: <BROADCAST,NOARP> mtu 8950 qdisc noop state DOWN group default
4: vxlan.calico: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 8900 qdisc noqueue state UNKNOWN group default
7: nodelocaldns: <BROADCAST,NOARP> mtu 8950 qdisc noop state DOWN group default
Linux cloud-hands-on-5-workers-1 5.15.0-73-generic #80-Ubuntu SMP Mon May 15 15:18:26 UTC 2023 x86_64 x86_64 x86_64 GNU/Linux
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
2: ens3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 8950 qdisc fq_codel state UP group default qlen 1000
3: kube-ipvs0: <BROADCAST,NOARP> mtu 8950 qdisc noop state DOWN group default
4: vxlan.calico: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 8900 qdisc noqueue state UNKNOWN group default
7: nodelocaldns: <BROADCAST,NOARP> mtu 8950 qdisc noop state DOWN group default
10: cali99c8b7bbade@if3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 8900 qdisc noqueue state UP group default
12: calif804f17060d@if3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 8900 qdisc noqueue state UP group default
13: cali8733a1b05e7@if3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 8900 qdisc noqueue state UP group default
14: cali6a05abb8864@if3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 8900 qdisc noqueue state UP group default
15: cali5959619d86e@if3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 8900 qdisc noqueue state UP group default
16: calid20c6930fe2@if3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 8900 qdisc noqueue state UP group default
18: calib3a760b59d4@if3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 8900 qdisc noqueue state UP group default
21: cali98f8cd52efd@if3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 8900 qdisc noqueue state UP group default
Linux cloud-hands-on-5-workers-2 5.15.0-73-generic #80-Ubuntu SMP Mon May 15 15:18:26 UTC 2023 x86_64 x86_64 x86_64 GNU/Linux
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
2: ens3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 8950 qdisc fq_codel state UP group default qlen 1000
3: kube-ipvs0: <BROADCAST,NOARP> mtu 8950 qdisc noop state DOWN group default
4: vxlan.calico: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 8900 qdisc noqueue state UNKNOWN group default
7: nodelocaldns: <BROADCAST,NOARP> mtu 8950 qdisc noop state DOWN group default
8: cali390eed781e9@if3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 8900 qdisc noqueue state UP group default
9: cali53f420f2775@if3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 8900 qdisc noqueue state UP group default
18: cali73a8dacb98e@if3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 8900 qdisc noqueue state UP group default
19: cali2e1da388cb8@if3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 8900 qdisc noqueue state UP group default
20: calie4c2b6c31e5@if3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 8900 qdisc noqueue state UP group default
```


## Q/A / FAQ / Notes

### Is there hands-on recording?
  [Yes](https://drive.google.com/file/d/1J7mNDIYuTsFbnIdJ-YHHersrr-4FX420/view?usp=drive_link)

## References
* https://fluxcd.io/flux/concepts/
* https://gitlab.ics.muni.cz/cloud/knowledgebase/-/blob/master/howtos/g2/flux-cd.md
* https://gitlab.ics.muni.cz/cloud/knowledgebase/-/blob/master/howtos/g2/how-to-deploy-g2-ostack-cloud-infrastructure.md
* https://gitlab.ics.muni.cz/cloud/knowledgebase/-/blob/master/howtos/g2/brno-coref.md

