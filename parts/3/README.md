# Cloud hands-on part 3, G1 to G2 OpenStack cloud migration

## Objectives
* [ ] A. What to know before starting a migration
* [ ] B. Identify G1 project for workload migration
  * [ ] B1. Doublecheck GPUs, ephem Vms are not used, no use of object store
* [ ] C. Create identical project in G2 OpenStack cloud using cloud-entities
* [ ] D. Create AAI resources for newly created project
* [ ] E. Execute G1 to G2 migration script via CI pipeline (migrate single VM / whole project)
* [ ] F. Validate G2 VM functionality

## Before hands-on

Get familiar with following materials
* https://docs.e-infra.cz/compute/openstack/migration-to-g2-openstack-cloud/
* https://gitlab.ics.muni.cz/cloud/g1-g2-ostack-cloud-migration/-/blob/master/README.md?ref_type=heads
* https://gitlab.ics.muni.cz/cloud/knowledgebase/-/blob/master/howtos/g1-g2-migration.md?ref_type=heads
* https://gitlab.ics.muni.cz/cloud/knowledgebase/-/blob/master/howtos/group-project-creation.md#perun-instance-g1-g2
* check whether you see following pages:
  * https://perun.e-infra.cz/organizations/3898/groups/15127/subgroups
  * https://gitlab.ics.muni.cz/cloud/hiera/-/blob/production/group/ostack/projects.yaml?ref_type=heads
  * https://gitlab.ics.muni.cz/cloud/production-cloud-entities-dump
  * https://gitlab.ics.muni.cz/cloud/g2/prod-brno-cloud-entities
  * https://horizon.brno.openstack.cloud.e-infra.cz/auth/login/?next=/

## List of projects to process:
* internal projects (we will migrate them all)
  | project                  | description                | VMs to migrate |
  | ------------------------ | -------------------------- | -------------- |
  | meta-cloud-new-openstack | #volumes: 4, #servers: 9   | pick two of below `freznicek-general-tf-demo-2024-05-23-cindertest-bastion-server`, `freznicek-general-tf-demo-2024-05-23-cindertest-server-*`, generally any |
  | meta-cloud-training      | #volumes: 153, #servers: 9 | generally any |

```
[freznicek@lenovo-t14 terraform.test 0]$ openstack server list | grep cindertest
| b5d7b5c9-a27b-4487-86be-2fd4351bbcba | freznicek-general-tf-demo-2024-05-23-cindertest-server-7       | ACTIVE  | freznicek-general-tf-demo-2024-05-23-cindertest_network=10.10.10.193                 | ubuntu-jammy-x86_64 | standard.large  |
| 1379fbf8-de28-4c5d-a05c-44d466137ca9 | freznicek-general-tf-demo-2024-05-23-cindertest-server-10      | ACTIVE  | freznicek-general-tf-demo-2024-05-23-cindertest_network=10.10.10.209                 | ubuntu-jammy-x86_64 | standard.large  |
| 1551ccf1-1c19-4228-8831-914470ec574e | freznicek-general-tf-demo-2024-05-23-cindertest-server-6       | ACTIVE  | freznicek-general-tf-demo-2024-05-23-cindertest_network=10.10.10.245                 | ubuntu-jammy-x86_64 | standard.large  |
| 5d0dba17-e8e4-4bd6-bd6d-deddf320de04 | freznicek-general-tf-demo-2024-05-23-cindertest-server-8       | ACTIVE  | freznicek-general-tf-demo-2024-05-23-cindertest_network=10.10.10.32                  | ubuntu-jammy-x86_64 | standard.large  |
| 72c4c44b-a77e-4834-92aa-80b6661c041d | freznicek-general-tf-demo-2024-05-23-cindertest-server-2       | ACTIVE  | freznicek-general-tf-demo-2024-05-23-cindertest_network=10.10.10.203                 | ubuntu-jammy-x86_64 | standard.large  |
| 98bf0deb-c120-4d47-a86a-df916ffd6685 | freznicek-general-tf-demo-2024-05-23-cindertest-server-3       | ACTIVE  | freznicek-general-tf-demo-2024-05-23-cindertest_network=10.10.10.89                  | ubuntu-jammy-x86_64 | standard.large  |
| bba8694b-d3bc-48c7-9277-345d94202faf | freznicek-general-tf-demo-2024-05-23-cindertest-server-4       | ACTIVE  | freznicek-general-tf-demo-2024-05-23-cindertest_network=10.10.10.212                 | ubuntu-jammy-x86_64 | standard.large  |
| c3b39966-e1d6-4a49-9424-af2ae6a5dabe | freznicek-general-tf-demo-2024-05-23-cindertest-server-5       | ACTIVE  | freznicek-general-tf-demo-2024-05-23-cindertest_network=10.10.10.130                 | ubuntu-jammy-x86_64 | standard.large  |
| ca5c4a27-7b93-4314-95cb-17ad318d9efa | freznicek-general-tf-demo-2024-05-23-cindertest-server-9       | ACTIVE  | freznicek-general-tf-demo-2024-05-23-cindertest_network=10.10.10.8                   | ubuntu-jammy-x86_64 | standard.large  |
| d6cbecab-50ea-444e-9166-c5bae8cf9fe7 | freznicek-general-tf-demo-2024-05-23-cindertest-server-1       | ACTIVE  | freznicek-general-tf-demo-2024-05-23-cindertest_network=10.10.10.22                  | ubuntu-jammy-x86_64 | standard.large  |
| bd0cf058-852e-4281-bcd3-f8f0ce0506df | freznicek-general-tf-demo-2024-05-23-cindertest-bastion-server | ACTIVE  | freznicek-general-tf-demo-2024-05-23-cindertest_network=10.10.10.183, 195.113.167.26 | ubuntu-jammy-x86_64 | standard.small  |
```

* other projects (data set/not set, we will just create G2 projects, migration takes place later)
  | project                                   | description                | migration date | notes            |
  | ----------------------------------------- | -------------------------- | -------------- | ---------------- |
  | meta-acc2                                 | #volumes: 1, #servers: 1   | 2024-05-27     |  `picked: freznicek`                |
  | meta-aicope                               | #volumes: 3, #servers: 2   | N/A            | ephem+gpu flavor, `picked: rpolasek` |
  | meta-alpha-charges                        | #volumes: 3, #servers: 1   | 2024-05-27     | `picked: jjezek`                 |
  | meta-amtdb                                | #volumes: 6, #servers: 3   | 2024-05-27     | `picked: jsmrcka`                 |
  | meta-analysis-for-nitrogen-kinetics       | #volumes: 3, #servers: 1   | 2024-05-27     | `picked: jkrystof`                 |
  | meta-archeogeofyzika                      | #volumes: 1, #servers: 1   | 2024-05-27     | `picked: jnemec`                 |
  | meta-automatic-summarization              | #volumes: 5, #servers: 1   | 2024-05-27     | `picked: kmoravcova`                 |
  | meta-bapm                                 | #volumes: 10, #servers: 10 | 2024-05-28     |                  |
  | meta-binding-of-pyrazinamide-derivativees | #volumes: 2, #servers: 1   | 2024-05-29     |                  |
  | meta-binocular-rivalry                    | #volumes: 4, #servers: 1   | N/A            | ephem+gpu flavor |
  | meta-c-scale                              | #volumes: 3, #servers: 2   | 2024-05-29     |                  |

## Steps


### A. What to know before starting a migration

Following [answers we typically need to find](https://gitlab.ics.muni.cz/cloud/g1-g2-ostack-cloud-migration/-/blob/master/README.md?ref_type=heads#migration-checklist).

### B. Identify G1 project for workload migration

Look into G1 [hiera/group/ostack/projects.yaml](https://gitlab.ics.muni.cz/cloud/hiera/-/blob/production/group/ostack/projects.yaml?ref_type=heads) to find the project and quotas.


#### B1. Doublecheck GPUs, ephem Vms are not used, no use of object store

Search the production dump for servers in the project following way:
1. identify project id ([example `meta-cloud-new-openstack`](https://gitlab.ics.muni.cz/search?group_id=232&nav_source=navbar&project_id=3988&repository_ref=master&scope=blobs&search=meta-cloud-new-openstack+filename%3A*-project.json&search_code=true)) by finding id in single found `*-project.json`.
1. identify all servers under the project by seeking the project id. ([example](https://gitlab.ics.muni.cz/search?group_id=232&nav_source=navbar&project_id=3988&repository_ref=master&scope=blobs&search=08d3355ea8aa4ab681cce7cb2254e04b+filename%3A*-server.json&search_code=true))

### C. Create identical project in G2 OpenStack cloud using cloud-entities

Create MR in `prod-brno-cloud-entities` similar to [this one for `meta-acc2`](https://gitlab.ics.muni.cz/cloud/g2/prod-brno-cloud-entities/-/merge_requests/27).

Source data can be found in [hiera/group/ostack/projects.yaml](https://gitlab.ics.muni.cz/cloud/hiera/-/blob/production/group/ostack/projects.yaml?ref_type=heads#L3077-3093)

The MR should contain:
 * project details
 * project quotas
 * keystone mapping for the project

### D. Create AAI resources for newly created project

Read notes and create [AAI resources for your project](https://gitlab.ics.muni.cz/cloud/knowledgebase/-/blob/master/howtos/group-project-creation.md#perun-instance-g1-g2)

### E. Execute G1 to G2 migration script via CI pipeline (migrate two VMs)

WARNING! We are going to migrate VMs from `meta-cloud-new-openstack` (eventually `meta-cloud-training`) not the others.

Go to https://gitlab.ics.muni.cz/cloud/g1-g2-ostack-cloud-migration#how-to-launch-g1-to-g2-migration and follow the steps. Pick two VMs `freznicek-general-tf-demo-2024-05-23-cindertest-bastion-server`, `freznicek-general-tf-demo-2024-05-23-cindertest-server-*`.

### F. Validate G2 migrated VM functionality

Try to test ssh port is open (`ncat -z <FIP> 22`, ssh via bastion).
Log in if you have ssh key there.
Test `apt update && apt upgrade` works there (bastion and your node).

## Q/A / FAQ / Notes

### Is there hands-on recording?
  [Yes](https://drive.google.com/file/d/1F7rpE0-6u08fhbc6VeckSvCsF0Bj36Mn/view?usp=drive_link)

