import sys
import requests
import logging
import traceback
from concurrent.futures import ThreadPoolExecutor

URL=sys.argv[1]
NR_REQUESTS=int(sys.argv[2])
MAX_WORKERS=int(sys.argv[3])
LOG_FILE=sys.argv[4]

logging.basicConfig(
    filename=LOG_FILE,   # Log to a file
    level=logging.DEBUG,            # Log info and above
    format='%(asctime)s - %(levelname)s - %(message)s'
)

# Function to make HTTP requests
def make_request(url):
    try:
        response = requests.get(url)
        logging.info(f"Request to {url} succeeded with status code: {response.status_code}")

        if response.status_code != 200:
            logging.debug(f"Response content: {response.text[:200]}")
            print(f"Failed request: {url}, Status Code: {response.status_code}")
    except requests.exceptions.RequestException as e:
        logging.error(f"Request to {url} failed with error: {e}")
        logging.error(f"Traceback while requesting {url}: {traceback.format_exc()}")

# List of URLs to test
urls = []
for indx in range(NR_REQUESTS):
    urls.append("%s?id=%d" % (URL, indx))

# Use ThreadPoolExecutor for multi-threading
with ThreadPoolExecutor(max_workers=MAX_WORKERS) as executor:
    executor.map(make_request, urls)
