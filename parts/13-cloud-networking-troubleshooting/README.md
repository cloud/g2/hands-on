# G2 Beskar-cloud networking troubleshooting example

## Objectives

* Cloud connection resets
  * Symptoms
  * Analysis
  * Hypothetical fix
  * Prove fix effectivity (QA workflow)
* Lesson learned


## Problem

From time to time connection to prod-brno cloud gets actively rejected (sent back RST packet).

### Symptoms

This causes several problems:

1. prod-brno-cloud-entities-dump CI/CD pipeline starts to [fail](https://gitlab.ics.muni.cz/cloud/g2/prod-brno-cloud-entities-dump/-/jobs/2038921#L237).

```
https://gitlab.ics.muni.cz/cloud/g2/prod-brno-cloud-entities-dump/-/jobs/2038921#L237

  [2025-02-26T09:20:30] INFO: Dump port entities (running 15m22s)
  compute version 2.79 is not in supported versions: 2, 2.1
  Unexpected exception for https://network.brno.openstack.cloud.e-infra.cz/v2.0/ports: ("Connection broken: ConnectionResetError(104, 'Connection reset by peer')", ConnectionResetError(104, 'Connection reset by peer'))
```

2. prod-brno-cloud-entities CI/CD pipeline starts to [fail](https://gitlab.ics.muni.cz/cloud/g2/prod-brno-cloud-entities/-/jobs/2038875#L1367) and [fail](https://gitlab.ics.muni.cz/cloud/g2/prod-brno-cloud-entities/-/jobs/2038904#L1540).

```
https://gitlab.ics.muni.cz/cloud/g2/prod-brno-cloud-entities/-/jobs/2038875#L1367
https://gitlab.ics.muni.cz/cloud/g2/prod-brno-cloud-entities/-/jobs/2038904#L1540

  ('Connection broken: IncompleteRead(8663 bytes read, 6810 more expected)', IncompleteRead(8663 bytes read, 6810 more expected))
```

3. end-to-end tests start to [fail as well](https://gitlab.ics.muni.cz/cloud/g2/e2e-tests-brno/-/jobs/2034362).

```
https://gitlab.ics.muni.cz/cloud/g2/e2e-tests-brno/-/jobs/2034362
│ Error: Error creating openstack_blockstorage_volume_v3: Post "https://volumev3.brno.openstack.cloud.e-infra.cz/v3/421f7075cc3047ef9cf96fdeab874e87/volumes": OpenStack connection error, retries exhausted. Aborting. Last error was: read tcp 172.17.0.4:45592->147.251.91.97:443: read: connection reset by peer
```

4. EGI ARGO probes also sees sporadic troubles, reported back to us by Enol. [Seen in ARGO monitoring](https://argo.egi.eu/egi/report-status/Critical/SITES/metrics/compute.brno.openstack.cloud.e-infra.cz/egi.cloud.openstack-vm/2025-02-27T10:08:28Z/CRITICAL/org.openstack.nova).

```
Hi František

I am having these errors from time to time, which prevent the cloud-info-provider to run properly:

keystoneauth1.exceptions.connection.ConnectFailure: Unable to establish connection to https://identity.brno.openstack.cloud.e-infra.cz/v3/auth/tokens: ('Connection aborted.', ConnectionResetError(104, 'Connection reset by peer'))
exit status 1

and in another attempt:

keystoneauth1.exceptions.connection.ConnectFailure: Unable to establish connection to https://compute.brno.openstack.cloud.e-infra.cz/v2.1/1561ad2c9e974ae9840d413b4bdea699/flavors/8b19d2a5-002d-41c3-a1aa-969aa4545108/os-extra_specs: ('Connection aborted.', ConnectionResetError(104, 'Connection reset by peer'))

Is there any rate-limiting or similar that could be causing this? I guess I should have some retrial in place to avoid this situation

Cheers
Enol
```

## Analysis & fix

### 0. Understand more in detaiol the networking error `Connection reset`.

When server does not want to talk to client or vise versa reset of the TCP/IP connection is issues via `RST packet`. Case [B]:

![](./images/Connection-attempts-a-successful-TCP-Connection-b-TCP-destination-port-closed-c.png)

Complete flow looks following way:

![](https://idea.popcount.org/2019-09-20-when-tcp-sockets-refuse-to-die/Tcp_state_diagram_fixed_new.svga.png)


### 1. Always draw the picture of infrastructure so you uncover where (at how many places) it may fail.

![](./images/prod-brno-cloud-connection-resets.jpg)

Where are places to doublecheck:
* kubernetes application (pods)
* edge-proxy (pods)
* kube-vip (pods)
* traffic on controlplane nodes


### 2. Reproduce problem reliably as external cloud user

Tool of first choice is Apache `ab`.
Unfortunatelly `ab` does not come with proper timestamping in debugging mode `ab -n 1000 -c 50 -v 4 ...`. Also from the log was not clear where in the handshake problem occured.

User crafted reproducer was needed. Here ChatGPT helps well `Is there a python http multithreaded client that could be used as ab or wrk?` and then `Using code with requests and concurrent.futures, how to add there the logging of the requests?`.

Ended with tool [ab.py](./ab.py) which was enriched with:
* proper traceback dumping
* proper logging
* sweeping URL to be able to track each request separately

To have near 100% reproducer I tweaked number of requests and parallelization so it does not hurt whole cloud.

### 3. Locate part of infrastructure where it is likely broken (directly / indirectly)

We saw problems in openstack part. Related I saw also prometheus misbehaving but I was not sure it is the same problem.

Thus I picked robust app within infrastructure where I perform tests: `welcome-hub`.

I proved that I can see problem there as well. Problem is not relevant to OpenStack at all.

Looking at the architecture diagram we need to prepare experiment.

### 4. Prepare experiment store all needed artifacts (logs)

Experiment should answer following questions:
1. Are failed requests seen by client, seen on tearget application (welcome-hub)?
2. Are failed requests seen by edge-proxy? NO they are not
3. Are failed requests seen at controlplanes?
4. Find best place to run the experiment. (fare away from our infra)

In the experiment we need to capture traffic on controlplanes, get logs of edge-proxy and welcome-hub.

### 5. Analyze the logs, prove you triggered the problem

#### Capturing the artifacts

```
Logging A - tcpdump on cp 001-005:
tcpdump -i any -nn host 195.113.243.68 &> /tmp/run4-prod-brno-4K-10.log

Logging B - welcome-hub endpoint application:
kubectl --context pb -n welcome-hub logs -f --tail 10 -l app=welcome-hub > welcome-hub.log

Logging C - welcome-hub endpoint application:
kubectl --context pb -n welcome-hub logs -f --tail 10 -l app=welcome-hub > welcome-hub.log

Logging D - edge-proxy:
kubectl --context pb -n edge-proxy logs -f --tail -1 -l app=edge-proxy -c logouter > edge-proxy-logouter.log
kubectl --context pb -n edge-proxy logs -f --tail 10  -l app=edge-proxy -c traefik > edge-proxy-traefik.log
```

#### Trigger the issue

```bash
# on 195.113.243.68
ubuntu@freznicek-general-tf-demo-bastion-server:~$ python3 ab.py https://brno.openstack.cloud.e-infra.cz/ 5000 10 run5-prod-brno-5K-10.log
```

Cloud traffic enters not just kube-vip leader but also the other controlplane node not being leader.

There were cloud IP addresses assinged to more controlplane nodes.

Doublechecked by kube-vip-analyzer.sh [kube-vip-analyzer-dump-before-fix.log](kube-vip-analyzer-dump-before-fix.log)


### 6. Propose manual fix

Remove cloud IP address from non-leader controlplane node.

```
[root@controlplane-002:prod-brno~0]# ip addr del 147.251.91.97/32 dev br-public
[root@controlplane-002:prod-brno~0]#
```

### 7. Extensively try to reproduce problem again & evaluate fix effectivity / completeness

1. Perform the same `ab.py` test multiple times. Rule of thumb is to stress infra at least 5 time more and not see the problem.
2. Enable all CI/CD pipelines to prove it is fixed, multiple executions are passing.


## Corrective actions

### Monitor this situation, that cloud ip address has only one controlplane node

https://gitlab.ics.muni.cz/cloud/internal-wiki/-/issues/2063

### kube-vip operator needed to make sure this does not happen again (avoid situation)

### kube-vip-analyzer tool should warn about the situation.

https://gitlab.ics.muni.cz/cloud/internal-wiki/-/issues/2062

### Report issue upstream.

https://github.com/kube-vip/kube-vip/issues/996#issuecomment-2710546792

### Upgrade to latest kube-vip

There is hypothesis kube-vip issue [#996](https://github.com/kube-vip/kube-vip/issues/996) is fixed in last kube-vip v0.8.9 which needs to be tested.

https://gitlab.ics.muni.cz/cloud/internal-wiki/-/issues/2064

### Investigate DoS behavior of the prod-brno

https://gitlab.ics.muni.cz/cloud/internal-wiki/-/issues/2065

## References
* https://gitlab.ics.muni.cz/cloud/internal-wiki/-/issues/2037
* https://gitlab.ics.muni.cz/cloud/internal-wiki/-/issues/2041
* https://www.pico.net/kb/what-is-a-tcp-connection-refused/
* https://idea.popcount.org/2019-09-20-when-tcp-sockets-refuse-to-die/
* https://github.com/kube-vip/kube-vip/issues/996#issuecomment-2710546792
* https://gitlab.ics.muni.cz/cloud/internal-wiki/-/issues/2062
* https://gitlab.ics.muni.cz/cloud/internal-wiki/-/issues/2063
* https://gitlab.ics.muni.cz/cloud/internal-wiki/-/issues/2064
* https://gitlab.ics.muni.cz/cloud/internal-wiki/-/issues/2065