# G2 Beskar-cloud kubernetes networking how-to & troubleshooting

## Objectives

### part A
* [ ] Understand kubernetes networking with Calico
* [ ] How to troubleshoot kubernetes networking
* [ ] Q&A


## Kubernetes networking overview

There are four networks:
 * hypervisor data network
 * Calico VxLAN network overlay (on data network)
 * Kubernetes service cluster network
 * Kubernetes pod cluster network

![](./images/k8s-network-architecture.png)


![](https://miro.medium.com/v2/resize:fit:1100/format:webp/1*oyGbXt7kStLd85ZT4it3oQ.png)

### container-to-container communication

![](https://miro.medium.com/v2/resize:fit:1100/format:webp/1*-ze224LkGbwRbgIC-7w5dg.jpeg)

https://kubernetes.io/docs/tasks/access-application-cluster/communicate-containers-same-pod-shared-volume/

### pod-to-pod communication

![](https://miro.medium.com/v2/resize:fit:1100/format:webp/1*-ze224LkGbwRbgIC-7w5dg.jpeg)


### pod-to-service communication

![](https://miro.medium.com/v2/resize:fit:1100/format:webp/1*B0AmH3WpQ0GYSRPw0NMK-g.jpeg)

## Understand kubernetes networking with Calico

Understanding Kubernetes networking with **Calico** requires familiarity with Kubernetes networking principles and how Calico enhances networking functionality. Here's a summarized guide:


### **Kubernetes Networking Basics**
1. **Pod Networking**:
   - Every Pod gets an IP address and can communicate with other Pods across nodes without NAT using `localhost`.
   - Highly-coupled **container-to-container** communications: this is solved by Pods and `localhost` communications.

2. **Cluster Networking**:
   - Kubernetes networking involves three main components:
     - **Pod-to-Pod** communication.
     - **Pod-to-Service** communication (using ClusterIP, NodePort, LoadBalancer).
     - **External-to-Service** communication (ingress/egress traffic).

3. **Network Policies**:
   - Define how Pods communicate with each other or with external resources.
   - Used to secure and manage traffic flows.

---

### **What is CNI**

Kubernetes CNI (Container Network Interface) is a framework for configuring networking in Kubernetes clusters. It enables the networking capabilities that allow containers to communicate with each other and with external networks. Here’s an overview:

### **What is CNI?**
CNI stands for **Container Network Interface**, a specification and set of libraries created by the [Cloud Native Computing Foundation (CNCF)](https://cncf.io/). CNI defines a standard way for networking plugins to configure network interfaces in container runtimes.

#### **Role of CNI in Kubernetes**
In Kubernetes, the CNI plugin is responsible for managing pod networking, including:
1. **Assigning IP addresses** to pods.
2. **Setting up the network interfaces** inside pods.
3. **Maintaining network connectivity** between pods, nodes, and external systems.
4. **Enforcing network policies** (e.g., who can communicate with whom).

The Kubernetes **Container Network Interface (CNI)** plugin integrates with the CNI specification to achieve these functionalities.

#### **How Kubernetes Uses CNI**
1. **Network Configuration**: When a pod is created, Kubernetes invokes the CNI plugin to set up networking for the pod.
2. **Plugins**: Kubernetes uses CNI plugins (like Calico, Flannel, Weave Net, etc.) to provide networking solutions. Each plugin may implement different networking models (e.g., overlay, underlay, or hybrid).
3. **Extendable**: Kubernetes supports custom CNI plugins, allowing administrators to tailor network behavior for specific use cases.

#### **CNI Plugins for Kubernetes**
Some popular CNI plugins include:
- **Calico**: Provides network policy enforcement and can operate as an overlay or routing-based network.
- **Flannel**: Simple overlay network solution.
- **Weave Net**: Provides an overlay network with multicast support.
- **Cilium**: Focuses on security and observability using eBPF (Extended Berkeley Packet Filter).
- **Multus**: Enables attaching multiple network interfaces to pods.

#### **CNI Configuration**
CNI configurations in Kubernetes are stored as files under `/etc/cni/net.d/`. These configuration files describe how the networking plugin should be set up and used by Kubernetes.

#### **Why is CNI Important?**
- **Standardization**: Provides a standard way to handle container networking.
- **Flexibility**: Offers multiple networking models to meet diverse requirements.
- **Integration**: Simplifies integration with container runtimes like Docker, CRI-O, and containerd.


### **What is Calico?**
**Calico** is an open-source networking and network security solution designed for Kubernetes and other platforms. It provides:
- **Networking**: Implements networking for Kubernetes Pods using native Linux capabilities (e.g., __iptables__, eBPF).
- **Network Policies**: Advanced enforcement for traffic between Pods, namespaces, and external endpoints.
- **IP Address Management (IPAM)**: Dynamically assigns and manages IP addresses for Pods.
- **High Performance**: Supports scalable networking with minimal overhead.

#### **Key Concepts of Calico Networking**
1. **Data Plane Options**:
   - **Linux Kernel** (iptables): Default mechanism for routing and firewall rules.
   - **eBPF**: High-performance alternative with advanced features (e.g., NAT-free connectivity, direct Packet Filtering).

2. **IP Pool Management**:
   - Defines ranges of IP addresses for Pod networking.
   - Supports **VLANs** and **subnet splitting**.

3. **Encapsulation Modes**:
   - **IP-in-IP**: Encapsulates traffic for cross-node communication.
   - **VXLAN**: Encapsulation for better compatibility with cloud environments.
   - **No Encapsulation**: Direct routing if nodes share the same Layer 2 network.

4. **Network Policies**:
   - Uses **Kubernetes-native policies** and **Calico-specific policies** for granular traffic control.
   - Policies are written in YAML and define rules based on labels, protocols, ports, etc.


## How to troubleshoot kubernetes networking

1. Visualize cluster state ([`./kbadpods.sh --context prod-brno`](./kbadpods.sh))
1. Understand whether where misbehavior lays:
   * pod-to-pod communication
   * pod-to-service communication
   * external-world-to-service communication -> kube-vip, edge-proxy
1. Investigate whether there are specific k8s networkpolicies in place `kubectl get networkpolicy --all-namespaces`
1. If kubernetes service is part of the problem:
   * Doublecheck service name and port
     * access via IP address or `<service-name>.<namespace>:<port>`
   * Doublecheck service has proper endpoints.
     * `kubectl -n openstack get service` vs `kubectl -n openstack get endpoints`
   * if there is no service endpoint then you need to investigate readiness probes `kubectl describe po [-l <labels>]`
1. Doublecheck whether misbehavior is caused by DNS system. Try to use IP addresses instead of DNS records.
   * If behavior disappeard when direct IP addresses used it is likely DNS problem, use `nslookup <service-name>` or simply `ncat -zv <service-name> <port>`
1. Doublecheck misbehavior is present on single node too. If present in communication between nodes only then it is likely Calico overlay which needs to be investigated.
   * Use `default/ds/distributed-debugger` to investigate whether problem persists on single k8s worker node or it is node-to-node traffic problem
1. Doublecheck Calico components are healthy `kubectl --context pb -n kube-system get ds,deploy | grep calico`
   * In case of troubles, inspect logs and eventually restart them
1. Inspect traffic with tcpdump on hypervisor in pod networking sandbox (networking namespace)
   * Find out networking namespace PID
   * Execute tcpdump in network namespace
   * tcpdump query to client and server and if not enough also on all ther components (example: `tcpdump -nn -i any -s0 -A 'port (8776 or 3306 or 5000 or 5672 or 9292 or 11211) and  host (10.233.79.193 or localhost)'`)

```console
# inspect node where pod runs:
[freznicek@lenovo-t14 ~ 1]$ kubectl --context pb -n openstack get po -owide cinder-api-747867b977-hc2fl
NAME                          READY   STATUS    RESTARTS   AGE     IP              NODE               NOMINATED NODE   READINESS GATES
cinder-api-747867b977-hc2fl   1/1     Running   0          6m58s   10.233.79.216   controlplane-003   <none>           <none>

# on node, finding container
[root@controlplane-003:prod-brno~0]# nerdctl ps | grep cinder-api
62cdb75cc0a7    registry.gitlab.ics.muni.cz:443/cloud/g2/custom-images/cinder:yoga-ubuntu_focal-20240127-CVE-2024-32498-patch-1    "/tmp/cinder-api.sh …"    32 minutes ago    Up                 k8s://openstack/cinder-api-747867b977-hc2fl/cinder-api
f9e4996f047c    registry.k8s.io/pause:3.9                                                                                          "/pause"                  33 minutes ago    Up                 k8s://openstack/cinder-api-747867b977-hc2fl

# find out network namespace pid
[root@controlplane-003:prod-brno~0]# nerdctl inspect 62cdb75cc0a7 | grep -i pid
            "Pid": 2253845,

# display pod IP address, should be matching IP above
[root@controlplane-003:prod-brno~0]# nsenter -t 2253845 -n ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
3: eth0@if3662: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 8950 qdisc noqueue state UP group default
    link/ether be:07:1a:15:4a:b9 brd ff:ff:ff:ff:ff:ff link-netnsid 0
    inet 10.233.79.216/32 scope global eth0
       valid_lft forever preferred_lft forever
    inet6 fe80::bc07:1aff:fe15:4ab9/64 scope link
       valid_lft forever preferred_lft forever

# execute tcpdump inside the network namespace
[root@controlplane-003:prod-brno~0]# nsenter -t 2253845 -n tcpdump -nn -i any -A -s 0 'port 8776 and host localhost'
tcpdump: data link type LINUX_SLL2
tcpdump: verbose output suppressed, use -v[v]... for full protocol decode
listening on any, link-type LINUX_SLL2 (Linux cooked v2), snapshot length 262144 bytes
19:24:11.881712 lo    In  IP 127.0.0.1.39312 > 127.0.0.1.8776: Flags [S], seq 3734780065, win 65495, options [mss 65495,sackOK,TS val 2011949814 ecr 0,nop,wscale 7], length 0
E..<|.@.@............."H..8..........0.........
w...........
19:24:11.881721 lo    In  IP 127.0.0.1.8776 > 127.0.0.1.39312: Flags [S.], seq 122449909, ack 3734780066, win 65483, options [mss 65495,sackOK,TS val 2011949814 ecr 2011949814,nop,wscale 7], length 0
E..<..@.@.<........."H...Lo...8......0.........
w...w.......
19:24:11.881730 lo    In  IP 127.0.0.1.39312 > 127.0.0.1.8776: Flags [.], ack 1, win 512, options [nop,nop,TS val 2011949814 ecr 2011949814], length 0
E..4|.@.@............."H..8..Lo......(.....
w...w...
19:24:11.881762 lo    In  IP 127.0.0.1.39312 > 127.0.0.1.8776: Flags [P.], seq 1:503, ack 1, win 512, options [nop,nop,TS val 2011949814 ecr 2011949814], length 502
E..*|.@.@............."H..8..Lo............
w...w...GET /v3/3bb037c55b2c41b6a2f1c9ea921e07b8/types HTTP/1.1
Host: localhost:8776
User-Agent: python-requests/2.27.1
Accept-Encoding: gzip, deflate
Accept: */*
Connection: keep-alive
X-Auth-Token: gAAAAABnP4O-MseyGC4xpwqFQyDf2lRGEGDP5GpRJ-djMH7UdP9xe01Z_hd75pIpclvVQV2BiAwe1YnpFHeonJpogY35OcV6SxCdx7inziTkO2aYdf-lRrtoBhvGSekLhwwrryjlJ3FL_a1VTxQvqvrlVaz9p6u1bt9uKAIitSME8OTyyDKSv7gcP1Ih1Tkr4izlJHJrHOEJITH95C1UPaIPIdqtY-kFTYP4Lh6v2jtdSDoE_U5dAcD2Cxaq6alFi6M0zAMOnyw7
Content-Type: application/json
...
```
9. To workaround incomming traffic from other components you may duplicate particular service into another workload (deploy/ds/sts) with different labels and disabled k8s probes. That way you will not see both normal incomming traffic (from service) and probes. This environment is then silent enough to see short tcpdump.
   * How to clone k8s deployment?
```diff
[freznicek@lenovo-t14 tmp 0]$ diff -u prod-ostrava-cinder-api-deploy.yaml prod-ostrava-cinder-api3-deploy.yaml
--- prod-ostrava-cinder-api-deploy.yaml 2024-11-25 14:37:52.448220712 +0100
+++ prod-ostrava-cinder-api3-deploy.yaml        2024-11-25 14:39:45.865895269 +0100
@@ -1,24 +1,12 @@
 apiVersion: apps/v1
 kind: Deployment
 metadata:
-  annotations:
-    deployment.kubernetes.io/revision: "4"
-    meta.helm.sh/release-name: openstack-cinder
-    meta.helm.sh/release-namespace: openstack
-    openstackhelm.openstack.org/release_uuid: ""
-  creationTimestamp: "2024-08-27T08:18:57Z"
-  generation: 4
   labels:
-    app.kubernetes.io/managed-by: Helm
     application: cinder
-    component: api
-    helm.toolkit.fluxcd.io/name: cinder
-    helm.toolkit.fluxcd.io/namespace: openstack
+    component: api3
     release_group: openstack-cinder
-  name: cinder-api
+  name: cinder-api3
   namespace: openstack
-  resourceVersion: "592231014"
-  uid: b4f26055-0a4b-45c7-bb00-8ce8d52227e9
 spec:
   progressDeadlineSeconds: 600
   replicas: 1
@@ -26,7 +14,7 @@
   selector:
     matchLabels:
       application: cinder
-      component: api
+      component: api3
       release_group: openstack-cinder
   strategy:
     rollingUpdate:
@@ -43,7 +31,7 @@
       creationTimestamp: null
       labels:
         application: cinder
-        component: api
+        component: api3
         release_group: openstack-cinder
     spec:
       affinity:
@@ -78,30 +66,11 @@
               command:
               - /tmp/cinder-api.sh
               - stop
-        livenessProbe:
-          failureThreshold: 3
-          httpGet:
-            path: /
-            port: 8776
-            scheme: HTTP
-          initialDelaySeconds: 30
-          periodSeconds: 10
-          successThreshold: 1
-          timeoutSeconds: 1
-        name: cinder-api
+        name: cinder-api3
         ports:
         - containerPort: 8776
           name: c-api
           protocol: TCP
-        readinessProbe:
-          failureThreshold: 3
-          httpGet:
-            path: /
-            port: 8776
-            scheme: HTTP
-          periodSeconds: 10
-          successThreshold: 1
-          timeoutSeconds: 1
         resources: {}
         securityContext:
           allowPrivilegeEscalation: false
@@ -223,22 +192,4 @@
           secretName: cinder-etc
       - emptyDir: {}
         name: cinder-coordination
-status:
-  availableReplicas: 1
-  conditions:
-  - lastTransitionTime: "2024-08-27T08:18:57Z"
-    lastUpdateTime: "2024-08-27T08:18:57Z"
-    message: Deployment has minimum availability.
-    reason: MinimumReplicasAvailable
-    status: "True"
-    type: Available
-  - lastTransitionTime: "2024-08-27T08:18:57Z"
-    lastUpdateTime: "2024-10-21T11:28:25Z"
-    message: ReplicaSet "cinder-api-649b659ffb" has successfully progressed.
-    reason: NewReplicaSetAvailable
-    status: "True"
-    type: Progressing
-  observedGeneration: 4
-  readyReplicas: 1
-  replicas: 1
-  updatedReplicas: 1
+
```

## References
* https://spacelift.io/blog/kubernetes-networking
* https://opensource.com/article/22/6/kubernetes-networking-fundamentals
* https://www.tigera.io/learn/guides/kubernetes-networking/kubernetes-cni/
* https://spacelift.io/blog/kubernetes-networking
* https://medium.com/google-cloud/understanding-kubernetes-networking-pods-7117dd28727
* https://www.getambassador.io/blog/kubernetes-networking-guide-top-engineers
* https://www.vmware.com/topics/kubernetes-networking
* https://learnk8s.io/kubernetes-services-and-load-balancing
* https://kubernetes.io/docs/concepts/cluster-administration/networking/
* https://kubernetes.io/docs/concepts/services-networking/
* https://cloudnativenow.com/topics/cloudnativenetworking/understanding-kubernetes-networking-architecture/


## Q&A

### How to perform network connection from inside the pod

Individual pod may not come with tools needed therefore there are several ways, here trying to make connection to cinder-api:

#### Kubernetes environment
```console
[freznicek@lenovo-t14 ~ 0]$ kubectl --context pb -n openstack get svc cinder-api -owide
NAME         TYPE       CLUSTER-IP      EXTERNAL-IP   PORT(S)          AGE    SELECTOR
cinder-api   NodePort   10.233.23.239   <none>        8776:30877/TCP   261d   application=cinder,component=api,release_group=openstack-cinder
[freznicek@lenovo-t14 ~ 0]$ kubectl --context pb -n openstack get endpoints cinder-api -owide
NAME         ENDPOINTS            AGE
cinder-api   10.233.79.247:8776   261d
```

```console
[freznicek@lenovo-t14 ~ 0]$ kubectl --context pb exec -it distributed-debugger-pc922 bash
# use ncat/nc
root@distributed-debugger-pc922:/# ncat -zv cinder-api.openstack 8776
Ncat: Version 7.80 ( https://nmap.org/ncat )
Ncat: Connected to 10.233.23.239:8776.
Ncat: 0 bytes sent, 0 bytes received in 0.08 seconds.
root@distributed-debugger-pc922:/# nc -zv cinder-api.openstack 8776
Ncat: Version 7.80 ( https://nmap.org/ncat )
Ncat: Connected to 10.233.23.239:8776.
Ncat: 0 bytes sent, 0 bytes received in 0.08 seconds.

# use python oneliner
root@distributed-debugger-pc922:/# python3 -c 'import socket,sys;s=socket.create_connection((sys.argv[1],int(sys.argv[2])),1);print(s)' cinder-api.openstack 8776; echo $?
<socket.socket fd=3, family=AddressFamily.AF_INET, type=SocketKind.SOCK_STREAM, proto=6, laddr=('10.233.105.10', 54034), raddr=('10.233.23.239', 8776)>
0

# use bash connection
root@distributed-debugger-pc922:/# </dev/tcp/cinder-api.openstack/8776; echo $?
0
```

### G2 kubernetes configuration we use

* [definition of nodes as yaml ansible inventory](https://gitlab.ics.muni.cz/cloud/g2/kubernetes-deployments/-/blob/master/deployments/prod-brno/configuration.v1.24.17/kubespray/inventory/deployment/hosts.yaml?ref_type=heads), note kube_node list contains also controlplane nodes. (Kubernetes controlplane runs kubernetes controlplane + OpenStack controlplane + monitoring)
* [kubernetes version: v1.24.17](https://gitlab.ics.muni.cz/cloud/g2/kubernetes-deployments/-/blob/master/deployments/prod-brno/configuration.v1.24.17/kubespray/inventory/deployment/group_vars/k8s_cluster/k8s-cluster.yml#L20)
* [pods IP subnet range: 10.233.64.0/18](https://gitlab.ics.muni.cz/cloud/g2/kubernetes-deployments/-/blob/master/deployments/prod-brno/configuration.v1.24.17/kubespray/inventory/deployment/group_vars/k8s_cluster/k8s-cluster.yml#L81)
* [services IP subnet range: 10.233.0.0/18](https://gitlab.ics.muni.cz/cloud/g2/kubernetes-deployments/-/blob/master/deployments/prod-brno/configuration.v1.24.17/kubespray/inventory/deployment/group_vars/k8s_cluster/k8s-cluster.yml#L76)
* [`kube-proxy` mode: IPVS](https://gitlab.ics.muni.cz/cloud/g2/kubernetes-deployments/-/blob/master/deployments/prod-brno/configuration.v1.24.17/kubespray/inventory/deployment/group_vars/k8s_cluster/k8s-cluster.yml#L125)
* calico
  * [iptables NFT backend](https://gitlab.ics.muni.cz/cloud/g2/kubernetes-deployments/-/blob/master/deployments/prod-brno/configuration.v1.24.17/kubespray/inventory/deployment/group_vars/k8s_cluster/k8s-net-calico.yml#L72)
  * [Encapsulation mode: VxLAN](https://gitlab.ics.muni.cz/cloud/g2/kubernetes-deployments/-/blob/master/deployments/prod-brno/configuration.v1.24.17/kubespray/inventory/deployment/group_vars/k8s_cluster/k8s-net-calico.yml#L89)
  * [VxLAN mode: CrossSubnet](https://gitlab.ics.muni.cz/cloud/g2/kubernetes-deployments/-/blob/master/deployments/prod-brno/configuration.v1.24.17/kubespray/inventory/deployment/group_vars/k8s_cluster/k8s-net-calico.yml#L98)
  * [Non-standard VxLAN port 4788](https://gitlab.ics.muni.cz/cloud/g2/kubernetes-deployments/-/blob/master/deployments/prod-brno/configuration.v1.24.17/kubespray/inventory/deployment/group_vars/k8s_cluster/k8s-net-calico.yml#L103) to avoid collisions with OpenStack's OVS.

### G2 controlplane kubernetes elements

#### network elements

```console
[ubuntu@controlplane-004:prod-brno~0]$ ip a | grep -E 'ipvs|cali'
261: cali0c09d186d47@if3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 8950 qdisc noqueue state UP group default
17: kube-ipvs0: <BROADCAST,NOARP> mtu 1500 qdisc noop state DOWN group default
    inet 10.233.7.92/32 scope global kube-ipvs0
    ...
    inet 10.233.25.210/32 scope global kube-ipvs0
    inet 10.233.20.182/32 scope global kube-ipvs0
19: cali9afd5bf4c4e@if3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 8950 qdisc noqueue state UP group default
20: cali631e35b94e2@if3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 8950 qdisc noqueue state UP group default
21: vxlan.calico: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 8950 qdisc noqueue state UNKNOWN group default
    inet 10.233.104.0/32 scope global vxlan.calico
190: cali28461933a6b@if3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 8950 qdisc noqueue state UP group default
...
243: cali9859101f42a@if3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 8950 qdisc noqueue state UP group default
```
##### `kube-ipvs0`

`kube-ipvs0` is a virtual network interface created by **kube-proxy** when it operates in **IPVS (IP Virtual Server)** mode. It plays a central role in Kubernetes networking by handling the routing of traffic to services within the cluster.

###### **Role of kube-ipvs0**
1. **Service Traffic Management**:
   - `kube-ipvs0` is a virtual loopback interface used to manage VIPs (Virtual IPs) for Kubernetes Services.
   - Each Service in the cluster is assigned a ClusterIP, which is implemented as a VIP bound to `kube-ipvs0`.
   - This allows traffic targeting a Service's ClusterIP to be routed efficiently to one of the associated Pods.

2. **Integration with IPVS**:
   - In IPVS mode, `kube-proxy` uses the Linux kernel's IP Virtual Server (IPVS) to implement load balancing.
   - `kube-ipvs0` is the interface where IPVS binds the ClusterIPs for Services.
   - IPVS rules direct traffic from the ClusterIP (VIP) to backend endpoints (Pods).

3. **Efficient Load Balancing**:
   - IPVS provides kernel-level load balancing, which is faster and more scalable than iptables-based routing.
   - `kube-ipvs0` supports different load-balancing algorithms like round-robin, least connections, and more.

4. **Service Reachability**:
   - Traffic destined for a ClusterIP enters `kube-ipvs0`, and the IPVS rules decide how to route it to the appropriate backend Pod, regardless of its location in the cluster.

###### **Comparison to Other Modes**
- In **iptables mode**, service routing uses iptables rules to NAT traffic.
- In **IPVS mode**, the `kube-ipvs0` interface makes routing more efficient by leveraging kernel space and IPVS’s sophisticated scheduling mechanisms.

###### **Technical Details**
- `kube-ipvs0` is a dummy interface, meaning it doesn't send or receive traffic but holds VIPs.
- You can inspect it using commands like:
```console
[ubuntu@controlplane-004:prod-brno~0]$ ip addr show kube-ipvs0
17: kube-ipvs0: <BROADCAST,NOARP> mtu 1500 qdisc noop state DOWN group default
    link/ether 12:30:d0:f2:2c:b1 brd ff:ff:ff:ff:ff:ff
    inet 10.233.7.92/32 scope global kube-ipvs0
    inet 10.233.18.190/32 scope global kube-ipvs0
    inet 10.233.42.107/32 scope global kube-ipvs0
    inet 10.233.23.239/32 scope global kube-ipvs0
    ...
    inet 10.233.20.182/32 scope global kube-ipvs0
```
  to see the list of ClusterIPs (VIPs) bound to it.

```console
[root@controlplane-004:prod-brno~0]# ipvsadm --list
IP Virtual Server version 1.2.1 (size=4096)
Prot LocalAddress:Port Scheduler Flags
  -> RemoteAddress:Port           Forward Weight ActiveConn InActConn
TCP  egicloud.brno.openstack.clou rr
  -> 10-233-102-196.edge-proxy.ed Masq    1      0          0
  -> 10-233-104-33.edge-proxy.edg Masq    1      0          0
  -> 10-233-112-247.edge-proxy.ed Masq    1      0          0
TCP  egicloud.brno.openstack.clou rr
  -> 10-233-102-196.edge-proxy.ed Masq    1      4          1
  -> 10-233-104-33.edge-proxy.edg Masq    1      5          1
  -> 10-233-112-247.edge-proxy.ed Masq    1      6          1
TCP  controlplane-004:30092 rr
  -> 10-233-112-252.glance-api.op Masq    1      0          0
TCP  memcached.openstack.svc.clus rr persistent 10800
  -> 10-233-102-219.memcached.ope Masq    1      27         4
TCP  mariadb-server.osh-infra.svc rr
  -> 10-233-79-255.mariadb-server Masq    1      0          0
  -> mariadb-server-1.mariadb-dis Masq    1      0          0
  -> mariadb-server-2.mariadb-dis Masq    1      0          0
TCP  mariadb-server.openstack.svc rr
  -> mariadb-server-2.mariadb-dis Masq    1      0          0
  -> mariadb-server-0.mariadb-dis Masq    1      0          0
  -> mariadb-server-1.mariadb-dis Masq    1      0          0
...
TCP  controlplane-004:32278 rr
  -> 10-233-102-196.edge-proxy.ed Masq    1      0          0
  -> 10-233-104-33.edge-proxy.edg Masq    1      0          0
  -> 10-233-112-247.edge-proxy.ed Masq    1      0          0
UDP  coredns.kube-system.svc.clus rr
  -> 10-233-73-55.coredns.kube-sy Masq    1      0          0
  -> 10-233-79-254.coredns.kube-s Masq    1      0          0
  -> 10-233-89-123.coredns.kube-s Masq    1      0          0
  -> 10-233-100-63.coredns.kube-s Masq    1      0          0
  -> 10-233-102-225.coredns.kube- Masq    1      0          0
  -> 10-233-104-20.coredns.kube-s Masq    1      0          0
  -> 10-233-110-144.coredns.kube- Masq    1      0          0
  -> 10-233-110-233.coredns.kube- Masq    1      0          0
  -> 10-233-112-203.coredns.kube- Masq    1      0          0
  -> 10-233-116-181.coredns.kube- Masq    1      0          0
```
What service is corresponds to 10.233.7.92 ip address?

```console
[root@controlplane-004:prod-brno~130]# kubectl get svc -owide --all-namespaces | grep "10.233.7.92"
openstack            mariadb-server                        ClusterIP      10.233.7.92     <none>          3306/TCP                       387d    application=mariadb,component=server,release_group=openstack-mariadb
```


###### **Advantages of IPVS Mode and kube-ipvs0**
- **Performance**: Kernel-space routing is faster than user-space or iptables-based approaches.
- **Scalability**: Handles large numbers of Services and backends efficiently.
- **Advanced Scheduling**: Offers more algorithms for distributing traffic.


##### `cali<hash>`

In Kubernetes networking, the `cali<hash>` interfaces are virtual network interfaces created by **Calico**, a popular Container Network Interface (CNI) plugin. These interfaces play a crucial role in providing connectivity and enforcing policies for pods.

###### **Purpose of `cali<hash>` Interfaces**
1. **Pod-to-Pod Communication**:
   - Each `cali<hash>` interface corresponds to a pod running on a Kubernetes node.
   - These interfaces are created by Calico on the node where the pod is scheduled.
   - They connect pods to the cluster's virtual network.

2. **Network Policy Enforcement**:
   - Calico uses these interfaces to enforce Kubernetes NetworkPolicies or Calico-specific policies.
   - Rules applied at these interfaces determine which traffic is allowed or denied for the corresponding pod.

3. **Connectivity through IP Routing**:
   - Calico does not rely on overlay networks like VXLAN by default. Instead, it routes traffic directly between nodes using standard IP routing.
   - The `cali<hash>` interfaces provide a way for pod traffic to enter and exit the Calico-managed network on the host node.

###### **How `cali<hash>` Works**:
- When a pod is created:
  1. Calico assigns the pod a unique IP address from the IP pool configured for the cluster.
  2. A `cali<hash>` interface is created and attached to the pod's network namespace.
  3. Routes are established to direct traffic from this interface to other pods, services, or external endpoints.

- The `<hash>` part of the interface name is derived from the pod's unique identifier, ensuring a distinct interface for each pod.

###### **Inspecting `cali<hash>` Interfaces**:
You can view these interfaces and their configuration using standard Linux tools like:
```bash
[root@controlplane-004:prod-brno~0]# ip addr show cali9afd5bf4c4e
19: cali9afd5bf4c4e@if3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 8950 qdisc noqueue state UP group default
    link/ether ee:ee:ee:ee:ee:ee brd ff:ff:ff:ff:ff:ff link-netns cni-8af99cfe-eee9-cb4c-ea97-ecdc1e08e031
[root@controlplane-004:prod-brno~130]# ip netns exec cni-8af99cfe-eee9-cb4c-ea97-ecdc1e08e031 ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
3: eth0@if19: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 8950 qdisc noqueue state UP group default
    link/ether 0a:e6:d3:3b:dd:4d brd ff:ff:ff:ff:ff:ff link-netnsid 0
    inet 10.233.104.4/32 scope global eth0
       valid_lft forever preferred_lft forever
    inet6 fe80::8e6:d3ff:fe3b:dd4d/64 scope link
       valid_lft forever preferred_lft forever
[root@controlplane-004:prod-brno~0]# ip route | grep cali
10.233.104.4 dev cali9afd5bf4c4e scope link
...
10.233.104.59 dev calia4f16107924 scope link

[root@controlplane-004:prod-brno~0]# kubectl get po -owide --all-namespaces | grep " 10.233.104.4 "
default             distributed-debugger-rfxmx                                        1/1     Running     21 (6d23h ago)   269d    10.233.104.4     controlplane-004   <none>           <none>
```
These commands will show the IP address and routes associated with each `cali<hash>` interface.

###### **Advantages of `cali<hash>` Interfaces**:
- **Performance**: By using direct routing instead of overlays, Calico achieves lower latency and higher throughput.
- **Simplicity**: Eliminates the complexity of managing overlay networks.
- **Policy Control**: The interfaces serve as enforcement points for fine-grained network security rules.

###### **Further Reading**:
- Official Calico documentation provides more in-depth details about its architecture and use of `cali<hash>` interfaces: [Project Calico Networking](https://projectcalico.docs.tigera.io/).

##### `vxlan.calico`

The `vxlan.calico` interface in Kubernetes networking is a virtual interface created by **Calico** when it operates in **VXLAN mode**. This interface is part of Calico's implementation of overlay networking, which enables communication between pods across different nodes in a cluster without requiring complex routing configuration.

###### **Role of `vxlan.calico`**
1. **Overlay Network Implementation**:
   - The `vxlan.calico` interface is used to encapsulate pod-to-pod traffic in VXLAN (Virtual Extensible LAN) packets.
   - VXLAN creates a Layer 2 overlay network on top of an existing Layer 3 infrastructure, enabling seamless pod communication across nodes.

2. **Pod Connectivity Across Nodes**:
   - In environments where direct routing is not possible (e.g., in public cloud setups without support for BGP), VXLAN enables pod communication by encapsulating traffic and tunneling it through the underlying network.
   - The `vxlan.calico` interface ensures that pods appear to be on the same network, regardless of their physical location.

3. **Node-to-Node Communication**:
   - The `vxlan.calico` interface is configured on each node to handle traffic entering and leaving the VXLAN overlay network.
   - It works in conjunction with Calico's routing table updates to manage the mapping of pod IPs to node IPs.

4. **Encapsulation and Decapsulation**:
   - Traffic originating from a pod is encapsulated in VXLAN headers at the `vxlan.calico` interface.
   - Upon reaching the destination node, the VXLAN headers are stripped, and the traffic is forwarded to the target pod.

###### **Why Use VXLAN Mode in Calico?**
- **Simplifies Networking in Complex Environments**:
  - VXLAN abstracts the underlying network, making it easier to deploy Kubernetes in environments where direct routing is challenging.
- **Works Without BGP**:
  - Unlike Calico's default mode, which relies on BGP for routing, VXLAN works independently of the underlying network's routing capabilities.
- **Network Isolation**:
  - Provides a logical separation of pod networks, which can be useful in multi-tenant environments.

###### **Configuring VXLAN in Calico**
To enable VXLAN in Calico, you can configure the IP pool to use VXLAN encapsulation:
```yaml
apiVersion: projectcalico.org/v3
kind: IPPool
metadata:
  name: default-ipv4-ippool
spec:
  cidr: 192.168.0.0/16
  encapsulation: VXLAN
  natOutgoing: true
  nodeSelector: all()
```
This configuration ensures that pods within the specified CIDR use VXLAN encapsulation for inter-node communication.

###### **Inspecting `vxlan.calico`**
You can view the interface details using commands like:
```bash
[root@controlplane-004:prod-brno~130]# ip addr show vxlan.calico
21: vxlan.calico: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 8950 qdisc noqueue state UNKNOWN group default
    link/ether 66:63:4c:00:20:a1 brd ff:ff:ff:ff:ff:ff
    inet 10.233.104.0/32 scope global vxlan.calico
       valid_lft forever preferred_lft forever

```

###### **Conclusion**
The `vxlan.calico` interface is critical for enabling overlay networking in Calico, providing flexibility for Kubernetes clusters deployed in diverse networking environments. By encapsulating traffic in VXLAN, it ensures reliable and scalable pod-to-pod communication without relying on complex underlay network configurations. For more details, see [Calico's VXLAN Documentation](https://projectcalico.docs.tigera.io/networking/vxlan).


#### kubernetes applications
##### kubelet
##### kube-proxy
##### etcd
##### kube-apiserver
##### kube-controller-manager
##### kube-scheduler
##### nginx-ingress-controller


### Kubernetes DNS system

Kubernetes DNS is a built-in service that provides name resolution for services and pods within a Kubernetes cluster. It simplifies communication by enabling workloads to address services by name rather than IP address, which can frequently change. We use [coredns + nodelocaldns](https://kubernetes.io/docs/tasks/administer-cluster/nodelocaldns/).

Let's have service:
```console
[freznicek@lenovo-t14 ~ 0]$ kubectl --context=prod-brno -n openstack get svc keystone-api -owide
NAME           TYPE       CLUSTER-IP      EXTERNAL-IP   PORT(S)          AGE   SELECTOR
keystone-api   NodePort   10.233.62.120   <none>        5000:30500/TCP   90d   application=keystone,component=api,release_group=openstack-ke
```

To access to the service you may use:
1. DNS service name
   * `keystone-api:5000` any pod within ns `openstack`
   * `keystone-api.openstack:5000` any pod
   * `keystone-api.openstack.svc.cluster.local:5000` any pod
   * `10-233-62-120.openstack.pod.cluster.local:5000` any pod
1. IP address itself `10.233.62.120:5000`

This assumes no strict network policy in place, otherwise network policy defines who can access the service from where.

### Why kubernetes executes for each **pod** a pause container?

In Kubernetes, the **pause container** is a small utility container that serves as the parent container for each pod's group of containers. It plays a critical role in Kubernetes container lifecycle and pod management. Here's what it is used for:

#### **Primary Roles of the Pause Container**
1. **Pod Namespace Management**:
   - The pause container acts as the "parent" process for the pod, owning and maintaining the shared namespaces (e.g., PID, network, IPC) for the pod. 
   - Containers in the pod join these namespaces, ensuring they can communicate and share resources.

2. **Network Namespace Anchor**:
   - The pause container holds the pod’s network namespace, keeping it alive even if all other containers in the pod restart.
   - It is crucial because restarting a container would otherwise recreate the network namespace, breaking connections.

3. **Minimal Resource Overhead**:
   - The pause container is lightweight and consumes minimal resources. It runs a simple process that does nothing but sleeps indefinitely (`pause` process), ensuring the namespace remains intact.

4. **Simplifies Container Management**:
   - By providing a consistent structure for pods, the pause container simplifies the Kubernetes networking model and namespace sharing.

5. **Facilitates Resource Cleanup**:
   - When a pod is deleted, the pause container is terminated last, allowing Kubernetes to clean up shared resources in an orderly manner.

#### **Technical Details**
- **Image**: The pause container typically uses a minimal image, such as `k8s.gcr.io/pause:3.x`.
- **Behavior**: It runs a no-op process, commonly implemented in C, to minimize memory and CPU usage.
- **Namespace Sharing**:
   - **Network Namespace**: Allows all containers in a pod to share the same IP and port space.
   - **PID Namespace**: Enables process visibility and management across containers.
   - **IPC Namespace**: Allows shared memory and semaphores.

#### **Benefits**
- **Stability**: Ensures that namespaces are persistent even during container restarts.
- **Efficiency**: Simplifies pod architecture while keeping resource usage low.
- **Consistency**: Provides a standard method for namespace management across all pods.

#### **Related Concepts**
- The pause container is critical for implementing Kubernetes' "share everything by default" model for pods. Without it, individual containers in a pod would need to manage shared namespaces independently, increasing complexity.


### Why pause container has different PID than container in a pod

Each application/container should have its own process ant thus PID


```console
[root@controlplane-002:prod-ostrava~0]# nerdctl ps | grep cinder-volume
123c1c6cc6c5    registry.gitlab.ics.muni.cz:443/cloud/g2/custom-images/cinder:yoga-ubuntu_focal-20240127-CVE-2024-32498-patch-1       "/tmp/cinder-volume.…"    6 weeks ago       Up                 k8s://openstack/cinder-volume-6bd86ddbc6-l9z8b/cinder-volume
e072603d7ff4    registry.k8s.io/pause:3.9                                                                                             "/pause"                  6 weeks ago       Up                 k8s://openstack/cinder-volume-6bd86ddbc6-l9z8b
[root@controlplane-002:prod-ostrava~0]# nerdctl inspect 123c1c6cc6c5 | grep -i pid
            "Pid": 721452,
[root@controlplane-002:prod-ostrava~0]# nerdctl inspect e072603d7ff4 | grep -i pid
            "Pid": 716047,
[root@controlplane-002:prod-ostrava~0]# pstree -p -Z 716047
pause(716047,`unconfined')
[root@controlplane-002:prod-ostrava~0]# pstree -p -Z 721452
cinder-volume(721452,`cri-containerd.apparmor.d (enforce)')
 └─cinder-volume(722330,`cri-containerd.apparmor.d (enforce)')
    ├─{cinder-volume}(722430,`cri-containerd.apparmor.d (enforce)')
    ├─{cinder-volume}(985994,`cri-containerd.apparmor.d (enforce)')
...
    └─{cinder-volume}(1718179,`cri-containerd.apparmor.d (enforce)')
    ```



### What is the process of creation of deployment via `kubectl apply`

When you deploy a Kubernetes Deployment using `kubectl apply`, a series of steps occurs to ensure the resource is created and the desired state is achieved. Here's an overview of the process:

---

#### 1. **Kubectl Client-Side Validation and Transformation**
   - **Command Execution**: The `kubectl apply -f <file>` command is executed.
   - **Parsing and Validation**:
     - `kubectl` parses the YAML/JSON manifest file(s).
     - It validates the manifest to ensure fields comply with the Kubernetes API schema.
   - **Client-Side Defaults and Merging**:
     - Default values (e.g., `replicas`, `strategy`) may be added based on the API version and kind.
   - **Patch/Server-Side Apply**:
     - For `apply`, the client generates a patch to communicate the desired state to the server.
     - Configuration changes are saved in the server-side `last-applied-configuration` annotation.

---

#### 2. **API Server Interaction**
   - **Authentication and Authorization**:
     - The API server authenticates the `kubectl` request (via tokens, certificates, etc.).
     - The server checks whether the user has the necessary permissions to create or modify the resource (e.g., via RBAC).
   - **Resource Handling**:
     - The API server checks if the Deployment already exists:
       - **New Deployment**: It creates the Deployment resource.
       - **Existing Deployment**: It updates the Deployment, calculating changes (diffs).
     - The Deployment object is persisted in etcd (Kubernetes' key-value store).

---

#### 3. **Controller Manager Actions**
   - The Deployment controller (part of the `kube-controller-manager`) detects the new or updated Deployment object.
   - **ReplicaSet Creation/Update**:
     - If the Deployment specifies a new desired state (e.g., new pod template), a new ReplicaSet is created.
     - Old ReplicaSets may remain for rollback purposes unless explicitly deleted.
   - **Scaling Management**:
     - The ReplicaSet controller ensures the number of pods matches the desired replicas in the Deployment spec.

---

#### 4. **Scheduler and Pod Creation**
   - The ReplicaSet controller creates Pods as needed:
     - For each new Pod, the API server passes it to the Scheduler.
   - **Pod Scheduling**:
     - The Scheduler identifies a suitable node for each Pod based on resource requests (CPU, memory), affinities, and constraints.
     - Pods are bound to their respective nodes.

---

#### 5. **Node and Kubelet Execution**
   - The `kubelet` (agent on each node) watches for assigned Pods.
   - **Pod Creation on Node**:
     - The `kubelet` pulls container images (via the specified `image` in the Pod spec) from container registries.
     - It creates and starts containers using the container runtime (e.g., containerd, CRI-O).
     - It configures networking (via CNI plugins) and mounts volumes if specified.
   - The `kubelet` sends Pod status updates back to the API server.

---

#### 6. **Ongoing Reconciliation**
   - Kubernetes continuously monitors the cluster to ensure the actual state matches the desired state:
     - If a Pod crashes or a node fails, the Deployment controller creates replacement Pods.
     - If resource usage changes, autoscalers (if configured) adjust replica counts or resource allocations.

---

#### 7. **Feedback and Monitoring**
   - **kubectl Output**:
     - The `kubectl apply` command provides feedback on resource creation or modification (e.g., `deployment.apps/my-deployment created`).
   - **Monitoring**:
     - The `kubectl get` or `kubectl describe` commands can show Deployment, ReplicaSet, and Pod statuses.
     - Logs and metrics from Pods can be inspected using `kubectl logs` and monitoring tools.

---

#### Summary of Key Components Involved
- **kubectl**: Validates and applies the manifest.
- **API Server**: Serves as the control plane's front door.
- **etcd**: Stores the cluster state.
- **Controllers**: Ensure the desired state (Deployment -> ReplicaSet -> Pods).
- **Scheduler**: Decides where Pods should run.
- **kubelet**: Ensures containers are running on nodes.

This process is Kubernetes' declarative nature in action, ensuring the desired state described in your YAML manifests is continuously maintained in the cluster.


#### More in detail step 5

When the `kubelet` creates a Pod in the scenario described, it follows a detailed process to ensure the Pod is correctly instantiated and runs on the node. Here's a step-by-step breakdown:

---

##### 5.1. **Watch for Pod Assignment**
   - **Pod Binding**: After the Scheduler assigns a Pod to a node, the API server updates the Pod object with the `nodeName` field, indicating its target node.
   - **Kubelet Watches API Server**: 
     - The `kubelet` continuously watches the API server (via the `/pods` endpoint) for Pods assigned to its node.
     - When a new Pod appears with the node’s name, the `kubelet` begins processing it.

---

##### 5.2. **Pod Validation and Preparation**
   - **Validation**: 
     - The `kubelet` validates the Pod spec (e.g., ensuring resource requests/limits fit the node’s capacity).
     - If validation fails, the Pod enters a `Failed` state, and the reason is recorded.
   - **Pod Volume Setup**:
     - For Pods using volumes:
       - The `kubelet` initializes the specified volumes (e.g., `emptyDir`, `hostPath`, `PersistentVolumeClaim`).
       - For network-based volumes (e.g., `NFS`, `Ceph`), it mounts the storage.
       - Volume mounts are prepared in the container filesystem namespace.
   - **Network Configuration**:
     - Using a Container Network Interface (CNI) plugin, the `kubelet` sets up the Pod's network:
       - Assigns a network namespace.
       - Allocates an IP address for the Pod.
       - Configures routes, DNS, and other network settings.

---

##### 5.3. **Container Runtime Interaction**
   - The `kubelet` interacts with the container runtime (e.g., **containerd**, **CRI-O**, or Docker via the Container Runtime Interface (CRI)) to manage container lifecycle.
   - **Image Management**:
     - For each container in the Pod:
       - The `kubelet` checks if the required container image is available on the node.
       - If not, it pulls the image from the specified registry.
       - If the `imagePullPolicy` is set to `Always`, the `kubelet` pulls the image regardless of whether it is already present.
       - It verifies the image (e.g., using checksums or signatures).
   - **Sandbox Container Creation** (if applicable):
     - For Pods using container runtimes like containerd, the `kubelet` creates a **Pod sandbox** (e.g., an isolated network namespace and a base container for the Pod).
   - **Container Creation**:
     - For each container:
       - A container is created with the specified image, resource requests/limits, and environment variables.
       - The container is configured with volume mounts, network settings, and command/entry points.
       - Health probes (`livenessProbe`, `readinessProbe`, `startupProbe`) are set up.

---

##### 5.4. **Container Initialization**
   - **Initialization Containers**:
     - If the Pod spec includes init containers, they are executed sequentially before the main containers.
     - The `kubelet` ensures each init container completes successfully before moving to the next.
     - If an init container fails, the Pod enters a `Pending` or `Failed` state, and no main containers start.
   - **Main Containers**:
     - Once all init containers finish, the `kubelet` starts the main application containers.

---

##### 5.5. **Health Checks and Monitoring**
   - The `kubelet` continuously monitors the status of running containers:
     - **Liveness Probes**:
       - Used to check if the container is still alive.
       - If a liveness probe fails, the container is restarted.
     - **Readiness Probes**:
       - Determine if the container is ready to serve traffic.
       - If readiness fails, the container is removed from the service endpoint list.
     - **Startup Probes**:
       - Used during container startup to delay other probes until the application is fully initialized.
   - The `kubelet` reports Pod and container statuses back to the API server, updating the Pod object.

---

##### 5.6. **Lifecycle Management**
   - **Resource Constraints**:
     - The `kubelet` ensures resource usage complies with the Pod's resource requests and limits.
     - If a container exceeds its resource limits (e.g., memory), the `kubelet` may terminate it.
   - **Crash Recovery**:
     - If a container crashes, the `kubelet` restarts it based on the Pod's `restartPolicy` (e.g., `Always`, `OnFailure`, `Never`).
   - **Graceful Shutdown**:
     - If a Pod is terminated (e.g., via `kubectl delete`), the `kubelet`:
       - Sends a termination signal (`SIGTERM`) to the container.
       - Waits for the container to gracefully shut down within the `terminationGracePeriodSeconds`.
       - Forces termination (`SIGKILL`) if the grace period expires.

---

##### 5.7. **Logging and Metrics**
   - **Container Logs**:
     - The `kubelet` streams container logs to files (e.g., under `/var/log/containers`) or makes them available for tools like `kubectl logs`.
   - **Metrics Collection**:
     - The `kubelet` exposes metrics (e.g., CPU/memory usage) for tools like Prometheus.

---

##### 5.8. **Error Handling**
   - If any step fails (e.g., image pull error, volume mount error), the `kubelet` marks the Pod as `Pending` or `Failed`.
   - Events are logged in the API server, visible via `kubectl describe pod`.

---

This process ensures the Pod is properly set up, runs as intended, and integrates seamlessly with the rest of the Kubernetes cluster. The `kubelet` acts as a critical agent, translating the cluster's desired state into running containers on the node.






