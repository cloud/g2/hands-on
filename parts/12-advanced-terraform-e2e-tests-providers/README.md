# Advanced terraform, E2E tests with terraform, Building custom terraform providers

## Objectives

### part A
* [x] Terraform 101
* [x] E2E cloud tests with terraform
* [x] Building custom terraform providers

### part B
* [ ] [Hackaton : terraform project data source development](#part-b-hackaton-terraform-project-data-source-development)

## Terraform 101 (basic knowledge)

**Terraform** is an open-source Infrastructure as Code (IaC) tool developed by **HashiCorp**. It enables users to define, provision, and manage infrastructure resources across multiple cloud providers and on-premises environments in a **declarative** configuration language. Here's a summary of its key aspects:

### 1. **Core Features**
- **Declarative Language:** Infrastructure is described in HashiCorp Configuration Language (HCL), which is human-readable and supports complex configurations.
- **Multi-Cloud Support:** Works with various providers such as AWS, Azure, Google Cloud, Kubernetes, and others, as well as on-premises environments.
- **Resource Graph:** Builds a dependency graph to determine the optimal order of operations for resource provisioning, updates, or deletion.
- **State Management:** Maintains the infrastructure state in a state file, allowing Terraform to track existing resources and make incremental changes.

### 2. **Workflow**
1. **Write:** Define resources in `.tf` configuration files.
2. **Plan:** Generate an execution plan showing proposed changes before applying them.
3. **Apply:** Execute the plan to create, update, or destroy resources.
4. **Destroy:** Tear down the infrastructure when no longer needed.

### 3. **Key Components**
- **Providers:** Plugins that interact with APIs of various platforms (e.g., AWS, Azure, Google Cloud).
- **Modules:** Reusable, configurable units of infrastructure, enabling best practices and modular design.
- **Terraform CLI:** Command-line interface for executing Terraform commands.
- **State File:** A JSON file storing metadata about managed resources and their current state.

### 4. **Advantages**
- **Multi-Cloud Flexibility:** Manage resources across different providers with a single tool.
- **Version Control:** Configuration files can be stored in Git, allowing tracking and collaboration.
- **Automation:** Enables repeatable and automated infrastructure provisioning.
- **Scalability:** Supports large-scale deployments.

### 5. **Common Use Cases**
- Setting up cloud infrastructure (e.g., virtual machines, storage, networks).
- Managing Kubernetes clusters and workloads.
- Deploying serverless applications.
- Automating disaster recovery and failover setups.

### 6. **Ecosystem**
- **Terraform Cloud & Enterprise:** SaaS offering for collaboration, state management, and policy enforcement.
- **Integration:** Works with CI/CD pipelines, monitoring tools, and other DevOps technologies.

Terraform is highly regarded for simplifying and standardizing infrastructure management, making it a cornerstone tool in modern DevOps practices.

### Terraform resources and data sources


#### **1. Resource**
- **Definition:** A **resource** is a component of your infrastructure that Terraform creates, manages, and potentially destroys. It represents something you want Terraform to provision and maintain.
- **Purpose:** To define, provision, and manage the lifecycle of infrastructure components.
- **Examples:**
  - Creating an AWS EC2 instance.
  - Setting up an Azure virtual network.
  - Deploying a Google Cloud storage bucket.
- **Lifecycle Management:** Terraform manages the lifecycle of a resource, including its creation, updates, and deletion, based on changes to the configuration.
- **Code Example:**
  ```hcl
  resource "aws_instance" "example" {
    ami           = "ami-12345678"
    instance_type = "t2.micro"
  }
  ```


#### **2. Data Source**
- **Definition:** A **data source** allows Terraform to retrieve information from existing infrastructure or services that are not directly managed by Terraform. It queries external data but does not manage the lifecycle of the queried entity.
- **Purpose:** To fetch and use information about existing infrastructure or services, or to get dynamic values for use in configurations.
- **Examples:**
  - Fetching details of an existing AWS S3 bucket or an EC2 AMI.
  - Querying the configuration of a DNS record.
  - Getting metadata from a cloud provider.
- **Lifecycle Management:** Data sources are read-only; Terraform does not create, update, or delete the queried entity.
- **Code Example:**
  ```hcl
  data "aws_ami" "example" {
    most_recent = true
    owners      = ["amazon"]
    filters = [
      {
        name   = "name"
        values = ["amzn2-ami-hvm-*-x86_64-gp2"]
      }
    ]
  }
  ```


#### **Key Differences**

| Feature                 | **Resource**                                            | **Data Source**                                            |
|--------------------------|--------------------------------------------------------|-----------------------------------------------------------|
| **Purpose**             | Create/manage new infrastructure.                      | Fetch/read information about existing infrastructure.     |
| **Lifecycle Management**| Full lifecycle management (create, update, delete).    | Read-only; no lifecycle management.                      |
| **Use Case**            | Provisioning resources (e.g., servers, networks).      | Querying existing data (e.g., available AMIs, existing resources). |
| **State Dependency**    | Tracked in the state file and managed by Terraform.     | Queried dynamically during each `terraform apply`.        |


#### **When to Use Each**
- **Use a Resource** when you want Terraform to create and manage infrastructure.
- **Use a Data Source** when you need to reference or fetch information about existing infrastructure or external services.

This distinction allows Terraform to handle both creating new infrastructure and integrating with pre-existing systems seamlessly.


#### Terraform provider methods needed for resource and data source

| **Method**         | **Resource** | **Data Source**  |
|--------------------|--------------|------------------|
| **Create**         | Required     | Not Applicable   |
| **Read**           | Required     | Required         |
| **Update**         | Recommended  | Not Applicable   |
| **Delete**         | Required     | Not Applicable   |


## E2E cloud tests with terraform

* common part https://gitlab.ics.muni.cz/cloud/g2/e2e-tests-common
* cloud environment instance https://gitlab.ics.muni.cz/cloud/g2/e2e-tests-brno

TODO

## Building custom terraform providers

https://gitlab.ics.muni.cz/cloud/knowledgebase/-/blob/master/howtos/terraform.md#custom-shell-terraform-providers

## Part B Hackaton: terraform project data source development

### Why

* Let's try to work together on a topic
* Improve knowledge on custom terraform providers
* Build something useful, needed for EGI

#### Why we need `ostack_project_children_v1` data source

See https://gitlab.ics.muni.cz/cloud/internal-wiki/-/issues/1254#note_429548, section (B) for more details.

### Goals

1. Create (shell based) terraform provider which achieves:
   * is able to receive list of nested (children) projects (names, IDs)
   * named `ostack_project_children_v1`
   * inputs are:
     * parent_project_id
   * outputs are: `{"nested-project-names": [ ... ], "nested-project-ids": [ ... ]}`
     * children project IDs
     * children project names
1. Create unit test suite for verifying the functionality
1. `ostack_project_children_v1` data source exists in common-cloud-entities
1. related unit tests are passing

### Task breakdown

* [x] 0. Agree on git branching:
  * proposed common-cloud-entities [branch `ostack_project_children_v1`](https://gitlab.ics.muni.cz/cloud/g2/common-cloud-entities/-/tree/ostack_project_children_v1?ref_type=heads)
  * all MRs with below tasks are merging into [branch `ostack_project_children_v1`](https://gitlab.ics.muni.cz/cloud/g2/common-cloud-entities/-/tree/ostack_project_children_v1?ref_type=heads)
  * at the end we merge `ostack_project_children_v1` to default `master` branch
* [x] A. (freznicek) Architecture agreed. Push initial structure. Initial thought is to re-use [`ostack_project_properties_v1` one](https://gitlab.ics.muni.cz/cloud/g2/common-cloud-entities/-/tree/master/ci/terraform/providers/shell/ostack_project_properties_v1?ref_type=heads). Pushed as [commit 9b6b86eb6](https://gitlab.ics.muni.cz/cloud/g2/common-cloud-entities/-/commit/9b6b86eb6d52c720830333b61edec468967b7533).
* [x] B. (freznicek) Temporary working repository exists. [common-cloud-entities, branch `ostack_project_properties_v1`](https://gitlab.ics.muni.cz/cloud/g2/common-cloud-entities/-/commits/%E2%80%8B%E2%80%8Bostack_project_properties_v1)
* [ ] C. (jnemec) `Read` method script exists.
* [ ] D. (todo) Terraform provider library + final `read` method exists.
  * inspiration Inspiration from https://gitlab.ics.muni.cz/cloud/g2/common-cloud-entities/-/blob/master/ci/terraform/providers/shell/ostack_project_properties_v1/read.sh?ref_type=heads + https://gitlab.ics.muni.cz/cloud/g2/common-cloud-entities/-/blob/master/ci/terraform/providers/shell/ostack_project_properties_v1/lib.sh?ref_type=heads
* [ ] E. (jjezek) Testing scenarios defined.
  * inspiration from https://gitlab.ics.muni.cz/cloud/g2/common-cloud-entities/-/blob/master/ci/terraform/providers/shell/ostack_project_properties_v1/tests/tests.bats?ref_type=heads search `@test`
* [ ] F. (jsmrcka) Mocked OpenStack API model created.
  * inspiration https://gitlab.ics.muni.cz/cloud/g2/common-cloud-entities/-/blob/master/ci/terraform/providers/shell/ostack_project_properties_v1/tests/openstack-cli-model.sh?ref_type=heads
* [ ] G. (team) Testing scenarios implemented.
  * [x] (freznicek) empty id, implemented https://gitlab.ics.muni.cz/cloud/g2/common-cloud-entities/-/compare/b02d0e34182a46f9656feda4c897a6edfb109e2f...996ca44e6f58951fb212a37bffe988962c29e668
  * [ ] (jnemec) non-existent project id
  * [ ] (jjezek) existent project id, no children
  * [ ] (jsmrcka) existent project id, some children
* [x] H. (freznicek) `ostack_project_children_v1` data source exists and unit tests are all passing.
* [ ] I. (todo) Move code and tests from temporary working repository into common-cloud-entitties (ci/terraform/providers/shell/ostack_project_children_v1) default branch.


#### Query nested projects

```log
[freznicek@lenovo-t14 ~ 0]$ openstack project list --parent c6632b0e0cd146b9b7309f815b897509 -fjson
[]
[freznicek@lenovo-t14 ~ 0]$ openstack project list --parent e0a34f41f9fa4b038aad57982c78cc5b -fjson
[
  {
    "ID": "2b533f4c3b524a8b9962a935fdcb3bb2",
    "Name": "371955e3b30c36096992864d607c31cfbe3ff6a5@lifescience-ri.eu"
  },
  {
    "ID": "8b7365ab0589441581e2adb998fb1d69",
    "Name": "5264f62d1620ed97a5890b0e5f33284a539b44ee@lifescience-ri.eu"
  },
  {
    "ID": "ab57ce7788d54fc6b02898ef4c0523f7",
    "Name": "9594a7d8f1e9d6f1fe6fed7856783d515b4a9b5c@lifescience-ri.eu"
  },
  {
    "ID": "b8344ef96f004fce8fae54b78abd97d3",
    "Name": "8c8a27ae32b0e3a01b43eeb144eb87dc35b03fe6@lifescience-ri.eu"
  }
]
```

## References
* E2E tests
  * https://gitlab.ics.muni.cz/cloud/g2/e2e-tests-common
  * https://gitlab.ics.muni.cz/cloud/g2/e2e-tests-brno
  * https://gitlab.ics.muni.cz/cloud/g2/e2e-tests-ostrava
* https://gitlab.ics.muni.cz/cloud/internal-wiki/-/issues/1254
* https://gitlab.ics.muni.cz/cloud/g2/common-cloud-entities/-/tree/master/ci/terraform/providers/shell/ostack_project_properties_v1?ref_type=heads
  * https://gitlab.ics.muni.cz/cloud/g2/common-cloud-entities/-/blob/master/ci/terraform/providers/shell/ostack_project_properties_v1/README.md?ref_type=heads
* https://gitlab.ics.muni.cz/cloud/knowledgebase/-/blob/master/howtos/terraform.md#custom-shell-terraform-providers
* https://registry.terraform.io/providers/scottwinkler/shell/latest/docs
  * minimal scottwinkler/shell data source: https://registry.terraform.io/providers/scottwinkler/shell/latest/docs/data-sources/shell_script 

## Q&A / FAQ

### Is there recording ?

Yes, but no audio sadly. https://drive.google.com/file/d/1B5wLPYwB4eA4Xn3ljMTS0LqjVC6DLrMp/view?usp=drive_link

### How to add bats unit test

Review following changes:
* 1] add data source for your test scenario (main.tf)
* 2] add variable for your test scenario into variables.tf
* 3] implement three units inside tests.bats
* 4] validate it is passing

First added test scenario, see [example code](https://gitlab.ics.muni.cz/cloud/g2/common-cloud-entities/-/compare/b02d0e34182a46f9656feda4c897a6edfb109e2f...996ca44e6f58951fb212a37bffe988962c29e668).