# Cloud hands-on part 1, Terraform, OpenStack, infra-config

## Objectives
* [ ] A. Install G2 infrastructure as IaaS inside OpenStack
* [ ] B. Execute basic infra-config playbooks


## Steps

### A. Install G2 infrastructure as IaaS inside OpenStack

Read 10 slides from https://docs.google.com/presentation/d/1sO95-eeZv_xg5liwLLUUnVCiXrYD7sDBU_N5Kv6k8Hg/edit#slide=id.g14cdd2aa762_0_1 and/or following summary https://gitlab.ics.muni.cz/cloud/knowledgebase/-/blob/master/team/onboarding/g2-infra-deep-dive.md#g2-repositories-at-a-glance-including-plans

#### Log on G2 Ostrava https://horizon.ostrava.openstack.cloud.e-infra.cz/auth/login/?next=/

#### Find your `meta-cloud-<jmeno>-training` project

#### Get your aplication credentials and store it locally

Hint: https://docs.e-infra.cz/compute/openstack/how-to-guides/obtaining-api-key/

#### Install the needed tools

```
apt -y update
apt -y install vim git-core util-linux python3-minimal python3-pip podman-docker sshuttle iptables bash-completion net-tools age apt-transport-https sshuttle

cd /tmp

# kubectl
if ! kubectl version&>/dev/null; then
    curl -LO https://dl.k8s.io/release/v1.24.9/bin/linux/amd64/kubectl
    chmod +x ./kubectl
    mv ./kubectl /usr/local/bin/
fi

# flux
# https://github.com/fluxcd/flux2/releases/tag/v0.41.2
if ! flux --version&>/dev/null; then
    curl -LO https://github.com/fluxcd/flux2/releases/download/v0.41.2/flux_0.41.2_linux_amd64.tar.gz
    tar xf flux_0.41.2_linux_amd64.tar.gz
    chmod +x ./flux
    mv -f ./flux /usr/local/bin/
fi

# sops
if ! sops --version&>/dev/null; then
    pkg="sops_3.7.3_amd64.deb"
    curl -Lo "${pkg}" "https://github.com/mozilla/sops/releases/latest/download/${pkg}"
    apt --fix-broken install ./${pkg}
fi

# openstack
if ! openstack --version&>/dev/null; then
    python3 -mpip install openstackclient
fi


# helm
if ! helm version&>/dev/null; then
    curl https://baltocdn.com/helm/signing.asc | gpg --dearmor | tee /usr/share/keyrings/helm.gpg > /dev/null
    echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/helm.gpg] https://baltocdn.com/helm/stable/debian/ all main" > /etc/apt/sources.list.d/helm-stable-debian.list
    apt -y update
    apt install helm
fi
# terraform
if ! terraform --version&>/dev/null; then
    apt update
    apt install  software-properties-common gnupg2 curl
    curl https://apt.releases.hashicorp.com/gpg | gpg --dearmor > hashicorp.gpg
    install -o root -g root -m 644 hashicorp.gpg /etc/apt/trusted.gpg.d/
    apt-add-repository "deb [arch=$(dpkg --print-architecture)] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
    apt -y update
    apt install terraform
fi

python3 --version
git --version
openstack --version
kubectl version
terraform --version
helm version
flux --version
sops --version
podman version
podman run hello-world
sshuttle --version
```
Note: Flux CD has to be at version 0.41.2!


#### Test your access to the cloud

```
[freznicek@lenovo-t14 tmp 0]$ source ~/c/g2-prod-ostrava-freznicek-meta-cloud-freznicek-training.sh.inc
[freznicek@lenovo-t14 kubernetes-infra-example-dev-ost.git 130]$ openstack version show | grep identity
| Ostrava     | identity       | 3.14    | CURRENT   | https://identity.ostrava.openstack.cloud.e-infra.cz/v3/       | None             | None             |
[freznicek@lenovo-t14 kubernetes-infra-example-dev-ost.git 0]$ openstack server list

[freznicek@lenovo-t14 kubernetes-infra-example-dev-ost.git 0]$

```

#### Create IaaS infrastructure

Read briefly following 10 slides in https://docs.google.com/presentation/d/1K-l0TNPN3zHEfHPID5bH1gD1JjJhz2Xfmwz4AejnbMc/edit#slide=id.g2cfc73977d5_0_216 to get started.

Follow https://gitlab.ics.muni.cz/cloud/kubernetes/kubernetes-infra-example-dev-ost/-/blob/master/README.md?ref_type=heads
Just steps to get VMs ready.

```
[freznicek@lenovo-t14 terraform 0]$ ls -la ~/.ssh/id_rsa
-rw-------. 1 freznicek freznicek 3381 18. bře  2022 /home/freznicek/.ssh/id_rsa
[freznicek@lenovo-t14 _g2-ostack 0]$ git clone git@gitlab.ics.muni.cz:cloud/kubernetes/kubernetes-infra-example-dev-ost.git kubernetes-infra-example-dev-ost.git
[freznicek@lenovo-t14 _g2-ostack 0]$ cd kubernetes-infra-example-dev-ost.git
[freznicek@lenovo-t14 kubernetes-infra-example-dev-ost.git 0]$ git submodule update --init

[freznicek@lenovo-t14 kubernetes-infra-example-dev-ost.git 0]$ vi terraform/main.tf
[freznicek@lenovo-t14 kubernetes-infra-example-dev-ost.git 0]$ git diff
diff --git a/terraform/main.tf b/terraform/main.tf
index c2dc1a9..d4f12b2 100644
--- a/terraform/main.tf
+++ b/terraform/main.tf
@@ -6,10 +6,10 @@ module "kubernetes_infra" {
   # Example of variable override
   ssh_public_key = "~/.ssh/id_rsa.pub"

-  infra_name = "infra-name"
+  infra_name = "meta-cloud-freznicek-training"

   control_nodes_count       = 3
-  control_nodes_volume_size = 30
+  control_nodes_volume_size = 200
   control_nodes_flavor      = "c3.8core-16ram" # brno hpc.8core-16ram

   bastion_flavor = "g2.tiny"

[freznicek@lenovo-t14 kubernetes-infra-example-dev-ost.git 130]$ openstack version show | grep identity
| Ostrava     | identity       | 3.14    | CURRENT   | https://identity.ostrava.openstack.cloud.e-infra.cz/v3/       | None             | None             |
[freznicek@lenovo-t14 kubernetes-infra-example-dev-ost.git 0]$ openstack server list

[freznicek@lenovo-t14 kubernetes-infra-example-dev-ost.git 0]$

[freznicek@lenovo-t14 terraform 0]$ terraform init

Initializing the backend...
Initializing modules...

Initializing provider plugins...
- Reusing previous version of terraform-provider-openstack/openstack from the dependency lock file
- Reusing previous version of hashicorp/local from the dependency lock file
- Using previously-installed terraform-provider-openstack/openstack v1.54.1
- Using previously-installed hashicorp/local v2.4.1

Terraform has been successfully initialized!

You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.
[freznicek@lenovo-t14 terraform 0]$ terraform validate
Success! The configuration is valid.

[freznicek@lenovo-t14 terraform 0]$ terraform plan --out plan
...
[freznicek@lenovo-t14 terraform 0]$ terraform apply "plan"
...

Apply complete! Resources: 46 added, 0 changed, 0 destroyed.

Outputs:

kubernetes_infra_bastion_external_ip = "195.113.243.108"
kubernetes_infra_control_instance_ip = [
  "10.0.0.143",
  "10.0.0.168",
  "10.0.0.113",
]
kubernetes_infra_floating_vip = "195.113.243.175"
kubernetes_infra_internal_vip = "10.0.0.5"
kubernetes_infra_worker_instance_ip = [
  "10.0.0.237",
  "10.0.0.219",
]
[freznicek@lenovo-t14 terraform 0]$


[freznicek@lenovo-t14 terraform 0]$ openstack server list
+--------------------------------------+----------------------------------------------+--------+-------------------------------------------------------------------+---------------------+----------------+
| ID                                   | Name                                         | Status | Networks                                                          | Image               | Flavor         |
+--------------------------------------+----------------------------------------------+--------+-------------------------------------------------------------------+---------------------+----------------+
| 9136db10-cf30-4d5f-9513-012d55bae28a | meta-cloud-freznicek-training-workers-1      | ACTIVE | meta-cloud-freznicek-training_network=10.0.0.237                  | ubuntu-jammy-x86_64 | g2.medium      |
| 9a5528d6-9c72-4e95-a627-4e165d73b3c4 | meta-cloud-freznicek-training-workers-2      | ACTIVE | meta-cloud-freznicek-training_network=10.0.0.219                  | ubuntu-jammy-x86_64 | g2.medium      |
| 69a01ce8-6254-4f2a-bd05-75c6cbf6e4ad | meta-cloud-freznicek-training-control-1      | ACTIVE | meta-cloud-freznicek-training_network=10.0.0.143                  | ubuntu-jammy-x86_64 | c3.8core-16ram |
| 7d805813-e9ef-4279-a473-9da0eeacf78c | meta-cloud-freznicek-training-control-2      | ACTIVE | meta-cloud-freznicek-training_network=10.0.0.168                  | ubuntu-jammy-x86_64 | c3.8core-16ram |
| bc41d308-a13f-40fc-92f8-2a50d67e75ea | meta-cloud-freznicek-training-control-3      | ACTIVE | meta-cloud-freznicek-training_network=10.0.0.113                  | ubuntu-jammy-x86_64 | c3.8core-16ram |
| b86be9df-8007-48f3-9611-9526ac740f1c | meta-cloud-freznicek-training-bastion-server | ACTIVE | meta-cloud-freznicek-training_network=10.0.0.228, 195.113.243.108 | ubuntu-jammy-x86_64 | g2.tiny        |
+--------------------------------------+----------------------------------------------+--------+-------------------------------------------------------------------+---------------------+----------------+
```

#### Get access to your IaaS infra on internal addresses with VPN/sshuttle

`sshuttle -r ubuntu@<FIP> 10.0.0.0/24`

eventually

`sshuttle -e "ssh -i ~/.ssh/id_rsa.cos8x" -r ubuntu@<FIP> 10.0.0.0/24`

```
[freznicek@lenovo-t14 terraform 0]$ openstack server list | grep -Eo '10.0.0.[0-9]{1,3}'
10.0.0.237
10.0.0.219
10.0.0.143
10.0.0.168
10.0.0.113
10.0.0.228
[freznicek@lenovo-t14 terraform 0]$ ips=$(openstack server list | grep -Eo '10.0.0.[0-9]{1,3}')
[freznicek@lenovo-t14 terraform 0]$ for i_ip in $ips; do ssh ubuntu@${i_ip} 'uname -a'; done
Linux meta-cloud-freznicek-training-meta-cloud-freznicek-training-work 5.15.0-73-generic #80-Ubuntu SMP Mon May 15 15:18:26 UTC 2023 x86_64 x86_64 x86_64 GNU/Linux
Linux meta-cloud-freznicek-training-meta-cloud-freznicek-training-work 5.15.0-73-generic #80-Ubuntu SMP Mon May 15 15:18:26 UTC 2023 x86_64 x86_64 x86_64 GNU/Linux
Linux meta-cloud-freznicek-training-control-1 5.15.0-73-generic #80-Ubuntu SMP Mon May 15 15:18:26 UTC 2023 x86_64 x86_64 x86_64 GNU/Linux
Linux meta-cloud-freznicek-training-control-2 5.15.0-73-generic #80-Ubuntu SMP Mon May 15 15:18:26 UTC 2023 x86_64 x86_64 x86_64 GNU/Linux
Linux meta-cloud-freznicek-training-control-3 5.15.0-73-generic #80-Ubuntu SMP Mon May 15 15:18:26 UTC 2023 x86_64 x86_64 x86_64 GNU/Linux
Linux meta-cloud-freznicek-training-bastion-server 5.15.0-73-generic #80-Ubuntu SMP Mon May 15 15:18:26 UTC 2023 x86_64 x86_64 x86_64 GNU/Linux
```

#### Try scaling number of compute nodes 2->4 and 4->2 (life-cycle management)

```
# 2->4

[freznicek@lenovo-t14 terraform 1]$ git diff
diff --git a/terraform/main.tf b/terraform/main.tf
index c2dc1a9..7a384d3 100644
--- a/terraform/main.tf
+++ b/terraform/main.tf
@@ -6,10 +6,10 @@ module "kubernetes_infra" {
   # Example of variable override
   ssh_public_key = "~/.ssh/id_rsa.pub"

-  infra_name = "infra-name"
+  infra_name = "meta-cloud-freznicek-training"

   control_nodes_count       = 3
-  control_nodes_volume_size = 30
+  control_nodes_volume_size = 200
   control_nodes_flavor      = "c3.8core-16ram" # brno hpc.8core-16ram

   bastion_flavor = "g2.tiny"
@@ -19,7 +19,7 @@ module "kubernetes_infra" {
       name        = "workers"
       flavor      = "g2.medium"
       volume_size = 30
-      count       = 2
+      count       = 4
     }
   ]
   # Ostrava

[freznicek@lenovo-t14 terraform 0]$ terraform validate;terraform plan --out plan;terraform apply plan
[freznicek@lenovo-t14 terraform 0]$ openstack server list

# 4->2
[freznicek@lenovo-t14 terraform 0]$ git diff
diff --git a/terraform/main.tf b/terraform/main.tf
index c2dc1a9..d4f12b2 100644
--- a/terraform/main.tf
+++ b/terraform/main.tf
@@ -6,10 +6,10 @@ module "kubernetes_infra" {
   # Example of variable override
   ssh_public_key = "~/.ssh/id_rsa.pub"

-  infra_name = "infra-name"
+  infra_name = "meta-cloud-freznicek-training"

   control_nodes_count       = 3
-  control_nodes_volume_size = 30
+  control_nodes_volume_size = 200
   control_nodes_flavor      = "c3.8core-16ram" # brno hpc.8core-16ram

   bastion_flavor = "g2.tiny"

[freznicek@lenovo-t14 terraform 0]$ terraform validate;terraform plan --out plan;terraform apply plan
[freznicek@lenovo-t14 terraform 0]$ openstack server list

```


---

### B. Execute basic infra-config playbooks



#### Create infra config inventory + your own set of playbooks + host definitions

```
[freznicek@lenovo-t14 /tmp 0]$ git clone git@gitlab.ics.muni.cz:cloud/g2/infra-config.git infra-config.git.alt2
[freznicek@lenovo-t14 /tmp 0]$ cd infra-config.git.alt2
[freznicek@lenovo-t14 infra-config.git.alt2 0]$ echo -n XYZ > .vault_pass
[freznicek@lenovo-t14 infra-config.git.alt2 0]$ grep -v fdsfgsdf ansible_hosts_dev group_vars/dev_all/* group_vars/dev_compute/* group_vars/dev_controlplane/* host_vars/compute-001.priv.dev.cloud.muni.cz.yml host_vars/compute-002.priv.dev.cloud.muni.cz.yml host_vars/controlplane-001.priv.dev.cloud.muni.cz.yml host_vars/controlplane-002.priv.dev.cloud.muni.cz.yml host_vars/controlplane-003.priv.dev.cloud.muni.cz.yml my_play_automated_common.yml
ansible_hosts_dev:[hosts:children]
ansible_hosts_dev:dev_controlplane
ansible_hosts_dev:dev_compute
ansible_hosts_dev:dev_all
ansible_hosts_dev:
ansible_hosts_dev:[dev_all:children]
ansible_hosts_dev:dev_controlplane_hw
ansible_hosts_dev:dev_compute_hw
ansible_hosts_dev:
ansible_hosts_dev:[dev_controlplane:children]
ansible_hosts_dev:dev_controlplane_hw
ansible_hosts_dev:
ansible_hosts_dev:[dev_compute:children]
ansible_hosts_dev:dev_compute_hw
ansible_hosts_dev:
ansible_hosts_dev:[dev_controlplane_hw]
ansible_hosts_dev:controlplane-[001:003].priv.dev.cloud.muni.cz ansible_host="{{ ip_mgmt }}"
ansible_hosts_dev:
ansible_hosts_dev:[dev_compute_hw]
ansible_hosts_dev:compute-[001:002].priv.dev.cloud.muni.cz ansible_host="{{ ip_mgmt }}"
group_vars/dev_all/main.yml:---
group_vars/dev_all/main.yml:# prod-brno hypervisor DNS zone
group_vars/dev_all/main.yml:dns_zone: cloud.muni.cz
group_vars/dev_all/main.yml:
group_vars/dev_all/main.yml:patch_enabled: false
group_vars/dev_all/main.yml:
group_vars/dev_all/main.yml:# shell configuration
group_vars/dev_all/main.yml:bash_root_ps1: '[\[\e[0;31m\]\u\[\e[0m\]@\h:\[\e[0;31m\]prod-brno\[\e[0m\]~\[$( [ "$?" == "0" ] && echo -n "\e[0;32m\]$_bash_prompt_exit_code\[\e[0m" || echo -n "\e[0;31m\]$_bash_prompt_exit_code\[\e[0m")\]]\$ '
group_vars/dev_all/main.yml:bash_nonroot_ps1: '[\[\e[0;33m\]\u\[\e[0m\]@\h:\[\e[0;31m\]prod-brno\[\e[0m\]~\[$( [ "$?" == "0" ] && echo -n "\e[0;32m\]$_bash_prompt_exit_code\[\e[0m" || echo -n "\e[0;31m\]$_bash_prompt_exit_code\[\e[0m")\]]\$ '
group_vars/dev_all/main.yml:
group_vars/dev_compute/main.yml:---
group_vars/dev_compute/main.yml:machine_role:
group_vars/dev_compute/main.yml:  - "hw_machine"
group_vars/dev_compute/main.yml:  - "custom_nat"
group_vars/dev_compute/main.yml:
group_vars/dev_compute/main.yml:cgroupsv2_disabled: true
group_vars/dev_compute/main.yml:
group_vars/dev_compute/main.yml:group_packages:
group_vars/dev_compute/main.yml:  - "libvirt-clients"
group_vars/dev_controlplane/main.yml:---
group_vars/dev_controlplane/main.yml:machine_role:
group_vars/dev_controlplane/main.yml:  - "hw_machine"
host_vars/compute-001.priv.dev.cloud.muni.cz.yml:---
host_vars/compute-001.priv.dev.cloud.muni.cz.yml:
host_vars/compute-001.priv.dev.cloud.muni.cz.yml:# compute-[001:002].priv.dev.cloud.muni.cz
host_vars/compute-001.priv.dev.cloud.muni.cz.yml:ip_mgmt: &ip 10.0.0.237
host_vars/compute-001.priv.dev.cloud.muni.cz.yml:ip_data: *ip
host_vars/compute-001.priv.dev.cloud.muni.cz.yml:ip_ipmi: *ip
host_vars/compute-001.priv.dev.cloud.muni.cz.yml:
host_vars/compute-001.priv.dev.cloud.muni.cz.yml:k8s_config:
host_vars/compute-001.priv.dev.cloud.muni.cz.yml:  hostnames: [compute-003.cluster.local, compute-003]
host_vars/compute-001.priv.dev.cloud.muni.cz.yml:
host_vars/compute-002.priv.dev.cloud.muni.cz.yml:---
host_vars/compute-002.priv.dev.cloud.muni.cz.yml:
host_vars/compute-002.priv.dev.cloud.muni.cz.yml:# compute-[001:002].priv.dev.cloud.muni.cz
host_vars/compute-002.priv.dev.cloud.muni.cz.yml:ip_mgmt: &ip 10.0.0.219
host_vars/compute-002.priv.dev.cloud.muni.cz.yml:ip_ipmi: *ip
host_vars/compute-002.priv.dev.cloud.muni.cz.yml:ip_data: *ip
host_vars/compute-002.priv.dev.cloud.muni.cz.yml:
host_vars/compute-002.priv.dev.cloud.muni.cz.yml:k8s_config:
host_vars/compute-002.priv.dev.cloud.muni.cz.yml:  hostnames: [compute-003.cluster.local, compute-003]
host_vars/compute-002.priv.dev.cloud.muni.cz.yml:
host_vars/controlplane-001.priv.dev.cloud.muni.cz.yml:---
host_vars/controlplane-001.priv.dev.cloud.muni.cz.yml:
host_vars/controlplane-001.priv.dev.cloud.muni.cz.yml:# controlplane-[001:003].priv.dev.cloud.muni.cz
host_vars/controlplane-001.priv.dev.cloud.muni.cz.yml:ip_data: &ip 10.0.0.143
host_vars/controlplane-001.priv.dev.cloud.muni.cz.yml:ip_mgmt: *ip
host_vars/controlplane-001.priv.dev.cloud.muni.cz.yml:ip_ipmi: *ip
host_vars/controlplane-001.priv.dev.cloud.muni.cz.yml:
host_vars/controlplane-001.priv.dev.cloud.muni.cz.yml:k8s_config:
host_vars/controlplane-001.priv.dev.cloud.muni.cz.yml:  hostnames: [controlplane-001.cluster.local, controlplane-001]
host_vars/controlplane-001.priv.dev.cloud.muni.cz.yml:
host_vars/controlplane-002.priv.dev.cloud.muni.cz.yml:---
host_vars/controlplane-002.priv.dev.cloud.muni.cz.yml:
host_vars/controlplane-002.priv.dev.cloud.muni.cz.yml:# controlplane-[001:003].priv.dev.cloud.muni.cz
host_vars/controlplane-002.priv.dev.cloud.muni.cz.yml:ip_data: &ip 10.0.0.168
host_vars/controlplane-002.priv.dev.cloud.muni.cz.yml:ip_mgmt: *ip
host_vars/controlplane-002.priv.dev.cloud.muni.cz.yml:ip_ipmi: *ip
host_vars/controlplane-002.priv.dev.cloud.muni.cz.yml:
host_vars/controlplane-002.priv.dev.cloud.muni.cz.yml:k8s_config:
host_vars/controlplane-002.priv.dev.cloud.muni.cz.yml:  hostnames: [controlplane-002.cluster.local, controlplane-002]
host_vars/controlplane-002.priv.dev.cloud.muni.cz.yml:
host_vars/controlplane-003.priv.dev.cloud.muni.cz.yml:---
host_vars/controlplane-003.priv.dev.cloud.muni.cz.yml:
host_vars/controlplane-003.priv.dev.cloud.muni.cz.yml:# controlplane-[001:003].priv.dev.cloud.muni.cz
host_vars/controlplane-003.priv.dev.cloud.muni.cz.yml:ip_data: &ip 10.0.0.113
host_vars/controlplane-003.priv.dev.cloud.muni.cz.yml:ip_mgmt: *ip
host_vars/controlplane-003.priv.dev.cloud.muni.cz.yml:ip_ipmi: *ip
host_vars/controlplane-003.priv.dev.cloud.muni.cz.yml:
host_vars/controlplane-003.priv.dev.cloud.muni.cz.yml:k8s_config:
host_vars/controlplane-003.priv.dev.cloud.muni.cz.yml:  hostnames: [controlplane-003.cluster.local, controlplane-003]
host_vars/controlplane-003.priv.dev.cloud.muni.cz.yml:
my_play_automated_common.yml:---
my_play_automated_common.yml:
my_play_automated_common.yml:# This playbook replaces Puppet. It is run every day by the service machine.
my_play_automated_common.yml:# You can also run it manually from your computer simply by running the following
my_play_automated_common.yml:# command in the toolbox:
my_play_automated_common.yml:# ansible-playbook play_automated_common.yml -i ./ansible_hosts -l controlplane
my_play_automated_common.yml:
my_play_automated_common.yml:- name: Common server maintenance.
my_play_automated_common.yml:  hosts: hosts
my_play_automated_common.yml:  roles:
my_play_automated_common.yml:    # Validate
my_play_automated_common.yml:    - { role: cli_validation, tags: ['always'] }
my_play_automated_common.yml:    - { role: install_packages, tags: ['always'] }
my_play_automated_common.yml:    - { role: hostname, tags: ['always']}
my_play_automated_common.yml:    - { role: shell, tags: ['always']}
my_play_automated_common.yml:    - { role: ssh_keys, tags: ['always']}
my_play_automated_common.yml:#    - { role: ipv6, tags: ['always'] }
my_play_automated_common.yml:#    - { role: networking, tags: ['always']}
my_play_automated_common.yml:#    - { role: iptables, tags: ['always'] }
my_play_automated_common.yml:    - { role: unattended_upgrades, tags: ['always']}
my_play_automated_common.yml:    - { role: systemd_timesyncd, tags: ['always']}
my_play_automated_common.yml:    - { role: hosts_file, tags: ['always']}
my_play_automated_common.yml:#    - { role: mount_disk_partitions, tags: ['always']}
```


#### Launch infra config (sshuttle enabled)


```
ansible-playbook -i ansible_hosts_dev -l dev_compute  my_play_automated_common.yml
ansible-playbook -i ansible_hosts_dev -l dev_controlplane  my_play_automated_common.yml
```

If you do not have ansible you may use [infra-config toolbox](https://gitlab.ics.muni.cz/cloud/g2/infra-config#toolbox)


---

