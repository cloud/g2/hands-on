# Cloud hands-on part 4, G1 to G2 OpenStack cloud migration of real projects

## Objectives
* [ ] TODO

## Before hands-on

Get familiar with [hands-on #3](/parts/3/README.md).

## List of projects to process:

TODO

* other projects (data set/not set, we will just create G2 projects, migration takes place later)
  | project                                   | description                | migration date | notes            |
  | ----------------------------------------- | -------------------------- | -------------- | ---------------- |
  | meta-acc2                                 | #volumes: 1, #servers: 1   | 2024-05-27     | `picked: freznicek`                |
  | meta-alpha-charges                        | #volumes: 3, #servers: 1   | 2024-05-27     | `picked: jjezek`                 |
  | meta-amtdb                                | #volumes: 6, #servers: 3   | 2024-05-27     | `picked: jsmrcka`                 |
  | meta-analysis-for-nitrogen-kinetics       | #volumes: 3, #servers: 1   | 2024-05-27     | `picked: jkrystof`                 |
  | meta-archeogeofyzika                      | #volumes: 1, #servers: 1   | 2024-05-27     | `picked: jsmrcka`                 |
  | meta-automatic-summarization              | #volumes: 5, #servers: 1   | 2024-05-27     | `picked: jjezek`                 |
  | meta-selkies | | | | |
  | meta-bapm                                 | #volumes: 10, #servers: 10 | 2024-05-28     | `picked: freznicek`                 |
  | meta-binding-of-pyrazinamide-derivativees | #volumes: 2, #servers: 1   | 2024-05-29     |                  |
  | meta-binocular-rivalry                    | #volumes: 4, #servers: 1   | N/A            | ephem+gpu flavor |
  | meta-c-scale                              | #volumes: 3, #servers: 2   | 2024-05-29     | `picked: rpolasek`                |
  | meta-aicope                               | #volumes: 3, #servers: 2   | N/A            | ephem+gpu flavor,  |

## Steps

TODO