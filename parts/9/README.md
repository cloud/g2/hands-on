# Cloud hands-on part 9, HW installation via Maas + new nodes are connected in Openstack

## Objectives
# Cloud hands-on part 9: HW provision, MAAS


### Checklist

### Session 1
* [ ] Přihlásím se do MAAS
    * http://maas.mgmt.priv.g2.cloud.muni.cz:5240/MAAS/
* [ ] Prošel jse základní popis funkcionality a životního cyklus v KB
    * https://gitlab.ics.muni.cz/cloud/knowledgebase/-/blob/master/howtos/g2/hypervisor-deployment.md
* [ ] V MAAS jsem si změnil heslo
* [ ] Vytvořil jsem si SSH klíč a uložil do MAAS
* [ ] Znám IPMI heslo pro uživatele cloud_admin
    * HINTS:
        * group_vars/hosts/vault.yml
        * https://gitlab.ics.muni.cz/cloud/ansible-infra/-/settings/ci_cd => _CLOUD_TEAM_REPO_ANSIBLE_VAULT_PASS
        * `yq eval .ipmi_password group_vars/hosts/vault_hands_on_fake.yml | ansible-vault decrypt`
* [ ] V seznamu strojů jsem si zaklejmoval stroj pro mé testování
    * https://docs.google.com/spreadsheets/d/13ZDQSKbtPwJJlt6f34qIEPXGCbVrrVUivHydj3RZ_LI/
* [ ] Nalogoval jsem se do IPMI management konzole testovacího stroje a provedl restart
* [ ] Vstoupil jsem do BIOSu a našel nastaveni IPMI pro nastaveni IP adresy a vychozi brany pro IPMI rozhrani
* [ ] Vyzkoušel jsem si použití nástroje ipmitool: ipmitool -f password.txt -I lanplus -U cloud_admin -H <IP> shell
    * `bmc info`
    * `lan print 1`
    * `user list`
* [ ] Provedl jsem ruční deployment stroje přes MAAS
    * [ ] Znám [schéma rozdělení disku](https://gitlab.ics.muni.cz/cloud/knowledgebase/-/blob/master/team/onboarding/g2-infra-introduction.md)
    * [ ] Prosel jsem automatizaci [maas_provision.sh](https://gitlab.ics.muni.cz/cloud/knowledgebase/-/blob/master/howtos/g2/maas/automation/maas_provision.sh?ref_type=heads) a rozumim flow celeho skriptu

### Session 2
* [ ] Provedl jsem automatizovaný deployment stroje přes MAAS
  * [ ] Vygeneroval jsem si API klíč pro můj MAAS profil
  * [ ] Nainstaloval jsem si [maas klienta](https://maas.io/docs/how-to-install-maas)
  * [ ] [Nalogoval](https://maas.io/docs/tutorial-try-the-maas-cli) jsem se do CLI klienta (`maas login <login> http://maas.mgmt.priv.g2.cloud.muni.cz:5240/MAAS/`, vypsal seznam strojů přes příkaz `maas <user_name> machines read` (a odhlásil jsem se `maas logout <login>`)
  * [ ] Můj [zaklejmovay stroj](https://docs.google.com/spreadsheets/d/13ZDQSKbtPwJJlt6f34qIEPXGCbVrrVUivHydj3RZ_LI) jse posunul do stavu deployed pomocí skriptu [maas_provision.sh](https://gitlab.ics.muni.cz/cloud/knowledgebase/-/blob/master/howtos/g2/maas/automation/maas_provision.sh?ref_type=heads)
* [ ] Aplikoval jsem infra-config na můj hands-on stroj
  * HINTS:
    - [inspirace HDH](https://gitlab.ics.muni.cz/cloud/g2/infra-config/-/commit/d7601daef607f66179fa063d02b7abe78db2d2f3) + [pohled na zasíťované stroje v G2](https://gitlab.ics.muni.cz/cloud/knowledgebase/-/blob/master/howtos/g2/network_configuration_and_setup/g2_toplogy.drawio.png)
    - `ansible_hosts_brno` nese záznam můj hands-on stroj
    -  vím jak pro můj hands-on stroj zjistit `nova_disk_partition_uuid` (https://gitlab.ics.muni.cz/cloud/knowledgebase/-/blob/master/howtos/g2/hypervisor-deployment.md)

    - vím k čemu jsou Ansible parametry `--check` a `--limit `a aktivně je používám
    ```
    ansible-playbook -i ansible_hosts_brno -l jkrystof-ho-9b --user=ubuntu --become --become-user=root  play_hw_provision.yml --check
    ```
    - zajístím správnou verzi C-Groups spuštením [play_single_grub.yml](https://gitlab.ics.muni.cz/cloud/g2/infra-config/-/blob/master/play_single_grub.yml)
    ```
    ansible-playbook -i ansible_hosts_brno -l jkrystof-ho-9b --user=ubuntu --become --become-user=root  play_single_grub.yml
    ```
    - zakončím rebootem stroje
* [ ] Mám checkoutované repo Kubespray z aktuální větve  [origin/downstream-kubernetes-v1.24.17](https://gitlab.ics.muni.cz/cloud/g2/kubespray/-/tree/downstream-kubernetes-v1.24.17?ref_type=heads)
* [ ] Mám checkoutované repo [kubernetes-deployments](https://gitlab.ics.muni.cz/cloud/g2/kubernetes-deployments/) z větvě master
* [ ] Připravil jsem si kontejner pro run-time závislosti vyžadované od nástroje [kubespray](https://www.redhat.com/sysadmin/kubespray-deploy-kubernetes)
```
KUBESPRAY_HOME=/home/jank/devel/git_repos/kubespray
MY_SSH_HOME=/home/jank/.ssh
KUBERNETES_DEPLOYMENTS_HOME=/home/jank/devel/git_repos/kubernetes-deployments

podman run \
  -it \
  -e HANDS_ON_HOME="/root/handson-9" \
  --name kubespray_runtime \
  -v $KUBESPRAY_HOME:/tmp/kubespray:ro \
  -v $MY_SSH_HOME:/tmp/ssh:ro \
  -v $KUBERNETES_DEPLOYMENTS_HOME:/tmp/kubernetes-deployments \
  docker.io/library/almalinux:8 \
  /bin/bash -c '\
    mkdir $HANDS_ON_HOME \
    && cp -r /tmp/kubespray $HANDS_ON_HOME \
    && cp -r /tmp/kubernetes-deployments/deployments/prod-brno/configuration.v1.24.17/kubespray/inventory/deployment $HANDS_ON_HOME/kubespray/inventory/deployment \
    && yum -y --nogpgcheck install git-core epel-release mc diffutils \
    && yum -y --nogpgcheck install python39-pip \
    && python3.9 -m pip install -r $HANDS_ON_HOME/kubespray/requirements.txt \
    && bash'
```
* [ ] [Přidal jsem nový stroj](https://github.com/kubernetes-sigs/kubespray/blob/master/docs/operations/nodes.md) do k8s a ověřil funkčnost
  ```
  ansible-playbook -i inventory/deployment/hosts.yaml --user=ubuntu --become --become-user=root scale.yml --limit=jkrystof-ho-9b
  ```
* [ ] Stroj jsem z k8s odebral
  ```
   ansible-playbook -i inventory/deployment/hosts.yaml --user=ubuntu --become --become-user=root remove-node.yml -e node=jkrystof-ho-9b -e reset_nodes=false -e allow_ungraceful_removal=true
  ```
* [ ] Provedl jsem reinstalaci hypervizoru openstack-u
  * odebrani hypervizoru z k8s clusteru
  * MAAS - release vcetne promazani disku
  * MAAS - deploy stroje 
  * infra-config: play_hw_provision + play_single_grub.yml
  * pridani hypervizoru do k8 clusteru
  * oznackovani hypervizoru 
    ```
    CLUSTER=hdg
    ZONE=Brno-A510-R1.34
    FLAVOR=a3
    kubectl label node hdg-001-cerit topology.kubernetes.io/zone=$ZONE
    kubectl label node hdg-002-cerit openstack-compute-node=enabled
    kubectl label node hdg-002-cerit openvswitch=enabled
    kubectl label node hdg-002-cerit ostack-cluster-name=$CLUSTER
    kubectl label node hdg-002-cerit ostack-flavor-type=$FLAVOR```
  * kontrola, ze openstackove komponenty bezi (hdg-002-cerit)
    * kubectl get pods --field-selector spec.nodeName=hdg-002-cerit


### Materials
* https://drive.google.com/drive/u/0/folders/1o9AF8OlyMGiMuj-sXdlMYJFHU3zK6eWA


### Further reading / watching
* https://knowm.org/how-to-create-a-bonded-network-interface/
* https://www.youtube.com/watch?v=7TNPEr00Efc&t=911s
