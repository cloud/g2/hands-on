# Cloud hands-on part 5, G2 Kubernetes-OpenStack cloud deployment - IaaS & Kubernetes

## Objectives
* [ ] Deploy IaaS infractructure for G2 cloud
  * [ ] Summary questions answered
* [ ] Configure & deploy Kubernetes on G2 cloud (VM) IaaS infractructure
  * [ ] Summary questions answered
* [ ] Kubernetes walkthrough
  * [ ] Summary questions answered
* [ ] Managing kubernetes remotely
  * [ ] Summary questions answered
* [ ] Basic Kubernetes operations
  * [ ] Summary questions answered
* [ ] Q&A

## Before hands-on

### 1. Deploy G2 cloud (VM) IaaS infractructure

Follow steps listed [below](#deploy-g2-cloud-vm-iaas-infractructure).

### 2. Configure & deploy Kubernetes on G2 cloud (VM) IaaS infractructure

Follow steps listed [below](#configure-deploy-kubernetes-on-g2-cloud-vm-iaas-infractructure).

### 3. Briefly walk through [onboarding kubernetes introduction](https://gitlab.ics.muni.cz/cloud/knowledgebase/-/blob/master/team/onboarding/kubernetes-introduction.md)

## Steps

We are using [prod-ostrava OpenStack cloud exclusively](https://ostrava.openstack.cloud.e-infra.cz/) in this hands-on.

While working the below steps it is recommended to close all unnecessary applications.

### Deploy G2 cloud (VM) IaaS infractructure

We are about to create following IaaS infrastructure, at this point we are creating VM servers in OpenStack cloud.
![](./pictures/k8s-on-demand-architecture-2.png)


```console
[freznicek@lenovo-t14 ~ 0]$ source ~/c/g2-prod-ostrava-freznicek-meta-cloud-freznicek-training.sh.inc
[freznicek@lenovo-t14 ~ 0]$ openstack version show | grep ident
| Ostrava     | identity       | 3.14    | CURRENT   | https://identity.ostrava.openstack.cloud.e-infra.cz/v3/       | None             | None             |
[freznicek@lenovo-t14 kubernetes 0]$ git clone https://gitlab.ics.muni.cz/cloud/kubernetes/kubernetes-infra-example-dev-ost.git
Cloning into 'kubernetes-infra-example-dev-ost'...
...
[freznicek@lenovo-t14 kubernetes 0]$ cd kubernetes-infra-example-dev-ost
[freznicek@lenovo-t14 kubernetes-infra-example-dev-ost 0]$ git submodule update --init
Submodule 'ansible/kubespray' (git@gitlab.ics.muni.cz:cloud/g2/kubespray.git) registered for path 'ansible/kubespray'
Cloning into '/home/freznicek/prjs/muni/sources/gitlab.ics.muni.cz/kubernetes/kubernetes-infra-example-dev-ost/ansible/kubespray'...
Submodule path 'ansible/kubespray': checked out 'e4f28a7d760dcbcbb15044a7b098d24cdf6e3e59'
[freznicek@lenovo-t14 kubernetes-infra-example-dev-ost 0]$ git checkout freznicek-hands-on-5
branch 'freznicek-hands-on-5' set up to track 'origin/freznicek-hands-on-5'.
Switched to a new branch 'freznicek-hands-on-5'
[freznicek@lenovo-t14 kubernetes-infra-example-dev-ost 0]$ git submodule update --init
[freznicek@lenovo-t14 kubernetes-infra-example-dev-ost 0]$ cd terraform/
[freznicek@lenovo-t14 terraform 0]$ terraform init
Initializing the backend...
Initializing modules...
Downloading git::https://gitlab.ics.muni.cz/cloud/terraform/modules/kubernetes-infra-dev-ost.git?ref=1.1.0 for kubernetes_infra...
- kubernetes_infra in .terraform/modules/kubernetes_infra

Initializing provider plugins...
- Finding terraform-provider-openstack/openstack versions matching "~> 1.54.1"...
- Finding hashicorp/local versions matching "~> 2.4.1"...
- Installing terraform-provider-openstack/openstack v1.54.1...
- Installed terraform-provider-openstack/openstack v1.54.1 (self-signed, key ID 4F80527A391BEFD2)
- Installing hashicorp/local v2.4.1...
- Installed hashicorp/local v2.4.1 (signed by HashiCorp)

Partner and community providers are signed by their developers.
If you'd like to know more about provider signing, you can read about it here:
https://www.terraform.io/docs/cli/plugins/signing.html

Terraform has created a lock file .terraform.lock.hcl to record the provider
selections it made above. Include this file in your version control repository
so that Terraform can guarantee to make the same selections by default when
you run "terraform init" in the future.

Terraform has been successfully initialized!

You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.
[freznicek@lenovo-t14 terraform 0]$ terraform validate
Success! The configuration is valid.
[freznicek@lenovo-t14 terraform 0]$ terraform plan --out plan
...
Plan: 46 to add, 0 to change, 0 to destroy.

Changes to Outputs:
  + kubernetes_infra_bastion_external_ip = (known after apply)
  + kubernetes_infra_control_instance_ip = [
      + (known after apply),
      + (known after apply),
      + (known after apply),
    ]
  + kubernetes_infra_floating_vip        = (known after apply)
  + kubernetes_infra_internal_vip        = "10.0.0.5"
  + kubernetes_infra_worker_instance_ip  = [
      + (known after apply),
      + (known after apply),
    ]
╷
│ Warning: Argument is deprecated
│
│   with provider["registry.terraform.io/terraform-provider-openstack/openstack"],
│   on <empty> line 0:
│   (source code not available)
│
│ Users not using loadbalancer resources can ignore this message. Support for neutron-lbaas will be removed on next
│ major release. Octavia will be the only supported method for loadbalancer resources. Users using octavia will
│ have to remove 'use_octavia' option from the provider configuration block. Users using neutron-lbaas will have to
│ migrate/upgrade to octavia.
╵

───────────────────────────────────────────────────────────────────────────────────────────────────────────────────

Saved the plan to: plan

To perform exactly these actions, run the following command to apply:
    terraform apply "plan"
[freznicek@lenovo-t14 terraform 0]$ terraform apply "plan"
module.kubernetes_infra.openstack_compute_keypair_v2.pubkey: Creating...
...
╷
│ Warning: Argument is deprecated
│
│   with provider["registry.terraform.io/terraform-provider-openstack/openstack"],
│   on <empty> line 0:
│   (source code not available)
│
│ Users not using loadbalancer resources can ignore this message. Support for neutron-lbaas will be removed on next
│ major release. Octavia will be the only supported method for loadbalancer resources. Users using octavia will
│ have to remove 'use_octavia' option from the provider configuration block. Users using neutron-lbaas will have to
│ migrate/upgrade to octavia.
╵

Apply complete! Resources: 46 added, 0 changed, 0 destroyed.

Outputs:

kubernetes_infra_bastion_external_ip = "195.113.243.177"
kubernetes_infra_control_instance_ip = [
  "10.0.0.82",
  "10.0.0.220",
  "10.0.0.124",
]
kubernetes_infra_floating_vip = "195.113.243.48"
kubernetes_infra_internal_vip = "10.0.0.5"
kubernetes_infra_worker_instance_ip = [
  "10.0.0.142",
  "10.0.0.253",
]
[freznicek@lenovo-t14 terraform 0]$ openstack server list | grep hands-on-5
| 1722823a-137b-4f3c-9156-1f9a15c5cb10 | cloud-hands-on-5-control-1         | ACTIVE | cloud-hands-on-5_network=10.0.0.82                      | ubuntu-jammy-x86_64      | c3.8core-16ram |
| 22fec933-5522-4262-baa2-0543917cb63e | cloud-hands-on-5-control-3         | ACTIVE | cloud-hands-on-5_network=10.0.0.124                     | ubuntu-jammy-x86_64      | c3.8core-16ram |
| 6687afc5-b89d-4bb6-a51f-fff976829366 | cloud-hands-on-5-control-2         | ACTIVE | cloud-hands-on-5_network=10.0.0.220                     | ubuntu-jammy-x86_64      | c3.8core-16ram |
| f8d397c7-5a60-45c4-9fd3-059bcebce1ee | cloud-hands-on-5-workers-1         | ACTIVE | cloud-hands-on-5_network=10.0.0.142                     | ubuntu-jammy-x86_64      | g2.medium      |
| 9ebc9e88-3d95-4456-ba26-528ecab67dd4 | cloud-hands-on-5-workers-2         | ACTIVE | cloud-hands-on-5_network=10.0.0.253                     | ubuntu-jammy-x86_64      | g2.medium      |
| 5b3c32f6-0d7d-4e5e-87c0-4ee406c026c0 | cloud-hands-on-5-bastion-server    | ACTIVE | cloud-hands-on-5_network=10.0.0.31, 195.113.243.177     | ubuntu-jammy-x86_64      | g2.tiny        |
[freznicek@lenovo-t14 terraform 255]$ ssh-keygen -R 195.113.243.177
# test access bastion
[freznicek@lenovo-t14 terraform 255]$ ssh ubuntu@195.113.243.177 'uname -a'
Linux cloud-hands-on-5-bastion-server 5.15.0-73-generic #80-Ubuntu SMP Mon May 15 15:18:26 UTC 2023 x86_64 x86_64 x86_64 GNU/Linux
# test access controlplane-1
[freznicek@lenovo-t14 terraform 0]$ ssh -J ubuntu@195.113.243.177 ubuntu@10.0.0.82 'uname -a'
Linux cloud-hands-on-5-control-1 5.15.0-73-generic #80-Ubuntu SMP Mon May 15 15:18:26 UTC 2023 x86_64 x86_64 x86_64 GNU/Linux
```

#### Wrap-up questions

1. How was generated ansible inventory file?

### Configure & deploy Kubernetes on G2 cloud (VM) IaaS infractructure

We are about to add software components (kubernetes and proxy) inside already created IaaS infrastructure now.
![](./pictures/k8s-on-demand-architecture-2.png)

```console
# prepare kubespray configuration:
[freznicek@lenovo-t14 kubernetes-infra-example-dev-ost 0]$ git diff
diff --git a/ansible/group_vars/all/openstack.yml b/ansible/group_vars/all/openstack.yml
index 600c9b7..78f6526 100644
--- a/ansible/group_vars/all/openstack.yml
+++ b/ansible/group_vars/all/openstack.yml
@@ -7,9 +7,9 @@ external_openstack_lbaas_provider: amphora
 ## Application credentials to authenticate against Keystone API
 ## Those settings will take precedence over username and password that might be set your environment
 ## All of them are required
-external_openstack_application_credential_name: <FILL_APPLICATION_CREDENTIALS>
-external_openstack_application_credential_id: <FILL_APPLICATION_CREDENTIALS>
-external_openstack_application_credential_secret: <FILL_APPLICATION_CREDENTIALS>
+external_openstack_application_credential_name: &cred-id 2394a399a1314fd09458a351f8ef5727
+external_openstack_application_credential_id: *cred-id
+external_openstack_application_credential_secret: mpVW*********************************************************6AQ

 ## The tag of the external OpenStack Cloud Controller image
 external_openstack_cloud_controller_image_tag: "latest"
@@ -18,5 +18,5 @@ external_openstack_cloud_controller_image_tag: "latest"
 ## Make sure to source in the openstack credentials
 cinder_csi_enabled: true
 cinder_csi_controller_replicas: 1
-cinder_application_credential_name: <FILL_APPLICATION_CREDENTIALS>
+cinder_application_credential_name: *cred-id


# launch deploying k8s from container to not break your version of tooling
# kubespray require specific version of ansible etc..

# mounting following items:
# * the repository itself
# * ssh keypair directory (for keypair authentication to infrastructure)
[freznicek@lenovo-t14 kubernetes-infra-example-dev-ost 0]$ podman run -it -v ${HOME}/.ssh:/root/.ssh:ro \
                                                                          -v ${HOME}/prjs/muni/sources/gitlab.ics.muni.cz/kubernetes/kubernetes-infra-example-dev-ost:/tmp/kubernetes-infra-example-dev-ost:ro \
                                                                          -v ${HOME}/c/g2-prod-ostrava-freznicek-meta-cloud-freznicek-training.sh.inc:/root/g2-prod-ostrava-freznicek-meta-cloud-freznicek-training.sh.inc:ro
                                                                          ubuntu:jammy

# doublecheck input mounted data
root@a6cee4576901:/# ls -la ~/.ssh/id_rsa
-rw-------. 1 root root 3381 Mar 18  2022 /root/.ssh/id_rsa
root@a6cee4576901:/# ls -la /tmp/kubernetes-infra-example-dev-ost
total 32
drwxr-xr-x. 5 root root 4096 May 31 12:53 .
drwxrwxrwt. 1 root root 4096 May 31 13:24 ..
drwxr-xr-x. 9 root root 4096 May 31 12:54 .git
-rw-r--r--. 1 root root  152 May 31 12:53 .gitmodules
-rw-r--r--. 1 root root 2579 May 31 12:53 README.md
drwxr-xr-x. 5 root root 4096 May 31 12:56 ansible
drwxr-xr-x. 3 root root 4096 May 31 12:56 terraform

# install needed tools I
root@6e0a59d331f5:/# apt update
...
root@6e0a59d331f5:/# apt install python3-minimal python3-pip openssh-client vim mc ncat
...

# doublecheck we are able to connect to the bastion
root@6e0a59d331f5:/# ssh ubuntu@195.113.243.177 'uname -a'
Linux cloud-hands-on-5-bastion-server 5.15.0-73-generic #80-Ubuntu SMP Mon May 15 15:18:26 UTC 2023 x86_64 x86_64 x86_64 GNU/Linux

# make clone of the repository within /root
root@6e0a59d331f5:/# cp -r /tmp/kubernetes-infra-example-dev-ost /root

# install needed tools II (kubespray)
root@6e0a59d331f5:/# cd /root/kubernetes-infra-example-dev-ost/ansible/kubespray/
root@6e0a59d331f5:~/kubernetes-infra-example-dev-ost/ansible/kubespray# python3 -mpip install -r requirements.txt
...

# install needed tools III (ostack client apps)
root@6e0a59d331f5:~/kubernetes-infra-example-dev-ost/ansible/kubespray# python3 -mpip install python-novaclient python-keystoneclient python-openstackclient python-heatclient python-neutronclient python-cinderclient python-octaviaclient python-glanceclient

# review your infrastructure inventory (generated by terraform)
root@6e0a59d331f5:~/kubernetes-infra-example-dev-ost/ansible/kubespray# cat ../ansible_inventory
##########################################################################
#                                                                        #
#    THIS FILE IS CONTROLLED BY TERRAFORM -  DO NOT CHANGE MANUALLY!     #
#                                                                        #
##########################################################################

[all]
cloud-hands-on-5-control-1 ansible_host=10.0.0.82
cloud-hands-on-5-control-2 ansible_host=10.0.0.220
cloud-hands-on-5-control-3 ansible_host=10.0.0.124
cloud-hands-on-5-workers-1 ansible_host=10.0.0.142
cloud-hands-on-5-workers-2 ansible_host=10.0.0.253

## configure a bastion host if your nodes are not directly reachable
[bastion]
bastion ansible_host=195.113.243.177 ansible_user=ubuntu ansible_internal_host=10.0.0.31

[kube_control_plane]
cloud-hands-on-5-control-1
cloud-hands-on-5-control-2
cloud-hands-on-5-control-3

[etcd]
cloud-hands-on-5-control-1
cloud-hands-on-5-control-2
cloud-hands-on-5-control-3

[kube_node]
cloud-hands-on-5-control-1
cloud-hands-on-5-control-2
cloud-hands-on-5-control-3
cloud-hands-on-5-workers-1
cloud-hands-on-5-workers-2

[calico_rr]

[k8s_cluster:children]
kube_control_plane
kube_node
calico_rr

# load ostack creds, list ostack VMs
root@6e0a59d331f5:~/kubernetes-infra-example-dev-ost/ansible/kubespray# source /root/g2-prod-ostrava-freznicek-meta-cloud-freznicek-training.sh.inc
root@6e0a59d331f5:~/kubernetes-infra-example-dev-ost/ansible/kubespray# openstack server list | grep hands-on-5
| 1722823a-137b-4f3c-9156-1f9a15c5cb10 | cloud-hands-on-5-control-1         | ACTIVE | cloud-hands-on-5_network=10.0.0.82                      | ubuntu-jammy-x86_64 | c3.8core-16ram |
| 22fec933-5522-4262-baa2-0543917cb63e | cloud-hands-on-5-control-3         | ACTIVE | cloud-hands-on-5_network=10.0.0.124                     | ubuntu-jammy-x86_64 | c3.8core-16ram |
| 6687afc5-b89d-4bb6-a51f-fff976829366 | cloud-hands-on-5-control-2         | ACTIVE | cloud-hands-on-5_network=10.0.0.220                     | ubuntu-jammy-x86_64 | c3.8core-16ram |
| f8d397c7-5a60-45c4-9fd3-059bcebce1ee | cloud-hands-on-5-workers-1         | ACTIVE | cloud-hands-on-5_network=10.0.0.142                     | ubuntu-jammy-x86_64 | g2.medium      |
| 9ebc9e88-3d95-4456-ba26-528ecab67dd4 | cloud-hands-on-5-workers-2         | ACTIVE | cloud-hands-on-5_network=10.0.0.253                     | ubuntu-jammy-x86_64 | g2.medium      |
| 5b3c32f6-0d7d-4e5e-87c0-4ee406c026c0 | cloud-hands-on-5-bastion-server    | ACTIVE | cloud-hands-on-5_network=10.0.0.31, 195.113.243.177     | ubuntu-jammy-x86_64 | g2.tiny        |

# deploy kubernetes
root@6e0a59d331f5:~/kubernetes-infra-example-dev-ost/ansible/kubespray# ansible-playbook -i ../ansible_inventory --user=ubuntu --become --become-user=root cluster.yml
... (takes around 20-40 minutes)
PLAY RECAP *****************************************************************************************************************************
bastion                    : ok=6    changed=0    unreachable=0    failed=0    skipped=14   rescued=0    ignored=0
cloud-hands-on-5-control-1 : ok=860  changed=38   unreachable=0    failed=0    skipped=1348 rescued=0    ignored=3
cloud-hands-on-5-control-2 : ok=734  changed=22   unreachable=0    failed=0    skipped=1201 rescued=0    ignored=1
cloud-hands-on-5-control-3 : ok=736  changed=22   unreachable=0    failed=0    skipped=1199 rescued=0    ignored=1
cloud-hands-on-5-workers-1 : ok=549  changed=12   unreachable=0    failed=0    skipped=840  rescued=0    ignored=1
cloud-hands-on-5-workers-2 : ok=549  changed=12   unreachable=0    failed=0    skipped=840  rescued=0    ignored=1
localhost                  : ok=3    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0

Friday 31 May 2024  14:43:49 +0000 (0:00:00.094)       0:24:41.320 ************
===============================================================================
kubernetes-apps/ansible : Kubernetes Apps | Start Resources -------------------------------------------------------------------- 92.12s
kubernetes-apps/csi_driver/cinder : Cinder CSI Driver | Apply Manifests -------------------------------------------------------- 79.19s
network_plugin/calico : Calico | Create Calico Kubernetes datastore resources -------------------------------------------------- 67.82s
kubernetes-apps/metrics_server : Metrics Server | Apply manifests -------------------------------------------------------------- 49.17s
etcd : Gen_certs | Write etcd member/admin and kube_control_plane client certs to other etcd nodes ----------------------------- 40.68s
kubernetes/kubeadm : Restart all kube-proxy pods to ensure that they load the new configmap ------------------------------------ 40.55s
policy_controller/calico : Start of Calico kube controllers -------------------------------------------------------------------- 40.46s
kubernetes-apps/snapshots/snapshot-controller : Snapshot Controller | Apply Manifests ------------------------------------------ 39.02s
kubernetes-apps/external_cloud_controller/openstack : External OpenStack Cloud Controller | Apply Manifests -------------------- 25.42s
kubernetes-apps/ansible : Kubernetes Apps | Lay Down CoreDNS templates --------------------------------------------------------- 21.83s
kubernetes/node-label : Kubernetes Apps | Wait for kube-apiserver -------------------------------------------------------------- 19.72s
network_plugin/calico : Start Calico resources --------------------------------------------------------------------------------- 19.52s
kubernetes/control-plane : Upload certificates so they are fresh and not expired ----------------------------------------------- 18.45s
kubernetes-apps/metrics_server : Metrics Server | Create manifests ------------------------------------------------------------- 16.95s
kubernetes-apps/csi_driver/cinder : Cinder CSI Driver | Generate Manifests ----------------------------------------------------- 11.99s
network_plugin/calico : Check if calico ready ---------------------------------------------------------------------------------- 11.83s
kubernetes/control-plane : kubeadm | Check apiserver.crt SAN hosts ------------------------------------------------------------- 11.37s
network_plugin/calico : Calico | Create calico manifests ----------------------------------------------------------------------- 11.37s
kubernetes-apps/snapshots/cinder-csi : Kubernetes Snapshots | Add Cinder CSI Snapshot Class ------------------------------------ 10.99s
kubernetes-apps/csi_driver/csi_crd : CSI CRD | Apply Manifests ----------------------------------------------------------------- 10.84s
root@6e0a59d331f5:~/kubernetes-infra-example-dev-ost/ansible/kubespray# echo $?
0


# when you encounter a failure then re-run exactly same command again...
```
#### Wrap-up questions

1. What techonologies we uses to build `G2 hands-on #5` k8s cloud?
1. What (kubernetes) components are running on a] master (control) nodes and b] worker nodes?


#### Using Bastion as proxy to kubernetes API

```console
root@6e0a59d331f5:~# cd /root/kubernetes-infra-example-dev-ost/ansible/01-playbook
root@6e0a59d331f5:~/kubernetes-infra-example-dev-ost/ansible/01-playbook# python3  -mpip install -r requirements.txt
...
root@6e0a59d331f5:~/kubernetes-infra-example-dev-ost/ansible/01-playbook# ansible-galaxy collection install --requirements-file requirements.yml
...

root@6e0a59d331f5:~/kubernetes-infra-example-dev-ost/ansible/01-playbook# ansible-playbook -v -i ../ansible_inventory --user=ubuntu --become --become-user=root play.yml
Using /root/kubernetes-infra-example-dev-ost/ansible/01-playbook/ansible.cfg as config file
...
PLAY RECAP *****************************************************************************************************************************
bastion                    : ok=6    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
localhost                  : ok=3    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0

Friday 31 May 2024  15:09:19 +0000 (0:00:01.673)       0:00:07.005 ************
===============================================================================
haproxy : Ensure HAProxy is installed. ------------------------------------------------------------------------------------------ 2.48s
storageClass : Apply storageClass.yaml ------------------------------------------------------------------------------------------ 1.67s
haproxy : Copy HAProxy configuration in place. ---------------------------------------------------------------------------------- 0.88s
haproxy : Ensure HAProxy is started and enabled on boot. ------------------------------------------------------------------------ 0.82s
haproxy : Ensure HAProxy is enabled (so init script will start it on Debian). --------------------------------------------------- 0.42s
haproxy : Get HAProxy version. -------------------------------------------------------------------------------------------------- 0.39s
modify : Modify admin.conf ------------------------------------------------------------------------------------------------------ 0.27s
haproxy : Set HAProxy version. -------------------------------------------------------------------------------------------------- 0.04s
modify : Include kubeconfig_localhost variable ---------------------------------------------------------------------------------- 0.02s
root@6e0a59d331f5:~/kubernetes-infra-example-dev-ost/ansible/01-playbook#
```

#### Check basic kubernetes functionality

```console
# get bastion and controlplane
root@6e0a59d331f5:~/kubernetes-infra-example-dev-ost/ansible/01-playbook# openstack server list | grep hands-on-5 | grep -E 'bastion|control-1'
| 1722823a-137b-4f3c-9156-1f9a15c5cb10 | cloud-hands-on-5-control-1         | ACTIVE | cloud-hands-on-5_network=10.0.0.82                      | ubuntu-jammy-x86_64 | c3.8core-16ram |
| 5b3c32f6-0d7d-4e5e-87c0-4ee406c026c0 | cloud-hands-on-5-bastion-server    | ACTIVE | cloud-hands-on-5_network=10.0.0.31, 195.113.243.177     | ubuntu-jammy-x86_64 | g2.tiny        |

# execute few kubectl on controlplane via bastion
root@6e0a59d331f5:~/kubernetes-infra-example-dev-ost/ansible/01-playbook# ssh -J ubuntu@195.113.243.177 ubuntu@10.0.0.82 'uname -a;sudo kubectl version;sudo kubectl get no'
Linux cloud-hands-on-5-control-1 5.15.0-73-generic #80-Ubuntu SMP Mon May 15 15:18:26 UTC 2023 x86_64 x86_64 x86_64 GNU/Linux
Client Version: version.Info{Major:"1", Minor:"24", GitVersion:"v1.24.17", GitCommit:"22a9682c8fe855c321be75c5faacde343f909b04", GitTreeState:"clean", BuildDate:"2023-08-23T23:44:35Z", GoVersion:"go1.20.7", Compiler:"gc", Platform:"linux/amd64"}
Kustomize Version: v4.5.4
Server Version: version.Info{Major:"1", Minor:"24", GitVersion:"v1.24.17", GitCommit:"22a9682c8fe855c321be75c5faacde343f909b04", GitTreeState:"clean", BuildDate:"2023-08-23T23:37:25Z", GoVersion:"go1.20.7", Compiler:"gc", Platform:"linux/amd64"}
WARNING: This version information is deprecated and will be replaced with the output from kubectl version --short.  Use --output=yaml|json to get the full version.
NAME                         STATUS   ROLES           AGE   VERSION
cloud-hands-on-5-control-1   Ready    control-plane   74m   v1.24.17
cloud-hands-on-5-control-2   Ready    control-plane   71m   v1.24.17
cloud-hands-on-5-control-3   Ready    control-plane   70m   v1.24.17
cloud-hands-on-5-workers-1   Ready    <none>          67m   v1.24.17
cloud-hands-on-5-workers-2   Ready    <none>          67m   v1.24.17

# locate k8s admin kubectl configuration
root@6e0a59d331f5:~# ls -la /root/kubernetes-infra-example-dev-ost/ansible/artifacts/admin.conf
-rw-------. 1 root root 5707 May 31 15:00 /root/kubernetes-infra-example-dev-ost/ansible/artifacts/admin.conf

# check whether k8s API server is UP
root@6e0a59d331f5:~# grep -i server: /root/kubernetes-infra-example-dev-ost/ansible/artifacts/admin.conf
    server: https://195.113.243.177:6443
root@6e0a59d331f5:~# ncat -z 195.113.243.177 6443; echo $?
0
```
#### Wrap-up questions

1. What infra component allows us to communicate directly with kubernetes API server on FIP address?

### Kubernetes walkthrough


#### Understand the kubernetes' goal

* https://en.wikipedia.org/wiki/Kubernetes#
* https://kubernetes.io/docs/tutorials/kubernetes-basics/
* https://www.mirantis.com/cloud-native-concepts/getting-started-with-kubernetes/what-is-kubernetes/
* https://gitlab.ics.muni.cz/cloud/knowledgebase/-/blob/master/team/onboarding/kubernetes-introduction.md#kubernetes-k8s
* https://kubernetes.io/docs/concepts/overview/

#### Understand the kubernetes architecture (cluster node roles)

* https://kubernetes.io/docs/concepts/architecture/
* https://gitlab.ics.muni.cz/cloud/knowledgebase/-/blob/master/team/onboarding/kubernetes-introduction.md#k8s-architecture

#### Understand how to access/manage kubernetes cluster

* https://kubernetes.io/docs/tasks/access-application-cluster/access-cluster/
* https://gitlab.ics.muni.cz/cloud/knowledgebase/-/blob/master/team/onboarding/kubernetes-introduction.md#k8s-access


#### Understand basic kubernetes resources

* https://kubernetes.io/docs/concepts/
* https://www.copado.com/resources/blog/kubernetes-deployment-vs-service-managing-your-pods
* https://gitlab.ics.muni.cz/cloud/knowledgebase/-/blob/master/team/onboarding/kubernetes-introduction.md#k8s-objects-kinds


#### Wrap-up questions

1. Which kubernetes resource (kind) you use for deploying stateful applications?
1. Which kubernetes resources are used for storing (application) configuration data/metadata?


### Managing kubernetes remotely

#### Get your kubectl ready

Get your kubectl installed [following official guide](https://kubernetes.io/docs/tasks/tools/).
Note as your cluster version is `1.24.z` [you should install kubectl >= 1.23.0, <1.26.0](https://gitlab.ics.muni.cz/cloud/knowledgebase/-/blob/master/team/onboarding/kubernetes-introduction.md#k8s-access).

#### Set-up your kubectl configuration

Copy / merge kubectl configuration from [`/root/kubernetes-infra-example-dev-ost/ansible/artifacts/admin.conf`](#check-basic-kubernetes-functionality) into `~/.kube/config`.

If you have existing kubernetes cluster configuration in `~/.kube/config` merge it following way:
```sh
cp -f ~/.kube/config ~/.kube/config.backup2024-05-31
KUBECONFIG=~/.kube/config:/root/kubernetes-infra-example-dev-ost/ansible/artifacts/admin.conf kubectl config view --flatten > ~/.kube/config.new
cp -f ~/.kube/config.new ~/.kube/config
```

#### Manage kubectl contexts

```console
[freznicek@lenovo-t14 ~ 0]$ kubectl config get-contexts
CURRENT   NAME                         CLUSTER                              AUTHINFO                                      NAMESPACE
          fc10a                        cluster.freznicek-c10a               kubernetes-admin.freznicek-c10a
          fc10b                        cluster.freznicek-c10b               kubernetes-admin.freznicek-c10b
          fc30.ceph                    cluster.fc30.ceph                    kubernetes-admin.fc30.ceph
          fc30.ostack                  cluster.fc30.ostack                  kubernetes-admin.fc30.ostack
          fc30c                        cluster.fc30.ceph                    kubernetes-admin.fc30.ceph
          fc30o                        cluster.fc30.ostack                  kubernetes-admin.fc30.ostack
          fc9a                         cluster.freznicek-c9a                kubernetes-admin.freznicek-c9a
          fc9b                         cluster.freznicek-c9b                kubernetes-admin.freznicek-c9b
          hands-on-5                   cluster.ho5                          kubernetes-admin.ho5
          ho5                          cluster.ho5                          kubernetes-admin.ho5
          ilab                         flux-openstack-lab                   kubernetes-admin-itera-lab
          k8s-infra-ost-dev-kube-vip   cluster.k8s-infra-ost-dev-kube-vip   kubernetes-admin.k8s-infra-ost-dev-kube-vip
          kuba                         kuba-cluster                         kuba-cluster                                  reznicek-ns
          kuba-cluster                 kuba-cluster                         kuba-cluster                                  reznicek-ns
          kv                           cluster.k8s-infra-ost-dev-kube-vip   kubernetes-admin.k8s-infra-ost-dev-kube-vip
          pb                           cluster.prod-brno                    kubernetes-admin.prod-brno
          po                           cluster.prod-ostrava                 kubernetes-admin.prod-ostrava
          prod-brno                    cluster.prod-brno                    kubernetes-admin.prod-brno
          prod-brno-cp-003             cluster.prod-brno-cp-003             kubernetes-admin.prod-brno
          prod-brno-cp-005             cluster.prod-brno-cp-005             kubernetes-admin.prod-brno
*         prod-ostrava                 cluster.prod-ostrava                 kubernetes-admin.prod-ostrava
          prod-ostrava-cln-19          cluster.prod-ostrava-cln-19          kubernetes-admin.prod-ostrava
          prod-ostrava-cln-25          cluster.prod-ostrava-cln-25          kubernetes-admin.prod-ostrava
          t1408.ceph                   cluster.t1408.ceph                   kubernetes-admin.t1408.ceph
          t1408.ostack                 cluster.t1408.ostack                 kubernetes-admin.t1408.ostack
          tc                           cluster.t1408.ceph                   kubernetes-admin.t1408.ceph
          to                           cluster.t1408.ostack                 kubernetes-admin.t1408.ostack
```

##### How to switch between clusters?

**a] Name kube context explicitly**

```sh
kubectl --context=ho5 version
```

**b] Change current kube context**
```sh
$ kubectl config current-context
prod-ostrava
$ kubectl config use-context ho5
Switched to context "ho5".
$ kubectl config current-context
ho5
$ kubectl version
```

**c] Use third-party tools**

https://home.robusta.dev/blog/switching-kubernets-context

##### How to alias existing context?

Use yaml anchors to create k8s context alias:
```diff
 kind: Config
 preferences: {}
 current-context: prod-ostrava
 clusters:
 - cluster:
     certificate-authority-data: X...Cg==
     server: https://195.113.243.177:6443
   name: cluster.ho5
 ...
 contexts:
-- context:
+- context: &hands-on-5
     cluster: cluster.ho5
     user: kubernetes-admin.ho5
   name: hands-on-5
+- context:
+    <<: *hands-on-5
+  name: ho5

 users:
 - name: kubernetes-admin-itera-lab
   user: ...
 ...
```

#### Does my kubectl work? What cluster I communicate to?

```sh
# detect client and server versions
kubectl version

# get more details about your cluster
kubectl cluster-info
```

#### How kubectl communicate with k8s API? How to control verbosity?

```sh
# default verbosity
$ kubectl version

# trace/debug verbosity
$ kubectl --v=9 version

# inspect what is being called as part of kubectl version
$ kubectl --v=9 version |& grep -Eo "curl.+version.*'"

# call kubectl version API way with curl
$ curl -k -XGET  -H "Accept: application/json, */*" 'https://<k8s-api-host>:<port>/version' | jq .
```


#### What are my permissions?

```sh
$ kubectl auth can-i delete namespace
yes
```
##### Am I admin?

```sh
$ kubectl auth can-i "*" "*"
yes
```

#### What kubernetes namespaces do we have?

[What is k8s namespace](https://kubernetes.io/docs/concepts/overview/working-with-objects/namespaces/)?

What kubernetes namespaces we got?
```sh
$ kubectl get namespace
```

Understand what is `kube-system` and `default` namespaces good for?
See [hints](https://kubernetes.io/docs/concepts/overview/working-with-objects/namespaces/#initial-namespaces).

#### What does run in the k8s cluster?

```sh
$ kubectl get all --all-namespaces -o wide
```

#### Understanding kubernetes manifests [`01-first`](./manifests/01-first)

* every application typically have multiple manifests, each per object(kind)
* manifests are yaml (eventually json files) named `<app-name>-<kind>.<yaml|json>`
* most important fields are:
  * [`kind` is object type](./manifests/01-first/awkws-deployment.yaml#L2)
  * [`metadata.name` is name of the object](./manifests/01-first/awkws-deployment.yaml#L4)
  * [`apiVersion` is version (revision) of the kubernetes object](./manifests/01-first/awkws-deployment.yaml#L1)
    * see `kubectl api-versions` for current object API versions supported
    * see [k8s API migration guide](https://kubernetes.io/docs/reference/using-api/deprecation-guide/) to understood how k8s is extending.

See [docs for more details](https://kubernetes.io/docs/concepts/overview/working-with-objects/kubernetes-objects/#required-fields).


##### How to deploy simple server application? [`01-first`](./manifests/01-first)

```sh
# deploy application from kubernetes manifests
kubectl apply -f ./manifests/01-first/

# observe application being deployed
kubectl rollout status -f ./manifests/01-first/*-deployment.yaml
```

##### How to troubleshoot pods in pending state? [`01-first`](./manifests/01-first)

You may end-up in situation when deployment is waiting for something not finishing for more than minute.

```sh
$ kubectl rollout status -f ./manifests/01-first/*-deployment.yaml
Waiting for deployment "awkws" rollout to finish: 0 of 2 updated replicas are available...

```
Problem inspection is needed:

```console
$ kubectl get po -owide
NAME                     READY   STATUS    RESTARTS   AGE    IP       NODE     NOMINATED NODE   READINESS GATES
awkws-75f4b67cb4-d474f   0/1     Pending   0          2m6s   <none>   <none>   <none>           <none>
awkws-75f4b67cb4-hxl6b   0/1     Pending   0          2m7s   <none>   <none>   <none>           <none>

$ kubectl describe po awkws-75f4b67cb4-hxl6b
Name:             awkws-75f4b67cb4-hxl6b
Namespace:        default
Priority:         0
Service Account:  default
Node:             <none>
Labels:           app=awkws
                  pod-template-hash=75f4b67cb4
Annotations:      <none>
Status:           Pending
IP:
IPs:              <none>
Controlled By:    ReplicaSet/awkws-75f4b67cb4
Containers:
  awkws:
    ...
Conditions:
  Type           Status
  PodScheduled   False
Volumes:
  kube-api-access-z2kmd:
    Type:                    Projected (a volume that contains injected data from multiple sources)
    TokenExpirationSeconds:  3607
    ConfigMapName:           kube-root-ca.crt
    ConfigMapOptional:       <nil>
    DownwardAPI:             true
QoS Class:                   BestEffort
Node-Selectors:              <none>
Tolerations:                 node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                             node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
Events:
  Type     Reason            Age   From               Message
  ----     ------            ----  ----               -------
  Warning  FailedScheduling  5m3s  default-scheduler  0/5 nodes are available: 5 node(s) had untolerated taint {node.cloudprovider.kubernetes.io/uninitialized: true}. preemption: 0/5 nodes are available: 5 Preemption is not helpful for scheduling.
```
We need to remove the taints

```console
$ kubectl get node cloud-hands-on-5-workers-1 -ojson | jq .spec.taints
[
  {
    "effect": "NoSchedule",
    "key": "node.cloudprovider.kubernetes.io/uninitialized",
    "value": "true"
  }
]
[freznicek@lenovo-t14 5-g2-infra-vms-k8s 1]$ kubectl taint nodes cloud-hands-on-5-workers-1 node.cloudprovider.kubernetes.io/uninitialized=true:NoSchedule-
node/cloud-hands-on-5-workers-1 untainted
[freznicek@lenovo-t14 5-g2-infra-vms-k8s 0]$ kubectl get node cloud-hands-on-5-workers-1 -ojson | jq .spec.taints
null
[freznicek@lenovo-t14 5-g2-infra-vms-k8s 0]$ kubectl taint nodes cloud-hands-on-5-workers-2 node.cloudprovider.kubernetes.io/uninitialized=true:NoSchedule-
node/cloud-hands-on-5-workers-2 untainted
[freznicek@lenovo-t14 5-g2-infra-vms-k8s 0]$ kubectl get po -owide
NAME                     READY   STATUS              RESTARTS   AGE   IP       NODE                         NOMINATED NODE   READINESS GATES
awkws-75f4b67cb4-d474f   0/1     ContainerCreating   0          10m   <none>   cloud-hands-on-5-workers-1   <none>           <none>
awkws-75f4b67cb4-hxl6b   0/1     ContainerCreating   0          10m   <none>   cloud-hands-on-5-workers-1   <none>           <none>
[freznicek@lenovo-t14 5-g2-infra-vms-k8s 0]$ kubectl rollout status -f ./manifests/01-first/*-deployment.yaml
Waiting for deployment "awkws" rollout to finish: 1 of 2 updated replicas are available...
deployment "awkws" successfully rolled out
```

#### Does deployment work well? What happens when I kill a pod. [`01-first`](./manifests/01-first)

```sh
# doublecheck we have deployed all manifests from files
$ kubectl get -f ./manifests/01-first/
# list resources inside the kubernetes, they should match
$ kubectl get po,deploy,svc -l app=awkws
# delete pods of the application awkws
$ kubectl delete pod -l app=awkws
# watch how kubernetes react on deleted pods (preferrably in separate terminal)
$ kubectl get po -l app=awkws --watch
```

#### Get kubectl (bash) completion working

```sh
# kubectl bash autocompletion
$ apt -y install bash-completion
$ kubectl completion bash > ~/.kube/completion.bash.inc
$ source ~/.kube/completion.bash.inc

# test completion
$ kubect get po a<TAB>
$ kubectl get po awkws-75f4b67cb4-<TAB><TAB>
awkws-75f4b67cb4-hgnwp  awkws-75f4b67cb4-46m6g
```

#### Executing into container? Showing logs. [`01-first`](./manifests/01-first)
```sh
kubectl get pod,deployment,service -l app=awkws
kubectl exec -it awkws-75f4b67cb4-hgnwp -- bash
> cat /etc/os-release
> ...
> hostname
> ...
> ps auxwww
> ...
> curl localhost:8080
> ...
> curl localhost:8080
> ...
^D
...
```
#### Showing apps logs. [`01-first`](./manifests/01-first)

```sh
# logs in single container only:
kubectl logs awkws-75f4b67cb4-hgnwp
kubectl logs awkws-75f4b67cb4-46m6g

# logs from all labeled pods
kubectl logs -l app=awkws

# logs from all labeled pods and follow them
kubectl logs -l app=awkws -f
```


##### How does service object work?. How applications interconnect within k8s (coredns/kube-dns)? [`01-first`](./manifests/01-first)

```sh
kubectl get po,deploy,svc -l app=awkws
kubectl exec -it awkws-75f4b67cb4-hgnwp -- bash
> curl localhost:8080
> curl awkws:80
^D
# logs now in both container instances:
kubectl logs -l app=awkws
```

![](https://gitlab.ics.muni.cz/cloud/g2/kubernetes-user-training/-/raw/master/docs/k8s-pod-lifecycle.png)


#### Manual application scaling [`01-first`](./manifests/01-first)

Application i kubernetes can be scaled manually (kubectl scale)  or automatically (HorizontalPodAutoscaler).


in terminal A:
```sh
$ kubectl get pod -l app=awkws --watch -o wide
```

in terminal B:
```sh
$ kubectl get -f ./kubernetes-introduction/manifests/01-first/
$ kubectl scale deploy awkws --replicas=10
...
$ kubectl scale deploy awkws --replicas=3
```

#### Wrap-up questions

1. How you can select particular pods belonging to single kubernetes application?
1. What is kubectl context and how to change current context to different one?


### Basic Kubernetes operations

#### List kubernetes node including ip addresses

```sh
$ kubectl get node -owide
NAME                         STATUS   ROLES           AGE   VERSION    INTERNAL-IP   EXTERNAL-IP   OS-IMAGE             KERNEL-VERSION      CONTAINER-RUNTIME
cloud-hands-on-5-control-1   Ready    control-plane   20h   v1.24.17   10.0.0.82     <none>        Ubuntu 22.04.2 LTS   5.15.0-73-generic   containerd://1.7.1
cloud-hands-on-5-control-2   Ready    control-plane   20h   v1.24.17   10.0.0.220    <none>        Ubuntu 22.04.2 LTS   5.15.0-73-generic   containerd://1.7.1
cloud-hands-on-5-control-3   Ready    control-plane   20h   v1.24.17   10.0.0.124    <none>        Ubuntu 22.04.2 LTS   5.15.0-73-generic   containerd://1.7.1
cloud-hands-on-5-workers-1   Ready    <none>          20h   v1.24.17   10.0.0.142    <none>        Ubuntu 22.04.2 LTS   5.15.0-73-generic   containerd://1.7.1
cloud-hands-on-5-workers-2   Ready    <none>          20h   v1.24.17   10.0.0.253    <none>        Ubuntu 22.04.2 LTS   5.15.0-73-generic   containerd://1.7.1
```

#### List kubernetes node including labels

```sh
$ kubectl get node --show-labels
NAME                         STATUS   ROLES           AGE   VERSION    LABELS
cloud-hands-on-5-control-1   Ready    control-plane   20h   v1.24.17   beta.kubernetes.io/arch=amd64,beta.kubernetes.io/os=linux,kubernetes.io/arch=amd64,kubernetes.io/hostname=cloud-hands-on-5-control-1,kubernetes.io/os=linux,node-role.kubernetes.io/control-plane=,node.kubernetes.io/exclude-from-external-load-balancers=
cloud-hands-on-5-control-2   Ready    control-plane   20h   v1.24.17   beta.kubernetes.io/arch=amd64,beta.kubernetes.io/os=linux,kubernetes.io/arch=amd64,kubernetes.io/hostname=cloud-hands-on-5-control-2,kubernetes.io/os=linux,node-role.kubernetes.io/control-plane=,node.kubernetes.io/exclude-from-external-load-balancers=
cloud-hands-on-5-control-3   Ready    control-plane   20h   v1.24.17   beta.kubernetes.io/arch=amd64,beta.kubernetes.io/os=linux,kubernetes.io/arch=amd64,kubernetes.io/hostname=cloud-hands-on-5-control-3,kubernetes.io/os=linux,node-role.kubernetes.io/control-plane=,node.kubernetes.io/exclude-from-external-load-balancers=
cloud-hands-on-5-workers-1   Ready    <none>          20h   v1.24.17   beta.kubernetes.io/arch=amd64,beta.kubernetes.io/os=linux,kubernetes.io/arch=amd64,kubernetes.io/hostname=cloud-hands-on-5-workers-1,kubernetes.io/os=linux
cloud-hands-on-5-workers-2   Ready    <none>          20h   v1.24.17   beta.kubernetes.io/arch=amd64,beta.kubernetes.io/os=linux,kubernetes.io/arch=amd64,kubernetes.io/hostname=cloud-hands-on-5-workers-2,kubernetes.io/os=linux
```

#### Renew kubernetes component certificates

Follow [KB guide](https://gitlab.ics.muni.cz/cloud/knowledgebase/-/blob/master/howtos/g2/Kubernetes/k8s-cert-renewal.md).


#### Perform maintenance kubernetes node reboot (with minimum outage)

```sh
# cordon + drain pods from a node
$ kubectl drain cloud-hands-on-5-control-2 --ignore-daemonsets --delete-emptydir-data
node/cloud-hands-on-5-control-2 cordoned
Warning: ignoring DaemonSet-managed Pods: kube-system/calico-node-dfj9k, kube-system/kube-proxy-fkprr, kube-system/nodelocaldns-9rt92, kube-system/openstack-cloud-controller-manager-sj778
node/cloud-hands-on-5-control-2 drained
$ kubectl get node -owide
NAME                         STATUS                     ROLES           AGE   VERSION    INTERNAL-IP   EXTERNAL-IP   OS-IMAGE             KERNEL-VERSION      CONTAINER-RUNTIME
cloud-hands-on-5-control-1   Ready                      control-plane   20h   v1.24.17   10.0.0.82     <none>        Ubuntu 22.04.2 LTS   5.15.0-73-generic   containerd://1.7.1
cloud-hands-on-5-control-2   Ready,SchedulingDisabled   control-plane   20h   v1.24.17   10.0.0.220    <none>        Ubuntu 22.04.2 LTS   5.15.0-73-generic   containerd://1.7.1
cloud-hands-on-5-control-3   Ready                      control-plane   20h   v1.24.17   10.0.0.124    <none>        Ubuntu 22.04.2 LTS   5.15.0-73-generic   containerd://1.7.1
...

# reboot node
ssh ubuntu@10.0.0.220 'uname -a;sudo reboot'

# wait till node becomes up and re-joins
$ kubectl get node --watch

# enable node scheduling
$ kubectl uncordon cloud-hands-on-5-control-2
.
```

More reading: 
* https://yuminlee2.medium.com/kubernetes-manage-nodes-with-drain-cordon-and-uncordon-commands-4f43e4203875
* https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands#uncordon

#### How to disable kubernetes node temporarily or permanently (disable scheduling)

```sh
$ kubectl get node
NAME                         STATUS   ROLES           AGE   VERSION
...
cloud-hands-on-5-workers-1   Ready    <none>          20h   v1.24.17
cloud-hands-on-5-workers-2   Ready    <none>          20h   v1.24.17

# disable kubernetes node
$ kubectl cordon cloud-hands-on-5-workers-2
node/cloud-hands-on-5-workers-2 cordoned
$ kubectl get node
NAME                         STATUS                     ROLES           AGE   VERSION
...
cloud-hands-on-5-workers-1   Ready                      <none>          20h   v1.24.17
cloud-hands-on-5-workers-2   Ready,SchedulingDisabled   <none>          20h   v1.24.17

# re-enable kubernetes node
$ kubectl uncordon cloud-hands-on-5-workers-2
node/cloud-hands-on-5-workers-2 uncordoned
$ kubectl get node
NAME                         STATUS   ROLES           AGE   VERSION
...
cloud-hands-on-5-workers-1   Ready    <none>          20h   v1.24.17
cloud-hands-on-5-workers-2   Ready    <none>          20h   v1.24.17
```

More reading: 
* https://yuminlee2.medium.com/kubernetes-manage-nodes-with-drain-cordon-and-uncordon-commands-4f43e4203875
* https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands#cordon
* https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands#uncordon
* https://balkrishan-nagpal.medium.com/kubernetes-how-to-avoid-scheduling-pods-on-certain-nodes-f5cc7945e557


#### Wrap-up questions

1. What command you use to renew kubernetes component certificates? Where you need to execute such command?
1. What command you use for enabling previously disabled node `mynode`?


## Q/A / FAQ / Notes

### Is there hands-on recording?
  [Yes](https://drive.google.com/file/d/1iPvYfRcNiQLVurjtCiMRqXBlnEgVbBGj/view?usp=drive_link)

## References
* https://kubernetes.io/docs/concepts/
* https://www.youtube.com/watch?v=X48VuDVv0do
* https://dev.to/leandronsp/kubernetes-101-part-i-the-fundamentals-23a1

