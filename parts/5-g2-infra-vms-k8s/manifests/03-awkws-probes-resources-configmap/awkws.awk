# /tmp/awkws.awk
BEGIN{
  RS = ORS = "\r\n"
  HttpService = "/inet/tcp/8080/0/0"
  Hello = sprintf("Hello, world, cnt=%d, hostname=%s", cnt, host)
  Len = length(Hello) + length(ORS)
  print "HTTP/1.0 200 OK"          |& HttpService
  print "Content-Length: " Len ORS |& HttpService
  print Hello                      |& HttpService
  while ((HttpService |& getline) > 0)
continue;
  close(HttpService)
}
