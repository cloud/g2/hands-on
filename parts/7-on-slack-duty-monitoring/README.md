# Cloud hands-on part 7, On-Slack duty, monitoring, communication

## Objectives
* [ ] Understand our on-slack duty definition
* [ ] Understand who is in charge today
* [ ] What channels to watch
* [ ] How to deal with ad-hoc questions on Slack
* [ ] Understand how to deal with alerts
  * [ ] workflow, priorities, taking the alert[s]
  * [ ] runbooks
  * [ ] knowledgebase
  * [ ] help from the team colleagues
  * [ ] How to silence alerts
* [ ] Fix one/few firing alerts
* [ ] Q&A


## Before hands-on

Read through following documents / videos / materials:
* [Prometheus monitroring overview](https://gitlab.ics.muni.cz/cloud/knowledgebase/-/blob/master/team/onboarding/prometheus-monitoring-introduction.md)
* [Incident workflow](https://gitlab.ics.muni.cz/cloud/knowledgebase/-/blob/master/team/incident-management.md)
* [G1 monitoring and alerting overview](https://drive.google.com/file/d/1wdjqF1WOt_5wkl8bqB1-fPZ57bt5GfW4/view?usp=sharing)

Doublecheck you are able to log into all below links:
* G1 monitoring tools
  * https://monitoring1.cloud.muni.cz/
    * https://prometheus1.cloud.muni.cz/
    * https://grafana1.cloud.muni.cz
    * https://thanos-query1.cloud.muni.cz
    * https://alertmanager1.cloud.muni.cz
    * https://pushgateway1.cloud.muni.cz
  * https://monitoring2.cloud.muni.cz/
    * https://prometheus2.cloud.muni.cz/
    * https://grafana2.cloud.muni.cz
    * https://thanos-query2.cloud.muni.cz
    * https://alertmanager2.cloud.muni.cz
    * https://pushgateway2.cloud.muni.cz
  * [MU icinga](https://monitor.ics.muni.cz/icingaweb2/monitoring/list/hostgroups?(hostgroup=%2Acloud-%2A|hostgroup_alias=%2Acloud-%2A))
* [G2 prod-brno monitoring](https://brno.openstack.cloud.e-infra.cz/monitoring/)
    * https://prometheus.brno.openstack.cloud.e-infra.cz
    * https://thanos-query.brno.openstack.cloud.e-infra.cz
    * https://alertmanager.brno.openstack.cloud.e-infra.cz
    * https://grafana.brno.openstack.cloud.e-infra.cz
* [G2 prod-ostrava monitoring](https://ostrava.openstack.cloud.e-infra.cz/monitoring/)
    * https://prometheus.ostrava.openstack.cloud.e-infra.cz
    * https://thanos-query.ostrava.openstack.cloud.e-infra.cz
    * https://alertmanager.ostrava.openstack.cloud.e-infra.cz
    * https://grafana.ostrava.openstack.cloud.e-infra.cz

Doublecheck you are able to see [below detailed slack channels](#what-channels-to-watch).

## Steps

### Understand our on-slack duty definition

Update your day priorities in the morning and reserve extra time for:
  * watching below defined channels for infra alerts (G1, G2 prod-brno, G2 prod-ostrava)
    * doublecheck each 30m or more frequently
  * watching below defined channels for external questions to the cloud/openstack team
    * doublecheck each 60m or more frequently
  * watching below defined channels for team merge requests on [internal team channels](https://gitlab.ics.muni.cz/cloud/knowledgebase/-/blob/master/team/README.md#team-channels)

### Understand who is in charge of on-slack duty today

Current rotation is [following](https://docs.google.com/spreadsheets/d/1EdAZRysRlzKVkZSAke5ITf8IzAdzMkDPkJmV0OOFp94).

### What channels to watch

#### Monitoring alert channels
* [G1 monitoring alerts](https://cesnet.slack.com/archives/G05URF9DU57)
  * in case of incident also doublecheck [G1 MU Icinga](https://monitor.ics.muni.cz/icingaweb2/monitoring/list/hostgroups?(hostgroup=%2Acloud-%2A|hostgroup_alias=%2Acloud-%2A))
* [G2 prod-brno monitoring alerts](https://cesnet.slack.com/archives/G05URF8SQ73)
* [G2 prod-ostrava monitoring alerts](https://cesnet.slack.com/archives/G05UF9SFHT6)

#### General purpose channels (external questions)
* [meta-cloud](https://cesnet.slack.com/archives/G05UF9STYHJ)
* [meta-cloud_general](https://cesnet.slack.com/archives/G05U672ECGN)
* [meta-openstack](https://cesnet.slack.com/archives/G05U673NX70)


### How to deal with ad-hoc external questions on Slack

We want to be responsive and answer quick questions sooner than we do in case of RT, but on the other side we do not want to use Slack instead of the RT system. Therefore assesment of the expected amount of effort needed should take place within first 5 minutes.


Knowing the estimated effort:
 * if you see effort needed as weight 1 or 2 then you may do it rightaway
 * if you need to involve more people or estimated effort is 5+ then suggest person him-herself creates RT for us.
 * if you are not sure how to answer, say you will get back on topic tomorrow and discuss on the scrum meeting.
 * otherwise promise just evaluation within the team and eventual actions in next sprint if priorities allow.


### Understand how to deal with alerts

#### where to find alerts?

[See list of channels above](#monitoring-alert-channels).


#### incident workflow

Read [incident workflow document](https://gitlab.ics.muni.cz/cloud/knowledgebase/-/blob/master/team/incident-management.md?ref_type=heads).

Prioritization of the alerts is based on alert severity (`critical`, `warning`, `info`) and number of alerts given type firing.

Alert Prioritization example, you received following alerts within 10 minutes, (A firing first, D last):
* A `[FIRING:20] :critical: PrometheusScrapeTargetNotUp`
* B `[FIRING:9] :warning: PublicIPAddressUnreachable`
* C `[FIRING:1] :warning: G2ContainerRegistryNotFinishing`
* D `[FIRING:1] :critical: pod_error_crash_loop_back_off`

Ordered processing:
* A `[FIRING:20] :critical: PrometheusScrapeTargetNotUp`
* D `[FIRING:1] :critical: pod_error_crash_loop_back_off`
* B `[FIRING:9] :warning: PublicIPAddressUnreachable`
* C `[FIRING:1] :warning: G2ContainerRegistryNotFinishing`

#### How to find related runbook(s)

All runbooks can be fount in:
* [G1 runbooks](https://gitlab.ics.muni.cz/cloud/knowledgebase/-/tree/master/runbooks)
* [G2 runbooks](https://gitlab.ics.muni.cz/cloud/knowledgebase/-/tree/master/runbooks/g2)

##### Navigate to the alert

![](./pictures/g2-alert-selection.png)

##### Finding runbook

![](./pictures/g2-alert-runbook.png)

Link should redirect us to [the particuler runbook](https://gitlab.ics.muni.cz/cloud/knowledgebase/-/blob/master/runbooks/g2/scrape-target-not-up.md) which should guide us through alert solving process.

Typical runbook consist of following section:
 * a] definition of the problem
 * b] troubleshooting and/or doublechecking section
 * c] steps how to resolve alert

You may need to browse [Knowledgebase for linked topics](https://gitlab.ics.muni.cz/cloud/knowledgebase).

##### Runbook evolution

Some of your runbooks are NOT perfect, therefore if you see weaknesses, try to address them as KB MR.

##### Maintaining stress when being lost

The gereral rule of thumb says you should have idea what has happened within 2-5 minutes from the moment you took the alert / incident.

If you find yourself under stress after 5 minutes of work on incident not having clue how to fix it ask your colleagues for ad-hoc team meeting.

If you see multiple people mentioning incident on the Slack channels and/or in the RT this should be trigger for involving the others.

In cases when multiple people are affected and want to communicate about the issue then you want to open empty Google doc with:
* name of the issue 
* status process
* FAQ section
* share this doc with anyone with link having rights to comment

All people wanting need to have a link so they are able to see current status and raise a question there (as comment) there. The person who is fixing issue should not be the person performing the communication work.

##### Monitoring silencing and inhibitions

G1 and G2 cloud monitoring stacks are set up the way that:
* all alerts are paused (inhibited) on openstack disabled compute hypervisor
  * if you are going to maintain a node please always first disable the hypervisor

Apart from inhibitions there are also silences in Alertmanagers.

Good silence has owner and short description. Duration of the silence should be aligned to your duty so once silce expires you are again on charge and can decide whether silence again or not. This means you are using silence duration unit of weeks (`3w`) or slightly less then N weeks (`2w6d22h`), that way you plan when you get disturbed by expired silence.

Note: Disabling openstack node / puppet agent / description of the monitoring silence should follow [the enough reasoning so the other colleagues are spending minimum time on why is particular subsystem not working](https://gitlab.ics.muni.cz/cloud/knowledgebase/-/blob/master/team/provide-reasons-when-possible.md).


### Fix one/few firing alerts

Take a look at all alert slack channnels and pick one/few alerts and try to resolve them.


## Q/A / FAQ / Notes

### Is there hands-on recording?
  [Yes](https://drive.google.com/file/d/1msPrTMNBswjn8EXStbKufEkBVvbyXk1X/view?usp=drive_link)

## References
* [Prometheus monitroring overview](https://gitlab.ics.muni.cz/cloud/knowledgebase/-/blob/master/team/onboarding/prometheus-monitoring-introduction.md)
* [Incident workflow](https://gitlab.ics.muni.cz/cloud/knowledgebase/-/blob/master/team/incident-management.md)
* [G1 monitoring and alerting overview](https://drive.google.com/file/d/1wdjqF1WOt_5wkl8bqB1-fPZ57bt5GfW4/view?usp=sharing)* https://gitlab.ics.muni.cz/cloud/knowledgebase/-/tree/master/runbooks
* https://gitlab.ics.muni.cz/cloud/internal-wiki/-/blob/master/.gitlab/issue_templates/onboarding-checklist.md?ref_type=heads&plain=0
* https://gitlab.ics.muni.cz/cloud/knowledgebase/-/blob/master/team/provide-reasons-when-possible.md

### Alert solution examples

#### 1. failed OpenStack jobs in g2 `prod-{brno,ostrava}`

Alert [`job_status_failed`](https://cesnet.slack.com/archives/G05UF9SFHT6/p1719311014303849).

Identified [two failed jobs](https://prometheus.ostrava.openstack.cloud.e-infra.cz/graph?g0.expr=kube_job_status_failed%20%3E%200&g0.tab=1&g0.stacked=0&g0.show_exemplars=0&g0.range_input=2d&g0.end_input=2024-06-25%2010%3A06%3A55&g0.moment_input=2024-06-25%2010%3A06%3A55) ([also Thanos](https://thanos-query.ostrava.openstack.cloud.e-infra.cz/graph?g0.expr=kube_job_status_failed%20%3E%200&g0.tab=1&g0.stacked=0&g0.range_input=2d&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=1&g0.store_matches=%5B%5D&g0.engine=prometheus&g0.analyze=0&g0.tenant=&g0.end_input=2024-06-25%2010%3A06%3A55&g0.moment_input=2024-06-25%2010%3A06%3A55)):

```console
kube_job_status_failed{app_kubernetes_io_managed_by="Helm", application="kube-state-metrics", component="metrics", environment="prod-ostrava", helm_toolkit_fluxcd_io_name="kube-state-metrics", helm_toolkit_fluxcd_io_namespace="osh-infra", hostname="controlplane-003", instance="10.233.79.223:8080", job="kube-state-metrics", job_name="heat-engine-cleaner-28595200", kubernetes_name="kube-state-metrics", kubernetes_namespace="osh-infra", namespace="openstack", release_group="osh-infra-kube-state-metrics"}	1
kube_job_status_failed{app_kubernetes_io_managed_by="Helm", application="kube-state-metrics", component="metrics", environment="prod-ostrava", helm_toolkit_fluxcd_io_name="kube-state-metrics", helm_toolkit_fluxcd_io_namespace="osh-infra", hostname="controlplane-003", instance="10.233.79.223:8080", job="kube-state-metrics", job_name="nova-service-cleaner-28595220", kubernetes_name="kube-state-metrics", kubernetes_namespace="osh-infra", namespace="openstack", release_group="osh-infra-kube-state-metrics"}	1
```
##### Resolution, improvements steps

1. [Read runbook](https://gitlab.ics.muni.cz/cloud/knowledgebase/-/blob/master/runbooks/g2/kubernetes-job-failed.md)
1. Find whether those jobs are failing constantly -> NO
1. Failed jobs are part of kube-vip maintenance, we can ignore failures
1. Delete jobs as failed ones are left there for inspectin but pod no longer exists `kubectl --context=prod-ostrava -n openstack delete job nova-service-cleaner-28595220 heat-engine-cleaner-28595200`
1. [Correct runbook to explicitly state after analysis failed jobs have to be deleted manually.](https://gitlab.ics.muni.cz/cloud/knowledgebase/-/compare/2a1275500430bc7f5e1592bab7a37e17c90feca9...e32ba3c432904790bb9c50f47676486233dcf687?from_project_id=2804&straight=false#86e7b5ed6a25a386eb2498902ee1e8387b62886c)
1. The metric is exported out of [`kube-state-metrics`](https://github.com/kubernetes/kube-state-metrics) which was at alpha version -> upgrade suggested. ([`prod-brno`](https://gitlab.ics.muni.cz/cloud/g2/ceph-openstack-lma/-/commit/ac9ee32b167eb331d2cf675bd278e5221a41a400), [`prod-ostrava`](https://gitlab.ics.muni.cz/cloud/g2/ceph-openstack-lma/-/commit/e88965f2f3da8b3fa2091aa0e69901c5b3164f0f)) Note we upgraded to not latest version as `kube-state-metrics` relies on k8s client-go which is wide backward compatible (same rules as for `kubectl` apply here).


Doublechecked [alert resolution](https://cesnet.slack.com/archives/G05UF9SFHT6/p1719329039467389).

Improvement: Improved runbook so everyone knows failed job is necessary to delete (for resolution). Upgraded [`kube-state-metrics`](https://github.com/kubernetes/kube-state-metrics).

#### 2. failed kube-dump jobs in g2 `prod-brno`

Alert [`pod_container_terminated`](https://cesnet.slack.com/archives/G05UF9SFHT6/p1719318689411979).

Identified [two failed job](https://prometheus.ostrava.openstack.cloud.e-infra.cz/graph?g0.expr=kube_pod_container_status_terminated_reason%7Breason%3D~%22OOMKilled%7CError%7CContainerCannotRun%22%7D%20%3E%200&g0.tab=1&g0.stacked=0&g0.show_exemplars=0&g0.range_input=2d&g0.end_input=2024-06-25%2012%3A40%3A47&g0.moment_input=2024-06-25%2012%3A40%3A47) ([also Thanos](https://thanos-query.ostrava.openstack.cloud.e-infra.cz/graph?g0.expr=kube_pod_container_status_terminated_reason%7Breason%3D~%22OOMKilled%7CError%7CContainerCannotRun%22%7D%20%3E%200&g0.tab=1&g0.stacked=0&g0.range_input=2d&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=1&g0.store_matches=%5B%5D&g0.engine=prometheus&g0.analyze=0&g0.tenant=&g0.end_input=2024-06-25%2012%3A40%3A47&g0.moment_input=2024-06-25%2012%3A40%3A47)):

```console
kube_pod_container_status_terminated_reason{app_kubernetes_io_managed_by="Helm", application="kube-state-metrics", cluster="g2-prod-ostrava", component="metrics", container="kube-dump", env="prod-ostrava", environment="prod-ostrava", helm_toolkit_fluxcd_io_name="kube-state-metrics", helm_toolkit_fluxcd_io_namespace="osh-infra", hostname="controlplane-003", instance="10.233.79.223:8080", job="kube-state-metrics", kubernetes_name="kube-state-metrics", kubernetes_namespace="osh-infra", namespace="kube-dump", pod="kube-dump-cronjob-28655225-jbkvj", reason="OOMKilled", region="ostrava", release_group="osh-infra-kube-state-metrics"}	1
```
##### Resolution, improvements steps

1. [Read runbook](https://gitlab.ics.muni.cz/cloud/knowledgebase/-/blob/master/runbooks/g2/kubernetes-pod-container-terminated.md)
1. `kubectl --context=prod-ostrava -n kube-dump describe po kube-dump-cronjob-28655225-jbkvj`
1. `kubectl --context=prod-ostrava -n kube-dump logs kube-dump-cronjob-28655225-jbkvj`
1. Pod was killed at the end of operation when all data were received and git was used to push Git repository -> it is necessary to increase limits
1. We use [custom-helm-charts/kube-dump helm chart](https://gitlab.ics.muni.cz/cloud/g2/custom-helm-charts/-/tree/downstream/kube-dump?ref_type=heads).
1. Increase of pod limits can be done in [`ceph-openstack-lma` repo](https://gitlab.ics.muni.cz/cloud/g2/ceph-openstack-lma/-/blob/downstream/apps/prod-brno/01-support/00-kube-dump/kube-dump.yaml) HelmReleases or directly in [custom-helm-charts/kube-dump helm chart](https://gitlab.ics.muni.cz/cloud/g2/custom-helm-charts/-/tree/downstream/kube-dump?ref_type=heads). [Later is preferred as we increase limits in all kube-dump instances which is what we want](https://gitlab.ics.muni.cz/cloud/g2/custom-helm-charts/-/commit/81cc1685f79424f72fcdfb636826fc4d0e1f6f4a). Note you need to increase helm chart version otherwise FluxCD truggle to find a change in the helm chart.
1. [Improve runbook.](https://gitlab.ics.muni.cz/cloud/knowledgebase/-/commit/1b8156e043ff4c83835309d75555620bd1d49573)

Doublechecked [alert resolution](https://cesnet.slack.com/archives/G05UF9SFHT6/p1719324689424249).

Improvement: Improved runbook and increased (more correct) kube-dump pod limits.

#### 3. Not actionable auto-resolving alert `prod-brno`

Alert [`pod_status_pending`](https://cesnet.slack.com/archives/G05UF9SFHT6/p1719368189387239).
Alert [resolved itself](https://cesnet.slack.com/archives/G05UF9SFHT6/p1719368489382979).

##### Resolution, improvements steps

1. [Read runbook](https://gitlab.ics.muni.cz/cloud/knowledgebase/-/blob/master/runbooks/g2/kubernetes-pod-pending.md)
1. There is quite long exclusive locking between `cinder-volume-usage-audit-28656125-h7d42` job pod and other cinder components which causing
1. [Runbook improved](https://gitlab.ics.muni.cz/cloud/knowledgebase/-/commit/e4848a58cb8cbe33af64c6b68c0af73918860c44).
1. Tune the alert so we wait longer period for such situation([`prod-brno`](https://gitlab.ics.muni.cz/cloud/g2/ceph-openstack-lma/-/commit/d0ec5475edec1ee9275ac4fd620c1b37723af35d), [`prod-ostrava`](https://gitlab.ics.muni.cz/cloud/g2/ceph-openstack-lma/-/commit/3cf669bcb25f24ef9be2530e1a0c03b9ae8e84be)). Note we are changing name of the alert to be inline with Prometheus best practices (CamelCase name).

Improvement: Tuned alert where alert sensititity is improved, runbook improved.
