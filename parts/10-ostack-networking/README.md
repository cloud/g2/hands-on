# G2 Beskar-cloud networking how-to & troubleshooting


## Objectives

### part A
* [ ] Refresh personal networking knowledge
* [ ] Understand what networking technologies we use
* [ ] Understand macroskopic cloud networking view 
* [ ] Understand microskopic cloud networking view 
* [ ] Understand ostack abstracted networking into hypervisor linux networking
* [ ] Q&A

### part B

* [ ] Prepare small ostack infrastructure
* [ ] Test access to the IaaS (VM) infrastructure using various ways
* [ ] Mastering TCPDump
* [ ] Tracing simple infrastructures (ARP, TCP/IP, tcpdump techniques)

### part C

* [ ] ipv6 +Theory
* [ ] Troubleshooting OpenStack ipv6 SLAAC
  * theory:
    * https://docs.openstack.org/neutron/yoga/admin/config-ipv6.html
  * configuration
    * https://gitlab.ics.muni.cz/cloud/g2/prod-brno-cloud-entities/-/blob/master/environments/prod-brno/openstack/networks/external_ipv6_general_public-network.tf?ref_type=heads
    * https://gitlab.ics.muni.cz/cloud/g2/prod-brno-cloud-entities/-/blob/master/environments/prod-brno/openstack/subnets/external_ipv6_general_public_2001-718-801-43b-subnet.tf?ref_type=heads

### part D

* [ ] Network troubleshooting
  * [ ] Tracing data to the VM
  * [ ] Uncover traffic blockage by ostack security groups
  * [ ] Uncover blockage by ostack VM itself

### part E

* [ ] General networking
  * no-carrier case
  * ARP packets not propagated properly
* [ ] end-to-end openstack network inspection including OVS


## Part A theory

### General networking theory

#### OSI / TCP/IP networking layers

![](https://www.lifewire.com/thmb/v1ELh58tFZVN1RadeZxUO77eayo=/750x0/filters:no_upscale():max_bytes(150000):strip_icc():format(webp)/OSImodel-8d93f19d50e543348f82110aa11f7a93.jpg)

![](https://allroundcomputersolutions.weebly.com/uploads/7/5/0/7/750797/6755282_orig.jpg)

![](https://ipcisco.com/wp-content/uploads/osi-model-headers.jpg)

References:
* https://en.wikipedia.org/wiki/OSI_model
* https://www.imperva.com/learn/application-security/osi-model/
* https://www.lifewire.com/osi-model-reference-guide-816289
* https://allroundcomputersolutions.weebly.com/osirm.html
* 

##### TCP/IP networking layers and headers

![](https://www.simplilearn.com/ice9/free_resources_article_thumb/TCP_Model_7.png)

![](https://www.researchgate.net/profile/Glenn-Fink/publication/277997102/figure/fig43/AS:654421587394564@1533037673236/IP-and-TCP-packet-headers.png)


References:
* https://www.simplilearn.com/tutorials/cyber-security-tutorial/what-is-tcp-ip-model
* https://www.researchgate.net/figure/IP-and-TCP-packet-headers_fig43_277997102


#### Network devices

Network devices like hubs, bridges, switches, and routers serve different roles in a network. Here are the key differences:

##### 1. **Hub**
- **Layer**: Operates at Layer 1 (Physical Layer) of the OSI model.
- **Function**: Broadcasts incoming data to all connected devices regardless of the destination, leading to unnecessary network traffic.
- **Ports**: Multiple ports, but no intelligence to filter traffic.
- **Efficiency**: Low efficiency due to data collisions and broadcasts; not ideal for larger networks.
- **Usage**: Simple and inexpensive, used mainly in very small networks.

##### 2. **Bridge**
- **Layer**: Operates at Layer 2 (Data Link Layer).
- **Function**: Connects and filters traffic between two or more LAN segments, forwarding frames based on MAC addresses.
- **Traffic Filtering**: Uses a MAC address table to forward or filter data.
- **Efficiency**: Improves efficiency by reducing collisions and segmenting the network into smaller parts.
- **Usage**: Typically used to extend or segment a network, providing isolation between segments.

##### 3. **Switch**
- **Layer**: Operates at Layer 2 (Data Link Layer), but some switches (Layer 3 switches) can also operate at the Network Layer.
- **Function**: Forwards data based on MAC addresses, maintaining a MAC address table to send frames only to the intended recipient.
- **Traffic Filtering**: Provides full-duplex communication and reduces collisions by creating a separate collision domain for each port.
- **Efficiency**: High efficiency, as each port creates a dedicated link, leading to improved network performance.
- **Usage**: Widely used in modern LANs to connect multiple devices with better performance and manageability than hubs.

##### 4. **Router**
- **Layer**: Operates at Layer 3 (Network Layer).
- **Function**: Routes packets between different networks, making decisions based on IP addresses. Determines the best path for data using routing tables.
- **Traffic Filtering**: Uses logical addressing (IP addresses) to forward packets, providing network segmentation and enabling communication between different subnets or networks.
- **Efficiency**: Supports features like NAT, traffic filtering, and QoS, enhancing security and traffic management.
- **Usage**: Used to connect different networks (e.g., LAN to WAN) and provide internet connectivity.

![](https://afteracademy.com/images/what-are-routers-hubs-switches-bridges-connecting-devices-00808cec092ce3db.jpg)

![](/parts/10-ostack-networking/images/hub-switch-router.png)

##### Summary:
- **Hub**: Simple device that broadcasts all data to every port; lacks intelligence.
- **Bridge**: Connects and filters traffic between network segments based on MAC addresses.
- **Switch**: Intelligent hub that forwards data to specific devices using MAC addresses, reducing collisions.
- **Router**: Connects different networks, using IP addresses to determine the best path for packet delivery.

References:
* https://afteracademy.com/blog/what-are-routers-hubs-switches-bridges/
* 


#### Basic L2 LAN networking

![](https://usercontent.one/wp/www.homenethowto.com/wp-content/uploads/mac-address-in-lan-network-1-768x450.jpg)

##### Basic server network configuration

To connect a computer to a basic LAN network, the following networking details need to be defined:

1. **IP Address**: A unique IP address is assigned to the computer to identify it on the network. This address can be configured manually (static) or automatically assigned by a DHCP server.

2. **Subnet Mask**: Defines the network portion of the IP address and determines the range of IP addresses within the same subnet. This helps in determining whether a destination IP is in the same network.

3. **Default Gateway**: The IP address of a router that forwards traffic from the local network to other networks or the internet. This is required for communication beyond the local subnet.

4. **DNS Server**: The IP address of a Domain Name System (DNS) server, which translates human-readable domain names into IP addresses. This allows the computer to resolve domain names when accessing external resources.

Assuming a computer has network interface card (NIC) which has its **MAC Address** - A unique hardware identifier for the computer's network interface card (NIC). This is usually assigned automatically, but it must be unique within the LAN to avoid conflicts.


##### Basic LAN network principles

1. **Addressing**: Devices are identified using MAC addresses (Layer 2) and IP addresses (Layer 3). This ensures that data is directed to the correct recipient.
2. **Collision Avoidance**: Ethernet uses Carrier Sense Multiple Access with Collision Detection (CSMA/CD) to manage collisions. Devices listen before transmitting to avoid collisions.
3. **Segmentation**: Networks are segmented into smaller collision or broadcast domains using switches and routers for better performance and manageability.
4. **Layered Architecture**: LAN networking follows the OSI model, with each layer serving a specific function (e.g., addressing, routing, error detection).

##### Basic LAN network handshakes

1. **[ARP (Address Resolution Protocol)](https://en.wikipedia.org/wiki/Address_Resolution_Protocol)**: Used to map IP addresses (L3) to MAC addresses (L2), ensuring data can be directed to the correct physical device.

![](https://media.geeksforgeeks.org/wp-content/uploads/20230418131313/ARP-in-Network-Layer.jpg)

![](https://study-ccna.com/wp-content/images/arp_process.jpg)


![](https://media.geeksforgeeks.org/wp-content/uploads/20240212121108/How-Address-Resolution-Protocol-ARP-works---gif-opt-(1).gif)


References:
* https://www.geeksforgeeks.org/how-address-resolution-protocol-arp-works/

2. **TCP 3-Way Handshake**: For reliable communication, TCP establishes a connection using a 3-step process:
   - **SYN**: The client sends a SYN (synchronize) packet to the server to initiate a connection.
   - **SYN-ACK**: The server responds with a SYN-ACK packet, acknowledging the request.
   - **ACK**: The client sends an ACK (acknowledge) packet, confirming the connection is established.

![](http://www.cablefree.net/support/radio/software/images/f/fc/Image2001.gif)


References:
* http://www.cablefree.net/support/radio/software/index.php/Manual:Connection_oriented_communication_(TCP/IP)#Connection_oriented_communication_(TCP/IP)

**Summary:**
Basic LAN networking involves using MAC and IP addresses for device identification, managing collisions, and ensuring reliable data delivery through protocols like ARP and TCP's 3-way handshake. These principles enable efficient and stable communication between devices in a local network.

##### Routing packets over multiple L2 networks

![](https://usercontent.one/wp/www.homenethowto.com/wp-content/uploads/mac-address-traffic-example.jpg)

##### Network NAT (Network Address Translation)

Network Address Translation (NAT) modifies IP addresses in packet headers, allowing private IP addresses to communicate with external networks. It comes in different forms, such as static, dynamic, and PAT (NAT overloading), and provides IP address conservation, security, and flexibility in managing private IP addresses. NAT is widely used to enable internal devices to access the internet while sharing a limited number of public IP addresses.

![](/parts/10-ostack-networking/images/NAT-11.jpg)

Most used NAT variant - PAT (Port Address Translation), also known as NAT Overloading:

Maps multiple private IP addresses to a single public IP address by differentiating traffic using unique port numbers.
Allows many devices to share one public IP address, with each connection identified by a different port number.
Provides the highest level of IP address conservation and is the most common form of NAT used in home and office networks.

![](https://www.researchgate.net/profile/Ahmad-Shawahna/publication/334557400/figure/fig4/AS:782174127128576@1563496252774/Network-Address-Translation-NAT-Working-Principle.jpg)



References:
* https://www.homenethowto.com/switching/mac-addresses/
* https://cs.wikipedia.org/wiki/Network_address_translation
* https://www.geeksforgeeks.org/network-address-translation-nat/

#### VLAN networking

![](https://www.chinacablesbuy.com/wp-content/uploads/2018/07/VLAN-%E5%9B%BE.gif)

The VLAN (Virtual Local Area Network) protocol is defined at **Layer 2 (Data Link Layer)** of the OSI model. VLANs operate by adding a tag (such as an IEEE 802.1Q tag) to Ethernet frames, which identifies the VLAN to which the frame belongs. This tagging allows switches to logically segment and manage network traffic within the same physical infrastructure. VLAN tag use a 12-bit identifier (VLAN ID), allowing a maximum of 4,096 unique VLANs (IDs ranging from 0 to 4095, but typically IDs 0 and 4095 are reserved).

The VLAN (Virtual Local Area Network) protocol was introduced to address several limitations and challenges of traditional LANs, particularly regarding scalability, security, and network management. Key reasons for introducing VLANs include:

1. **Network Segmentation**: In a traditional LAN, all devices within the same broadcast domain receive broadcast traffic, which can lead to congestion as the number of devices increases. VLANs allow segmentation of a physical network into multiple logical networks, reducing broadcast traffic and improving network efficiency.

2. **Security**: VLANs provide isolation between different groups of devices, enhancing security by preventing unauthorized access between segments. For example, employees from different departments (e.g., finance and HR) can be placed in separate VLANs, reducing the risk of data breaches.

3. **Flexibility and Simplified Management**: VLANs allow network administrators to group devices based on function, department, or project, regardless of their physical location. This simplifies network management and makes it easier to reconfigure the network without changing physical cabling.

4. **Scalability**: VLANs make it easier to scale the network by logically segmenting it, which reduces broadcast domains and improves performance. This segmentation also enables better control of network resources as the number of users grows.

5. **Traffic Control**: VLANs allow better control of traffic flow, ensuring that broadcast and multicast traffic is limited to the relevant devices within the VLAN, leading to more efficient use of network resources.



Summary:
VLANs were introduced to provide network segmentation, improve security, simplify network management, enable scalability, and enhance traffic control. They allow logical separation of networks over shared physical infrastructure, making networks more flexible and efficient.


References:
* https://www.chinacablesbuy.com/vlan-vs-lan-what-is-the-difference.html


#### VxLAN networking

![](https://images.ctfassets.net/aoyx73g9h2pg/7KruqglUX6rMd1HRsUgdUM/e72c3a2a8dae7a917e0a1ed1d3a6054a/VLAN-vs-VXLAN-Network-Engineer-Guide-Diagram.jpg)

VXLAN (Virtual Extensible LAN) was introduced to address the limitations of traditional VLANs, especially in large-scale, dynamic, and virtualized data center environments. Key reasons for the introduction of VXLAN include:

1. **Scalability**: Traditional VLANs are limited to 4,096 unique IDs, which restricts the number of network segments that can be created. VXLAN uses a 24-bit identifier (VNI), allowing up to 16 million unique segments, making it more suitable for large-scale environments.

2. **Network Virtualization**: With the rise of cloud computing and virtualization, the need for isolating multiple tenants within the same physical infrastructure grew. VXLAN enables logical network segmentation and isolation for multiple tenants, providing better scalability in virtualized environments.

3. **Overlay Networking**: VXLAN is an overlay protocol that encapsulates Layer 2 frames within Layer 3 UDP packets. This allows for the creation of virtual Layer 2 networks on top of existing Layer 3 infrastructure, enabling flexible network design that is not restricted by physical topology.

4. **Support for Multi-Tenant Environments**: In cloud data centers, multiple tenants may need isolated networks. VXLAN provides an efficient way to create multiple virtual networks that are logically separated from each other, enabling secure multi-tenancy.

5. **Mobility of Virtual Machines**: VXLAN supports seamless virtual machine (VM) mobility across data centers. Since VXLAN can extend Layer 2 networks across Layer 3 boundaries, VMs can be migrated between physical hosts without changing their IP addresses, facilitating workload balancing and high availability.

VXLAN uses a 24-bit identifier (VXLAN Network Identifier or VNI), allowing a maximum of 16,777,216 unique VXLANs (VNIs ranging from 0 to 16,777,215, with some VNIs reserved for special purposes).

VXLAN (Virtual Extensible LAN) is primarily defined at **Layer 2 (Data Link Layer)** of the OSI model, as it extends Layer 2 networks over Layer 3 infrastructure. However, VXLAN operates as an **overlay protocol**, encapsulating Layer 2 Ethernet frames in Layer 3 UDP packets for transport across an IP network. Thus, VXLAN functions across multiple OSI layers:
 - **Encapsulation (Layer 2 within Layer 3)**: VXLAN encapsulates Layer 2 frames inside Layer 3 UDP packets.
 - **Transport Layer (Layer 4)**: VXLAN uses UDP for encapsulation, with UDP port 4789 as the default.


Summary:
VXLAN was introduced to overcome the limitations of traditional VLANs, particularly in terms of scalability, multi-tenancy, and VM mobility. It provides a scalable network virtualization solution for large and dynamic data center environments by creating virtual Layer 2 networks over existing Layer 3 infrastructure.

#### Note on overlay protocols

Overlay protocols were introduced to provide scalability, network virtualization, multi-tenant isolation, flexibility, support for VM mobility, and efficient traffic management. They allow virtual networks to be built on top of existing physical infrastructure, addressing the needs of modern, dynamic data center and cloud environments.

The most widely used overlay protocols—such as VXLAN, GRE, IPsec, MPLS, NVGRE, LISP and GENEVE — are leveraged to create scalable, secure, and flexible virtual networks over physical infrastructure. They are commonly used for data center virtualization, multi-tenant environments, VPNs, and optimized traffic engineering.

#### LACP / etherchannel

Link aggregation is used to combine multiple network connections into a single logical link to achieve several benefits:

1. **Increased Bandwidth**: By aggregating multiple links, the total bandwidth is increased, providing higher data transfer rates and improving overall network performance.

2. **Redundancy and Fault Tolerance**: If one link in the aggregation fails, traffic is automatically redistributed across the remaining links, enhancing network reliability and reducing downtime.

3. **Load Balancing**: Link aggregation helps distribute network traffic evenly across multiple links, preventing any single link from becoming a bottleneck and ensuring efficient resource utilization.

4. **Scalability**: It provides a scalable way to increase network capacity without major hardware upgrades, allowing for more flexible and cost-effective network growth.

Link aggregation is widely used to enhance network efficiency, reliability, and capacity, particularly in data centers and enterprise environments.

![](https://help.firewalla.com/hc/article_attachments/4410747501459)

##### Linux Bond

Linux bonding is used to combine multiple network interfaces into a single logical interface, providing several advantages:

1. **Increased Throughput**: By aggregating multiple network interfaces, Linux bonding can increase the overall bandwidth, improving data transfer rates and performance.

2. **Redundancy and Fault Tolerance**: Bonding provides failover capabilities by allowing traffic to switch automatically to another interface if one fails, ensuring network reliability and minimizing downtime.

3. **Load Balancing**: It helps distribute network traffic across multiple interfaces, which prevents congestion and optimizes resource utilization.

4. **Simplified Management**: A bonded interface simplifies management by treating multiple physical interfaces as a single logical interface, making configuration and monitoring easier.

Linux bonding is especially useful in server environments where high availability, improved performance, and simplified management are crucial.

![](https://i0.wp.com/www.mustafabektastepe.com/wp-content/uploads/2018/10/Linux-interface-bonding.png)

References:
* https://en.wikipedia.org/wiki/Link_aggregation
* https://help.firewalla.com/hc/en-us/articles/4409583011091-Link-Aggregation-Groups-LAG
* http://www.mustafabektastepe.com/2017/02/10/bonding-nedir-linux-isletim-sistemlerinde-nic-bonding-yapilandirmasi/
* https://www.ibm.com/docs/pt-br/linux-on-systems?topic=overview-linux-channel-bonding-concept

#### Linux Bridge

A Linux bridge is used to connect multiple network interfaces at the data link layer, functioning like a virtual network switch. Key uses include:

1. **Network Virtualization**: Linux bridges are often used in virtualized environments to connect virtual machines (VMs) to physical or virtual networks, enabling communication between VMs and external devices.

2. **Container Networking**: They are used to provide networking between containers and external networks, making them popular in Docker and other container-based environments.

3. **Traffic Isolation**: Bridges can be used to segment networks, isolating traffic between different groups of devices for better security and traffic management.

4. **Transparent Forwarding**: They allow data to be forwarded between network interfaces based on MAC addresses without involving higher-level routing, making them useful for layer 2 forwarding and bridging networks.

Linux bridges are widely used in virtualization, containerization, and network management to create flexible, software-defined layer 2 networks.

![](https://miro.medium.com/v2/resize:fit:4800/format:webp/1*R-5_9QIZyg_TbSg_2yFHbA.png)

References:
* https://www.ibm.com/docs/en/linux-on-systems?topic=choices-using-linux-bridge
* https://docs.nvidia.com/networking-ethernet-software/knowledge-base/Demos-and-Training/Training/What-Is-a-Bridge/
* https://medium.com/@amazingandyyy/learn-linux-bridge-with-graphs-a425aa92945f

#### Linux network namespacing

Linux network namespaces provide isolated network environments within a single host. Key points include:

1. **Network Isolation**: Each namespace has its own network stack, including interfaces, routing tables, and firewall rules, allowing for complete network isolation between processes.

2. **Virtual Networking**: Namespaces are often used to create separate virtual networks for containers or applications, enabling multiple isolated networks on the same system.

3. **Flexibility and Security**: By isolating network resources, namespaces enhance security and allow different services or applications to operate independently without interfering with each other.

Linux network namespaces are fundamental to containerization technologies, providing isolated and flexible networking environments.

![](https://miro.medium.com/v2/resize:fit:4800/format:webp/1*AcZ5xYvn5OfJK1D-9lpZ5A.png)

References:
* https://ramesh-sahoo.medium.com/linux-network-namespace-and-five-use-cases-using-various-methods-f45b1ec5db8f
* https://medium.com/@tech_18484/how-to-create-network-namespace-in-linux-host-83ad56c4f46f
* https://blog.scottlowe.org/2013/09/04/introducing-linux-network-namespaces/

#### Linux firewall (iptables)

Linux `iptables` is a firewall tool used to manage packet filtering and network traffic rules. Key features include:

1. **Traffic Filtering**: `iptables` allows filtering of incoming, outgoing, and forwarded network packets based on parameters like source/destination IP, ports, and protocols.

2. **Policy Control**: It provides control over the flow of traffic using chains and rules, defining how packets should be handled (e.g., accepted, dropped, or modified).

3. **Network Address Translation (NAT)**: `iptables` can perform NAT, enabling multiple devices to share a single public IP address and managing port forwarding.

4. **Customizable Security**: It enables fine-grained security settings, making it possible to create complex rules to protect the system against unauthorized access and attacks.

Linux `iptables` is widely used to implement flexible and powerful firewall solutions for securing Linux-based systems.

![](https://phoenixnap.com/kb/wp-content/uploads/2024/05/iptables-tables-and-chains.png)

![](https://miro.medium.com/v2/resize:fit:4800/format:webp/0*5zAvmOh4Lo-1y0g4)

References:
* https://en.wikipedia.org/wiki/Iptables
* https://phoenixnap.com/kb/iptables-linux
* https://natnat1.medium.com/iptables-b9ce0602253f


#### SDN (Software defined networking)

Software-Defined Networking (SDN) offers several benefits:

1. **Centralized Management**: SDN separates the control plane from the data plane, enabling centralized network management. This simplifies network configuration and management.

2. **Network Automation**: SDN enables automated provisioning, configuration, and management of network resources, reducing the need for manual intervention and minimizing human errors.

3. **Scalability and Flexibility**: SDN allows for scalable and flexible network adjustments, making it easier to adapt to changes in network demand and infrastructure.

4. **Cost Efficiency**: By using programmable, commodity hardware instead of proprietary network devices, SDN can reduce both capital and operational costs.

5. **Enhanced Security**: Centralized control allows for consistent and efficient enforcement of security policies across the network, improving threat detection and response.

6. **Improved Performance**: Traffic can be dynamically routed based on current network conditions, ensuring optimal performance and reducing congestion.

7. **Innovation and Customization**: SDN provides an open and programmable network, encouraging innovation and enabling customization to meet specific business requirements. 

Overall, SDN enhances the agility, efficiency, and manageability of networks, making them more suitable for modern IT environments.

![](https://media.geeksforgeeks.org/wp-content/uploads/20230405132159/Software-Defined-Networking.png)


![](https://res.cloudinary.com/canonical/image/fetch/f_auto,q_auto,fl_sanitize,c_fill,w_720/https://lh5.googleusercontent.com/5qtBVU0fwaHRr0JaDr3lN_Hly5LF0bwibMN6S84MaDOv2g3LML841884AAOjF0xmE7E7V4WtuYooiYLtTspRd1XVM3E497ijul1SLgLfmcaQLEuwJDPax5k-lzs11B5kwbQy2GTf)


![](http://www.yet.org/images/posts/ovs-archi.png)


![](https://hustcat.github.io/assets/ovs/ovs_architecture_01.png)

References:
* https://en.wikipedia.org/wiki/Software-defined_networking
* https://www.geeksforgeeks.org/software-defined-networking/
* https://docs.openvswitch.org/en/latest/intro/what-is-ovs/
* https://ubuntu.com/blog/data-centre-networking-what-is-ovs
* http://www.yet.org/2014/09/openvswitch-troubleshooting/
* https://hustcat.github.io/an-introduction-to-ovs-architecture/


#### Linux box as a switch and/or as a router

Yes, a Linux box can be configured to act as both a switch and a router, but with different setups for each. Most of the SOHO (wifi) routers are based on Linux.

##### 1. **Linux as a Switch**
A switch works at Layer 2 (Data Link Layer) and is responsible for forwarding packets based on MAC addresses. To make a Linux machine act like a switch, you can use **bridging**. Bridging allows the Linux box to forward traffic between interfaces as if it were a Layer 2 switch.

####### Steps:
- Install `bridge-utils` or use `iproute2` (modern systems).
- Create a bridge interface and add physical interfaces to it.

Example commands:
```bash
# Install bridge utilities (if necessary)
sudo apt-get install bridge-utils

# Create a bridge
sudo brctl addbr br0

# Add physical interfaces (eth0 and eth1) to the bridge
sudo brctl addif br0 eth0
sudo brctl addif br0 eth1

# Bring up the interfaces and the bridge
sudo ip link set eth0 up
sudo ip link set eth1 up
sudo ip link set br0 up
```

After this setup, the Linux box will act like a Layer 2 switch, forwarding Ethernet frames between the interfaces.

##### 2. **Linux as a Router**
A router works at Layer 3 (Network Layer) and forwards traffic based on IP addresses. To configure a Linux box as a router, you need to enable **IP forwarding** and set up routing rules.

###### Steps:
- Enable IP forwarding.
- Configure routing tables and optionally set up Network Address Translation (NAT) for internet access.

Example commands:
```bash
# Enable IP forwarding
sudo sysctl -w net.ipv4.ip_forward=1

# Make it permanent by editing sysctl.conf
echo "net.ipv4.ip_forward=1" | sudo tee -a /etc/sysctl.conf

# Set up routing between interfaces (e.g., eth0 and eth1)
sudo ip route add 192.168.1.0/24 via 192.168.2.1 dev eth0

# Optionally, set up NAT for internet access (using iptables)
sudo iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
```

After this setup, the Linux machine will route packets between different subnets or networks.

##### How to see Linux box NIC MAC addresses?

```console
root@freznicek-jammy:~ 0 # ip link show
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN mode DEFAULT group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
2: ens3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1442 qdisc fq_codel state UP mode DEFAULT group default qlen 1000
    link/ether fa:16:3e:21:c9:30 brd ff:ff:ff:ff:ff:ff
    altname enp0s3
3: ens4: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP mode DEFAULT group default qlen 1000
    link/ether fa:16:3e:21:27:59 brd ff:ff:ff:ff:ff:ff
    altname enp0s4
```

##### How to see Linux box IP addresses?

```console
root@freznicek-jammy:~ 0 # ip addr show
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: ens3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1442 qdisc fq_codel state UP group default qlen 1000
    link/ether fa:16:3e:21:c9:30 brd ff:ff:ff:ff:ff:ff
    altname enp0s3
    inet 172.16.0.88/22 metric 100 brd 172.16.3.255 scope global dynamic ens3
       valid_lft 29820sec preferred_lft 29820sec
    inet6 fe80::f816:3eff:fe21:c930/64 scope link
       valid_lft forever preferred_lft forever
3: ens4: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether fa:16:3e:21:27:59 brd ff:ff:ff:ff:ff:ff
    altname enp0s4
    inet6 2001:718:801:432:f816:3eff:fe21:2759/64 scope global mngtmpaddr noprefixroute
       valid_lft forever preferred_lft forever
    inet6 fe80::f816:3eff:fe21:2759/64 scope link
       valid_lft forever preferred_lft forever
```

##### How to see Linux box ARP table?

```console
root@freznicek-jammy:~ 0 # ip nei
172.16.0.1 dev ens3 lladdr fa:16:3e:56:98:8a DELAY
172.16.3.225 dev ens3  FAILED
172.16.3.199 dev ens3  FAILED
172.16.2.244 dev ens3 lladdr fa:16:3e:2d:26:e5 STALE
2001:718:801:432:f816:3eff:fe17:3638 dev ens4 lladdr fa:16:3e:17:36:38 STALE
fe80::f816:3eff:fe21:6d46 dev ens4 lladdr fa:16:3e:21:6d:46 router STALE
2001:718:801:432:f816:3eff:fe18:88a4 dev ens4 lladdr fa:16:3e:18:88:a4 STALE
2001:718:801:432::1 dev ens4 lladdr 88:d9:8f:75:1e:60 router STALE
fe80::8ad9:8f00:3275:1e60 dev ens4 lladdr 88:d9:8f:75:1e:60 router STALE
```

##### How to see Linux box routing table?

```console
# IPv4 routing
root@freznicek-jammy:~ 0 # ip route
default via 172.16.0.1 dev ens3 proto dhcp src 172.16.0.88 metric 100
147.251.4.33 via 172.16.0.1 dev ens3 proto dhcp src 172.16.0.88 metric 100
147.251.6.10 via 172.16.0.1 dev ens3 proto dhcp src 172.16.0.88 metric 100
169.254.169.254 via 172.16.0.2 dev ens3 proto dhcp src 172.16.0.88 metric 100
172.16.0.0/22 dev ens3 proto kernel scope link src 172.16.0.88 metric 100
172.16.0.1 dev ens3 proto dhcp scope link src 172.16.0.88 metric 100
172.16.0.2 dev ens3 proto dhcp scope link src 172.16.0.88 metric 100
192.168.122.0/24 dev virbr0 proto kernel scope link src 192.168.122.1 linkdown

# IPv6 routing
root@freznicek-jammy:~ 0 # ip -6 route
::1 dev lo proto kernel metric 256 pref medium
2001:718:801:432::/64 dev ens4 proto ra metric 100 pref medium
fe80::/64 dev ens4 proto kernel metric 256 pref medium
fe80::/64 dev ens3 proto kernel metric 256 pref medium
default proto ra metric 100 expires 65529sec mtu 1500 pref medium
        nexthop via fe80::f816:3eff:fe21:6d46 dev ens4 weight 1
        nexthop via fe80::8ad9:8f00:3275:1e60 dev ens4 weight 1
```

##### How to see Linux box apps listening for TCP/IP connections (servers)?

```console
root@freznicek-jammy:~ 0 # netstat -nlp | grep -E '^(tcp|Proto)'
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name
tcp        0      0 192.168.122.1:53        0.0.0.0:*               LISTEN      2219/dnsmasq
tcp        0      0 0.0.0.0:4330            0.0.0.0:*               LISTEN      2258027/pmlogger
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN      3238890/sshd: /usr/
tcp        0      0 127.0.0.1:33060         0.0.0.0:*               LISTEN      1994287/mysqld
tcp        0      0 127.0.0.1:6640          0.0.0.0:*               LISTEN      639/ovsdb-server
tcp        0      0 127.0.0.53:53           0.0.0.0:*               LISTEN      610/systemd-resolve
tcp        0      0 127.0.0.1:11211         0.0.0.0:*               LISTEN      806/memcached
tcp        0      0 0.0.0.0:44322           0.0.0.0:*               LISTEN      1294/pmproxy
tcp        0      0 0.0.0.0:44323           0.0.0.0:*               LISTEN      1294/pmproxy
tcp        0      0 0.0.0.0:44321           0.0.0.0:*               LISTEN      1243/pmcd
tcp        0      0 0.0.0.0:3306            0.0.0.0:*               LISTEN      1994287/mysqld
tcp        0      0 0.0.0.0:25672           0.0.0.0:*               LISTEN      849/beam.smp
tcp        0      0 127.0.0.1:37547         0.0.0.0:*               LISTEN      893/containerd
tcp6       0      0 :::4369                 :::*                    LISTEN      1/init
tcp6       0      0 :::4330                 :::*                    LISTEN      2258027/pmlogger
tcp6       0      0 :::80                   :::*                    LISTEN      810566/apache2
tcp6       0      0 :::22                   :::*                    LISTEN      3238890/sshd: /usr/
tcp6       0      0 :::5672                 :::*                    LISTEN      849/beam.smp
tcp6       0      0 :::44322                :::*                    LISTEN      1294/pmproxy
tcp6       0      0 :::44323                :::*                    LISTEN      1294/pmproxy
tcp6       0      0 :::44321                :::*                    LISTEN      1243/pmcd
```

This clearly shows `sshd` process (pid `3238890`) listens on both IPv4 and IPv6 on TCP/IP port 22 and accepts remote connections from outside (Local Address 0.0.0.0 / ::).


##### Summary:
- **As a switch**: Use bridging (`brctl` or `iproute2`).
- **As a router**: Enable IP forwarding and configure routing with `ip` or `iptables` for NAT.

Each role (switch or router) requires different configurations depending on the desired functionality.


#### F.A.Q

##### What is network overlay?

A network overlay is a virtual network built on top of an existing physical network infrastructure. Key aspects include:

1. **Virtualization and Abstraction**: Overlays abstract the underlying physical network, creating virtual networks that are independent of the physical hardware. This allows flexibility in creating logical topologies.

2. **Encapsulation**: They use tunneling protocols (e.g., VXLAN, GRE) to encapsulate data packets, enabling isolated communication across shared physical infrastructure.

3. **Scalability**: Network overlays allow for scalable networking by supporting multiple isolated networks, which is useful in large data centers or cloud environments.

4. **Use Cases**: They are commonly used in software-defined networking (SDN), data center virtualization, and multi-tenant cloud environments to provide network isolation, segmentation, and dynamic reconfiguration.

Network overlays enable the creation of flexible, scalable, and isolated virtual networks without being constrained by physical network limitations.

##### What is the meaning of ip nei routing table states?

The `ip nei` command (short for `ip neighbor`) is used to display the ARP (Address Resolution Protocol) table in Linux. It shows the relationship between IP addresses and MAC addresses for directly connected devices on a local network. Each entry in the ARP table has a state that indicates the status of the neighbor relationship.

Here are the possible states you can find in the ARP table:

### 1. **REACHABLE**
- This state means that the neighbor is known, and communication has recently occurred. The link-layer (MAC) address is still considered valid for communication, and the ARP entry is active.
  
### 2. **STALE**
- This state indicates that the neighbor is known, but the system hasn't communicated with it recently. The link-layer address is still cached, but the system will verify the address again the next time it needs to send a packet.

### 3. **DELAY**
- After a packet is sent to a neighbor in the **STALE** state, the neighbor transitions to the **DELAY** state. In this state, the system waits to see if the neighbor responds. If the neighbor responds, it moves to the **REACHABLE** state. If there is no response after a timeout, the state changes to **PROBE**.

### 4. **PROBE**
- In this state, the system is actively trying to verify the neighbor's link-layer address. It sends unicast ARP requests to the neighbor. If the neighbor responds, the state transitions to **REACHABLE**. If it doesn't respond, the entry will be removed from the ARP table after a timeout.

### 5. **INCOMPLETE**
- This state means that the system is still in the process of resolving the neighbor's link-layer address (i.e., the ARP request has been sent, but the ARP reply has not yet been received). The system is waiting for the neighbor to respond with its MAC address.

### 6. **NOARP**
- This state means that no ARP request will be sent for this neighbor because the entry was manually configured or ARP is disabled for this entry. This state is rare and generally appears when a static ARP entry is added.

### 7. **FAILED**
- This state indicates that the system failed to resolve the link-layer address after sending multiple ARP requests. The entry may eventually be removed from the ARP table.

### 8. **PERMANENT**
- This state is used for static, manually configured entries in the ARP table. These entries do not expire and will not be automatically removed.


### G2 Beskar-cloud networking diving in


#### Macroskopic view

Summary:
 * switch redundancy
 * link aggregation (etherchannel)
 * network connections
   * IPMI
   * provisioning / management
   * data (plane)
 * power distribution redundancy

![](/parts/10-ostack-networking/images/g2-network-macroview.jpg)


#### Hypervisor infra-config entities view

Summary:
 * Network connections for G2
   * controlplane
   * compute
 * entities deployed by infra-config via netplan
 * existence of trunks / VLAn bundles

![](/parts/10-ostack-networking/images/g2-network-bundles-bonds.jpg)

#### Hypervisor OpenStack details network entities

Summary:
 * connection of Linux hypervisor networking with virtual one
 * brief OpenStack OVS bridge infrastructure

![](/parts/10-ostack-networking/images/g2-network-brief-internals.jpg)

#### Openstack networking from abstracted entities down-to the Linux networking elements

Summary:
 * mapping of abstracted ostack network entities to Linux low-level networking entities

![](/parts/10-ostack-networking/images/g2-network-abstraction-to-internals-mapping.jpg)

#### Detailed OpenStack OVS DVR

![](https://docs.openstack.org/neutron/yoga/_images/deploy-ovs-ha-dvr-overview.png)

More details in https://docs.openstack.org/neutron/yoga/admin/deploy-ovs-ha-dvr.html.


## Part B practical hands-on

### Meaning of the networking error messages

#### Connection established - Connected

TCP/IP connection established i.e. 3-way handshake passed.

```console
[freznicek@lenovo-t14 ~ 1]$ ncat -zv localhost 22
Ncat: Version 7.92 ( https://nmap.org/ncat )
Ncat: Connected to ::1:22.
Ncat: 0 bytes sent, 0 bytes received in 0.02 seconds.
```

#### (ncat) Connection refused

TCP/IP connection refused i.e. 3-way handshake did not pass, moreover the other side sent `RST` packet to reject TCP/IP connection OR no application listend on particular port.

```console
[freznicek@lenovo-t14 ~ 0]$ ncat -zv localhost 453
Ncat: Version 7.92 ( https://nmap.org/ncat )
Ncat: Connection to ::1 failed: Connection refused.
Ncat: Trying next address...
Ncat: Connection refused.
[freznicek@lenovo-t14 ~ 1]$
```

#### (ncat) Connection timed out

TCP/IP connection refused i.e. 3-way handshake did not pass, the other side did not send any reply.

```console
[freznicek@lenovo-t14 ~ 1]$ LANG=C ncat -zv 78.128.250.99 22
Ncat: Version 7.92 ( https://nmap.org/ncat )
Ncat: TIMEOUT.
[freznicek@lenovo-t14 ~ 1]$
```

#### ping `Destination Host Unreachable`

todo


### Setup the environment

Follow [hands-on #2](/parts/2/README.md) to build two node hands on infrastructure (bastion + node).

```console
hands-on-2-tf-demo-bastion-server | ubuntu-jammy-x86_64 | 10.10.10.10, 195.113.243.246
hands-on-2-tf-demo-server-1       | ubuntu-jammy-x86_64 | 10.10.10.11
```


### Networking troubleshooting hands-on

#### Accessing infrastructure

##### Access the bastion via public FIP (`ssh <>`)

```console
[freznicek@lenovo-t14 ~ 0]$ ssh -i ~/.ssh/id_rsa ubuntu@195.113.243.246 'uname -a'
Linux hands-on-2-tf-demo-bastion-server 5.15.0-119-generic #129-Ubuntu SMP Fri Aug 2 19:25:20 UTC 2024 x86_64 x86_64 x86_64 GNU/Linux
[freznicek@lenovo-t14 ~ 0]$
```

##### Access the server node via bastion ssh jump (`ssh -J <> <>`)

```console
[freznicek@lenovo-t14 ~ 0]$ ssh -i ~/.ssh/id_rsa -J ubuntu@195.113.243.246 ubuntu@10.10.10.11 'uname -a'
Linux hands-on-2-tf-demo-server-1 5.15.0-119-generic #129-Ubuntu SMP Fri Aug 2 19:25:20 UTC 2024 x86_64 x86_64 x86_64 GNU/Linux
[freznicek@lenovo-t14 ~ 0]$
```

##### Access the server node via internal address with sshuttle

Establish SSH VPN for 10.10.10.0/24 subnet
```console
[freznicek@lenovo-t14 ~ 0]$ sshuttle -r ubuntu@195.113.243.246 10.10.10.0/24
[local sudo] Password:
c : Connected to server.
```

Perform SSH connection over SSH VPN
```console
[freznicek@lenovo-t14 ~ 0]$ ssh ubuntu@10.10.10.10 'uname -a'
Linux hands-on-2-tf-demo-bastion-server 5.15.0-119-generic #129-Ubuntu SMP Fri Aug 2 19:25:20 UTC 2024 x86_64 x86_64 x86_64 GNU/Linux
[freznicek@lenovo-t14 ~ 0]$
```

##### Access the bastion via internal address with sshuttle

Establish SSH VPN for single node 10.10.10.11
```console
[freznicek@lenovo-t14 ~ 1]$ sshuttle -e "ssh -i ~/.ssh/id_rsa" -r ubuntu@195.113.243.246 10.10.10.11/32
c : Connected to server.
```

```
[freznicek@lenovo-t14 ~ 0]$ ssh -i ~/.ssh/id_rsa ubuntu@10.10.10.11 'uname -a'
Linux hands-on-2-tf-demo-server-1 5.15.0-119-generic #129-Ubuntu SMP Fri Aug 2 19:25:20 UTC 2024 x86_64 x86_64 x86_64 GNU/Linux
[freznicek@lenovo-t14 ~ 0]$
```

#### Mastering TCPDump

`tcpdump` is a command-line packet analyzer that captures and displays network traffic. It's widely used for network troubleshooting, security analysis, and protocol debugging.

`tcpdump` requires root or elevated privileges to capture traffic, and it’s a powerful tool that must be used carefully to avoid violating privacy or security policies.

##### Filtering interesting traffic from the whole traffic

It is very important to find your IP address and filter traffic for your IP address.

Use standard ping or TCP/IP handshake non-standard port like 82.

Therefore to use `src` `dst` or `host` filter for instance:

```
tcpdump -nn -i any host 78.128.225.150 and port 22
```


###### PING case.

Capturing console:
```console
root@hands-on-2-tf-demo-bastion-server:~# tcpdump -nn -i any icmp
tcpdump: data link type LINUX_SLL2
tcpdump: verbose output suppressed, use -v[v]... for full protocol decode
listening on any, link-type LINUX_SLL2 (Linux cooked v2), snapshot length 262144 bytes
07:05:01.573033 ens3  In  IP 78.128.225.150 > 10.10.10.10: ICMP echo request, id 60424, seq 1, length 64
07:05:01.573058 ens3  Out IP 10.10.10.10 > 78.128.225.150: ICMP echo reply, id 60424, seq 1, length 64
```

Traffic injection:
```console
[freznicek@lenovo-t14 ~ 0]$ ping -c 1 195.113.243.246
PING 195.113.243.246 (195.113.243.246) 56 (84) datových bajtů.
64 bajtů od 195.113.243.246: pořadí=1 TTL=56 čas=7,20 ms

--- Statistika pingů na 195.113.243.246 ---
1 paketů odesláno, 1 přijato, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 7.202/7.202/7.202/0.000 ms
```

My IP address IS `78.128.225.150`.

###### TCP/IP PING (ncat) I

Capturing console:
```console
root@hands-on-2-tf-demo-bastion-server:~# tcpdump -i any -nn port 82
tcpdump: data link type LINUX_SLL2
tcpdump: verbose output suppressed, use -v[v]... for full protocol decode
listening on any, link-type LINUX_SLL2 (Linux cooked v2), snapshot length 262144 bytes
```

Traffic injection:
```console
[freznicek@lenovo-t14 ~ 0]$ ncat -zv  195.113.243.246 82
Ncat: Version 7.92 ( https://nmap.org/ncat )
Ncat: TIMEOUT.
[freznicek@lenovo-t14 ~ 1]$
```

No traffic found, we need to get to hypervisor.


###### TCP/IP PING (ncat) II

Let's get down to hypervisor to see if TCP/IP ping on port 82 is visible there.

Find where VM runs:
```console
[freznicek@lenovo-t14 ~ 0]$ openstack version show | grep identity
| Ostrava     | identity       | 3.14    | CURRENT   | https://identity.ostrava.openstack.cloud.e-infra.cz/v3/       | None             | None             |
[freznicek@lenovo-t14 ~ 0]$ openstack floating ip list | grep 195.113.243.246
| fa9f3f80-59e4-44a5-a07e-bb09385d79e2 | 195.113.243.246     | 10.10.10.10      | 1fa5c146-2dcc-4831-9cd5-4c8c91c7b638 | 5a778b8d-4194-48fd-880d-181aaf7222c2 | 18ca55e64e7b4de6bfa886850fb94e95 |
[freznicek@lenovo-t14 ~ 0]$ openstack port show 1fa5c146-2dcc-4831-9cd5-4c8c91c7b638 -fjson
{
  "admin_state_up": true,
  "binding_host_id": "cln-27.priv.cld.it4i.cz",
  ...
}
```

Capturing console:
```console
[root@cln-27:prod-ostrava~0]# tcpdump -i any -nn port 82
tcpdump: data link type LINUX_SLL2
tcpdump: verbose output suppressed, use -v[v]... for full protocol decode
listening on any, link-type LINUX_SLL2 (Linux cooked v2), snapshot length 262144 bytes
07:16:28.018961 ens8f0 P   IP13 (invalid)
07:16:28.018961 bond0 P   IP13 (invalid)
07:16:28.019023 qvo1fa5c146-2d Out IP 78.128.225.150.59400 > 10.10.10.10.82: Flags [S], seq 1388480674, win 64240, options [mss 1250,sackOK,TS val 4069831029 ecr 0,nop,wscale 7], length 0
07:16:28.019024 qvb1fa5c146-2d P   IP 78.128.225.150.59400 > 10.10.10.10.82: Flags [S], seq 1388480674, win 64240, options [mss 1250,sackOK,TS val 4069831029 ecr 0,nop,wscale 7], length 0
```

Traffic injection:
```console
[freznicek@lenovo-t14 ~ 1]$ timeout 1 ncat -zv  195.113.243.246 82
Ncat: Version 7.92 ( https://nmap.org/ncat )
[freznicek@lenovo-t14 ~ 124]$
```

My IP address IS `78.128.225.150`.

###### Filtering protocols

```sh
# ICPMP (ping)
tcpdump icmp

# TCP/IP
tcpdump tcp

# ARP
tcpdump arp

# IPv6 TCP/IP
tcpdump 'tcp and ip6'
```

###### Filtering based on addresses

```sh
# src or dst address is 78.128.225.150 and port is 22
tcpdump -nn -i any host 78.128.225.150 and port 22

# src address is 78.128.225.150 and port is 22 or 3389
tcpdump -i any -nn "src 78.128.225.150 and port (22 or 3389)"
```


##### Tracing ARP handshake

A. List L2 ARP table:
```console
ubuntu@hands-on-2-tf-demo-bastion-server:~$ ip  nei
10.10.10.1 dev ens3 lladdr fa:16:3e:72:93:c0 REACHABLE
10.10.10.3 dev ens3 lladdr fa:16:3e:05:dd:7e STALE
10.10.10.11 dev ens3 lladdr fa:16:3e:54:4b:84 STALE
10.10.10.13 dev ens3  FAILED
10.10.10.2 dev ens3 lladdr fa:16:3e:4a:9a:b5 STALE
```

B. Ping known hosts at L2 network

Traffic initiation:
```console
ubuntu@hands-on-2-tf-demo-bastion-server:~$ ping -c 1 10.10.10.10
PING 10.10.10.10 (10.10.10.10) 56(84) bytes of data.
64 bytes from 10.10.10.10: icmp_seq=1 ttl=64 time=0.027 ms

--- 10.10.10.10 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 0.027/0.027/0.027/0.000 ms

ubuntu@hands-on-2-tf-demo-bastion-server:~$ ping -c 1 10.10.10.11
PING 10.10.10.11 (10.10.10.11) 56(84) bytes of data.
64 bytes from 10.10.10.11: icmp_seq=1 ttl=64 time=0.807 ms

--- 10.10.10.11 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 0.807/0.807/0.807/0.000 ms
```

Traffic capture:
```console
# hands-on-2-tf-demo-bastion-server
root@hands-on-2-tf-demo-bastion-server:~# tcpdump -nn -i any arp
tcpdump: data link type LINUX_SLL2
tcpdump: verbose output suppressed, use -v[v]... for full protocol decode
listening on any, link-type LINUX_SLL2 (Linux cooked v2), snapshot length 262144 bytes
```

```console
# hands-on-2-tf-demo-server-1
root@hands-on-2-tf-demo-server-1:~# tcpdump -nn -i any arp
tcpdump: data link type LINUX_SLL2
tcpdump: verbose output suppressed, use -v[v]... for full protocol decode
listening on any, link-type LINUX_SLL2 (Linux cooked v2), snapshot length 262144 bytes
```

Review B]:
* `ping -c 1 10.10.10.10` does not cause any ARP traffic as it is local IP address so it is hands-on-2-tf-demo-bastion-server/localhost traffic.
* `ping -c 1 10.10.10.11` does not cause any ARP traffic as it is VM on the same L2 network and IP vs MAC addresses are already present in ARP table of hands-on-2-tf-demo-bastion-server.


C. Ping not-known hosts

Traffic initiation:
```console
ubuntu@hands-on-2-tf-demo-bastion-server:~$ timeout 2 ping -c 1 10.10.10.111
PING 10.10.10.111 (10.10.10.111) 56(84) bytes of data.
ubuntu@hands-on-2-tf-demo-bastion-server:~$ echo $?
124
```

Traffic capture:
```console
# hands-on-2-tf-demo-bastion-server
root@hands-on-2-tf-demo-bastion-server:~# tcpdump -nn -i any arp
tcpdump: data link type LINUX_SLL2
tcpdump: verbose output suppressed, use -v[v]... for full protocol decode
listening on any, link-type LINUX_SLL2 (Linux cooked v2), snapshot length 262144 bytes
07:46:38.271296 ens3  Out ARP, Request who-has 10.10.10.111 tell 10.10.10.10, length 28
07:46:39.275849 ens3  Out ARP, Request who-has 10.10.10.111 tell 10.10.10.10, length 28
07:46:40.299829 ens3  Out ARP, Request who-has 10.10.10.111 tell 10.10.10.10, length 28
```

```console
# hhands-on-2-tf-demo-server-1
root@hands-on-2-tf-demo-server-1:~# tcpdump -nn -i any arp
tcpdump: data link type LINUX_SLL2
tcpdump: verbose output suppressed, use -v[v]... for full protocol decode
listening on any, link-type LINUX_SLL2 (Linux cooked v2), snapshot length 262144 bytes
07:46:38.272614 ens3  B   ARP, Request who-has 10.10.10.111 tell 10.10.10.10, length 28
07:46:39.276642 ens3  B   ARP, Request who-has 10.10.10.111 tell 10.10.10.10, length 28
07:46:40.300530 ens3  B   ARP, Request who-has 10.10.10.111 tell 10.10.10.10, length 28
```

Review:
`ping -c 1 10.10.10.111` causes ARP traffic as the IP address is not know (and no device has assigned such IP address). ARP Requests are sent, but no reply returned.

##### Tracing TCP/IP handshake

TODO: Trace your own TCP/IP handshake in your hands-on #2 environment.

Use minimalistic client-server ncat scenario:
* `ncat -l 8080`
* `ncat -zv localhost 8080` / `ncat -zv 10.10.10.10 8080`

[Solution](./solutions/capture-tcp-ip-handshake.md).


## Part C ostack ipv6 + SLAAC

* [ ] Troubleshooting OpenStack ipv6 SLAAC
  * theory:
    * https://docs.openstack.org/neutron/yoga/admin/config-ipv6.html
  * configuration
    * https://gitlab.ics.muni.cz/cloud/g2/prod-brno-cloud-entities/-/blob/master/environments/prod-brno/openstack/networks/external_ipv6_general_public-network.tf?ref_type=heads
    * https://gitlab.ics.muni.cz/cloud/g2/prod-brno-cloud-entities/-/blob/master/environments/prod-brno/openstack/subnets/external_ipv6_general_public_2001-718-801-43b-subnet.tf?ref_type=heads
  * theory:
    * https://docs.openstack.org/neutron/yoga/admin/config-ipv6.html
  * configuration
    * https://gitlab.ics.muni.cz/cloud/g2/prod-brno-cloud-entities/-/blob/master/environments/prod-brno/openstack/networks/external_ipv6_general_public-network.tf?ref_type=heads
    * https://gitlab.ics.muni.cz/cloud/g2/prod-brno-cloud-entities/-/blob/master/environments/prod-brno/openstack/subnets/external_ipv6_general_public_2001-718-801-43b-subnet.tf?ref_type=heads
  * what has been tried:
    * A] networking ICS router sends RA
      * slaac/slaac subnet
        * neutron with radvd (custom image) - partial functionality (now)
        * neutron w/o radvd (default upstream) - no functionality
      * - /slaac subnet
        * neutron with radvd (custom image) - no functionality
      * ostack router
        * default: HA+distributed - partial functionality
        * no-ha + distributed - partial functionality (no change)
        * no-ha + centralized - partial functionality (no change)
    * B] networking ICS router does not send RA
      * slaac/slaac subnet
        * neutron with radvd (custom image) - no functionality
  * to be tried
    * understand next hop addresses
      * one matching the ostack router
    * take G1 ubuntu jammy to G2 (identical image)

## Part D - ostack networking troubleshooting

### Objectives
* [ ] Understand what components are in the networking path
* [ ] Be able to find out network behavior when SG are dropping the traffic
* [ ] Be able to find out network behavior when VM itself is dropping the traffic

### Theory

#### Overview
![](https://docs.openstack.org/neutron/yoga/_images/deploy-ovs-ha-dvr-overview.png)


#### Detailed components
![](https://docs.openstack.org/neutron/yoga/_images/deploy-ovs-ha-dvr-compconn1.png)


### Steps
 1. create single tiny VM in your project on prod-ostrava, assign public FIP address
 1. test ssh access `ssh user@FIP`
 1. find out your VM NATed address `curl http://ipinfo.io/ip` from inside the VM
 1. find NATed address `curl http://ipinfo.io/ip` of node from where you send the traffic
 1. find VM hypervisor `openstack server show <ID/NAME>`
 1. inspect traffic on SSH port on hypervisor `tcpdump -i any port 22 and host <NAT-IP>`
 1. add server on 8080 inside the VM `ncat -l 8080 --broker`
 1. test server listens on 0.0.0.0 `netstat -nlp | grep 8080`
 1. inspect traffic on 8080 port on hypervisor `tcpdump -i any port 8080 and host <NAT-IP>` on TCP/IP traffic from outside of the cloud caused `ncat -zv FIP 8080`
 1. add 8080 port security group in openstack
 1. inspect traffic on 8080 port on hypervisor `tcpdump -i any port 8080 and host <NAT-IP>` on TCP/IP traffic from outside of the cloud caused `ncat -zv FIP 8080`
 1. block traffic to port 8080 inside VM via resetting the connection (RST) (`iptables -A INPUT -p tcp --destination-port 8080 -j REJECT --reject-with tcp-reset`)
 1. inspect traffic on 8080 port from outside of the cloud
 1. block traffic to port 8080 inside VM via timeouting the connection (DROP) (`iptables -A INPUT -p tcp --destination-port 8080 -j DROP`)


Inspection of traffic involves following steps:
 * client connection is `ncat -zv FIP 8080`
 * server connection is acceptad by the VM using `ncat -l 8080 --broker`
 * tracing the connection is possible on below places with `tcpdump -i any port 8080 [and host <NAT-IP>]`
   * VM itself
   * hypervisor
   * node sending the traffic
 * follow [FIP unreachable runbook](https://gitlab.ics.muni.cz/cloud/knowledgebase/-/blob/master/runbooks/openstack/public-ip-unreachable.md?ref_type=heads#solution) to 
   * identify the tap device[s]: `virsh dumpxml <VM-ID> | grep -E '<(nova:)?name>|<uuid>|<interface|target dev=.tap|source bridge|mac address|interfaceid'`
   * inspect the libvirt kvm network counters: `virsh domifstat <VM-ID> <TAP-devices>`

Manipulation with iptables:
```console
# list current state
root@freznicek-h-on-10d:~# iptables -S INPUT
-P INPUT ACCEPT

# add rule and check state
root@freznicek-h-on-10d:~# iptables -A INPUT -p tcp --destination-port 8080 -j REJECT
root@freznicek-h-on-10d:~# iptables -S INPUT
-P INPUT ACCEPT
-A INPUT -p tcp -m tcp --dport 8080 -j REJECT --reject-with icmp-port-unreachable

# remove rule
root@freznicek-h-on-10d:~# iptables -L INPUT --line-numbers
Chain INPUT (policy ACCEPT)
num  target     prot opt source               destination
1    REJECT     tcp  --  anywhere             anywhere             tcp dpt:http-alt reject-with icmp-port-unreachable
root@freznicek-h-on-10d:~# iptables -D INPUT 1

# list current state
root@freznicek-h-on-10d:~# iptables -S INPUT
-P INPUT ACCEPT
```

Quizz questions:
 * At which interface gets traffic blocked by OpenStack security groups?
 * How to recognize situations when the VM traffic is blocked by OpenStack security groups or VM itself?
 * How to prove that VM is dropping our requests but requests from valid user is granted?
 * How changes network configuration of OpenStack hypervisor when we add additional SG on port 8080? Are you able to find out what and where neutron changes it?


## Part E - leftovers + ostack end-to-end networking

### No carrier case

```console
[root@hdi-005-meta:prod-brno~0]# ip a | grep -E '^[0-9]+:'
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
2: eno1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 9000 qdisc mq master br-provisioning state UP group default qlen 1000
3: eno2: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 9000 qdisc mq state DOWN group default qlen 1000
4: enp227s0f0: <NO-CARRIER,BROADCAST,MULTICAST,SLAVE,UP> mtu 9000 qdisc mq master bond0 state DOWN group default qlen 1000
6: enp227s0f1: <NO-CARRIER,BROADCAST,MULTICAST,SLAVE,UP> mtu 9000 qdisc mq master bond0 state DOWN group default qlen 1000
7: bond0: <NO-CARRIER,BROADCAST,MULTICAST,MASTER,UP> mtu 9000 qdisc noqueue state DOWN group default qlen 1000
8: br-data: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 9000 qdisc noqueue state DOWN group default qlen 1000
9: br-provisioning: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 9000 qdisc noqueue state UP group default qlen 1000
10: br-storage: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 9000 qdisc noqueue state DOWN group default qlen 1000
11: bond0.893@bond0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 9000 qdisc noqueue master br-data state LOWERLAYERDOWN group default qlen 1000
12: bond0.892@bond0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 9000 qdisc noqueue master br-storage state LOWERLAYERDOWN group default qlen 1000
```

After network guys fix:

```console
[root@hdi-005-meta:prod-brno~0]# ip a | grep -E '^[0-9]+:'
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
2: eno1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 9000 qdisc mq master br-provisioning state UP group default qlen 1000
3: eno2: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 9000 qdisc mq state DOWN group default qlen 1000
4: enp227s0f0: <BROADCAST,MULTICAST,SLAVE,UP,LOWER_UP> mtu 9000 qdisc mq master bond0 state UP group default qlen 1000
6: enp227s0f1: <BROADCAST,MULTICAST,SLAVE,UP,LOWER_UP> mtu 9000 qdisc mq master bond0 state UP group default qlen 1000
7: bond0: <BROADCAST,MULTICAST,MASTER,UP,LOWER_UP> mtu 9000 qdisc noqueue state UP group default qlen 1000
8: br-data: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 9000 qdisc noqueue state UP group default qlen 1000
9: br-provisioning: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 9000 qdisc noqueue state UP group default qlen 1000
10: br-storage: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 9000 qdisc noqueue state UP group default qlen 1000
11: bond0.893@bond0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 9000 qdisc noqueue master br-data state UP group default qlen 1000
12: bond0.892@bond0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 9000 qdisc noqueue master br-storage state UP group default qlen 1000
```

### ARP packets not propagated properly

![](./images/stage-brno-architecture.jpg)

#### A. everything works as expected

```console
// correct ARP table learning


// empty ARP table
[root@service-001:stage~0]# ip -s -s neigh flush all
10.16.161.4 dev br-provisioning lladdr 00:50:56:b9:24:6c ref 1 used 65/0/65 probes 4 REACHABLE
10.16.161.15 dev br-provisioning lladdr de:fc:2b:85:21:12 used 23/23/0 probes 4 STALE

*** Round 1, deleting 2 entries ***
*** Flush is complete after 1 round ***

// cause traffic
[ubuntu@service-001:stage~0]$ ping -c1  10.16.161.15
PING 10.16.161.15 (10.16.161.15) 56(84) bytes of data.
64 bytes from 10.16.161.15: icmp_seq=1 ttl=64 time=0.320 ms

--- 10.16.161.15 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 0.320/0.320/0.320/0.000 ms
[ubuntu@service-001:stage~0]$


// listen on ARP on local internal networks

// initiator of the ping
[root@service-001:stage~0]# tcpdump -i any -nn "arp and ( net 10.16.161.0/24 or net 10.16.162.0/24 or net 10.16.163.0/24)"
tcpdump: data link type LINUX_SLL2
tcpdump: verbose output suppressed, use -v[v]... for full protocol decode
listening on any, link-type LINUX_SLL2 (Linux cooked v2), snapshot length 262144 bytes
15:43:58.115958 br-provisioning Out ARP, Request who-has 10.16.161.15 tell 10.16.161.10, length 28
15:43:58.115962 bond0.895 Out ARP, Request who-has 10.16.161.15 tell 10.16.161.10, length 28
15:43:58.115963 bond0 Out ARP, Request who-has 10.16.161.15 tell 10.16.161.10, length 28
15:43:58.115967 eno2  Out ARP, Request who-has 10.16.161.15 tell 10.16.161.10, length 28
15:43:58.116100 eno2  P   ARP, Reply 10.16.161.15 is-at de:fc:2b:85:21:12, length 46
15:43:58.116100 bond0 P   ARP, Reply 10.16.161.15 is-at de:fc:2b:85:21:12, length 46
15:43:58.116100 bond0.895 P   ARP, Reply 10.16.161.15 is-at de:fc:2b:85:21:12, length 46
15:43:58.116100 br-provisioning In  ARP, Reply 10.16.161.15 is-at de:fc:2b:85:21:12, length 46
^C
8 packets captured
16 packets received by filter
0 packets dropped by kernel

// neibor
[root@controlplane-001:stage~0]# tcpdump -i any -nn "arp and ( net 10.16.161.0/24 or net 10.16.162.0/24 or net 10.16.163.0/24)"
tcpdump: data link type LINUX_SLL2
tcpdump: verbose output suppressed, use -v[v]... for full protocol decode
listening on any, link-type LINUX_SLL2 (Linux cooked v2), snapshot length 262144 bytes
15:43:58.115964 eno2  B   ARP, Request who-has 10.16.161.15 tell 10.16.161.10, length 46
15:43:58.115964 bond0 B   ARP, Request who-has 10.16.161.15 tell 10.16.161.10, length 46
15:43:58.115964 bond0.895 B   ARP, Request who-has 10.16.161.15 tell 10.16.161.10, length 46
15:43:58.115964 br-provisioning B   ARP, Request who-has 10.16.161.15 tell 10.16.161.10, length 46
^C
4 packets captured
12 packets received by filter
0 packets dropped by kernel

// ping host
[root@compute-002:stage~0]# tcpdump -i any -nn "arp and ( net 10.16.161.0/24 or net 10.16.162.0/24 or net 10.16.163.0/24)"
tcpdump: data link type LINUX_SLL2
tcpdump: verbose output suppressed, use -v[v]... for full protocol decode
listening on any, link-type LINUX_SLL2 (Linux cooked v2), snapshot length 262144 bytes
15:43:58.113954 enp4s0f1 B   ARP, Request who-has 10.16.161.15 tell 10.16.161.10, length 46
15:43:58.113954 bond0 B   ARP, Request who-has 10.16.161.15 tell 10.16.161.10, length 46
15:43:58.113954 bond0.895 B   ARP, Request who-has 10.16.161.15 tell 10.16.161.10, length 46
15:43:58.113954 br-provisioning B   ARP, Request who-has 10.16.161.15 tell 10.16.161.10, length 46
15:43:58.113966 br-provisioning Out ARP, Reply 10.16.161.15 is-at de:fc:2b:85:21:12, length 28
15:43:58.113968 bond0.895 Out ARP, Reply 10.16.161.15 is-at de:fc:2b:85:21:12, length 28
15:43:58.113969 bond0 Out ARP, Reply 10.16.161.15 is-at de:fc:2b:85:21:12, length 28
15:43:58.113970 enp4s0f1 Out ARP, Reply 10.16.161.15 is-at de:fc:2b:85:21:12, length 28
15:43:58.113985 enp4s0f0 B   ARP, Request who-has 10.16.161.15 tell 10.16.161.10, length 46
^C
9 packets captured
23 packets received by filter
0 packets dropped by kernel

```

#### B. ARP reply not propagated

```console
// incorrect ARP table learning
// ARP broadcast propagated, ARP reply does not appear on requestors NIC

// initiator
[ubuntu@service-001:stage~0]$ ping -c1  10.16.163.15
PING 10.16.163.15 (10.16.163.15) 56(84) bytes of data.
From 10.16.163.10 icmp_seq=1 Destination Host Unreachable

--- 10.16.163.15 ping statistics ---
1 packets transmitted, 0 received, +1 errors, 100% packet loss, time 0ms

[ubuntu@service-001:stage~1]$


// initiator traffic
[root@service-001:stage~0]# tcpdump -i any -nn "arp and ( net 10.16.161.0/24 or net 10.16.162.0/24 or net 10.16.163.0/24)"
tcpdump: data link type LINUX_SLL2
tcpdump: verbose output suppressed, use -v[v]... for full protocol decode
listening on any, link-type LINUX_SLL2 (Linux cooked v2), snapshot length 262144 bytes

15:47:19.885690 br-data Out ARP, Request who-has 10.16.163.15 tell 10.16.163.10, length 28
15:47:19.885696 bond0.897 Out ARP, Request who-has 10.16.163.15 tell 10.16.163.10, length 28
15:47:19.885698 bond0 Out ARP, Request who-has 10.16.163.15 tell 10.16.163.10, length 28
15:47:19.885703 eno2  Out ARP, Request who-has 10.16.163.15 tell 10.16.163.10, length 28

15:47:20.886844 br-data Out ARP, Request who-has 10.16.163.15 tell 10.16.163.10, length 28
15:47:20.886849 bond0.897 Out ARP, Request who-has 10.16.163.15 tell 10.16.163.10, length 28
15:47:20.886851 bond0 Out ARP, Request who-has 10.16.163.15 tell 10.16.163.10, length 28
15:47:20.886855 eno2  Out ARP, Request who-has 10.16.163.15 tell 10.16.163.10, length 28

15:47:21.910845 br-data Out ARP, Request who-has 10.16.163.15 tell 10.16.163.10, length 28
15:47:21.910850 bond0.897 Out ARP, Request who-has 10.16.163.15 tell 10.16.163.10, length 28
15:47:21.910852 bond0 Out ARP, Request who-has 10.16.163.15 tell 10.16.163.10, length 28
15:47:21.910856 eno2  Out ARP, Request who-has 10.16.163.15 tell 10.16.163.10, length 28
^C
12 packets captured
20 packets received by filter
0 packets dropped by kernel

// any other node just seeing the broadcasts
[root@controlplane-001:stage~0]# tcpdump -i any -nn "arp and ( net 10.16.161.0/24 or net 10.16.162.0/24 or net 10.16.163.0/24)"
tcpdump: data link type LINUX_SLL2
tcpdump: verbose output suppressed, use -v[v]... for full protocol decode
listening on any, link-type LINUX_SLL2 (Linux cooked v2), snapshot length 262144 bytes

15:47:19.885681 eno2  B   ARP, Request who-has 10.16.163.15 tell 10.16.163.10, length 46
15:47:19.885681 bond0 B   ARP, Request who-has 10.16.163.15 tell 10.16.163.10, length 46
15:47:19.885681 bond0.897 B   ARP, Request who-has 10.16.163.15 tell 10.16.163.10, length 46
15:47:19.885681 br-data B   ARP, Request who-has 10.16.163.15 tell 10.16.163.10, length 46

15:47:20.886825 eno2  B   ARP, Request who-has 10.16.163.15 tell 10.16.163.10, length 46
15:47:20.886825 bond0 B   ARP, Request who-has 10.16.163.15 tell 10.16.163.10, length 46
15:47:20.886825 bond0.897 B   ARP, Request who-has 10.16.163.15 tell 10.16.163.10, length 46
15:47:20.886825 br-data B   ARP, Request who-has 10.16.163.15 tell 10.16.163.10, length 46

15:47:21.910834 eno2  B   ARP, Request who-has 10.16.163.15 tell 10.16.163.10, length 46
15:47:21.910834 bond0 B   ARP, Request who-has 10.16.163.15 tell 10.16.163.10, length 46
15:47:21.910834 bond0.897 B   ARP, Request who-has 10.16.163.15 tell 10.16.163.10, length 46
15:47:21.910834 br-data B   ARP, Request who-has 10.16.163.15 tell 10.16.163.10, length 46
^C
12 packets captured
20 packets received by filter
0 packets dropped by kernel


// ping host, seing the broadcast and replying
[root@compute-002:stage~0]# tcpdump -i any -nn "arp and ( net 10.16.161.0/24 or net 10.16.162.0/24 or net 10.16.163.0/24)"
tcpdump: data link type LINUX_SLL2
tcpdump: verbose output suppressed, use -v[v]... for full protocol decode
listening on any, link-type LINUX_SLL2 (Linux cooked v2), snapshot length 262144 bytes

15:47:19.883891 enp4s0f0 B   ARP, Request who-has 10.16.163.15 tell 10.16.163.10, length 46
15:47:19.883894 enp4s0f1 B   ARP, Request who-has 10.16.163.15 tell 10.16.163.10, length 46
15:47:19.883894 bond0 B   ARP, Request who-has 10.16.163.15 tell 10.16.163.10, length 46
15:47:19.883894 bond0.897 B   ARP, Request who-has 10.16.163.15 tell 10.16.163.10, length 46
15:47:19.883894 br-data B   ARP, Request who-has 10.16.163.15 tell 10.16.163.10, length 46
15:47:19.883909 br-data Out ARP, Reply 10.16.163.15 is-at fe:75:73:45:6a:8b, length 28
15:47:19.883911 bond0.897 Out ARP, Reply 10.16.163.15 is-at fe:75:73:45:6a:8b, length 28
15:47:19.883912 bond0 Out ARP, Reply 10.16.163.15 is-at fe:75:73:45:6a:8b, length 28
15:47:19.883914 enp4s0f1 Out ARP, Reply 10.16.163.15 is-at fe:75:73:45:6a:8b, length 28

15:47:20.885046 enp4s0f0 B   ARP, Request who-has 10.16.163.15 tell 10.16.163.10, length 46
15:47:20.885050 enp4s0f1 B   ARP, Request who-has 10.16.163.15 tell 10.16.163.10, length 46
15:47:20.885050 bond0 B   ARP, Request who-has 10.16.163.15 tell 10.16.163.10, length 46
15:47:20.885050 bond0.897 B   ARP, Request who-has 10.16.163.15 tell 10.16.163.10, length 46
15:47:20.885050 br-data B   ARP, Request who-has 10.16.163.15 tell 10.16.163.10, length 46
15:47:20.885055 br-data Out ARP, Reply 10.16.163.15 is-at fe:75:73:45:6a:8b, length 28
15:47:20.885056 bond0.897 Out ARP, Reply 10.16.163.15 is-at fe:75:73:45:6a:8b, length 28
15:47:20.885057 bond0 Out ARP, Reply 10.16.163.15 is-at fe:75:73:45:6a:8b, length 28
15:47:20.885058 enp4s0f1 Out ARP, Reply 10.16.163.15 is-at fe:75:73:45:6a:8b, length 28

15:47:21.909049 enp4s0f0 B   ARP, Request who-has 10.16.163.15 tell 10.16.163.10, length 46
15:47:21.909052 enp4s0f1 B   ARP, Request who-has 10.16.163.15 tell 10.16.163.10, length 46
15:47:21.909052 bond0 B   ARP, Request who-has 10.16.163.15 tell 10.16.163.10, length 46
15:47:21.909052 bond0.897 B   ARP, Request who-has 10.16.163.15 tell 10.16.163.10, length 46
15:47:21.909052 br-data B   ARP, Request who-has 10.16.163.15 tell 10.16.163.10, length 46
15:47:21.909056 br-data Out ARP, Reply 10.16.163.15 is-at fe:75:73:45:6a:8b, length 28
15:47:21.909057 bond0.897 Out ARP, Reply 10.16.163.15 is-at fe:75:73:45:6a:8b, length 28
15:47:21.909057 bond0 Out ARP, Reply 10.16.163.15 is-at fe:75:73:45:6a:8b, length 28
15:47:21.909058 enp4s0f1 Out ARP, Reply 10.16.163.15 is-at fe:75:73:45:6a:8b, length 28
^C
27 packets captured
37 packets received by filter
0 packets dropped by kernel
```


#### C. ARP request not propagated

```console
// incorrect ARP table learning - ARP broadcast not propagated

// initiator
[ubuntu@controlplane-001:stage~0]$ ping -c1  10.16.162.1
PING 10.16.162.1 (10.16.162.1) 56(84) bytes of data.
From 10.16.162.11 icmp_seq=1 Destination Host Unreachable

--- 10.16.162.1 ping statistics ---
1 packets transmitted, 0 received, +1 errors, 100% packet loss, time 0ms

[ubuntu@controlplane-001:stage~1]$

// neibor
[root@service-001:stage~0]# tcpdump -i any -nn "arp and ( net 10.16.161.0/24 or net 10.16.162.0/24 or net 10.16.163.0/24)"
tcpdump: data link type LINUX_SLL2
tcpdump: verbose output suppressed, use -v[v]... for full protocol decode
listening on any, link-type LINUX_SLL2 (Linux cooked v2), snapshot length 262144 bytes
^C
0 packets captured
8 packets received by filter
0 packets dropped by kernel

// initiator
[root@controlplane-001:stage~0]# tcpdump -i any -nn "arp and ( net 10.16.161.0/24 or net 10.16.162.0/24 or net 10.16.163.0/24)"
tcpdump: data link type LINUX_SLL2
tcpdump: verbose output suppressed, use -v[v]... for full protocol decode
listening on any, link-type LINUX_SLL2 (Linux cooked v2), snapshot length 262144 bytes
16:16:07.726492 br-storage Out ARP, Request who-has 10.16.162.1 tell 10.16.162.11, length 28
16:16:07.726498 bond0.896 Out ARP, Request who-has 10.16.162.1 tell 10.16.162.11, length 28
16:16:07.726500 bond0 Out ARP, Request who-has 10.16.162.1 tell 10.16.162.11, length 28
16:16:07.726505 eno1  Out ARP, Request who-has 10.16.162.1 tell 10.16.162.11, length 28
16:16:08.750704 br-storage Out ARP, Request who-has 10.16.162.1 tell 10.16.162.11, length 28
16:16:08.750710 bond0.896 Out ARP, Request who-has 10.16.162.1 tell 10.16.162.11, length 28
16:16:08.750711 bond0 Out ARP, Request who-has 10.16.162.1 tell 10.16.162.11, length 28
16:16:08.750716 eno1  Out ARP, Request who-has 10.16.162.1 tell 10.16.162.11, length 28
16:16:09.774706 br-storage Out ARP, Request who-has 10.16.162.1 tell 10.16.162.11, length 28
16:16:09.774712 bond0.896 Out ARP, Request who-has 10.16.162.1 tell 10.16.162.11, length 28
16:16:09.774713 bond0 Out ARP, Request who-has 10.16.162.1 tell 10.16.162.11, length 28
16:16:09.774718 eno1  Out ARP, Request who-has 10.16.162.1 tell 10.16.162.11, length 28
^C
12 packets captured
20 packets received by filter
0 packets dropped by kernel

// neibor
[root@compute-002:stage~0]# tcpdump -i any -nn "arp and ( net 10.16.161.0/24 or net 10.16.162.0/24 or net 10.16.163.0/24)"
tcpdump: data link type LINUX_SLL2
tcpdump: verbose output suppressed, use -v[v]... for full protocol decode
listening on any, link-type LINUX_SLL2 (Linux cooked v2), snapshot length 262144 bytes
^C
0 packets captured
8 packets received by filter
0 packets dropped by kernel
```

### end-to-end openstack network inspection including OVS

![](/parts/10-ostack-networking/images/g2-network-brief-internals.jpg)

![](/parts/10-ostack-networking/images/ostack-end-to-end2.jpg)


Let's have two VMs and track the traffic  147.251.245.243 -> 78.128.250.99:82
 * VM 147.251.245.243 in prod-brno, f18e839f-8eec-4bbc-8190-ba10c13efed3, crf-002-ics
 * VM 78.128.250.99 in G1 production

```console
[freznicek@lenovo-t14 ~ 0]$ openstack server show f18e839f-8eec-4bbc-8190-ba10c13efed3 | grep OS-EXT-SRV-ATTR:hypervisor_hostname
| OS-EXT-SRV-ATTR:hypervisor_hostname | crf-002-ics.priv.g2.cloud.muni.cz                                                                                                        ```

1. Traffic initiation

```console
root@freznicek-e2e-net-test:~# ncat -zv 78.128.250.99 82
Ncat: Version 7.80 ( https://nmap.org/ncat )
Ncat: Connected to 78.128.250.99:82.
Ncat: 0 bytes sent, 0 bytes received in 0.12 seconds.

```
2. Traffic inspection on VM

```console
root@freznicek-e2e-net-test:~# tcpdump -nn -i any port 82
tcpdump: data link type LINUX_SLL2
tcpdump: verbose output suppressed, use -v[v]... for full protocol decode
listening on any, link-type LINUX_SLL2 (Linux cooked v2), snapshot length 262144 bytes
08:55:10.387194 ens3  Out IP 172.16.0.58.44832 > 78.128.250.99.82: Flags [S], seq 283201931, win 64240, options [mss 1460,sackOK,TS val 3864656859 ecr 0,nop,wscale 7], length 0

08:55:10.391521 ens3  In  IP 78.128.250.99.82 > 172.16.0.58.44832: Flags [S.], seq 2927023560, ack 283201932, win 65160, options [mss 1460,sackOK,TS val 2922392228 ecr 3864656859,nop,wscale 7], length 0

08:55:10.391671 ens3  Out IP 172.16.0.58.44832 > 78.128.250.99.82: Flags [.], ack 1, win 502, options [nop,nop,TS val 3864656863 ecr 2922392228], length 0

08:55:10.391951 ens3  Out IP 172.16.0.58.44832 > 78.128.250.99.82: Flags [F.], seq 1, ack 1, win 502, options [nop,nop,TS val 3864656863 ecr 2922392228], length 0
08:55:10.396306 ens3  In  IP 78.128.250.99.82 > 172.16.0.58.44832: Flags [F.], seq 1, ack 2, win 510, options [nop,nop,TS val 2922392234 ecr 3864656863], length 0
08:55:10.396534 ens3  Out IP 172.16.0.58.44832 > 78.128.250.99.82: Flags [.], ack 2, win 502, options [nop,nop,TS val 3864656868 ecr 2922392234], length 0
^C
6 packets captured
9 packets received by filter
0 packets dropped by kernel
root@freznicek-e2e-net-test:~#
```

3. Traffic inspection on G2 hypervisor

```console
[root@crf-002-ics:prod-brno~0]# tcpdump -nn -i any port 82
tcpdump: data link type LINUX_SLL2
tcpdump: verbose output suppressed, use -v[v]... for full protocol decode
listening on any, link-type LINUX_SLL2 (Linux cooked v2), snapshot length 262144 bytes
08:55:10.387570 tap2b058d3a-d1 P   IP 172.16.0.58.44832 > 78.128.250.99.82: Flags [S], seq 283201931, win 64240, options [mss 1460,sackOK,TS val 3864656859 ecr 0,nop,wscale 7], length 0
08:55:10.387734 qvb2b058d3a-d1 Out IP 172.16.0.58.44832 > 78.128.250.99.82: Flags [S], seq 283201931, win 64240, options [mss 1460,sackOK,TS val 3864656859 ecr 0,nop,wscale 7], length 0
08:55:10.387738 qvo2b058d3a-d1 P   IP 172.16.0.58.44832 > 78.128.250.99.82: Flags [S], seq 283201931, win 64240, options [mss 1460,sackOK,TS val 3864656859 ecr 0,nop,wscale 7], length 0
08:55:10.387903 bond0 Out IP 147.251.245.243.9015 > 78.128.250.99.82: Flags [S], seq 283201931, win 64240, options [mss 1460,sackOK,TS val 3864656859 ecr 0,nop,wscale 7], length 0
08:55:10.387909 enp175s0f0 Out IP 147.251.245.243.9015 > 78.128.250.99.82: Flags [S], seq 283201931, win 64240, options [mss 1460,sackOK,TS val 3864656859 ecr 0,nop,wscale 7], length 0

08:55:10.390998 enp175s0f0 P   IP 78.128.250.99.82 > 147.251.245.243.9015: Flags [S.], seq 2927023560, ack 283201932, win 65160, options [mss 1460,sackOK,TS val 2922392228 ecr 3864656859,nop,wscale 7], length 0
08:55:10.390998 bond0 P   IP 78.128.250.99.82 > 147.251.245.243.9015: Flags [S.], seq 2927023560, ack 283201932, win 65160, options [mss 1460,sackOK,TS val 2922392228 ecr 3864656859,nop,wscale 7], length 0
08:55:10.391160 qvo2b058d3a-d1 Out IP 78.128.250.99.82 > 172.16.0.58.44832: Flags [S.], seq 2927023560, ack 283201932, win 65160, options [mss 1460,sackOK,TS val 2922392228 ecr 3864656859,nop,wscale 7], length 0
08:55:10.391162 qvb2b058d3a-d1 P   IP 78.128.250.99.82 > 172.16.0.58.44832: Flags [S.], seq 2927023560, ack 283201932, win 65160, options [mss 1460,sackOK,TS val 2922392228 ecr 3864656859,nop,wscale 7], length 0
08:55:10.391258 tap2b058d3a-d1 Out IP 78.128.250.99.82 > 172.16.0.58.44832: Flags [S.], seq 2927023560, ack 283201932, win 65160, options [mss 1460,sackOK,TS val 2922392228 ecr 3864656859,nop,wscale 7], length 0

08:55:10.391697 tap2b058d3a-d1 P   IP 172.16.0.58.44832 > 78.128.250.99.82: Flags [.], ack 1, win 502, options [nop,nop,TS val 3864656863 ecr 2922392228], length 0
08:55:10.391774 qvb2b058d3a-d1 Out IP 172.16.0.58.44832 > 78.128.250.99.82: Flags [.], ack 1, win 502, options [nop,nop,TS val 3864656863 ecr 2922392228], length 0
08:55:10.391776 qvo2b058d3a-d1 P   IP 172.16.0.58.44832 > 78.128.250.99.82: Flags [.], ack 1, win 502, options [nop,nop,TS val 3864656863 ecr 2922392228], length 0
08:55:10.391872 bond0 Out IP 147.251.245.243.9015 > 78.128.250.99.82: Flags [.], ack 1, win 502, options [nop,nop,TS val 3864656863 ecr 2922392228], length 0
08:55:10.391880 enp175s0f0 Out IP 147.251.245.243.9015 > 78.128.250.99.82: Flags [.], ack 1, win 502, options [nop,nop,TS val 3864656863 ecr 2922392228], length 0

08:55:10.391966 tap2b058d3a-d1 P   IP 172.16.0.58.44832 > 78.128.250.99.82: Flags [F.], seq 1, ack 1, win 502, options [nop,nop,TS val 3864656863 ecr 2922392228], length 0
08:55:10.391993 qvb2b058d3a-d1 Out IP 172.16.0.58.44832 > 78.128.250.99.82: Flags [F.], seq 1, ack 1, win 502, options [nop,nop,TS val 3864656863 ecr 2922392228], length 0
08:55:10.391994 qvo2b058d3a-d1 P   IP 172.16.0.58.44832 > 78.128.250.99.82: Flags [F.], seq 1, ack 1, win 502, options [nop,nop,TS val 3864656863 ecr 2922392228], length 0
08:55:10.392018 bond0 Out IP 147.251.245.243.9015 > 78.128.250.99.82: Flags [F.], seq 1, ack 1, win 502, options [nop,nop,TS val 3864656863 ecr 2922392228], length 0
08:55:10.392019 enp175s0f0 Out IP 147.251.245.243.9015 > 78.128.250.99.82: Flags [F.], seq 1, ack 1, win 502, options [nop,nop,TS val 3864656863 ecr 2922392228], length 0
08:55:10.395958 enp175s0f0 P   IP 78.128.250.99.82 > 147.251.245.243.9015: Flags [F.], seq 1, ack 2, win 510, options [nop,nop,TS val 2922392234 ecr 3864656863], length 0
08:55:10.395958 bond0 P   IP 78.128.250.99.82 > 147.251.245.243.9015: Flags [F.], seq 1, ack 2, win 510, options [nop,nop,TS val 2922392234 ecr 3864656863], length 0
08:55:10.396116 qvo2b058d3a-d1 Out IP 78.128.250.99.82 > 172.16.0.58.44832: Flags [F.], seq 1, ack 2, win 510, options [nop,nop,TS val 2922392234 ecr 3864656863], length 0
08:55:10.396119 qvb2b058d3a-d1 P   IP 78.128.250.99.82 > 172.16.0.58.44832: Flags [F.], seq 1, ack 2, win 510, options [nop,nop,TS val 2922392234 ecr 3864656863], length 0
08:55:10.396226 tap2b058d3a-d1 Out IP 78.128.250.99.82 > 172.16.0.58.44832: Flags [F.], seq 1, ack 2, win 510, options [nop,nop,TS val 2922392234 ecr 3864656863], length 0
08:55:10.396562 tap2b058d3a-d1 P   IP 172.16.0.58.44832 > 78.128.250.99.82: Flags [.], ack 2, win 502, options [nop,nop,TS val 3864656868 ecr 2922392234], length 0
08:55:10.396613 qvb2b058d3a-d1 Out IP 172.16.0.58.44832 > 78.128.250.99.82: Flags [.], ack 2, win 502, options [nop,nop,TS val 3864656868 ecr 2922392234], length 0
08:55:10.396615 qvo2b058d3a-d1 P   IP 172.16.0.58.44832 > 78.128.250.99.82: Flags [.], ack 2, win 502, options [nop,nop,TS val 3864656868 ecr 2922392234], length 0
08:55:10.396704 bond0 Out IP 147.251.245.243.9015 > 78.128.250.99.82: Flags [.], ack 2, win 502, options [nop,nop,TS val 3864656868 ecr 2922392234], length 0
08:55:10.396708 enp175s0f0 Out IP 147.251.245.243.9015 > 78.128.250.99.82: Flags [.], ack 2, win 502, options [nop,nop,TS val 3864656868 ecr 2922392234], length 0
^C
30 packets captured
118 packets received by filter
3 packets dropped by kernel
[root@crf-002-ics:prod-brno~0]#
```
Adding OVS bridge snooper can be found [here](https://wiki.openstack.org/wiki/OpsGuide-Network-Troubleshooting#Visualizing_OpenStack_Networking_Service_Traffic_in_the_Cloud).

## Frequently asked Questions

### Is there hands-on recording?
* [Yes, part 10a](https://drive.google.com/file/d/13DCdcyIEat_Z0h2d3tG-0xIvBTLMb0pi/view?usp=drive_link)
* [Yes, part 10b](https://drive.google.com/file/d/1PibphzzQLzG-hnkRilSkHdF9bTuCR9N3/view?usp=drive_link)
* No, part 10c
* [Yes, part 10d](https://drive.google.com/file/d/1UwroG5QM032VEguYSApMtvrMPrB-V7Hz/view?usp=drive_link)
* [Yes, part 10e](https://drive.google.com/file/d/1qIEMfjRhC7SAh5fW6fmogVFFIUGZIlhQ/view?usp=drive_link)

## References:
* https://gitlab.ics.muni.cz/cloud/knowledgebase/-/blob/master/howtos/g2/OpenStack/networking-troubleshooting.md
