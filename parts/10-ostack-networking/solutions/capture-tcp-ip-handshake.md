# Capture TCP/IP handshake in simple 2 node environment

## Infra summary

```console
hands-on-2-tf-demo-bastion-server | ubuntu-jammy-x86_64 | 10.10.10.10, 195.113.243.246
hands-on-2-tf-demo-server-1       | ubuntu-jammy-x86_64 | 10.10.10.11
```

### A. TCP/IP traffic on single node


#### A1. Launching the server

```console
# doublecheck we can bind port 8080
root@hands-on-2-tf-demo-bastion-server:~# netstat -nlp | grep 8080
root@hands-on-2-tf-demo-bastion-server:~# ncat -l 8080 --broker
```


#### A2. Connecting to the server on localhost

```console
# doublecheck TCP/IP port 8080 is bound
root@hands-on-2-tf-demo-bastion-server:~# netstat -nlp | grep 8080
tcp        0      0 0.0.0.0:8080            0.0.0.0:*               LISTEN      255580/ncat
tcp6       0      0 :::8080                 :::*                    LISTEN      255580/ncat

# initiate TCP/IP ping
root@hands-on-2-tf-demo-bastion-server:~# ncat -z localhost 8080
```

#### A3. Capture the traffic (localhost)

```console
root@hands-on-2-tf-demo-bastion-server:~# tcpdump -nn -i any port 8080
tcpdump: data link type LINUX_SLL2
tcpdump: verbose output suppressed, use -v[v]... for full protocol decode
listening on any, link-type LINUX_SLL2 (Linux cooked v2), snapshot length 262144 bytes
# 3-way initiation handshake
06:14:55.171858 lo    In  IP 127.0.0.1.53084 > 127.0.0.1.8080: Flags [S], seq 2907513402, win 65495, options [mss 65495,sackOK,TS val 3897116699 ecr 0,nop,wscale 7], length 0
06:14:55.171867 lo    In  IP 127.0.0.1.8080 > 127.0.0.1.53084: Flags [S.], seq 558710088, ack 2907513403, win 65483, options [mss 65495,sackOK,TS val 3897116699 ecr 3897116699,nop,wscale 7], length 0
06:14:55.171876 lo    In  IP 127.0.0.1.53084 > 127.0.0.1.8080: Flags [.], ack 1, win 512, options [nop,nop,TS val 3897116699 ecr 3897116699], length 0

# 4-way termination handshake
06:14:55.171900 lo    In  IP 127.0.0.1.53084 > 127.0.0.1.8080: Flags [F.], seq 1, ack 1, win 512, options [nop,nop,TS val 3897116699 ecr 3897116699], length 0
06:14:55.171930 lo    In  IP 127.0.0.1.8080 > 127.0.0.1.53084: Flags [F.], seq 1, ack 2, win 512, options [nop,nop,TS val 3897116699 ecr 3897116699], length 0
06:14:55.171934 lo    In  IP 127.0.0.1.53084 > 127.0.0.1.8080: Flags [.], ack 2, win 512, options [nop,nop,TS val 3897116699 ecr 3897116699], length 0
```

#### A4. Connecting to the server on internal address

```console
# doublecheck TCP/IP port 8080 is bound
root@hands-on-2-tf-demo-bastion-server:~# netstat -nlp | grep 8080
tcp        0      0 0.0.0.0:8080            0.0.0.0:*               LISTEN      255580/ncat
tcp6       0      0 :::8080                 :::*                    LISTEN      255580/ncat

# initiate TCP/IP ping
root@hands-on-2-tf-demo-bastion-server:~# ncat -z 10.10.10.10 8080
```

#### A5. Capture the traffic (internal IP address)

```console
root@hands-on-2-tf-demo-bastion-server:~# tcpdump -nn -i any port 8080
tcpdump: data link type LINUX_SLL2
tcpdump: verbose output suppressed, use -v[v]... for full protocol decode
listening on any, link-type LINUX_SLL2 (Linux cooked v2), snapshot length 262144 bytes
# 3-way initiation handshake
06:15:07.923849 lo    In  IP 10.10.10.10.45514 > 10.10.10.10.8080: Flags [S], seq 1037112167, win 65495, options [mss 65495,sackOK,TS val 1952130765 ecr 0,nop,wscale 7], length 0
06:15:07.923861 lo    In  IP 10.10.10.10.8080 > 10.10.10.10.45514: Flags [S.], seq 3214617426, ack 1037112168, win 65483, options [mss 65495,sackOK,TS val 1952130765 ecr 1952130765,nop,wscale 7], length 0
06:15:07.923870 lo    In  IP 10.10.10.10.45514 > 10.10.10.10.8080: Flags [.], ack 1, win 512, options [nop,nop,TS val 1952130765 ecr 1952130765], length 0

# 4-way termination handshake
06:15:07.923897 lo    In  IP 10.10.10.10.45514 > 10.10.10.10.8080: Flags [F.], seq 1, ack 1, win 512, options [nop,nop,TS val 1952130765 ecr 1952130765], length 0
06:15:07.923926 lo    In  IP 10.10.10.10.8080 > 10.10.10.10.45514: Flags [F.], seq 1, ack 2, win 512, options [nop,nop,TS val 1952130765 ecr 1952130765], length 0
06:15:07.923931 lo    In  IP 10.10.10.10.45514 > 10.10.10.10.8080: Flags [.], ack 2, win 512, options [nop,nop,TS val 1952130765 ecr 1952130765], length 0
```

#### A6. Connecting to the server on external FIP address

```console
# doublecheck TCP/IP port 8080 is bound
root@hands-on-2-tf-demo-bastion-server:~# netstat -nlp | grep 8080
tcp        0      0 0.0.0.0:8080            0.0.0.0:*               LISTEN      255580/ncat
tcp6       0      0 :::8080                 :::*                    LISTEN      255580/ncat

# initiate TCP/IP ping
root@hands-on-2-tf-demo-bastion-server:~# ncat -z 195.113.243.246 8080
root@hands-on-2-tf-demo-bastion-server:~# echo $?
1
```
#### A7. Capture the traffic (external FIP IP address)

```console
root@hands-on-2-tf-demo-bastion-server:~# tcpdump -nn -i any port 8080
tcpdump: data link type LINUX_SLL2
tcpdump: verbose output suppressed, use -v[v]... for full protocol decode
listening on any, link-type LINUX_SLL2 (Linux cooked v2), snapshot length 262144 bytes

# part of 3-way initiation handshake
06:22:38.851471 ens3  Out IP 10.10.10.10.49844 > 195.113.243.246.8080: Flags [S], seq 1539694317, win 62370, options [mss 8910,sackOK,TS val 2030976455 ecr 0,nop,wscale 7], length 0
06:22:39.851859 ens3  Out IP 10.10.10.10.49844 > 195.113.243.246.8080: Flags [S], seq 1539694317, win 62370, options [mss 8910,sackOK,TS val 2030977456 ecr 0,nop,wscale 7], length 0
06:22:41.867855 ens3  Out IP 10.10.10.10.49844 > 195.113.243.246.8080: Flags [S], seq 1539694317, win 62370, options [mss 8910,sackOK,TS val 2030979472 ecr 0,nop,wscale 7], length 0
06:22:45.931855 ens3  Out IP 10.10.10.10.49844 > 195.113.243.246.8080: Flags [S], seq 1539694317, win 62370, options [mss 8910,sackOK,TS val 2030983536 ecr 0,nop,wscale 7], length 0
```

In this scenario client goes to the server through public internet, so traffic leaves the node and returns back.
As we can see just initial packet of TCP/IP 3-way initiation handshake is sent, this means following:
 * SYN packet is sent via ncat in client mode (`ncat -z 195.113.243.246 8080`)
 * SYN+ACK is not sent back by ncat in server/broker mode (`ncat -l 8080 --broker`)

Nearly all Linux distro networking defaults are:
 * allow outgoing traffic - we really see outgoing traffic
 * block incomming traffic - we do not see the incomming SYN packet on ens3 NIC


Remember: iptables may block the traffic but traffic should first still visible on NICs before being dropped

As we do not see traffic re-entering the node we suffer from another blockage, in this case OpenStack security groups.

Note also `SYN` packets are listed muptiple times as ncat in client mode sents it with 1/2/4 secs gap, look at timing.

Enabling OpenStack security groups for TCP/IP traffic 0.0.0.0/0 port 8080.


#### A8. Connecting to the server on external FIP address II

```console
# doublecheck TCP/IP port 8080 is bound
root@hands-on-2-tf-demo-bastion-server:~# netstat -nlp | grep 8080
tcp        0      0 0.0.0.0:8080            0.0.0.0:*               LISTEN      255580/ncat
tcp6       0      0 :::8080                 :::*                    LISTEN      255580/ncat

# initiate TCP/IP ping
root@hands-on-2-tf-demo-bastion-server:~# ncat -z 195.113.243.246 8080
root@hands-on-2-tf-demo-bastion-server:~# echo $?
0
```
#### A9. Capture the traffic (external FIP IP address) II

```console
root@hands-on-2-tf-demo-bastion-server:~# tcpdump -nn -i any port 8080
tcpdump: data link type LINUX_SLL2
tcpdump: verbose output suppressed, use -v[v]... for full protocol decode
listening on any, link-type LINUX_SLL2 (Linux cooked v2), snapshot length 262144 bytes

# part of 3-way initiation handshake
06:36:00.137783 ens3  Out IP 10.10.10.10.35720 > 195.113.243.246.8080: Flags [S], seq 1432492551, win 62370, options [mss 8910,sackOK,TS val 2031777741 ecr 0,nop,wscale 7], length 0
06:36:00.137929 ens3  In  IP 195.113.243.246.7699 > 10.10.10.10.8080: Flags [S], seq 1432492551, win 62370, options [mss 8910,sackOK,TS val 2031777741 ecr 0,nop,wscale 7], length 0
06:36:00.137942 ens3  Out IP 10.10.10.10.8080 > 195.113.243.246.7699: Flags [S.], seq 4016469701, ack 1432492552, win 62286, options [mss 8910,sackOK,TS val 2031777742 ecr 2031777741,nop,wscale 7], length 0
06:36:00.137999 ens3  In  IP 195.113.243.246.8080 > 10.10.10.10.35720: Flags [S.], seq 4016469701, ack 1432492552, win 62286, options [mss 8910,sackOK,TS val 2031777742 ecr 2031777741,nop,wscale 7], length 0
06:36:00.138009 ens3  Out IP 10.10.10.10.35720 > 195.113.243.246.8080: Flags [.], ack 1, win 488, options [nop,nop,TS val 2031777742 ecr 2031777742], length 0

# 4-way termination handshake
06:36:00.138027 ens3  Out IP 10.10.10.10.35720 > 195.113.243.246.8080: Flags [F.], seq 1, ack 1, win 488, options [nop,nop,TS val 2031777742 ecr 2031777742], length 0
06:36:00.138075 ens3  In  IP 195.113.243.246.7699 > 10.10.10.10.8080: Flags [.], ack 1, win 488, options [nop,nop,TS val 2031777742 ecr 2031777742], length 0
06:36:00.138076 ens3  In  IP 195.113.243.246.7699 > 10.10.10.10.8080: Flags [F.], seq 1, ack 1, win 488, options [nop,nop,TS val 2031777742 ecr 2031777742], length 0
06:36:00.138121 ens3  Out IP 10.10.10.10.8080 > 195.113.243.246.7699: Flags [F.], seq 1, ack 2, win 487, options [nop,nop,TS val 2031777742 ecr 2031777742], length 0
06:36:00.138161 ens3  In  IP 195.113.243.246.8080 > 10.10.10.10.35720: Flags [F.], seq 1, ack 2, win 487, options [nop,nop,TS val 2031777742 ecr 2031777742], length 0
06:36:00.138165 ens3  Out IP 10.10.10.10.35720 > 195.113.243.246.8080: Flags [.], ack 2, win 488, options [nop,nop,TS val 2031777742 ecr 2031777742], length 0
06:36:00.138214 ens3  In  IP 195.113.243.246.7699 > 10.10.10.10.8080: Flags [.], ack 2, win 488, options [nop,nop,TS val 2031777742 ecr 2031777742], length 0

```
The tcpdump capture shows important how the same packets are leaving and entering same bastion node, one example:
```console
06:36:00.137783 ens3  Out IP 10.10.10.10.35720 > 195.113.243.246.8080: Flags [S], seq 1432492551, win 62370, options [mss 8910,sackOK,TS val 2031777741 ecr 0,nop,wscale 7], length 0
06:36:00.137929 ens3  In  IP 195.113.243.246.7699 > 10.10.10.10.8080: Flags [S], seq 1432492551, win 62370, options [mss 8910,sackOK,TS val 2031777741 ecr 0,nop,wscale 7], length 0

```


### B. between two nodes

TCP/IP connection hands-on-2-tf-demo-server-1 -> hands-on-2-tf-demo-bastion-server.


#### B1. Launching the server

```console
root@hands-on-2-tf-demo-bastion-server:~# ncat -l 8080 --broker
```


#### B2. Connecting to the server on internal addresses

```console
root@hands-on-2-tf-demo-server-1:~# ncat -zv 10.10.10.10 8080
Ncat: Version 7.80 ( https://nmap.org/ncat )
Ncat: Connected to 10.10.10.10:8080.
Ncat: 0 bytes sent, 0 bytes received in 0.05 seconds.
```

#### B3a. packet capture as seen on client node `hands-on-2-tf-demo-server-1`

```console
root@hands-on-2-tf-demo-server-1:~# tcpdump -nn -i any port 8080
tcpdump: data link type LINUX_SLL2
tcpdump: verbose output suppressed, use -v[v]... for full protocol decode
listening on any, link-type LINUX_SLL2 (Linux cooked v2), snapshot length 262144 bytes

06:45:06.993052 ens3  Out IP 10.10.10.11.46876 > 10.10.10.10.8080: Flags [S], seq 2131212507, win 62370, options [mss 8910,sackOK,TS val 2680689573 ecr 0,nop,wscale 7], length 0
06:45:06.993614 ens3  In  IP 10.10.10.10.8080 > 10.10.10.11.46876: Flags [S.], seq 2426320633, ack 2131212508, win 62286, options [mss 8910,sackOK,TS val 150292010 ecr 2680689573,nop,wscale 7], length 0
06:45:06.993632 ens3  Out IP 10.10.10.11.46876 > 10.10.10.10.8080: Flags [.], ack 1, win 488, options [nop,nop,TS val 2680689574 ecr 150292010], length 0
06:45:06.993678 ens3  Out IP 10.10.10.11.46876 > 10.10.10.10.8080: Flags [F.], seq 1, ack 1, win 488, options [nop,nop,TS val 2680689574 ecr 150292010], length 0
06:45:06.993969 ens3  In  IP 10.10.10.10.8080 > 10.10.10.11.46876: Flags [F.], seq 1, ack 2, win 487, options [nop,nop,TS val 150292010 ecr 2680689574], length 0
06:45:06.993974 ens3  Out IP 10.10.10.11.46876 > 10.10.10.10.8080: Flags [.], ack 2, win 488, options [nop,nop,TS val 2680689574 ecr 150292010], length 0
```

#### B3b. packet capture as seen on server node `hands-on-2-tf-demo-bastion-server`

```console
root@hands-on-2-tf-demo-bastion-server:~# tcpdump -nn -i any port 8080
tcpdump: data link type LINUX_SLL2
tcpdump: verbose output suppressed, use -v[v]... for full protocol decode
listening on any, link-type LINUX_SLL2 (Linux cooked v2), snapshot length 262144 bytes

06:45:06.994278 ens3  In  IP 10.10.10.11.46876 > 10.10.10.10.8080: Flags [S], seq 2131212507, win 62370, options [mss 8910,sackOK,TS val 2680689573 ecr 0,nop,wscale 7], length 0
06:45:06.994298 ens3  Out IP 10.10.10.10.8080 > 10.10.10.11.46876: Flags [S.], seq 2426320633, ack 2131212508, win 62286, options [mss 8910,sackOK,TS val 150292010 ecr 2680689573,nop,wscale 7], length 0
06:45:06.994603 ens3  In  IP 10.10.10.11.46876 > 10.10.10.10.8080: Flags [.], ack 1, win 488, options [nop,nop,TS val 2680689574 ecr 150292010], length 0
06:45:06.994603 ens3  In  IP 10.10.10.11.46876 > 10.10.10.10.8080: Flags [F.], seq 1, ack 1, win 488, options [nop,nop,TS val 2680689574 ecr 150292010], length 0
06:45:06.994710 ens3  Out IP 10.10.10.10.8080 > 10.10.10.11.46876: Flags [F.], seq 1, ack 2, win 487, options [nop,nop,TS val 150292010 ecr 2680689574], length 0
06:45:06.994877 ens3  In  IP 10.10.10.11.46876 > 10.10.10.10.8080: Flags [.], ack 2, win 488, options [nop,nop,TS val 2680689574 ecr 150292010], length 0
```


#### B4. Connecting to the server on external FIP addresses

```console
root@hands-on-2-tf-demo-server-1:~# ncat -zv 195.113.243.246 8080
Ncat: Version 7.80 ( https://nmap.org/ncat )
Ncat: Connected to 195.113.243.246:8080.
Ncat: 0 bytes sent, 0 bytes received in 0.62 seconds.
```

#### B5a. packet capture as seen on client node `hands-on-2-tf-demo-server-1`

```console
root@hands-on-2-tf-demo-server-1:~# tcpdump -nn -i any port 8080
tcpdump: data link type LINUX_SLL2
tcpdump: verbose output suppressed, use -v[v]... for full protocol decode
listening on any, link-type LINUX_SLL2 (Linux cooked v2), snapshot length 262144 bytes

06:48:14.047734 ens3  Out IP 10.10.10.11.33168 > 195.113.243.246.8080: Flags [S], seq 2705715419, win 62370, options [mss 8910,sackOK,TS val 4007110022 ecr 0,nop,wscale 7], length 0
06:48:14.048278 ens3  P   IP 10.10.10.11.33168 > 195.113.243.246.8080: Flags [S], seq 2705715419, win 62370, options [mss 8910,sackOK,TS val 4007110022 ecr 0,nop,wscale 7], length 0
06:48:14.621204 ens3  In  IP 195.113.243.246.8080 > 10.10.10.11.33168: Flags [S.], seq 2908130555, ack 2705715420, win 62286, options [mss 8910,sackOK,TS val 1366828680 ecr 4007110022,nop,wscale 7], length 0
06:48:14.621249 ens3  Out IP 10.10.10.11.33168 > 195.113.243.246.8080: Flags [.], ack 1, win 488, options [nop,nop,TS val 4007110595 ecr 1366828680], length 0

06:48:14.621357 ens3  Out IP 10.10.10.11.33168 > 195.113.243.246.8080: Flags [F.], seq 1, ack 1, win 488, options [nop,nop,TS val 4007110596 ecr 1366828680], length 0
06:48:14.621365 ens3  P   IP 10.10.10.11.33168 > 195.113.243.246.8080: Flags [.], ack 1, win 488, options [nop,nop,TS val 4007110595 ecr 1366828680], length 0
06:48:14.621423 ens3  P   IP 10.10.10.11.33168 > 195.113.243.246.8080: Flags [F.], seq 1, ack 1, win 488, options [nop,nop,TS val 4007110596 ecr 1366828680], length 0
06:48:14.621795 ens3  In  IP 195.113.243.246.8080 > 10.10.10.11.33168: Flags [F.], seq 1, ack 2, win 487, options [nop,nop,TS val 1366828681 ecr 4007110596], length 0
06:48:14.621802 ens3  Out IP 10.10.10.11.33168 > 195.113.243.246.8080: Flags [.], ack 2, win 488, options [nop,nop,TS val 4007110596 ecr 1366828681], length 0
06:48:14.621847 ens3  P   IP 10.10.10.11.33168 > 195.113.243.246.8080: Flags [.], ack 2, win 488, options [nop,nop,TS val 4007110596 ecr 1366828681], length 0
```

#### B5b. packet capture as seen on server node `hands-on-2-tf-demo-bastion-server`

```console
root@hands-on-2-tf-demo-bastion-server:~# tcpdump -nn -i any port 8080
tcpdump: data link type LINUX_SLL2
tcpdump: verbose output suppressed, use -v[v]... for full protocol decode
listening on any, link-type LINUX_SLL2 (Linux cooked v2), snapshot length 262144 bytes

06:48:14.621278 ens3  In  IP 195.113.243.144.55166 > 10.10.10.10.8080: Flags [S], seq 2705715419, win 62370, options [mss 8910,sackOK,TS val 4007110022 ecr 0,nop,wscale 7], length 0
06:48:14.621309 ens3  Out IP 10.10.10.10.8080 > 195.113.243.144.55166: Flags [S.], seq 2908130555, ack 2705715420, win 62286, options [mss 8910,sackOK,TS val 1366828680 ecr 4007110022,nop,wscale 7], length 0
06:48:14.622518 ens3  In  IP 195.113.243.144.55166 > 10.10.10.10.8080: Flags [.], ack 1, win 488, options [nop,nop,TS val 4007110595 ecr 1366828680], length 0

06:48:14.622560 ens3  In  IP 195.113.243.144.55166 > 10.10.10.10.8080: Flags [F.], seq 1, ack 1, win 488, options [nop,nop,TS val 4007110596 ecr 1366828680], length 0
06:48:14.622653 ens3  Out IP 10.10.10.10.8080 > 195.113.243.144.55166: Flags [F.], seq 1, ack 2, win 487, options [nop,nop,TS val 1366828681 ecr 4007110596], length 0
06:48:14.622884 ens3  In  IP 195.113.243.144.55166 > 10.10.10.10.8080: Flags [.], ack 2, win 488, options [nop,nop,TS val 4007110596 ecr 1366828681], length 0
```

