# Cloud hands-on part 8, G2 personal development infrastructure

## Objectives
* [ ] Refresh [#5](https://gitlab.ics.muni.cz/cloud/g2/hands-on/-/blob/master/parts/5-g2-infra-vms-k8s/README.md) and [#6](https://gitlab.ics.muni.cz/cloud/g2/hands-on/-/blob/master/parts/6-g2-infra-k8s-fluxcd-ceph-ostack/README.md) hands-on sessions
* [ ] Build IaaS ephemeral based infrastructure using Terraform
* [ ] Deploy Kubernetes
* [ ] Bootstrap G2 infra using FluxCD
* [ ] Deploy internal ceph to provide k8s internal distributed storage
* [ ] Deploy bigger part of the OpenStack on Kubernetes
* [ ] Deploy rest of the OpenStack on Kubernetes (part 8c)
* [ ] Infrastructure loadbalancer and certificates (part 8d)
* [ ] Q&A

## Before hands-on

Refresh [#5](https://gitlab.ics.muni.cz/cloud/g2/hands-on/-/blob/master/parts/5-g2-infra-vms-k8s/README.md) and [#6](https://gitlab.ics.muni.cz/cloud/g2/hands-on/-/blob/master/parts/6-g2-infra-k8s-fluxcd-ceph-ostack/README.md) hands-on sessions.

You may [start building IaaS infrastructure if you have some spare time](https://gitlab.ics.muni.cz/cloud/g2/hands-on/-/tree/master/parts/8-g2-dev-ephem-personal-infra?ref_type=heads#build-iaas-ephemeral-based-infrastructure-using-terraform).

## Steps

As usual we are working in personal training project in `prod-ostrava`.

### Build IaaS ephemeral based infrastructure using Terraform

Follow https://gitlab.ics.muni.cz/cloud/g2/hands-on/-/tree/master/parts/5-g2-infra-vms-k8s?ref_type=heads#1-deploy-g2-cloud-vm-iaas-infractructure.

Use ephemeral flavor `r3.8core-30ram-30edisk` (preferred, single ephemeral disk) eventually `p3.core-30ram-30disk-30edisk` (first disk ceph, second ephemeral).

Feel free to use [kubernetes-infra-example-dev-ost's branch `freznicek-hands-on-8`](https://gitlab.ics.muni.cz/cloud/kubernetes/kubernetes-infra-example-dev-ost/-/commits/freznicek-hands-on-8).

You should get to following infrastructure:
 * [logs/iaas-terraform.log](./logs/iaas-terraform.log)
 * [logs/accessing-infra.log](./logs/accessing-infra.log)


### Deploy Kubernetes

Follow [hands-on #5 chapter](https://gitlab.ics.muni.cz/cloud/g2/hands-on/-/tree/master/parts/5-g2-infra-vms-k8s?ref_type=heads#configure-deploy-kubernetes-on-g2-cloud-vm-iaas-infractructure) with following changes:
* feel free to disable kube-vip (already disabled on [kubernetes-infra-example-dev-ost's branch `freznicek-hands-on-8`](https://gitlab.ics.muni.cz/cloud/kubernetes/kubernetes-infra-example-dev-ost/-/commits/freznicek-hands-on-8))
* feel free to disable cinder based block storage (already disabled on [kubernetes-infra-example-dev-ost's branch `freznicek-hands-on-8`](https://gitlab.ics.muni.cz/cloud/kubernetes/kubernetes-infra-example-dev-ost/-/commits/freznicek-hands-on-8))

You should get to following infra state:
 * [logs/deploying-k8s.log](./logs/deploying-k8s.log)
 * [logs/deploying-infra-proxy.log](./logs/deploying-infra-proxy.log)
 * [logs/accessing-infra-kubectl.log](./logs/accessing-infra-kubectl.log)


### Bootstrap G2 infra using FluxCD

Follow [hands-on #6 chapter](https://gitlab.ics.muni.cz/cloud/g2/hands-on/-/tree/master/parts/6-g2-infra-k8s-fluxcd-ceph-ostack?ref_type=heads#bootstrap-g2-infra-using-fluxcd).

You should get to following infra state:
 * [logs/flux-cd-bootstrap.log](logs/flux-cd-bootstrap.log)

### Deploy internal ceph to provide k8s internal distributed storage

Follow [hands-on #6 chapter](https://gitlab.ics.muni.cz/cloud/g2/hands-on/-/tree/master/parts/6-g2-infra-k8s-fluxcd-ceph-ostack?ref_type=heads#deploy-internal-ceph-to-provide-k8s-internal-distributed-storage).

You should get to following infra state:
 * [logs/flux-cd-ceph.log](logs/flux-cd-ceph.log)


### Deploy bigger part of the OpenStack on Kubernetes

Start with [hands-on #6 beginning](https://gitlab.ics.muni.cz/cloud/g2/hands-on/-/tree/master/parts/6-g2-infra-k8s-fluxcd-ceph-ostack?ref_type=heads#deploy-part-of-the-openstack-on-kubernetes-backed-by-distributed-ceph) and then try to continue with oter components, you may freely skip `radosgw-openstack`, `prometheus-openstack-exporter`, `openstack-exporter`, `mariadb-backup`, those will be added at the end.


At this point you should see:
 * [logs/ostack-till-memcached.log](logs/ostack-till-memcached.log)


#### Create ceph pools and users for OpenStack
Adding `cinder` and `glance` openstack components require creation of the dedicated ceph pools so they are able to communicate with distributed rook-ceph storage directly. You can achieve it following way:
```sh
[freznicek@lenovo-t14 ipv6 0]$ kubectl --context ho8 -n rook-ceph exec -it rook-ceph-tools-74576878f6-d7v9d bash

# test connection to ceph
[rook@cloud-hands-on-8-control-2 /]$ ceph -s
  cluster:
    id:     0c33dbdf-8ca7-40fa-a891-4b88a52b2458
    health: HEALTH_WARN

  services:
    mon: 3 daemons, quorum a,b,c (age 2d)
    mgr: b(active, since 2d), standbys: a
    osd: 6 osds: 6 up (since 2d), 6 in (since 3d)
    rgw: 1 daemon active (1 hosts, 1 zones)

  data:
    pools:   10 pools, 121 pgs
    objects: 513 objects, 212 MiB
    usage:   2.7 GiB used, 1.2 TiB / 1.2 TiB avail
    pgs:     26.446% pgs not active
             89 active+clean
             32 undersized+peered

# initially pre-created ceph pools
[rook@cloud-hands-on-8-control-2 /]$ ceph df
--- RAW STORAGE ---
CLASS     SIZE    AVAIL     USED  RAW USED  %RAW USED
hdd    1.2 TiB  1.2 TiB  2.7 GiB   2.7 GiB       0.23
TOTAL  1.2 TiB  1.2 TiB  2.7 GiB   2.7 GiB       0.23

--- POOLS ---
POOL                                 ID  PGS   STORED  OBJECTS     USED  %USED  MAX AVAIL
.mgr                                  1    1  577 KiB        2  1.7 MiB      0    379 GiB
ceph-blockpool                        2   32  128 MiB      126  384 MiB   0.03    379 GiB
ceph-objectstore.rgw.control          3    8      0 B        8      0 B      0    379 GiB
ceph-objectstore.rgw.meta             4    8  4.4 KiB       11  102 KiB      0    379 GiB
ceph-objectstore.rgw.log              5    8   30 KiB      339  1.9 MiB      0    379 GiB
ceph-objectstore.rgw.buckets.index    6    8   66 KiB       11  197 KiB      0    379 GiB
ceph-objectstore.rgw.buckets.non-ec   7    8      0 B        0      0 B      0    379 GiB
ceph-objectstore.rgw.otp              8    8      0 B        0      0 B      0    379 GiB
.rgw.root                             9    8  4.8 KiB       16  180 KiB      0    379 GiB
ceph-objectstore.rgw.buckets.data    10   32      0 B        0      0 B      0    682 GiB

# create pools for glance and cinder
# we are picking #PGs same as ceph-blockpool
# ------------------------------------------
# Manual calculation shows that 16 and 32 are the optimal values here:
# Total number of PGs are: Total PGs=(Total Number of OSDs×100)/Replication Factor
#                          200      = 6*100 / 3
# Calculating available PGS: 200 - (1+32+56+32) = 79 (assuming ceph-objectstore.rgw.buckets.data is also replication pool as others)
# Below four ceph pools should get 1/4 of 79 (19.75) and has to be number in 2**N exponent so either 16 or 32
[rook@cloud-hands-on-8-control-2 /]$ for i_pool in cinder-volumes cinder-backups cinder-ephemeral-volumes glance; do   ceph osd pool create $i_pool 32 32;   ceph osd pool application enable $i_pool rbd;   rbd pool init $i_pool; done
pool 'cinder-volumes' created
enabled application 'rbd' on pool 'cinder-volumes'
pool 'cinder-backups' created
enabled application 'rbd' on pool 'cinder-backups'
pool 'cinder-ephemeral-volumes' created
enabled application 'rbd' on pool 'cinder-ephemeral-volumes'
pool 'glance' created
enabled application 'rbd' on pool 'glance'

# new ceph pools
[rook@cloud-hands-on-8-control-2 /]$ ceph df
--- RAW STORAGE ---
CLASS     SIZE    AVAIL     USED  RAW USED  %RAW USED
hdd    1.2 TiB  1.2 TiB  2.7 GiB   2.7 GiB       0.23
TOTAL  1.2 TiB  1.2 TiB  2.7 GiB   2.7 GiB       0.23

--- POOLS ---
POOL                                 ID  PGS   STORED  OBJECTS     USED  %USED  MAX AVAIL
...
cinder-volumes                       11   32     19 B        1   12 KiB      0    379 GiB
cinder-backups                       12   32     19 B        1   12 KiB      0    379 GiB
cinder-ephemeral-volumes             13   32     19 B        1   12 KiB      0    379 GiB
glance                               14   32     19 B        1   12 KiB      0    379 GiB

# create cinder and glance ceph users
[rook@cloud-hands-on-8-control-2 /]$ ceph auth get-or-create client.glance mon 'allow r' osd 'allow class-read object_prefix rbd_children, allow rwx pool=glance'
[client.glance]
        key = AQ************==
[rook@cloud-hands-on-8-control-2 /]$ ceph auth get-or-create client.cinder-backup mon 'allow r' osd 'allow class-read object_prefix rbd_children, allow rwx pool=cinder-backups'
[client.cinder-backup]
        key = AQB***********A==
[rook@cloud-hands-on-8-control-2 /]$ ceph auth get-or-create client.cinder mon 'allow r' osd 'allow class-read object_prefix rbd_children, allow rwx pool=cinder-volumes, allow rwx pool=cinder-ephemeral-volumes, allow rx pool=glance'
[client.cinder]
        key = AQB**********8jQ==
```
The created ceph users are then added to [`images-rbd-keyring`](https://gitlab.ics.muni.cz/cloud/g2/hands-on-ceph-openstack-lma/-/blob/downstream/apps/freznicek-hands-on-8/03-openstack/08-ceph-client-glance-key-images-rbd-keyring-encryptedsecret.yaml?ref_type=heads), [`cinder-backup-rbd-keyring`](https://gitlab.ics.muni.cz/cloud/g2/hands-on-ceph-openstack-lma/-/blob/downstream/apps/freznicek-hands-on-8/03-openstack/09-ceph-client-cinder-backup-rbd-keyring-encryptedsecret.yaml?ref_type=heads) and [`cinder-volume-rbd-keyring`](https://gitlab.ics.muni.cz/cloud/g2/hands-on-ceph-openstack-lma/-/blob/downstream/apps/freznicek-hands-on-8/03-openstack/09-ceph-client-cinder-volume-rbd-keyring-encryptedsecret.yaml?ref_type=heads) secrets which need to be excrypted.

#### OpenStack rabbitmq

https://gitlab.ics.muni.cz/cloud/g2/hands-on-ceph-openstack-lma/-/blob/downstream/apps/freznicek-hands-on-8/03-openstack/05-rabbitmq.yaml?ref_type=heads

#### OpenStack keystone

https://gitlab.ics.muni.cz/cloud/g2/hands-on-ceph-openstack-lma/-/blob/downstream/apps/freznicek-hands-on-8/03-openstack/06-keystone.yaml?ref_type=heads

Do not forget to add kestone related secrets into common secret, ([example](https://gitlab.ics.muni.cz/cloud/g2/hands-on-ceph-openstack-lma/-/commit/d8df374d5945ba0933acce857c05187d1084668b)).

You should get to following infra state:
 * [logs/flux-cd-infra-till-keystone.log](logs/flux-cd-infra-till-keystone.log)

#### OpenStack Glance + Cinder + OVS

https://gitlab.ics.muni.cz/cloud/g2/hands-on-ceph-openstack-lma/-/commit/f562e14742a25522b965967a223b3b3a6922ef20 As usual it is necessary to add referenced sensitive (`secret/common`) and non-sensitive data (`configmap/common`).

While deploying Glance, Cinder and others you may see initial deploy fails with `install retries exhausted` as below:
```console
[freznicek@lenovo-t14 ~ 0]$ flux -n openstack get hr
NAME                    REVISION                SUSPENDED       READY   MESSAGE
ceph-cluster-config     1.1.1                   False           True    Release reconciliation succeeded
cinder                                          False           False   install retries exhausted
...
```

This is caused by fact that Galera cluster on rook-ceph on real hdd ceph is slow. It was measured that DB migrations for `glance` or `cinder` run for more than 10 minutes and thus fails Helm/FluxCD readiness timeout.
Once corresponding db-sync job finishes (for instance `cinder-db-sync-vm4lw`) then perform `flux -n openstack suspend hr cinder && flux -n openstack resume hr cinder`.

Encryption of the [`images-rbd-keyring`](https://gitlab.ics.muni.cz/cloud/g2/hands-on-ceph-openstack-lma/-/blob/downstream/apps/freznicek-hands-on-8/03-openstack/08-ceph-client-glance-key-images-rbd-keyring-encryptedsecret.yaml?ref_type=heads) [`cinder-backup-rbd-keyring`](https://gitlab.ics.muni.cz/cloud/g2/hands-on-ceph-openstack-lma/-/blob/downstream/apps/freznicek-hands-on-8/03-openstack/09-ceph-client-cinder-backup-rbd-keyring-encryptedsecret.yaml?ref_type=heads) and [`cinder-volume-rbd-keyring`](https://gitlab.ics.muni.cz/cloud/g2/hands-on-ceph-openstack-lma/-/blob/downstream/apps/freznicek-hands-on-8/03-openstack/09-ceph-client-cinder-volume-rbd-keyring-encryptedsecret.yaml?ref_type=heads) secrets is done following way:
1. create plan secret
```yaml
# /apps/<cluster-name>/03-openstack/08-ceph-client-glance-key-images-rbd-keyring-plainsecret.yaml
apiVersion: v1
kind: Secret
metadata:
    name: images-rbd-keyring
    namespace: openstack
stringData:
    key: AQ************==
type: kubernetes.io/rbd
```
2. Encrypt such secret
```
$ ../../../scripts/encrypt_k8s_secret.sh ../../../clusters/<cluster-name>/.sops.yaml  ./08-ceph-client-glance-key-images-rbd-keyring-plainsecret.yaml > ./08-ceph-client-glance-key-images-rbd-keyring-encryptedsecret.yaml
```
3. Push secret to the GitOps repository ([hands-on-ceph-openstack-lma](https://gitlab.ics.muni.cz/cloud/g2/hands-on-ceph-openstack-lma)).

You should get to following infra state:
 * [logs/flux-cd-infra-till-ovs.log](logs/flux-cd-infra-till-ovs.log)

#### Libvirtd + OpenStack Nova + neutron + placement

Make sure to switch back to kernel cgroupsv1 on compute nodes ([details](./logs/compute-cgroupsv1.log)).

Manifests:
 * https://gitlab.ics.muni.cz/cloud/g2/hands-on-ceph-openstack-lma/-/blob/downstream/apps/freznicek-hands-on-8/03-openstack/11-libvirt.yaml?ref_type=heads
 * https://gitlab.ics.muni.cz/cloud/g2/hands-on-ceph-openstack-lma/-/blob/downstream/apps/freznicek-hands-on-8/03-openstack/12-nova.yaml?ref_type=heads
 * https://gitlab.ics.muni.cz/cloud/g2/hands-on-ceph-openstack-lma/-/blob/downstream/apps/freznicek-hands-on-8/03-openstack/13-placement.yaml?ref_type=heads
 * https://gitlab.ics.muni.cz/cloud/g2/hands-on-ceph-openstack-lma/-/blob/downstream/apps/freznicek-hands-on-8/03-openstack/13-placement.yaml?ref_type=heads
 * https://gitlab.ics.muni.cz/cloud/g2/hands-on-ceph-openstack-lma/-/blob/downstream/apps/freznicek-hands-on-8/03-openstack/14-neutron.yaml?ref_type=heads


You should get to following infra state:
 * [logs/flux-cd-infra-till-neutron.log](logs/flux-cd-infra-till-neutron.log)


### Deploy rest of the OpenStack on Kubernetes (part 8c)

TODO

#### How to patch OpenStack component (example Neutron)

Sometimes it is needed to tune OpenStack component. We likely use [custom-images](https://gitlab.ics.muni.cz/cloud/g2/custom-images) and build container image on top of community one:

* https://gitlab.ics.muni.cz/cloud/g2/custom-images/-/tree/master/neutron_2?ref_type=heads
* https://gitlab.ics.muni.cz/cloud/g2/custom-images/-/commit/bd93a77b938663e5c4217a4dd3fa5a8fe49ce707

#### How to debug helm chart deployment (helm chart into helm release)

There are situations when FluxCD does not deploy HelmRelease and ends with an error for instance []`error while running post render on files: may not add resource with an already registered id`](https://github.com/fluxcd/flux2/issues/4368).

In these cases you have to debug HelmRelease values to see how is helm rendering your values into kubernetes manifests.

This is done with 

```sh
# take Flux CD HelmRelease manifests and merge them
yq eval-all '. as $item ireduce ({}; . * $item)'  03-openstack.base/14-neutron.yaml 03-openstack.specific/14-neutron.yaml | yq e .spec.values > helm-chart-debugging/neutron.helm.values0.yaml
# if you have just one file then use:
yq e .spec.values 03-openstack/14-neutron.yaml > helm-chart-debugging/neutron.helm.values0.yaml

# fill in valuesFrom passwords and cshared data
vi helm-chart-debugging/neutron.helm.values0.yaml #save to helm-chart-debugging/neutron.helm.values1.yaml

# render helm release values with helm cli
helm template --debug -f helm-chart-debugging/neutron.helm.values1.yaml &> helm-chart-debugging/neutron.helm.values1.log
vim helm-chart-debugging/neutron.helm.values1.log

# change values and re-render
vim helm-chart-debugging/neutron.helm.values1.yaml # save to helm-chart-debugging/neutron.helm.values2.yaml
helm template --debug -f helm-chart-debugging/neutron.helm.values2.yaml &> helm-chart-debugging/neutron.helm.values2.log

# commit changes to Flux CD GitOps repo
```

Examples of neutron helm values and rendered products:
* [`helm-chart-debugging/neutron.helm.values1.yaml`](helm-chart-debugging/neutron.helm.values1.yaml)
  * [`helm-chart-debugging/neutron.helm.values1.yaml.log`](helm-chart-debugging/neutron.helm.values1.yaml.log)
* [`helm-chart-debugging/neutron.helm.values2.yaml`](helm-chart-debugging/neutron.helm.values2.yaml)
  * [`helm-chart-debugging/neutron.helm.values2.yaml.log`](helm-chart-debugging/neutron.helm.values2.yaml.log)


#### heat + horizon

TODO

#### Topics to be solved TODOs

1. Deploy all components as in prod-brno except esaco, object-storage and lma.
1. Find the way how to proxy data to dev cloud via bastion
1. Decide how to deal with dev environment DNS records and certificates.


## References
* [How to set ceph pool PGs](./ceph-pgs.md)

## Frequently asked Questions

### Is there hands-on recording?
* [Yes, part 8a](https://drive.google.com/file/d/1VsMnBIsLE1XiwV3Wy1fbcVJ6NCZwUvii/view?usp=drive_link)
* [Yes, part 8b](https://drive.google.com/file/d/1Q-zHwf4wcVdTwjmKty8DTwXSd4LLZru4/view?usp=drive_link)
* [Yes, part 8c](https://drive.google.com/file/d/1Zp4HpTbtXK7ZczmNtwgNTs7Z4jlEQbbW/view?usp=drive_link)

### Kubernetes cluster networking does not work - troubleshooting

Doublecheck whether you see following behavior
```sh
# add inspection pods https://kubernetes.io/docs/tasks/debug/debug-application/debug-service/
kubectl --context=ho8 create deployment hostnames --image=registry.k8s.io/serve_hostname
kubectl --context=ho8 scale deployment hostnames --replicas=3
kubectl --context=ho8 expose deployment hostnames --port=80 --target-port=9376

# get debugger pod name
[freznicek@lenovo-t14 ipv6 0]$ kubectl --context ho8 get po | grep -Eo 'debugger-[a-z0-9]+-[a-z0-9]+' | head -1
debugger-5b64bdff9f-b5jsd
[freznicek@lenovo-t14 ipv6 0]$ dbg_pod_name=$(kubectl --context ho8 get po | grep -Eo 'debugger-[a-z0-9]+-[a-z0-9]+' | head -1)
[freznicek@lenovo-t14 ipv6 0]$ echo $dbg_pod_name
debugger-5b64bdff9f-b5jsd
[freznicek@lenovo-t14 ipv6 0]$ kubectl --context ho8  exec $dbg_pod_name -- timeout 2 curl -s http://hostnames
command terminated with exit code 124
[freznicek@lenovo-t14 ipv6 124]$ kubectl --context ho8  exec $dbg_pod_name -- timeout 2 curl -s http://hostnames
command terminated with exit code 124
[freznicek@lenovo-t14 ipv6 124]$ kubectl --context ho8  exec $dbg_pod_name -- timeout 2 curl -s http://hostnames
hostnames-5f5bb5f5b6-rmfs7
```

if you see it then you are suffering with non-functional calico and you likely use VxLAN CrossSubnet configuration. To confirm that perform:

```console
[freznicek@lenovo-t14 ipv6 0]$ kubectl --context=ho8 -n kube-system get ippools.crd.projectcalico.org default-pool -ojson | jq .spec.vxlanMode
"CrossSubnet"
```

Let's change this configuration to full VxLAN encapsulation via following command:
```console
[freznicek@lenovo-t14 ipv6 0]$ kubectl --context=ho8 -n kube-system patch --type=merge ippools.crd.projectcalico.org default-pool -p '{"spec":{"vxlanMode":"Always"}}'
ippool.crd.projectcalico.org/default-pool patched
[freznicek@lenovo-t14 ipv6 0]$ kubectl --context=ho8 -n kube-system get ippools.crd.projectcalico.org default-pool -ojson | jq .spec.vxlanMode
"Always"
```

And re-test the k8s service networking:

```console
[freznicek@lenovo-t14 ipv6 0]$ kubectl --context ho8  exec $dbg_pod_name -- timeout 2 curl -s http://hostnames
hostnames-5f5bb5f5b6-nqk5b
[freznicek@lenovo-t14 ipv6 0]$ kubectl --context ho8  exec $dbg_pod_name -- timeout 2 curl -s http://hostnames
hostnames-5f5bb5f5b6-xw82v
[freznicek@lenovo-t14 ipv6 0]$ kubectl --context ho8  exec $dbg_pod_name -- timeout 2 curl -s http://hostnames
hostnames-5f5bb5f5b6-rmfs7
[freznicek@lenovo-t14 ipv6 0]$ kubectl --context ho8  exec $dbg_pod_name -- timeout 2 curl -s http://hostnames
hostnames-5f5bb5f5b6-nqk5b
```

Kubespray ansible configuration [has been updated accordingly](https://gitlab.ics.muni.cz/cloud/kubernetes/kubernetes-infra-example-dev-ost/-/blob/freznicek-hands-on-8/ansible/group_vars/k8s_cluster/k8s-net-calico.yaml?ref_type=heads).


### Re-create openstack ceph pools

First delete created ceph pools:
```console
[freznicek@lenovo-t14 ipv6 0]$ kubectl --context ho8 -n rook-ceph exec -it rook-ceph-tools-74576878f6-d7v9d bash
[rook@cloud-hands-on-8-control-2 /]$ for i_pool in cinder-volumes cinder-backups cinder-ephemeral-volumes glance; do ceph osd pool delete $i_pool $i_pool --yes-i-really-really-mean-it; done
pool 'cinder-volumes' removed
pool 'cinder-backups' removed
pool 'cinder-ephemeral-volumes' removed
pool 'glance' removed
[rook@cloud-hands-on-8-control-2 /]$
```

Then re-create them again following the updated guide [above](#create-ceph-pools-and-users-for-openstack).


### Mariadb galera cluster not starting up

Follow https://gitlab.ics.muni.cz/cloud/knowledgebase/-/blob/master/howtos/g2/OpenStack/issues-database/database-not-starting-up.md search [through KB](https://gitlab.ics.muni.cz/search?search=mariadb&nav_source=navbar&project_id=2804&group_id=232&search_code=true&repository_ref=master).

