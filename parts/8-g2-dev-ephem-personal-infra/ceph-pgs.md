## How to properly set number of placement groups (PG) when creating ceph pool?

Setting the correct number of Placement Groups (PGs) in Ceph is crucial for ensuring optimal performance and distribution of data across the cluster. Here’s a guide on how to properly set the number of PGs when creating a Ceph pool:

### 1. Understanding PGs

Placement Groups (PGs) are a way Ceph organizes objects to manage and distribute data efficiently. Each object in the cluster is mapped to a PG, and each PG is mapped to a set of OSDs (Object Storage Daemons). 

### 2. Calculate the Number of PGs

The number of PGs should be chosen based on the total number of OSDs and the expected number of pools. The general formula recommended by Ceph is:

\[ \text{Total PGs} = (\text{Total Number of OSDs} \times 100) / \text{Replication Factor} \]

This formula can be adjusted based on the number of pools and the expected growth. Ceph also provides a tool called `pgcalc` to help calculate the number of PGs.

### 3. Considerations

- **Replication Factor**: This is typically 2 or 3, depending on your redundancy requirements.
- **Number of OSDs**: The total number of OSDs in your cluster.
- **Growth**: Anticipate future growth of the cluster to avoid having to rebalance frequently.

### 4. Practical Example

Suppose you have a cluster with 30 OSDs and you plan to use a replication factor of 3. Using the formula:

\[ \text{Total PGs} = (30 \times 100) / 3 = 1000 \]

This would suggest a total of 1000 PGs. If you have multiple pools, you need to divide this total number of PGs among the pools.

### 5. Adjusting PG Count

After calculating the number of PGs, you can create a new pool with the specified number of PGs:

```sh
ceph osd pool create {pool-name} {pg-num}
```

For instance:

```sh
ceph osd pool create mypool 1000
```

If you need to adjust the number of PGs for an existing pool, use:

```sh
ceph osd pool set {pool-name} pg_num {new-pg-num}
ceph osd pool set {pool-name} pgp_num {new-pg-num}
```

Ensure `pg_num` and `pgp_num` are set to the same value to avoid imbalance.

### 6. Monitoring and Adjustments

Monitor the distribution and performance using Ceph’s monitoring tools. If the cluster grows or if performance issues arise, you might need to adjust the number of PGs. This can be done by:

```sh
ceph osd pool set {pool-name} pg_num {new-pg-num}
ceph osd pool set {pool-name} pgp_num {new-pg-num}
```

### 7. Automating with `ceph osd pool autoscale`

Ceph Octopus and later versions include an autoscale feature for PGs. Enable it with:

```sh
ceph osd pool autoscale-status
ceph osd pool set {pool-name} pg_autoscale_mode {mode}
```

Modes can be `on`, `off`, or `warn`. This helps in automatically adjusting PG counts based on usage.

### Conclusion

Setting the correct number of PGs is essential for Ceph’s performance and efficiency. By using the above guidelines, you can calculate and set an appropriate number of PGs, monitor the cluster, and adjust as necessary to maintain optimal operation.