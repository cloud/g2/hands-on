# Cloud hands-on part 2, Terraform, OpenStack

Create your own issue from the template [`g2-hands-on-part-2`](https://gitlab.ics.muni.cz/cloud/internal-wiki/-/blob/master/.gitlab/issue_templates/g2-hands-on-part-2.md?ref_type=heads).

## Objectives
* [ ] A. Get fluent with Terraform IaaC
  * [ ] A1. Deploy code at https://gitlab.ics.muni.cz/cloud/g2/hands-on
  * [ ] A2. Scale code to single bastion and 3 nodes
* [ ] B. Gain ability to perform small Terraform code adjustments:
  * [ ] B1. Add to all nodes (not bastion) 2 external disks
  * [ ] B2. Define fixed internal IP addresses for all nodes (not bastion)
  * [ ] B3. Optional challenge goal: Implement mechanism how to define variable number of disks using additional variable
* [ ] C. Initiate VM state with initialization script Terraform IaaC
  * [ ] C1. Using OpenStack initialization script of cloud-init
* [ ] D. Get familar with terraform command-line arguments
* [ ] E. Workaround following Terraform warning


## Steps


### A. Get fluent with Terraform IaaC

We use prod-ostrava cloud instance.

#### A1. Deploy code at https://gitlab.ics.muni.cz/cloud/g2/hands-on

Checkout code from https://gitlab.ics.muni.cz/cloud/g2/hands-on/-/tree/master/parts/2/terraform?ref_type=heads

Steps are:
```console
source ~/c/g2-prod-ostrava-freznicek-meta-cloud-freznicek-training.sh.inc
openstack version show
git clone ....
cd hands-on/parts/2/terraform
terraform init
terraform validate
terraform plan --out plan
terraform apply plan
openstack server list
```

---

#### A2. Scale code to single bastion and 3 nodes

1. Edit [parts/2/terraform/main.tf](https://gitlab.ics.muni.cz/cloud/g2/hands-on/-/blob/master/parts/2/terraform/main.tf)
1. Refeploy as shown above

---

### B. Gain ability to perform small Terraform code adjustments:

#### B1. Add to all nodes (not bastion) 2 external disks

0. Terraform [IaaC code shows adding single volume to nodes](https://gitlab.ics.muni.cz/cloud/g2/hands-on/-/blob/master/parts/2/terraform/modules/common/volumes.tf?ref_type=heads).
1. Modify it to get two volumes.
2. Redeploy again and test 2nd disks are present ([you may need sshuttle](https://gitlab.ics.muni.cz/cloud/g2/hands-on/-/tree/master/parts/2/terraform?ref_type=heads#access-to-the-vm-nodes))

---

#### B2. Define fixed internal IP addresses for all nodes

OpenStack internal addresses are not specified explicitly. See OpenStack ports.

Let's change the IaC Terraform description to have following IP addresses:
```
10.10.10.10 - bastion
10.10.10.11 - node #1
...
```

[Initial direction help](https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs/resources/networking_port_v2).

---

#### B3. Optional challenge goal: Implement mechanism how to define variable number of disks using additional variable

Find the way how to define number of volumes in an additional terraform variable nodes_extra_volume_count. Keep this part as last item of your hands-on.

Initial direction help:
* https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs/resources/blockstorage_volume_v3
* https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs/resources/compute_volume_attach_v2


---

### C. Initiate VM state with initialization script Terraform IaaC

#### C1. Using OpenStack initialization script of cloud-init

Add to created VMs (all of them):
 * at least 2 SSH pubkeys
 * explicitly disable SSH authentication with password(s)
 * format & mount extra disks when they are present
 * install packages providing gawk and git
 * clone repository https://gitlab.ics.muni.cz/cloud/g2/hands-on into directory ~ubuntu/repositories/hands-on

Initial direction help:
* https://cloudinit.readthedocs.io/en/latest/
* https://cloudinit.readthedocs.io/en/latest/reference/examples.html#create-partitions-and-filesystems
* https://cloudinit.readthedocs.io/en/latest/reference/examples.html#install-arbitrary-packages

---

### D. Get familar with terraform command-line arguments

* Find and view content of your local terraform state file and the backup (`*.tfstate`)
  * [scale up and down infra](https://gitlab.ics.muni.cz/cloud/g2/hands-on/-/blob/master/parts/2/terraform/main.tf?ref_type=heads#L19) and compare how terraform state file and terraform backup state file changes
* [List all openstack entities created `terraform providers`](https://developer.hashicorp.com/terraform/cli/commands/providers)
* [List all used terraform providers `terraform state list`](https://developer.hashicorp.com/terraform/cli/commands/state/list)
* [List all cloud entities with dependencies `terraform graph`](https://developer.hashicorp.com/terraform/cli/commands/graph)
* Read how we manage G2 admin entities with [common-cloud-entities](https://gitlab.ics.muni.cz/cloud/g2/common-cloud-entities) and [prod-ostrava-cloud-entities](https://gitlab.ics.muni.cz/cloud/g2/prod-ostrava-cloud-entities).
  * [common-cloud-entities/README.md](https://gitlab.ics.muni.cz/cloud/g2/common-cloud-entities/-/blob/master/README.md?ref_type=heads#openstack-entities)
  * [prod-ostrava-cloud-entities/README.md](https://gitlab.ics.muni.cz/cloud/g2/prod-ostrava-cloud-entities/-/blob/master/README.md?ref_type=heads)

---

### E. Workaround following Terraform warning

```
│ Warning: Deprecated Resource
│
│   with module.toplevel.openstack_compute_floatingip_associate_v2.bastion_fip_associate,
│   on modules/2tier_public_bastion_private_vm_farm/bastion-networks.tf line 6, in resource "openstack_compute_floatingip_associate_v2" "bastion_fip_associate":
│    6: resource "openstack_compute_floatingip_associate_v2" "bastion_fip_associate" {
│
│ use openstack_networking_floatingip_associate_v2 resource instead
```

Initial direction help:
* https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs/resources/compute_floatingip_associate_v2
* https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs/resources/networking_floatingip_associate_v2

## Q/A / FAQ / Notes

### Is there hands-on recording?
  [Yes](https://drive.google.com/file/d/13RQGqm57jMnrriJW86Ons1QEJv65s_Zp/view?usp=drive_link)

### Hands-on structure updates
  1. study section needed upfront
  1. less content (approx. -20%)
  1. keep one challenge goal
